import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { RouteReuseStrategy } from '@angular/router';

import { IonicModule, IonicRouteStrategy } from '@ionic/angular';
import { AppComponent } from './app.component';
import { AppRoutingModule } from './app-routing.module';
import { provideAuth, getAuth } from '@angular/fire/auth';
import { provideDatabase, getDatabase } from '@angular/fire/database';
import { getFirestore, provideFirestore } from '@angular/fire/firestore';
import { UserService } from './Services/user-service';
import { HttpClientModule } from '@angular/common/http';
import { AngularFireModule } from '@angular/fire/compat';
import { environment } from 'src/environments/environment';
import { DatabaseModule } from '@angular/fire/database';
import { UtilityService } from './services/utility.service';
import { TimesService } from './services/times.service';
import { DatePicker } from '@ionic-native/date-picker/ngx';
import { NgxPubSubService } from '@pscoped/ngx-pub-sub';
import { ReactiveFormsModule } from '@angular/forms';
import { MapService } from './Services/map-service';
import { initializeApp, provideFirebaseApp } from '@angular/fire/app';
import { provideMessaging, getMessaging } from '@angular/fire/messaging';
import { CallNumber } from '@ionic-native/call-number/ngx';
import { LaunchNavigator } from '@ionic-native/launch-navigator/ngx';
import { StatusBar } from '@ionic-native/status-bar/ngx';
import { SplashScreen } from '@ionic-native/splash-screen/ngx';
import { BackgroundMode } from '@ionic-native/background-mode/ngx';

// import { AngularFireModule } from '@angular/fire';

@NgModule({
  declarations: [AppComponent],
  entryComponents: [],
  imports: [
    BrowserModule,
    IonicModule.forRoot(),
    AppRoutingModule,
    AngularFireModule.initializeApp(environment.firebase),
    DatabaseModule,
    ReactiveFormsModule,
    provideFirebaseApp(() => initializeApp(environment.firebase)),
    provideMessaging(() => getMessaging()),
    provideAuth(() => getAuth()),
    provideDatabase(() => getDatabase()),
    //  provideFirebaseApp(() => initializeApp(environment.firebase)),
    provideFirestore(() => getFirestore()),
    HttpClientModule,
    AngularFireModule.initializeApp(environment.firebase),
    DatabaseModule,
    ReactiveFormsModule,
  ],

  // AngularFireAuthModule,
  // AngularFireStorageModule,
  // AngularFireDatabaseModule
  providers: [
    {
      provide: RouteReuseStrategy,
      useClass: IonicRouteStrategy,
    },
    UserService,
    UtilityService,
    TimesService,
    DatePicker,
    NgxPubSubService,
    MapService,
    CallNumber,
    LaunchNavigator,
    StatusBar,
    SplashScreen,
    BackgroundMode,
  ],
  bootstrap: [AppComponent],
})
export class AppModule {}
