import { Component, Injector, OnInit } from '@angular/core';
import { BasePage } from '../base-page/base-page';

@Component({
  selector: 'app-stats',
  templateUrl: './stats.page.html',
  styleUrls: ['./stats.page.scss'],
})
export class StatsPage extends BasePage implements OnInit {
  curUser: any;
  ref: any;
  data: any;
  allbookings: any;
  count: any;
  avgrating: any;
  avgRatings: any;
  completedBookings: any;
  LifetimeBookings: any;
  userRef: any;
  last_billing_ammount: any;

  currentDate: any;
  todayBookings: any;

  currentMonth: any;
  monthBookings: any;

  todays_earning: any;
  months_earning: any;
  weeks_earning: any;

  constructor(injecotr: Injector) {
    super(injecotr);
  }

  async ngOnInit() {
    this.curUser = await this.firebaseService.getCurrentUser();
    this.userRef = this.firebaseService.getDbRef('users/' + this.curUser.uid);
    this.ref = this.firebaseService.getDbRef(
      'users/' + this.curUser.uid + '/bookings'
    );
    this.ref.once('value', (snapshot: any) => {
      this.allbookings = [];
      let count = 0;
      let countratings = 0;

      this.todays_earning = 0;
      this.months_earning = 0;
      this.weeks_earning = 0;

      if (snapshot.val()) {
        this.data = snapshot.val();
        for (let key in this.data) {
          this.data[key].key = key;
          this.allbookings.push(this.data[key]);
          // console.log(this.allbookings);
          // console.log(this.allbookings.length);
        }
      }
      for (let i = 0; i < this.allbookings.length; i++) {
        // console.log(this.allbookings[i].ratings);
        if (this.allbookings[i].ratings) {
          countratings += this.allbookings[i].ratings.rating;
          count += 1;
          this.avgRatings = countratings / count;
          this.avgRatings = this.avgRatings.toFixed(1);
        }
        // console.log(this.avgRatings);
      }

      this.completedBookings = 0;
      this.LifetimeBookings = 0;
      for (let j = 0; j < this.allbookings.length; j++) {
        if (
          this.allbookings[j].status == 'ENDED' ||
          this.allbookings[j].status == 'REVIEW'
        ) {
          this.LifetimeBookings += this.allbookings[j].trip_cost;
          this.completedBookings += 1;
        }
      }
      this.LifetimeBookings = this.LifetimeBookings.toFixed(2);

      console.log('Ere', this.allbookings);
      for (let x = 0; x < this.allbookings.length; x++) {
        if (
          this.allbookings[x].status == 'ENDED' ||
          this.allbookings[x].status == 'REVIEW'
        ) {
          console.log(this.allbookings[x]);
          let date1 = new Date(this.allbookings[x].tripdate);
          let date2 = new Date();
          let timeDiff = Math.abs(date2.getTime() - date1.getTime());
          let days_past = Math.ceil(timeDiff / (1000 * 3600 * 24));
          console.log(days_past, this.allbookings[x].trip_cost);
          if (days_past <= 1) {
            this.todays_earning =
              parseInt(this.todays_earning) +
              parseInt(this.allbookings[x].trip_cost);
          }
          if (days_past <= 7) {
            this.weeks_earning =
              parseInt(this.weeks_earning) +
              parseInt(this.allbookings[x].trip_cost);
          }
          if (days_past <= 30) {
            this.months_earning =
              parseInt(this.months_earning) +
              parseInt(this.allbookings[x].trip_cost);
          }
        }
      }

      //time difference || today's earning && month earning
      /*this.todayBookings = 0;
      this.currentDate = new Date().toISOString().substring(0, 10);
      this.monthBookings = 0;
      this.currentMonth = new Date().toISOString().substring(0, 7);
      for( let t=0; t<this.allbookings.length; t++){
          if(this.allbookings[t].tripdate){
              let time = this.allbookings[t].tripdate;
              let newtime = time.substring(0, 10);
              let newMonth = time.substring(0, 7);
                  if(this.currentDate == newtime){
                      this.todayBookings += this.allbookings[t].trip_cost;
                  }

                  if(this.currentMonth == newMonth){
                      this.monthBookings += this.allbookings[t].trip_cost;
                  }
                  console.log(newMonth);
                  console.log("month bookings:" + this.monthBookings);
              //console.log(newtime);
              console.log("bookings today :" + this.todayBookings);
          }
      }*/
    });
    //last_billing_ammount
    this.userRef.on('value', (snap1: any) => {
      if (snap1.val()) {
        if (snap1.val().last_billing_id) {
          let billingId = snap1.val().last_billing_id;
          let billingRef = this.firebaseService.getDbRef(
            'users/' + this.curUser.uid + '/bookings/' + billingId
          );

          billingRef.once('value', (snap2: any) => {
            if (snap2.val()) {
              if (snap2.val().trip_cost) {
                this.last_billing_ammount = snap2.val().trip_cost;
              }
            }
          });
        }
      }
    });

    /*
            this.userRef.on('value',(snap1:any)=>{
                if(snap1.val()){

                    this.ref = this.firebaseService.getDbRef('users/'+ this.curUser.uid+ '/bookings');
                        this.ref.once('value',(snapshot:any)=>{
                            this.allbookings=[];
                            let count=0;
                            let countratings = 0;

                            //last billing
                            if(snapshot.val()){
                              this.data = snapshot.val();
                                  if(snap1.val().last_billing_id){
                                    let billingId = snap1.val().last_billing_id;
                                    let billingRef = this.firebaseService.getDbRef('users/' + this.curUser.uid + '/bookings/'+ billingId);

                                        billingRef.once('value',(snap2:any)=>{
                                          if(snap2.val()){
                                              if(snap2.val().trip_cost){
                                                this.last_billing_ammount = snap2.val().trip_cost;
                                              }
                                          }
                                        })
                                  }

                            //allbookings
                              for(let key in this.data){
                                this.data[key].key=key;
                                this.allbookings.push(this.data[key]);
                                  console.log(this.allbookings);
                                  console.log(this.allbookings.length);

                                }
                            }
                                //average rating
                                for(let i=0; i<this.allbookings.length; i++ ){
                                    console.log(this.allbookings[i].ratings);
                                    if(this.allbookings[i].ratings){
                                        countratings += this.allbookings[i].ratings.rating;
                                        count +=1;
                                        this.avgRatings = countratings/count;
                                        this.avgRatings= this.avgRatings.toFixed(1);
                                    }
                                    console.log(this.avgRatings);
                                }

                                //complete bookings || lifetime booking
                                this.completedBookings = 0;
                                this.LifetimeBookings = 0;
                                for( let j = 0; j<this.allbookings.length; j++){
                                    if(this.allbookings[j].status == "ENDED" || this.allbookings[j].status == "REVIEW"){
                                        this.LifetimeBookings +=this.allbookings[j].trip_cost;
                                        this.completedBookings +=1;
                                    }
                                }

                                //time difference || today's earning && month earning
                                this.todayBookings = 0;
                                this.currentDate = new Date().toISOString().substring(0, 10);
                                this.monthBookings = 0;
                                this.currentMonth = new Date().toISOString().substring(0, 7);
                                for( let t=0; t<this.allbookings.length; t++){
                                    if(this.allbookings[t].tripdate){
                                        let time = this.allbookings[t].tripdate;
                                        let newtime = time.substring(0, 10);
                                        let newMonth = time.substring(0, 7);
                                            if(this.currentDate == newtime){
                                                this.todayBookings += this.allbookings[t].trip_cost;
                                            }

                                            if(this.currentMonth == newMonth){
                                                this.monthBookings += this.allbookings[t].trip_cost;
                                            }
                                            console.log(newMonth);
                                            console.log("month bookings:" + this.monthBookings);
                                        //console.log(newtime);
                                        console.log("bookings today :" + this.todayBookings);
                                    }
                                }

                        });


                 }
            });*/

    /*  let dateTime = this.formatLocalDate();
      console.log(dateTime);*/
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad StatsPage');
  }

  formatLocalDate() {
    var now = new Date(),
      tzo = -now.getTimezoneOffset(),
      dif = tzo >= 0 ? '+' : '-',
      pad = function (num) {
        var norm = Math.abs(Math.floor(num));
        return (norm < 10 ? '0' : '') + norm;
      };
    return (
      now.getFullYear() +
      '-' +
      pad(now.getMonth() + 1) +
      '-' +
      pad(now.getDate())
    );
  }
}
