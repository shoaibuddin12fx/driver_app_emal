import { Component, Injector, OnInit } from '@angular/core';
import { BasePage } from '../base-page/base-page';

@Component({
  selector: 'app-splash',
  templateUrl: './splash.page.html',
  styleUrls: ['./splash.page.scss'],
})
export class SplashPage extends BasePage implements OnInit {
  constructor(injector: Injector) {
    super(injector);
  }

  ngOnInit() {
    setTimeout(() => {
      this.checkLogin();
    }, 1000);
  }

  async checkLogin() {
    let user = await this.firebaseService.getCurrentUser();
    if (user) this.nav.push('pages/tabs');
    else this.nav.push('pages/login');
  }
}
