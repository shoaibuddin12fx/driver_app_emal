import {
  Component,
  ElementRef,
  Injector,
  OnInit,
  ViewChild,
} from '@angular/core';
import { BasePage } from '../base-page/base-page';
import { CallNumber } from '@ionic-native/call-number/ngx';
import {
  LaunchNavigator,
  LaunchNavigatorOptions,
} from '@awesome-cordova-plugins/launch-navigator/ngx';

declare const google;

@Component({
  selector: 'app-active-booking',
  templateUrl: './active-booking.page.html',
  styleUrls: ['./active-booking.page.scss'],
})
export class ActiveBookingPage extends BasePage implements OnInit {
  @ViewChild('map', { static: false }) mapElement: ElementRef;
  curUser: any;
  userProfile: any;
  clickable = true;
  bookingKey: any;

  booking: any;

  map: any;

  customerID: any;
  driverID: any;
  stripeID: any;
  callCustomer: any;

  mapReady: boolean;

  pushToken: string;

  personal_hour: any = 0;
  personal_minimum: any = 0;
  pickup_base_km: any = 0;
  pickup_hour: any = 0;
  pickup_km: any = 0;
  pickup_minimum: any = 0;
  pickup_minute: any = 0;

  paymentclick = true;
  start: string;
  constructor(
    injector: Injector,
    private callNumber: CallNumber,
    private launchNavigator: LaunchNavigator
  ) {
    super(injector);
    this.start = '';
    this.totalLocation = [];
    let params = this.getQueryParams();

    this.bookingKey = params?.bookingKey;
    console.log('BookingKey ->', this.bookingKey);
    let bookingRef = this.firebaseService.getDbRef(
      '/bookings/' + this.bookingKey
    );

    this.mapReady = false;
    this.mapService.showmap = true;

    bookingRef.on('value', (snapshot: any) => {
      this.booking = snapshot.val();
      if (this.booking != undefined && this.booking != null) {
        this.booking.image_url = snapshot.val().customer_image;

        this.customerID = snapshot.val().customer;
        this.driverID = snapshot.val().driver;
        this.pushToken = snapshot.val().customer_push_token;
        if (this.booking.status == 'CANCELLED') {
          this.nav.setRoot('tabs');
        }
        if (this.booking.status == 'PAYMENT') {
          this.paymentclick = false;
          this.setClickable(false);
          this.mapService.showmap = false;
        }

        var paystripe = this.firebaseService.getDbRef(
          '/users/' + this.booking.customer
        );
        paystripe.on('value', (_snapshot: any) => {
          if (_snapshot.val()) {
            this.stripeID = _snapshot.val().stripeID;
            console.log('this.stripeID.................', this.stripeID);
          }
        });
      } else {
        this.nav.setRoot('tabs');
      }
    });
    var rateRef = this.firebaseService.getDbRef('rates');

    rateRef.on('value', (snapshot: any) => {
      if (snapshot.val()) {
        this.personal_hour = snapshot.val().personal_hour;
        this.personal_minimum = snapshot.val().personal_minimum;
        this.pickup_base_km = snapshot.val().pickup_base_km;
        this.pickup_hour = snapshot.val().pickup_hour;
        this.pickup_km = snapshot.val().pickup_km;
        this.pickup_minimum = snapshot.val().pickup_minimum;
        this.pickup_minute = snapshot.val().pickup_minute;
      }
    });
  }

  ngOnInit() {
    this.platform.ready().then(() => {
      this.loadMap();
    });
  }

  loadMap() {
    let self = this;
    const location = {
      lat: parseFloat(this.booking.pickup.lat),
      lng: parseFloat(this.booking.pickup.lng),
    };

    this.map = new google.maps.Map(this.mapElement.nativeElement, {
      zoom: 15,
      center: location,
    });

    // this.map = new GoogleMap('map', {
    //   'backgroundColor': 'white',
    //   'controls': {
    //     'compass': false,
    //     'myLocationButton': false,
    //     'indoorPicker': false,
    //     'zoom': true
    //   },
    //   'gestures': {
    //     'scroll': true,
    //     'tilt': true,
    //     'rotate': true,
    //     'zoom': true
    //   },
    //   'camera': {
    //     'latLng': location,
    //     'tilt': 30,
    //     'zoom': 15//,
    //     // 'bearing': 50
    //   }
    // });

    google.maps.event.addListenerOnce(this.map, 'idle', function () {
      // do something only the first time the map is loaded
      this.map?.markers.clear();
      this.mapReady = true;
      console.log('Map is ready!');
      this.markerSetTo = 'pickup';
    });

    google.maps.event.addListenerOnce(this.map, 'idle', function () {
      this.mapReady = true;
      let bookingRef = self.firebaseService.getDbRef(
        '/bookings/' + this.bookingKey
      );
      bookingRef.on('value', (snapshot: any) => {
        console.log('activeBooking -> snapshot', snapshot.val());
        // this.map.clear();
        this.booking = snapshot.val();
        console.log('mY serice type check address........');
        console.log(this.booking);
        if (this.booking)
          this.booking.image_url = snapshot.val()?.customer_image;

        let marker = new google.maps.Marker({
          position: location,
          title: 'Pickup Location',
        });

        marker.setMap(this.map);
        //  marker.showInfoWindow();
        // this.map.setCenter(location);
      });

      console.log('Map is ready!');
    });
  }

  countTime: any;
  time: any;
  totalLocation: any;
  totalKm: any;
  allbookingDetails: any;
  ammount: any;

  doPayemnet() {
    /*  AAded By HR PATEL  */
    var paymentbill = this.firebaseService.getDbRef(
      '/bookings/' + this.bookingKey + '/trip_cost'
    );
    paymentbill.once('value', (snapbill) => {
      let totalBill = snapbill.val();
      console.log('MY paypal bill.........', totalBill);
      var payRef = this.firebaseService.getDbRef(
        '/users/' + this.booking.customer + '/payment_methods'
      );
      payRef.once('value', (snapshot) => {
        if (snapshot.val()) {
          let cards = snapshot.val();
          let cardID = '';
          console.log('cards.........', cards);
          let GotIt = false;
          for (let i = 0; i < cards.length; i++) {
            if (cards[i].primary) {
              GotIt = true;
              cardID = cards[i].stripeCardID;
            }
          }
          if (GotIt) {
            console.log(cardID, totalBill);
            this.usersService
              .stripePayment(cardID, this.stripeID, totalBill)
              .subscribe(
                (response) => {
                  console.log('stripe payment response', response);
                  let temp: any;
                  temp = response;
                  if (temp.status == 'succeeded') {
                    this.alertService.Alert('Payment', 'Payment succeeded');
                    // alert("succeeded payment");
                    // alert('Payment succeeded');
                    this.usersService
                      .sendPush(
                        'Trip Ended - Payment Success !!',
                        'Your cost for the trip is $' + totalBill,
                        this.booking.customer_push_token
                      )
                      .subscribe((response) => console.log(response));
                    this.paymentclick = true;
                    // this.firebaseService.getDbRef('users/' + customer + '/bookings/' + this.bookingKey + '/status' ).set('PAYMENT');
                    // this.firebaseService.getDbRef('users/' + driver + '/bookings/' + this.bookingKey + '/status').set('PAYMENT');
                    this.nav.push('job-complete', {
                      bookingKey: this.bookingKey,
                    });
                  } else {
                    // let alert = this.alertCtrl.create({
                    //   title: 'Payment',
                    //   subTitle: temp.message,
                    //   buttons: ['OK']
                    // });
                    // let alert = this.alertCtrl.create();
                    // alert.present();
                    alert(temp.message);
                    // this.firebaseService.getDbRef('users/' + customer + '/bookings/' + this.bookingKey + '/status' ).set('REVIEW');
                    // this.firebaseService.getDbRef('users/' + driver + '/bookings/' + this.bookingKey + '/status').set('REVIEW');
                    this.usersService
                      .sendPush(
                        'Payment Not Approve !!',
                        'No Payment Method Found. Your cost for the trip is $' +
                          totalBill,
                        this.booking.customer_push_token
                      )
                      .subscribe((response) => console.log(response));
                  }
                },
                (err) => {
                  //alert('Server Error ! Try again');
                  // this.firebaseService.getDbRef('users/' + customer + '/bookings/' + this.bookingKey + '/status' ).set('PAYMENT');
                  // this.firebaseService.getDbRef('users/' + driver + '/bookings/' + this.bookingKey + '/status').set('PAYMENT');
                }
              );
          } else {
            alert('Customer Not Selected Primary card');

            // this.firebaseService.getDbRef('users/' + customer + '/bookings/' + this.bookingKey + '/status' ).set('PAYMENT');
            // this.firebaseService.getDbRef('users/' + driver + '/bookings/' + this.bookingKey + '/status').set('PAYMENT');
            this.usersService
              .sendPush(
                'Payment Failed !!',
                'Not Selected Primary card. Your cost for the trip is $' +
                  totalBill,
                this.booking.customer_push_token
              )
              .subscribe((response) => console.log(response));
          }
        } else {
          alert('Customer Card Not Available');
          this.usersService
            .sendPush(
              'Payment Failed !!',
              ' Card Not Available. Your cost for the trip is $' + totalBill,
              this.booking.customer_push_token
            )
            .subscribe((response) => console.log(response));
        }
      });
    });
    // });
    /* Ended By HR PATEL */
  }

  async changeStatus(statusSelected) {
    console.log('change status......................', statusSelected);
    this.setClickable(true);
    this.mapService.showmap = true;
    let currentStatus = this.booking.status;
    if (
      this.getStatusPriority(statusSelected) ==
      this.getStatusPriority(currentStatus) + 1
    ) {
      let bookingRef = this.firebaseService.getDbRef(
        '/bookings/' + this.bookingKey
      );
      bookingRef.once('value', (snapshot: any) => {
        this.booking = snapshot.val();
        let customer = this.booking.customer;
        let driver = this.booking.driver;
        bookingRef.child('status').set(statusSelected);
        this.firebaseService
          .getDbRef(
            'users/' + customer + '/bookings/' + this.bookingKey + '/status'
          )
          .set(statusSelected);
        this.firebaseService
          .getDbRef(
            'users/' + driver + '/bookings/' + this.bookingKey + '/status'
          )
          .set(statusSelected);

        if (statusSelected == 'REVIEW') {
          this.firebaseService
            .getDbRef('/users/' + this.booking.driver + '/movementStatus')
            .remove();
          // this.firebaseService.getDbRef('/users/' + this.booking.driver + '/movementStatus').set('LOCATE_ONLY');

          let geoRef = this.firebaseService.getDbRef('geofire');
          geoRef.child(this.bookingKey).remove();

          let distanceref = this.firebaseService.getDbRef(
            '/bookings/' + this.bookingKey
          );
          distanceref.once('value', (snap2: any) => {
            if (snap2.val()) {
              this.allbookingDetails = snap2.val();
              if (snap2.val().triplocations) {
                let locationsArr = snap2.val().triplocations;
                for (let key in locationsArr)
                  this.totalLocation.push(locationsArr[key]);
              }
              var _eQuatorialEarthRadius = 6378.137;
              var _d2r = Math.PI / 180.0;
              var distanceTotal = 0;
              for (var i = 0; i < this.totalLocation.length - 1; i++) {
                var dlong =
                  (this.totalLocation[i + 1].lng - this.totalLocation[i].lng) *
                  _d2r;
                var dlat =
                  (this.totalLocation[i + 1].lat - this.totalLocation[i].lat) *
                  _d2r;
                var a =
                  Math.pow(Math.sin(dlat / 2.0), 2.0) +
                  Math.cos(this.totalLocation[i].lat * _d2r) *
                    Math.cos(this.totalLocation[i + 1].lat * _d2r) *
                    Math.pow(Math.sin(dlong / 2.0), 2.0);
                var c = 2.0 * Math.atan2(Math.sqrt(a), Math.sqrt(1.0 - a));
                var d = _eQuatorialEarthRadius * c;
                distanceTotal += d;
                this.totalKm = distanceTotal.toFixed();
                console.log('this.Total Distance - >' + this.totalKm);
              }
            }
          });

          let etime = this.formatLocalDate();
          this.firebaseService
            .getDbRef('/bookings/' + this.bookingKey + '/trip_end_time')
            .set(etime);

          let starttime = new Date(snapshot.val().trip_start_time);
          let endtime = new Date(etime);

          var difference = endtime.getTime() - starttime.getTime();
          var resultInHour = Math.round(difference / 360000);

          // var resultInMinute = (resultInHour * 60);
          var resultInMinute = (Math.round(difference) / (1000 * 60)) % 60;

          console.log('time calculation - >' + resultInMinute);

          this.firebaseService
            .getDbRef('/bookings/' + this.bookingKey + '/total_trip_time')
            .set(resultInMinute);
          this.firebaseService
            .getDbRef('/users/' + this.booking.driver + '/last_billing_id')
            .set(this.bookingKey);

          let totalBill = 0;

          if (this.booking.serviceType == 'Pickup') {
            let totalDistance = this.totalKm;

            let billableKms = 0;
            if (totalDistance > parseInt(this.pickup_base_km)) {
              billableKms = totalDistance - parseInt(this.pickup_base_km);
              console.log('bilable km - >' + billableKms);
              console.log('minimum rate -> ' + this.pickup_minimum);
            }
            // let totalBill = parseInt(this.pickup_minimum)  + (billableKms * parseInt(this.pickup_km)) + (resultInMinute * parseInt(this.pickup_minute));
            let totalBill =
              parseInt(this.pickup_minimum) +
              billableKms * parseInt(this.pickup_km) +
              resultInMinute * this.pickup_minute;
            totalBill = Math.round(totalBill * 100) / 100;
            if (this.booking.hasOwnProperty('estimate')) {
              totalBill = this.booking.estimate;
            }
            if (this.booking.hasOwnProperty('trip_cost')) {
              totalBill = this.booking.trip_cost;
            }

            console.log('total bill - >' + totalBill);

            //promo code calculation
            if (this.allbookingDetails.promo_code) {
              if (this.allbookingDetails.promo_code.promoType == 'by_ammount') {
                this.ammount =
                  totalBill - this.allbookingDetails.promo_code.promoValue;
              } else if (
                this.allbookingDetails.promo_code.promoType == 'by_percentage'
              ) {
                this.ammount =
                  totalBill -
                  totalBill / this.allbookingDetails.promo_code.promoValue;
              } else if (
                this.allbookingDetails.promo_code.promoType == 'by_ride'
              ) {
                this.ammount = totalBill - totalBill;
              }
              this.firebaseService
                .getDbRef('/bookings/' + this.bookingKey + '/trip_cost')
                .set(this.ammount);
              this.firebaseService
                .getDbRef(
                  '/users/' +
                    this.booking.customer +
                    '/bookings/' +
                    this.bookingKey +
                    '/trip_cost'
                )
                .set(this.ammount);
              this.firebaseService
                .getDbRef(
                  '/users/' +
                    this.booking.driver +
                    '/bookings/' +
                    this.bookingKey +
                    '/trip_cost'
                )
                .set(this.ammount);
              this.usersService
                .sendPush(
                  'Trip Ended',
                  'Your cost for the trip is $' + this.ammount,
                  this.booking.customer_push_token
                )
                .subscribe((response) => console.log(response));
            } else {
              this.firebaseService
                .getDbRef('/bookings/' + this.bookingKey + '/trip_cost')
                .set(totalBill);
              this.firebaseService
                .getDbRef(
                  '/users/' +
                    this.booking.customer +
                    '/bookings/' +
                    this.bookingKey +
                    '/trip_cost'
                )
                .set(totalBill);
              this.firebaseService
                .getDbRef(
                  '/users/' +
                    this.booking.driver +
                    '/bookings/' +
                    this.bookingKey +
                    '/trip_cost'
                )
                .set(totalBill);
              this.usersService
                .sendPush(
                  'Trip Ended',
                  'Your cost for the trip is $' + totalBill,
                  this.booking.customer_push_token
                )
                .subscribe((response) => console.log(response));
            }
            this.paymentclick = false;
            //promo code calculation End

            //  this.firebaseService.getDbRef('/bookings/' + this.bookingKey + '/trip_cost').set(totalBill);
            // this.firebaseService.getDbRef('/users/' + this.booking.customer + '/bookings/' + this.bookingKey + '/trip_cost' ).set(totalBill);
            // this.firebaseService.getDbRef('/users/' + this.booking.driver + '/bookings/' + this.bookingKey + '/trip_cost' ).set(totalBill);

            //  this.usersService.sendPush('Trip Ended',"Your cost for the trip is " + totalBill + "$",this.booking.customer_push_token).subscribe(response=>console.log(response));
          } else {
            if (this.booking.triptime > resultInHour) {
              totalBill = this.booking.estimate;
            } else {
              let totalBill =
                parseInt(this.personal_minimum) +
                resultInHour * parseInt(this.personal_hour);
            }

            totalBill = Math.round(totalBill * 100) / 100;

            //promo code calculation
            if (this.allbookingDetails.promo_code) {
              if (this.allbookingDetails.promo_code.promoType == 'by_ammount') {
                this.ammount =
                  totalBill - this.allbookingDetails.promo_code.promoValue;
              } else if (
                this.allbookingDetails.promo_code.promoType == 'by_percentage'
              ) {
                this.ammount =
                  totalBill -
                  totalBill / this.allbookingDetails.promo_code.promoValue;
              } else if (
                this.allbookingDetails.promo_code.promoType == 'by_ride'
              ) {
                this.ammount = totalBill - totalBill;
              }
              this.firebaseService
                .getDbRef('/bookings/' + this.bookingKey + '/trip_cost')
                .set(this.ammount);
              this.firebaseService
                .getDbRef(
                  '/users/' +
                    this.booking.customer +
                    '/bookings/' +
                    this.bookingKey +
                    '/trip_cost'
                )
                .set(this.ammount);
              this.firebaseService
                .getDbRef(
                  '/users/' +
                    this.booking.driver +
                    '/bookings/' +
                    this.bookingKey +
                    '/trip_cost'
                )
                .set(this.ammount);
              this.usersService
                .sendPush(
                  'Trip Ended',
                  'Your cost for the trip is $' + this.ammount,
                  this.booking.customer_push_token
                )
                .subscribe((response) => console.log(response));
            } else {
              this.firebaseService
                .getDbRef('/bookings/' + this.bookingKey + '/trip_cost')
                .set(totalBill);
              this.firebaseService
                .getDbRef(
                  '/users/' +
                    this.booking.customer +
                    '/bookings/' +
                    this.bookingKey +
                    '/trip_cost'
                )
                .set(totalBill);
              this.firebaseService
                .getDbRef(
                  '/users/' +
                    this.booking.driver +
                    '/bookings/' +
                    this.bookingKey +
                    '/trip_cost'
                )
                .set(totalBill);
              this.usersService
                .sendPush(
                  'Trip Ended',
                  'Your cost for the trip is $' + totalBill,
                  this.booking.customer_push_token
                )
                .subscribe((response) => console.log(response));
            }

            this.paymentclick = false;
            //promo code calculation End

            //  this.firebaseService.getDbRef('/bookings/' + this.bookingKey + '/trip_cost').set(totalBill);
            // this.firebaseService.getDbRef('/users/' + this.booking.customer + '/bookings/' + this.bookingKey + '/trip_cost' ).set(totalBill);
            // this.firebaseService.getDbRef('/users/' + this.booking.driver + '/bookings/' + this.bookingKey + '/trip_cost' ).set(totalBill);

            // this.usersService.sendPush('Trip Ended',"Your cost for the trip is " + totalBill + "$",this.booking.customer_push_token).subscribe(response=>console.log(response));
          }

          // HR comment to payment check..........
          // this.navCtrl.setRoot(JobcompletePage,{
          //     bookingKey:this.bookingKey
          // });

          //**************************************** */

          //    this.firebaseService.getDbRef('/users/' + this.booking.driver + '/movementStatus').remove();

          //    let geoRef = this.firebaseService.getDbRef('geofire');
          //    geoRef.child(this.bookingKey).remove();

          //    let etime = this.formatLocalDate();
          //    this.firebaseService.getDbRef('/bookings/' + this.bookingKey + '/trip_end_time').set(etime);

          //    let starttime = new Date(snapshot.val().trip_start_time);
          //    let endtime = new Date(etime);

          //    var difference = endtime.getTime() - starttime.getTime();
          //    var resultInHour = Math.round(difference / 360000);

          //    this.firebaseService.getDbRef('/bookings/' + this.bookingKey + '/total_trip_time').set(resultInHour);
          //    this.firebaseService.getDbRef('/users/' +this.booking.driver + "/last_billing_id").set(this.bookingKey);
          //    let returntrip = false;
          //    if(snapshot.val().tripType == "Return"){
          //        returntrip = true;
          //    }

          //    let totalBill = 0;

          //    if(this.booking.serviceType=="Pickup"){

          //         ////////  HAVE TO CHANGE THIS  ///////
          //        let totalDistance = this.booking.distance/1000;

          //        let billableKms = 0;
          //        if(totalDistance > parseInt(this.pickup_base_km)){
          //            billableKms = totalDistance - parseInt(this.pickup_base_km);
          //        }
          //        let totalBill = parseInt(this.pickup_minimum)  + (billableKms * parseInt(this.pickup_km)) + (resultInHour * parseInt(this.pickup_hour));

          //        totalBill = Math.round(totalBill * 100) / 100;
          //    }else{
          //        if(this.booking.triptime>resultInHour){
          //            totalBill = this.booking.estimate;
          //        }else{
          //            let totalBill = parseInt(this.personal_minimum) + (resultInHour * parseInt(this.personal_hour));
          //        }

          //        totalBill = Math.round(totalBill * 100) / 100;
          //    }

          //        this.firebaseService.getDbRef('/bookings/' + this.bookingKey + '/trip_cost').set(totalBill);
          //        this.firebaseService.getDbRef('/users/' + this.booking.customer + '/bookings/' + this.bookingKey + '/trip_cost' ).set(totalBill);
          //        this.firebaseService.getDbRef('/users/' + this.booking.driver + '/bookings/' + this.bookingKey + '/trip_cost' ).set(totalBill);

          //        var payRef = this.firebaseService.getDbRef("/users/" + this.booking.customer + "/payment_methods");
          //        payRef.once("value",snapshot=>{
          //            if(snapshot.val()){
          //                let cards = snapshot.val();
          //                let cardID = "";
          //                console.log(cards);
          //                let GotIt =false;
          //                for(let i=0;i<cards.length;i++){
          //                    if(cards[i].primary){
          //                        GotIt = true;
          //                        cardID = cards[i].id;

          //                    }
          //                }
          //                if(GotIt){
          //                        this.usersService.chargePayment(cardID,totalBill).subscribe(response=>{
          //                            let temp :any;
          //                            temp = response;
          //                            if(temp.state =="approved"){
          //                                this.usersService.sendPush('Trip Ended - Payment Success !!',"Your cost for the trip is " + totalBill + "$",this.booking.customer_push_token).subscribe(response=>console.log(response));
          //                            }else{
          //                                this.usersService.sendPush('Payment Failed !!',"No Payment Method Found. Your cost for the trip is " + totalBill + "$",this.booking.customer_push_token).subscribe(response=>console.log(response));
          //                            }

          //                        });
          //                }else{
          //                    this.usersService.sendPush('Payment Failed !!',"No Payment Method Found. Your cost for the trip is " + totalBill + "$",this.booking.customer_push_token).subscribe(response=>console.log(response));
          //                }

          //            }else{
          //                this.usersService.sendPush('Payment Failed !!',"Your cost for the trip is " + totalBill + "$",this.booking.customer_push_token).subscribe(response=>console.log(response));
          //            }
          //        });

          //       // this.usersService.sendPush('Trip Ended',"Your cost for the trip is " + totalBill + "$",this.booking.customer_push_token).subscribe(response=>console.log(response));

          // //  this.locationTracker.stopTracking();
          //  // this.navCtrl.setRoot(CurrentbookingsPage);
          //   this.navCtrl.setRoot(JobcompletePage,{
          //       bookingKey:this.bookingKey
          //   });
          //**************************************** */

          this.doPayemnet();
        }
        if (statusSelected == 'ARRIVING') {
          this.firebaseService
            .getDbRef('/users/' + this.booking.driver + '/movementStatus')
            .set('LOCATE_ONLY');
          this.usersService
            .sendPush(
              'Driver is on their way to',
              this.booking.pickup.add,
              this.pushToken
            )
            .subscribe((response) => console.log(response));
        }
        if (statusSelected == 'ARRIVED') {
          this.firebaseService
            .getDbRef('/users/' + this.booking.driver + '/movementStatus')
            .remove();
          this.usersService
            .sendPush('Driver Arrived', this.booking.pickup.add, this.pushToken)
            .subscribe((response) => console.log(response));
        }
        if (statusSelected == 'STARTED') {
          this.firebaseService
            .getDbRef('/bookings/' + this.bookingKey + '/trip_start_time')
            .set(this.formatLocalDate());
          this.firebaseService
            .getDbRef('/users/' + this.booking.driver + '/movementStatus')
            .set(this.bookingKey);
        }
      });
    } else if (statusSelected === 'PAYMENT') {
      console.log('payment.......');

      if (!this.paymentclick) {
        // let bookingRef = this.firebaseService.getDbRef('/bookings/' + this.bookingKey);
        // bookingRef.once('value', (snapshot:any) => {

        // this.booking = snapshot.val();
        // let customer = this.booking.customer;
        // let driver = this.booking.driver;

        /*  AAded By HR PATEL  */
        var paymentbill = this.firebaseService.getDbRef(
          '/bookings/' + this.bookingKey + '/trip_cost'
        );
        paymentbill.once('value', (snapbill) => {
          let totalBill = snapbill.val();
          console.log('MY paypal bill.........', totalBill);
          var payRef = this.firebaseService.getDbRef(
            '/users/' + this.booking.customer + '/payment_methods'
          );
          payRef.once('value', (snapshot) => {
            if (snapshot.val()) {
              let cards = snapshot.val();
              let cardID = '';
              console.log('cards.........', cards);
              let GotIt = false;
              for (let i = 0; i < cards.length; i++) {
                if (cards[i].primary) {
                  GotIt = true;
                  cardID = cards[i].stripeCardID;
                }
              }
              if (GotIt) {
                console.log(cardID, totalBill);
                this.usersService
                  .stripePayment(cardID, this.stripeID, totalBill)
                  .subscribe(
                    (response) => {
                      console.log('stripe payment response', response);
                      let temp: any;
                      temp = response;
                      if (temp.status == 'succeeded') {
                        // alert("succeeded payment");
                        // let alert = this.alertCtrl.create({
                        //   title: 'Payment',
                        //   subTitle: 'Payment succeeded',
                        //   buttons: ['OK']
                        // });
                        // alert.present();
                        alert('Payment succeeded');
                        this.usersService
                          .sendPush(
                            'Trip Ended - Payment Success !!',
                            'Your cost for the trip is $' + totalBill,
                            this.booking.customer_push_token
                          )
                          .subscribe((response) => console.log(response));
                        this.paymentclick = true;
                        // this.firebaseService.getDbRef('users/' + customer + '/bookings/' + this.bookingKey + '/status' ).set('PAYMENT');
                        // this.firebaseService.getDbRef('users/' + driver + '/bookings/' + this.bookingKey + '/status').set('PAYMENT');
                        this.nav.setRoot('job-complete', {
                          bookingKey: this.bookingKey,
                        });
                      } else {
                        // let alert = this.alertCtrl.create({
                        //   title: 'Payment',
                        //   subTitle: temp.message,
                        //   buttons: ['OK']
                        // });
                        // let alert = this.alertCtrl.create();
                        // alert.present();
                        alert(temp.message);
                        // this.firebaseService.getDbRef('users/' + customer + '/bookings/' + this.bookingKey + '/status' ).set('REVIEW');
                        // this.firebaseService.getDbRef('users/' + driver + '/bookings/' + this.bookingKey + '/status').set('REVIEW');
                        this.usersService
                          .sendPush(
                            'Payment Not Approve !!',
                            'No Payment Method Found. Your cost for the trip is $' +
                              totalBill,
                            this.booking.customer_push_token
                          )
                          .subscribe((response) => console.log(response));
                      }
                    },
                    (err) => {
                      // [TODO ]
                      // alert('Server Error ! Try again');
                      // this.firebaseService.getDbRef('users/' + customer + '/bookings/' + this.bookingKey + '/status' ).set('PAYMENT');
                      // this.firebaseService.getDbRef('users/' + driver + '/bookings/' + this.bookingKey + '/status').set('PAYMENT');
                    }
                  );
              } else {
                alert('Customer Not Selected Primary card');

                // this.firebaseService.getDbRef('users/' + customer + '/bookings/' + this.bookingKey + '/status' ).set('PAYMENT');
                // this.firebaseService.getDbRef('users/' + driver + '/bookings/' + this.bookingKey + '/status').set('PAYMENT');
                this.usersService
                  .sendPush(
                    'Payment Failed !!',
                    'Not Selected Primary card. Your cost for the trip is $' +
                      totalBill,
                    this.booking.customer_push_token
                  )
                  .subscribe((response) => console.log(response));
              }
            } else {
              alert('Customer Card Not Available');
              this.usersService
                .sendPush(
                  'Payment Failed !!',
                  ' Card Not Available. Your cost for the trip is $' +
                    totalBill,
                  this.booking.customer_push_token
                )
                .subscribe((response) => console.log(response));
            }
          });
        });
        // });
        /* Ended By HR PATEL */
      } else {
        let alert = await this.alertCtrl.create();
        this.setClickable(false);
        this.mapService.showmap = false;

        alert.title = 'Alert';
        alert.message = 'This status is not correct.';
        alert.buttons.push({
          text: 'Dismiss',
          handler: (data) => {
            this.setClickable(true);
            this.mapService.showmap = true;
            alert.dismiss();
          },
        });
        alert.present();
      }
    } else {
      let alert = await this.alertCtrl.create();
      this.setClickable(false);
      this.mapService.showmap = false;

      alert.title = 'Alert';
      alert.message = 'This status is not correct.';
      alert.buttons.push({
        text: 'Dismiss',
        handler: (data) => {
          this.setClickable(true);
          this.mapService.showmap = true;
          alert.dismiss();
        },
      });
      alert.present();
    }
  }

  getStatusPriority(status) {
    if (status == 'ASSIGNED') return 0;
    else if (status == 'ARRIVING') return 1;
    else if (status == 'ARRIVED') return 2;
    else if (status == 'STARTED') return 3;
    else if (status == 'REVIEW') return 4;
    else if (status == 'ENDED') return 5;
  }

  async presentActionSheet() {
    console.log(this.map);
    this.setClickable(false);
    this.mapService.showmap = false;
    let buttons = [
      {
        text: 'Going for Customer pickup',
        handler: () => {
          this.changeStatus('ARRIVING');
          console.log('Going for Customer pickup clicked');
        },
      },
      {
        text: 'Arrived at Pickup point',
        //*TODO*/ disbaled:'true',
        handler: () => {
          this.changeStatus('ARRIVED');
          console.log('Arrived at Pickup point clicked');
        },
      },
      {
        text: 'Start Trip',
        //*TODO*/ disbaled:'true',
        handler: () => {
          this.changeStatus('STARTED');
          console.log('Start Trip clicked');
        },
      },
      {
        text: 'End Trip',
        //*TODO*/ disbaled:'true',
        handler: () => {
          this.changeStatus('REVIEW');
          console.log('End Trip clicked');
        },
      },
      // {
      //   text: 'Payment',
      //    disbaled:'true',
      //   handler: () => {
      //     this.changeStatus('PAYMENT');
      //     console.log('Payment Pay clicked');
      //   }
      // },
      {
        text: 'Cancel',
        role: 'cancel',
        handler: () => {
          this.setClickable(true);
          this.mapService.showmap = true;
          console.log('Cancel clicked');
        },
      },
    ];

    let currentStatus = this.booking.status;
    let _buttons = [];
    let x = this.getStatusPriority(currentStatus) ?? 5;
    if (x >= 0) _buttons.push(buttons[x]);
    _buttons.push(buttons[buttons.length - 1]);
    // for (var y = x; y < buttons.length; y++) {
    //   _buttons.push(buttons[y]);
    // }

    let actionSheet = await this.actionSheetController.create({
      header: 'Change Booking Status',
      buttons: _buttons,
    });
    console.log(actionSheet);
    actionSheet.present();
  }

  //get help
  getHelp() {
    this.nav.push('pages/support', {
      bookingKey: this.bookingKey,
    });
  }

  formatLocalDate() {
    var now = new Date(),
      tzo = -now.getTimezoneOffset(),
      dif = tzo >= 0 ? '+' : '-',
      pad = function (num) {
        var norm = Math.abs(Math.floor(num));
        return (norm < 10 ? '0' : '') + norm;
      };
    return (
      now.getFullYear() +
      '-' +
      pad(now.getMonth() + 1) +
      '-' +
      pad(now.getDate()) +
      'T' +
      pad(now.getHours()) +
      ':' +
      pad(now.getMinutes()) +
      ':' +
      pad(now.getSeconds()) +
      '.000Z'
    );
  }

  giveCall() {
    let customerRef = this.firebaseService.getDbRef(
      '/users/' + this.customerID
    ); //customerUserId need to create in constructor

    customerRef.once('value', (snapshot: any) => {
      this.callCustomer = snapshot.val().phone;

      this.callNumber
        .callNumber(this.callCustomer, true)
        .then(() => console.log('Launched dialer!'))
        .catch(() => console.log('Error launching dialer'));
    });
  }

  ngDoCheck() {
    if (this.mapReady) {
      if (this.mapService.showmap) {
        this.setClickable(true);
      } else {
        this.setClickable(false);
      }
    }
  }

  routePickup(pickup, drop, serviceType) {
    console.log('pickup', pickup, 'serviceType', serviceType);

    if (serviceType === 'Pickup') {
      let options: LaunchNavigatorOptions = {
        start: pickup,
        // app: LaunchNavigator.APP.Waze
      };
      this.launchNavigator.navigate(drop, options).then(
        (success) => alert('Launched navigator'),
        (error) => alert('Error launching navigator: ' + error)
      );
    } else {
      let options: LaunchNavigatorOptions = {
        start: this.start,
        app: this.launchNavigator.APP.Waze,
      };
      console.log('Launching...');

      this.launchNavigator.navigate(pickup, options).then(
        (success) => alert('Launched navigator'),
        (error) => alert('Error launching navigator: ' + error)
      );
      // this.launchNavigator.navigate('Toronto, ON', options).then(
      //   (success) => console.log('Launched navigator'),
      //   (error) => console.log('Error launching navigator', error)
      // );
    }
  }

  setClickable(clickable) {
    this.clickable = clickable;
  }
}
