import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { ActiveBookingPageRoutingModule } from './active-booking-routing.module';

import { ActiveBookingPage } from './active-booking.page';
import { LaunchNavigator } from '@awesome-cordova-plugins/launch-navigator/ngx';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    ActiveBookingPageRoutingModule,
  ],
  declarations: [ActiveBookingPage],
  providers: [LaunchNavigator],
})
export class ActiveBookingPageModule {}
