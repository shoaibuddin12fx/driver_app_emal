import { Component, Injector, OnInit } from '@angular/core';
import { CameraService } from 'src/app/Services/camera.service';
import { BasePage } from '../base-page/base-page';

@Component({
  selector: 'app-profile',
  templateUrl: './profile.page.html',
  styleUrls: ['./profile.page.scss'],
})
export class ProfilePage extends BasePage implements OnInit {
  usertype: any;
  showCustomer: boolean;
  showDriver: boolean;
  driver = 'driver';
  customer = 'customer';
  data: any;
  //profileimage
  profileImage: any;

  curUser: any;

  constructor(injecor: Injector, private camera: CameraService) {
    super(injecor);
    this.usersService.presentLoading();
    this.data = {
      fullname: '',
      email: '',
      phone: '',
      registration: '',
      cityname: '',
    };
  }

  async ngOnInit() {
    this.curUser = await this.firebaseService.getCurrentUser();
    console.log('curr_user', this.curUser);

    var ref = this.getDbRef('/users/' + this.curUser.uid);
    ref.on('value', (_snapshot: any) => {
      if (_snapshot.val()) {
        this.data = _snapshot.val();
        this.profileImage = _snapshot.val().profile_image;
        this.usersService.dismissLoader();
        //  this.usertype = _snapshot.val().type;
        // console.log(this.usertype);
        /*  if(this.usertype==this.customer){
              this.showCustomer= true;
          }else if((this.usertype==this.driver)) {
              this.showCustomer= false;
          }else{
            alert("no type found");
          }*/
      } else {
        this.usersService.dismissLoader();
        console.log('no type added');
      }
    });
  }

  //constructor end
  async doAdd() {
    // console.log(this.data,userId);
    // this.usersService.updateData(this.data,this.curUser.uid)
    let profileRef = this.getDbRef('/users/' + this.curUser.uid);
    profileRef.child('fullname').set(this.data.fullname);
    profileRef.child('email').set(this.data.email);
    profileRef.child('phone').set(this.data.phone);
    profileRef.child('cityname').set(this.data.cityname);
    profileRef.child('profile_image').set(this.profileImage);
    let alert = await this.alertCtrl.create({
      header: 'Success',
      subHeader: 'Profile updated successfully',
      buttons: [
        {
          text: 'Ok',
          handler: () => {
            this.events.publish('login_event');
            // this.nav.push('pages/tabs/schedule');
            this.nav.pop();
          },
        },
      ],
    });
    alert.present();
  }

  captureDataUrl: any;
  //open camera picture
  // openCamera() {
  //   this.camera.
  //     Camera.getPicture(cameraOptions).then((imageData) => {
  //       this.captureDataUrl = 'data:image/jpeg;base64,' + imageData;
  //       console.log("CaptureDataUrl:" + this.captureDataUrl);
  //       this.upload();
  //     }, (err) => {
  //     });
  // }
  // //open galary picture
  // openGalary() {
  //   const cameraOptions: CameraOptions = {
  //     targetHeight: 150,
  //     targetWidth: 150,
  //     allowEdit: true,
  //     quality: 50,
  //     destinationType: Camera.DestinationType.DATA_URL,
  //     encodingType: Camera.EncodingType.JPEG,
  //     mediaType: Camera.MediaType.PICTURE,
  //     sourceType: Camera.PictureSourceType.PHOTOLIBRARY,
  //   };
  //   Camera.getPicture(cameraOptions).then((imageData) => {
  //     this.captureDataUrl = 'data:image/jpeg;base64,' + imageData;
  //     console.log("CaptureDataUrl:" + this.captureDataUrl);
  //     this.upload();
  //   }, (err) => {
  //   });
  // }
  //upload firebase
  upload() {
    this.profileImage = this.captureDataUrl;
    return;
    let storageRef = this.firebaseService.fireStorage.ref('/');
    const filename = Math.floor(Date.now() / 1000);
    const imageRef = storageRef.child(`images/${filename}.jpg`);
    console.log('Upload Const ImageRef:' + imageRef);
    //*TODO*// firebase.storage.StringFormat.DATA_URL
    imageRef.putString(this.captureDataUrl).then((snapshot) => {
      console.log('SnapShot:' + JSON.stringify(snapshot.ref.getDownloadURL()));
      this.saveDatabase(snapshot);
    });
  }
  //upload database
  users = [];
  async saveDatabase(snapshot) {
    let imgUrl = snapshot.ref.getDownloadURL();
    var ref = this.getDbRef('users/' + this.curUser.uid);
    ref.child('profile_image').set(snapshot.downloadURL);

    let user = await this.firebaseService.getCurrentUser();
    var ref = this.getDbRef('/users/' + user.uid + '/bookings');
    // var bookingref = this.getDbRef('bookings');
    ref.on('value', (snap1) => {
      let userdata = snap1.val();
      for (var key in userdata) {
        userdata[key].uid = key;
        if (key != user.uid) {
          this.users.push(userdata[key]);
        }
      }
      this.bookingsUpdateData(this.users, imgUrl);
    });
  }

  async bookingsUpdateData(userIds, imgUrl) {
    let currentuser = await this.firebaseService.getCurrentUser();
    var ref = this.getDbRef('/users/' + currentuser.uid);
    ref.once('value', (snap2) => {
      let userType = snap2.val().type;
      if (userType == 'customer') {
        for (let i = 0; i < userIds.length; i++) {
          console.log(userIds[i].uid);
          var bookingref = this.getDbRef('/bookings/' + userIds[i].uid);
          let userBookings = this.getDbRef(
            '/users/' + userIds[i].driver + '/bookings/' + userIds[i].uid
          );
          bookingref.child('customer_image').set(imgUrl);
          userBookings.child('customer_image').set(imgUrl);
        }
      } else {
        for (let i = 0; i < userIds.length; i++) {
          // let id = userIds[i].uid;
          var bookingref = this.getDbRef('/bookings/' + userIds[i].uid);
          let userBookings = this.getDbRef(
            '/users/' + userIds[i].customer + '/bookings/' + userIds[i].uid
          );
          bookingref.child('driver_image').set(imgUrl);
          userBookings.child('driver_image').set(imgUrl);
        }
      }
    });
  }

  //option Select Camera or Album
  async doGetPicture() {
    let base64Image = await this.camera.selectImage();
    if (base64Image && base64Image !== '') {
      this.captureDataUrl = base64Image;
      console.log(this.captureDataUrl);
      this.upload();
    }
  }
  //End Camera and Album related work

  getDbRef(ref) {
    return this.firebaseService.getDbRef(ref);
  }
}
