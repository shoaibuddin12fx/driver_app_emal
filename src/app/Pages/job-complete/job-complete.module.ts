import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { IonicRatingModule } from 'ionic-rating';

import { IonicModule } from '@ionic/angular';

import { JobCompletePageRoutingModule } from './job-complete-routing.module';

import { JobCompletePage } from './job-complete.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    JobCompletePageRoutingModule,
    IonicRatingModule,
  ],
  declarations: [JobCompletePage],
})
export class JobCompletePageModule {}
