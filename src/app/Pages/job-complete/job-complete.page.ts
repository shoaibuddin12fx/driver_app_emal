import { Component, Injector, OnInit } from '@angular/core';
import { BasePage } from '../base-page/base-page';

@Component({
  selector: 'app-job-complete',
  templateUrl: './job-complete.page.html',
  styleUrls: ['./job-complete.page.scss'],
})
export class JobCompletePage extends BasePage implements OnInit {
  bookingKey: any;
  data: any;
  public customerRating: any = 0;
  //tripDistance:any;
  tripCost: any;

  constructor(injector: Injector) {
    super(injector);
    let self = this;
    let params = this.getQueryParams();
    this.bookingKey = params?.bookingKey;
    let ref = this.firebaseService.getDbRef('bookings/' + this.bookingKey);
    ref.on('value', (snapshot: any) => {
      if (snapshot.val()) {
        self.data = snapshot.val();

        /*if(self.data.distance != undefined){
            self.tripDistance = (self.data.distance/1000).toFixed(1);
        }*/

        if (self.data.trip_cost != undefined) {
          self.tripCost = self.data.trip_cost;
        }

        // if (self.data.ratings != undefined) {
        // }
        console.log('DATA', self.data);

        self.customerRating = self.data.ratings?.rating;
      }
    });
  }

  ngOnInit() {}

  ionViewDidLoad() {
    console.log('ionViewDidLoad JobcompletePage');
  }
}
