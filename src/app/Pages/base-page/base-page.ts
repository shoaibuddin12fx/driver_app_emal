import { NavService } from 'src/app/services/nav.service';
import { Injector, NgZone } from '@angular/core';
import { UtilityService } from 'src/app/services/utility.service';
import { CommonModule, Location } from '@angular/common';
import {
  Platform,
  MenuController,
  AlertController,
  ModalController,
} from '@ionic/angular';
import { EventsService } from 'src/app/services/events.service';
import { FormBuilder } from '@angular/forms';
import { PopoversService } from 'src/app/services/basic/popovers.service';

import { ModalService } from 'src/app/services/basic/modal.service';
import { Router, ActivatedRoute } from '@angular/router';
import { UserService } from 'src/app/Services/user-service';
import { MapService } from 'src/app/Services/map-service';
import { ActionSheetController } from '@ionic/angular';
import { FirebaseService } from 'src/app/Services/firebase.service';
import { GeolocationsService } from 'src/app/Services/geolocations-service.service';
import { AlertService } from 'src/app/Services/alert.service';

export abstract class BasePage {
  public utility: UtilityService;
  public nav: NavService;
  public location: Location;
  public common: CommonModule;
  public events: EventsService;
  public platform: Platform;
  public formBuilder: FormBuilder;
  public popover: PopoversService;
  public usersService: UserService;
  public modals: ModalService;
  public menuCtrl: MenuController;
  public router: Router;
  public activatedRoute: ActivatedRoute;
  public zone: NgZone;
  public mapService: MapService;
  public modalCtrl: ModalController;
  public actionSheetController: ActionSheetController;
  public viewCtrl: any; // temp fix
  public geoLocation: GeolocationsService;
  public firebaseService: FirebaseService;
  public alertService: AlertService;
  public alertCtrl: AlertController;

  constructor(injector: Injector) {
    this.platform = injector.get(Platform);
    this.usersService = injector.get(UserService);
    this.utility = injector.get(UtilityService);
    this.location = injector.get(Location);
    this.common = injector.get(CommonModule);
    this.events = injector.get(EventsService);
    this.nav = injector.get(NavService);
    this.formBuilder = injector.get(FormBuilder);
    this.popover = injector.get(PopoversService);
    this.modals = injector.get(ModalService);
    this.menuCtrl = injector.get(MenuController);
    this.router = injector.get(Router);
    this.activatedRoute = injector.get(ActivatedRoute);
    this.zone = injector.get(NgZone);
    this.mapService = injector.get(MapService);
    this.modalCtrl = injector.get(ModalController);
    this.actionSheetController = injector.get(ActionSheetController);
    this.geoLocation = injector.get(GeolocationsService);
    this.firebaseService = injector.get(FirebaseService);
    this.alertService = injector.get(AlertService);
    this.alertCtrl = injector.get(AlertController);
  }

  getParams() {
    return this.activatedRoute.snapshot.params;
  }

  getQueryParams() {
    return this.activatedRoute.snapshot.queryParams;
  }

  popToRoot() {
    this.nav.navigateTo('pages/home');
  }

  ToggleMenuBar() {
    this.menuCtrl.open();
  }
}
