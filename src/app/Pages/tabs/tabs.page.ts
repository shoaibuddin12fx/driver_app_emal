import { Component, Injector, OnInit, ViewChild } from '@angular/core';
import { IonTabs } from '@ionic/angular';
import { BasePage } from '../base-page/base-page';

@Component({
  selector: 'app-tabs',
  templateUrl: './tabs.page.html',
  styleUrls: ['./tabs.page.scss'],
})
export class TabsPage extends BasePage implements OnInit {
  @ViewChild('tabs', { static: false }) tabs: IonTabs;
  static selectedTab;

  constructor(injector: Injector) {
    super(injector);
  }

  ngOnInit() {
    this.menuCtrl.enable(true, 'drawer');
  }

  setCurrentTab() {
    TabsPage.selectedTab = this.tabs.getSelected();
  }
}
