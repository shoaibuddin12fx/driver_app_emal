import { Component, Injector, OnInit } from '@angular/core';
import { BasePage } from '../../base-page/base-page';
import { Observable } from 'rxjs/Observable';
import { GeoFire } from 'geofire';
import { TabsPage } from '../tabs.page';
import { ViewWillEnter } from '@ionic/angular';

@Component({
  selector: 'app-schedule',
  templateUrl: './schedule.page.html',
  styleUrls: ['./schedule.page.scss'],
})
export class SchedulePage extends BasePage implements OnInit, ViewWillEnter {
  biddingLimit: any = {
    lowerLimit: 0,
    biddingPrice: 0,
    bidText: '',
  };
  changeBidcolor: boolean = false;
  accptCustomar: boolean = true;
  curUser: any;
  userProfile: any;
  Profile: any;
  driverlat: any;
  driverlng: any;
  bookingData: any;
  driver_avg_rating = 0;
  changeBidCalled: boolean = false;
  private data: Observable<Array<string>>;

  jobs = [];

  msg: any;
  static needsVerificationCheck = false;

  constructor(injector: Injector) {
    super(injector);
    console.log('SchedulePage -> constructor');
    // this.usersService.presentLoading();
  }

  ngOnInit() {
    this.events.subscribe('NOTIFICATION_RECEIVED', () => {
      this.getJobs();
    });

    this.events.subscribe('BID_ACCEPTED', () => {
      this.nav.push('pages/tabs/current-bookings');
    });
  }

  async initialize() {
    let self = this;
    let user = await this.firebaseService.getCurrentUser();
    this.curUser = user;
    let ref = this.firebaseService.getDbRef('/users/' + user.uid);
    ref.once('value', (snapshot: any) => {
      console.log(snapshot.val(), 'DRIVER_DATA');

      this.userProfile = snapshot.val();
      if (
        this.userProfile != undefined &&
        this.userProfile.hasOwnProperty('profile_image') &&
        this.userProfile.profile_image != null
      ) {
        this.userProfile.profile_image = this.userProfile.profile_image;
      } else {
        this.userProfile.profile_image = 'assets/images/notfound.png';
      }

      if (this.userProfile.hasOwnProperty('ratings')) {
        let key;
        let ratingLen = 0;

        for (key in this.userProfile.ratings) {
          this.driver_avg_rating += parseFloat(this.userProfile.ratings[key]);
          ratingLen += 1;
        }
        this.driver_avg_rating = this.driver_avg_rating / ratingLen;
        console.log(this.driver_avg_rating, ratingLen);
      }
      console.log(this.userProfile);
      //self.usersService.dismissLoader();
    });
  }

  ionViewWillEnter() {
    this.initialize();
    console.log('ionViewWillEnter -> schedule');
    setTimeout(() => {
      this.getJobs();
    }, 200);

    // if (SchedulePage.needsVerificationCheck) this.initialize();
  }

  getDate(date): Date {
    const d = new Date(date);
    const myDate = new Date(
      d.getTime() - d.getTimezoneOffset() * 60000
    ).toISOString();
    var _date = new Date(myDate);
    console.log('getDate', _date);

    return _date;
  }

  getJobs() {
    console.log('ionviewWillEnter');
    this.jobs = [];
    let self = this;

    this.usersService.presentLoading();

    let ref = this.firebaseService.getDbRef('bookings');
    ref.on('value', (snapshot) => {
      console.log('Bookings Received');
      //   this.jobs = [];
      let allBookings = snapshot.val();
      this.geoLocation
        .getCurrentLocationCoordinates()
        .then((position) => {
          console.log('Position ->', position);
          this.jobs = [];
          var geoFire = new GeoFire(this.firebaseService.getDbRef('geofire'));
          var geoQuery = geoFire.query({
            center: [position['lat'], position['lng']],
            radius: 50,
          });

          geoQuery.on('key_entered', (key, location, distance) => {
            console.log('key_entered', key, location, distance);
            //console.log(allBookings[key])
            //  if (TabsPage.selectedTab === 'schedule') {
            console.log('onSchedule');
            try {
              this.zone.run(() => {
                allBookings[key].uid = key;
                this.jobs.push(allBookings[key]);
                let job = allBookings[key];
                let dt = new Date(job.tripdate);
                let jt = this.getDate(dt).getTime();

                console.log('JT_T', jt);

                // let jt = new Date(
                //   dt.getFullYear(),
                //   dt.getMonth(),
                //   dt.getDate(),
                //   dt.getHours(),
                //   dt.getMinutes() + 5,
                //   dt.getSeconds()
                // );
                //dt.getTime(); //  trip date + 5 minutes
                let now = new Date().getTime();
                if (jt > now) {
                  console.log('NEWJOB ARRIVED');
                  console.log('DATE_TIME', dt);
                  job.isNew = true;
                  // setTimeout(() => {
                  //   self.getJobs();
                  // }, _time - now);
                }

                if (allBookings[key].status === 'Finding your driver') {
                  //  if(allBookings[key].drop == undefined || allBookings[key].drop){    // new line added by me part 2 .....
                  // this.zone.run(() => {
                  job['changeBidcolor'] = false;
                  job['currentBid'] = job.estimate;
                  job['lowerLimit'] = (job['currentBid'] / 2).toFixed(2);
                  // this.jobs.push(allBookings[key]);
                  // this.jobs.sort(function (a, b) {
                  //     return b.tripdate - a.tripdate;
                  // });
                  // });
                  // }
                } else if (job.status === 'Bidding Start') {
                  // this.zone.run(() => {
                  //     job.uid = key;
                  // });
                  if (job.hasOwnProperty('bidding')) {
                    for (let i = 0; i < job.bidding.length; i++) {
                      if (job.bidding[i].driver == this.curUser.uid) {
                        job['isBidding'] = true; //current user already bid for this ride
                        job['yourBid'] = job.bidding[i].rate;
                        job.isNew = false;
                      }
                      if (job.bidding.length > 1) {
                        let bidLength = job.bidding.length;
                        let amt = job.bidding[bidLength - 1].rate;
                        job['changeBidcolor'] = true;
                        job['currentBid'] = parseFloat(amt);
                      } else {
                        job['changeBidcolor'] = false;
                        job['currentBid'] = parseFloat(job.estimate);
                      }
                      job['lowerLimit'] = (job['currentBid'] / 2).toFixed(2);
                    }
                  }
                }
                this.jobs.sort(function (a, b) {
                  console.log('hehdew');
                  let aa: any = Number(new Date(a.tripdate));
                  let bb: any = Number(new Date(b.tripdate));
                  console.log(bb - aa);
                  return bb - aa;
                });
              });
            } catch (e) {
              console.log('Undefined Error handled by Catch');
            }
            //   }
            this.usersService.dismissLoader();
          });

          geoQuery.on('key_exited', (key, location, distance) => {
            this.zone.run(() => {
              console.log('key_exited', key);
              for (let i = 0; i < this.jobs.length; i++) {
                if (this.jobs[i].uid == key) {
                  this.jobs.slice(i, 1);
                }
              }
            });
          });
        })
        .catch((error) => {
          console.log('Error getting location', error);
        });
    });
  }

  // setJobs(data){
  //   var promise = new Promise((resolve, reject) => {
  //     if(err){}
  //
  //   });
  // }

  //alert
  presentAlert(message) {
    this.alertService.Alert('ALERT', message);
  }

  promo: any;
  corporate_booking_name: any;
  customer_email: any;
  customer_image: any;
  booking_type: any;
  customer_name: any;

  okSchedule(i) {
    this.changeBidCalled = false;
    console.log(this.jobs[i]);
    this.usersService.presentLoading();
    if (this.jobs[i].promo_code) {
      this.promo = this.jobs[i].promo_code;
    } else {
      this.promo = null;
    }

    if (this.jobs[i].corporate_booking_name) {
      this.corporate_booking_name = this.jobs[i].corporate_booking_name;
    } else {
      this.corporate_booking_name = null;
    }

    if (this.jobs[i].customer_email) {
      this.customer_email = this.jobs[i].customer_email;
    } else {
      this.customer_email = null;
    }

    if (this.jobs[i].customer_image) {
      this.customer_image = this.jobs[i].customer_image ?? '';
    } else {
      this.customer_image = '';
    }

    if (this.jobs[i].booking_type) {
      this.booking_type = this.jobs[i].booking_type;
    } else {
      this.booking_type = null;
    }

    if (this.jobs[i].customer_name) {
      this.customer_name = this.jobs[i].customer_name;
    } else {
      this.customer_name = null;
    }

    var bidding = [];
    console.log();

    var key = this.jobs[i].uid;
    var ref = this.firebaseService.getDbRef('bookings/' + key);
    ref.on('value', (snapshot) => {
      if (
        snapshot.val().hasOwnProperty('bidding') &&
        snapshot.val().bidding.length > 0
      ) {
        for (var j = 0; j < snapshot.val().bidding.length; j++) {
          if (snapshot.val().bidding[j].driver !== this.curUser.uid) {
            bidding.push(snapshot.val().bidding[j]);
          }
        }
        bidding.push({
          rate: this.jobs[i].estimate,
          driver: this.curUser.uid,
          driver_name: this.userProfile.fullname,
          driver_image: this.userProfile.profile_image ?? '',
          status: 'Bidding Start',
          driver_rating: this.driver_avg_rating,
          driver_push_token: this.userProfile.pushToken ?? '',
          job_id: key,
        });
      } else {
        bidding = [
          {
            rate: this.jobs[i].estimate,
            driver: this.curUser.uid,
            driver_name: this.userProfile.fullname,
            driver_image: this.userProfile.profile_image ?? '',
            status: 'Bidding Start',
            driver_rating: this.driver_avg_rating,
            driver_push_token: this.userProfile.pushToken ?? '',
            job_id: key,
          },
        ];
      }
    });
    this.bookingData = {
      customer: this.jobs[i].customer,
      customer_name: this.jobs[i].customer_name,
      customer_image: this.jobs[i].customer_image ?? '',
      pickup: this.jobs[i].pickup,
      // drop: this.jobs[i].drop,
      tripdate: this.jobs[i].tripdate,
      status: 'Bidding Start',
      driver: this.curUser.uid,
      driver_name: this.userProfile.fullname,
      driver_image: this.userProfile.profile_image ?? '',
      //   distance: this.jobs[i].distance,
      triptime: this.jobs[i].triptime,
      transmission: this.jobs[i].transmission,
      estimate: this.jobs[i].estimate,
      // tripType: this.jobs[i].tripType,
      serviceType: this.jobs[i].serviceType,
      customer_push_token: this.jobs[i].customer_push_token ?? null,
      // promo_code:this.jobs[i].promo_code
      promo_code: this.promo,
      bidding: bidding,
    };
    if (this.jobs[i].drop) {
      this.bookingData['drop'] = this.jobs[i].drop;
      this.bookingData['distance'] = this.jobs[i].distance;

      ref.update(this.bookingData);
      let ref2 = this.firebaseService.getDbRef('bookings/' + key + '/driver');
      ref2.on('value', (snapshot) => {
        if (snapshot.val() == this.curUser.uid) {
          var customerData = {
            customer_email: this.customer_email,
            corporate_booking_name: this.corporate_booking_name,
            customer_image: this.customer_image ?? '',
            booking_type: this.booking_type,
            customer_name: this.customer_name,

            customer: this.bookingData.customer,
            driver: this.curUser.uid,
            driver_name: this.userProfile.fullname,
            driver_image: this.userProfile.profile_image ?? '',
            status: 'Bidding Start',
            pickupAddress: this.bookingData.pickup.add,
            dropAddress: this.bookingData.drop.add,
            tripdate: this.bookingData.tripdate,
            serviceType: this.bookingData.serviceType,
            bidding: bidding,
          };

          var updates = {};
          // updates['/users/' + this.curUser.uid + '/bookings/' + key] = driverData;
          updates['/users/' + this.bookingData.customer + '/bookings/' + key] =
            customerData;
          console.log(key);
          this.firebaseService.getDatabase().ref().update(updates);

          //  firebase.database().ref("geofire").child(key).remove();
          //this.usersService.sendPush('Driver Assigned to Job',"Your driver will arrive soon.",this.bookingData.customer_push_token).subscribe(response=>console.log(response));
          this.usersService
            .sendPush(
              'Driver bid your trip',
              'Driver ' + this.userProfile.fullname + ' bid your trip.',
              this.bookingData.customer_push_token
            )
            .subscribe((response) => console.log(response));
          this.usersService.dismissLoader();
          this.nav.setRoot('tabs');
        } else {
          this.usersService.dismissLoader();
          console.log('Error');
          console.log(snapshot.val());
          this.firebaseService
            .getDbRef('/users/' + this.curUser.uid + '/bookings/' + key)
            .remove();
          this.nav.setRoot('tabs');
        }
      });
    } else {
      ref.update(this.bookingData);
      let ref2 = this.firebaseService.getDbRef('bookings/' + key + '/driver');
      ref2.on('value', (snapshot) => {
        console.log(snapshot.val());
        if (snapshot.val() == this.curUser.uid) {
          var customerData = {
            customer_email: this.customer_email,
            corporate_booking_name: this.corporate_booking_name,
            customer_image: this.customer_image ?? '',
            booking_type: this.booking_type,
            customer_name: this.customer_name,

            customer: this.bookingData.customer,
            driver: this.curUser.uid,
            driver_name: this.userProfile.fullname,
            driver_image: this.userProfile.profile_image ?? '',
            status: 'Bidding Start',
            pickupAddress: this.bookingData.pickup.add,
            //dropAddress: this.bookingData.drop.add,
            tripdate: this.bookingData.tripdate,
            serviceType: this.bookingData.serviceType,
            bidding: bidding,
          };

          var updates = {};
          // updates['/users/' + this.curUser.uid + '/bookings/' + key] = driverData;
          updates['/users/' + this.bookingData.customer + '/bookings/' + key] =
            customerData;

          this.firebaseService.getDatabase().ref().update(updates);

          this.usersService
            .sendPush(
              'Driver is bid your trip',
              'Driver ' + this.userProfile.fullname + ' bid your trip.',
              this.bookingData.customer_push_token
            )
            .subscribe((response) => console.log(response));
          this.usersService.dismissLoader();
          this.nav.setRoot('tabs');
        } else {
          this.usersService.dismissLoader();
          console.log('Error');
          console.log(snapshot.val());
          this.firebaseService
            .getDbRef('/users/' + this.curUser.uid + '/bookings/' + key)
            .remove();
          this.nav.setRoot('tabs');
        }
      });
    }
  }

  okCancle() {
    //this.navCtrl.setRoot(MapPage);
  }

  bidAmount(job) {
    job['driver_rating'] = this.driver_avg_rating;
    this.nav.push('bid-price', {
      job: job,
      currentuser: this.curUser,
      userprofile: this.userProfile,
    });
  }

  setMyClasses(job) {
    let className = '';
    if (job.changeBidcolor) {
      className = 'bid-color';
    }
    console.log('classes', job.changeBidcolor, className);
    return className;
  }

  changeBid(action, job) {
    //this.changeBidCalled=true;
    delete job.yourBid;

    if (action == 'inc') {
      job.estimate += 1;
    } else if (action == 'dec' && job.estimate > job.lowerLimit) {
      job.estimate -= 1;
    }
    console.log(this.biddingLimit.biddingPrice);
  }

  ImgError(source) {
    console.log('ImgError, '), source;

    source.src = 'assets/images/notfound.png';
    source.onerror = '';
    return true;
  }
}
