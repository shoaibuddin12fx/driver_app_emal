import { Component, Injector, OnInit } from '@angular/core';
import { BasePage } from '../../base-page/base-page';

@Component({
  selector: 'app-bookings',
  templateUrl: './bookings.page.html',
  styleUrls: ['./bookings.page.scss'],
})
export class BookingsPage extends BasePage implements OnInit {

  curUser: any;
  userProfile: any;
  bookingList = [];

  constructor(injector: Injector) {
    super(injector)
  }

  ngOnInit() { }

  ionViewDidLoad() {
    console.log('ionViewDidLoad BookingsPage');
  }

  async ionViewWillEnter() {
    //ngOnInit(){
    this.curUser = await this.firebaseService.getCurrentUser();
    let ref = this.firebaseService.getDbRef('/users/' + this.curUser.uid);

    ref.on('value', (snapshot: any) => {
      if (snapshot.val() != null) {

        this.bookingList = [];
        this.zone.run(() => {

          this.userProfile = snapshot.val();
          for (let key in this.userProfile.bookings) {

            let bs = this.userProfile.bookings[key].status;
            this.userProfile.bookings[key].uid = key;
            if (bs == "ENDED" || bs == "CANCELLED") {
              this.userProfile.bookings[key].image_url = this.userProfile.bookings[key].customer_image;
              this.bookingList.push(this.userProfile.bookings[key]);
            }
          }

          this.bookingList.sort(function (a, b) {
            console.log("hehdew");
            let aa: any = Number(new Date(a.tripdate));
            let bb: any = Number(new Date(b.tripdate));
            console.log(bb - aa)
            return bb - aa;
          });
          console.log(this.bookingList)

        });
      }
    });

  }


  openPreviousBooking(index) {
    console.log(this.bookingList[index]);
    this.nav.push('pages/job-complete', {
      bookingKey: this.bookingList[index].uid
    })
  }

}
