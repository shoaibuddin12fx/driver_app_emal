import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { CurrentBookingsPage } from './current-bookings.page';

const routes: Routes = [
  {
    path: '',
    component: CurrentBookingsPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class CurrentBookingsPageRoutingModule {}
