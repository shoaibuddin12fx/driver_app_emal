import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { CurrentBookingsPageRoutingModule } from './current-bookings-routing.module';

import { CurrentBookingsPage } from './current-bookings.page';
import { HeaderModule } from 'src/app/Components/header/header.module';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    CurrentBookingsPageRoutingModule,
    HeaderModule,
  ],
  declarations: [CurrentBookingsPage],
})
export class CurrentBookingsPageModule {}
