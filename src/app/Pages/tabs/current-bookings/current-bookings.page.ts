import { Component, Injector, OnInit } from '@angular/core';
import { BasePage } from '../../base-page/base-page';

@Component({
  selector: 'app-current-bookings',
  templateUrl: './current-bookings.page.html',
  styleUrls: ['./current-bookings.page.scss'],
})
export class CurrentBookingsPage extends BasePage implements OnInit {

  curUser: any;
  userProfile: any;
  bookingList = [];

  constructor(injector: Injector) {
    super(injector)
  }

  ngOnInit() {

  }
  async ionViewWillEnter() {
    this.curUser = await this.firebaseService.getCurrentUser();
    let ref = this.firebaseService.getDbRef('/users/' + this.curUser.uid);

    ref.on('value', (snapshot: any) => {

      if (snapshot.val() != null) {
        console.log(snapshot.val());
        console.log("userId values");
        this.bookingList = [];
        this.zone.run(() => {
          this.userProfile = snapshot.val();
          for (let key in this.userProfile.bookings) {

            this.userProfile.bookings[key].uid = key;

            if (this.userProfile.bookings[key].status != "ENDED" && this.userProfile.bookings[key].status != "CANCELLED" && this.userProfile.bookings[key].driver == this.curUser.uid) {
              this.userProfile.bookings[key].image_url = this.userProfile.bookings[key].customer_image;
              this.bookingList.push(this.userProfile.bookings[key]);
              console.log(this.bookingList);
            }
          }
          this.bookingList.sort(function (a, b) {
            console.log("hehdew");
            let aa: any = Number(new Date(a.tripdate));
            let bb: any = Number(new Date(b.tripdate));
            console.log(bb - aa)
            return bb - aa;
          });
        });
      }

    });

  }

  openActiveBooking(position) {
    console.log(this.bookingList[position])
    if (this.bookingList[position].status == "REVIEW") {


      this.nav.push('pages/job-complete', {
        bookingKey: this.bookingList[position].uid
      });
    } else {
      this.nav.push('pages/active-booking', {
        bookingKey: this.bookingList[position].uid
      });
    }
  }

}
