import { Component, Injector, OnInit } from '@angular/core';
import { BasePage } from '../base-page/base-page';
import { SocialSharing } from '@ionic-native/social-sharing/ngx';

@Component({
  selector: 'app-share',
  templateUrl: './share.page.html',
  styleUrls: ['./share.page.scss'],
})
export class SharePage extends BasePage implements OnInit {

  constructor(injector: Injector, private socialSharing: SocialSharing) {
    super(injector)
  }

  ngOnInit() {

  }

  //facebook share
  shareWithFacebook() {
    this.socialSharing.shareViaFacebook("message", "image", "www.PickMyRide.com").then((success) => {
      console.log("ok: " + success);
    }).catch((error) => {
      console.log(error);
    });
  }
  //twitter share
  shareWithTwitter() {
    this.socialSharing.shareViaTwitter("message", "image", "www.PickMyRide.com").then((success) => {
      console.log("ok: " + success);
    }).catch((error) => {
      console.log(error);
    });
  }
  //massage share
  shareWithMassage() {
    this.socialSharing.shareViaSMS("messge", "9038505480").then((success) => {
      console.log("ok: " + success);
    }).catch((error) => {
      console.log(error);
    });
  }

  data: any;
  shareWithEmail() {
    this.data = 'abc@email.com';

    this.socialSharing.shareViaEmail('Body', 'Subject', this.data).then((success) => {
      console.log("ok: " + success);
    }).catch((error) => {
      console.log(error);
    });

  }

}
