import { Component, Injector, OnInit } from '@angular/core';
import { BasePage } from '../base-page/base-page';

@Component({
  selector: 'app-bid-price',
  templateUrl: './bid-price.page.html',
  styleUrls: ['./bid-price.page.scss'],
})
export class BidPricePage extends BasePage implements OnInit {
  curUser: any;
  userProfile: any;
  bookingData: any;
  selected: any = {
    pickup: '',
    drop: '',
    pickupservice: false,
  };
  jobs;
  biddingLimit: any = {
    lowerLimit: 0,
    biddingPrice: 0,
    bidText: '',
  };
  changeBidcolor: boolean = false;
  promo: any;
  corporate_booking_name: any;
  customer_email: any;
  customer_image: any;
  booking_type: any;
  customer_name: any;

  constructor(injector: Injector) {
    super(injector);
    let params = this.getQueryParams();
    //console.log(navParams.get('job'));
    this.jobs = params?.job;
    // this.curUser = navParams.get('currentuser');
    this.userProfile = params?.userprofile;
    if (
      this.userProfile != undefined &&
      this.userProfile.hasOwnProperty('profile_image') &&
      this.userProfile.profile_image != undefined
    ) {
      this.userProfile.profile_image = this.userProfile.profile_image;
    } else {
      this.userProfile.profile_image = 'assets/images/notfound.png';
    }
  }

  async ngOnInit() {
    this.curUser = await this.firebaseService.getCurrentUser();
    let ref = this.getDbRef('/users/' + this.curUser.uid);
    let self = this;

    ref
      .once('value', (snapshot: any) => {
        if (snapshot.val().phone) {
          if (this.curUser.emailVerified || snapshot.val().authToken) {
            console.log(snapshot.val().active);
            if (snapshot.val().active) {
              this.userProfile = snapshot.val();
              console.log(this.userProfile, this.curUser);
            } else {
              this.firebaseService.signOut();
              this.nav.pop();
              var msg =
                'Please Contact Ridebidder Authority to activate your account';
              //this.presentAlert(this.msg);
              // alert("Please Contact PickMyRide Authority to activate your account");
              this.alertService.Alert('ALERT', msg);
            }
          } else {
            this.curUser.sendEmailVerification().then(
              function () {
                self.firebaseService.signOut();

                var msg =
                  'Email has been sent. Please verify your email first.';
                // this.presentAlert(msg);
                // alert("Email has been sent. Please verify your email first.");
                this.alertService.Alert('ALERT', msg);
              },
              (error) => {
                console.log(error);
              }
            );
          }
        } else {
          // this.navCtrl.setRoot(RegistercustomerPage);
        }
        this.usersService.dismissLoader();
      })
      .catch(() => {
        this.usersService.dismissLoader();
      });
  }

  setMyClasses() {
    let className = '';
    if (this.changeBidcolor) {
      className = 'bid-color';
    }
    return className;
  }

  ionViewDidLoad() {
    console.log(this.jobs);
    this.selected.pickup = this.jobs.pickup.add;

    if (this.jobs.hasOwnProperty('bidding') && this.jobs.bidding.length > 1) {
      let bidLength = this.jobs.bidding.length;
      let amt = this.jobs.bidding[bidLength - 1].rate;
      this.changeBidcolor = true;
      this.biddingLimit.biddingPrice = parseFloat(amt);
    } else {
      this.changeBidcolor = false;
      this.biddingLimit.biddingPrice = parseFloat(this.jobs.estimate);
    }
    this.biddingLimit.lowerLimit = (this.biddingLimit.biddingPrice / 2).toFixed(
      2
    );

    if (this.jobs.hasOwnProperty('yourBid')) {
      this.biddingLimit.bidText =
        'You had bidden $' + this.jobs.yourBid.toFixed(2) + ' for this ride.';
    } else {
      this.biddingLimit.bidText = 'Put your bid.';
    }
    if (this.jobs.hasOwnProperty('drop') && this.jobs.drop !== undefined) {
      this.selected.drop = this.jobs.drop.add;
    }

    this.selected.pickupservice = true;
  }
  changeBid(action) {
    if (action == 'inc') {
      this.biddingLimit.biddingPrice += 1;
    } else if (
      action == 'dec' &&
      this.biddingLimit.biddingPrice > this.biddingLimit.lowerLimit
    ) {
      this.biddingLimit.biddingPrice -= 1;
    }
    console.log(this.biddingLimit.biddingPrice);
  }

  presentAlert(message) {
    this.alertService.Alert('ALERT', message);
  }
  okSchedule() {
    if (this.jobs.promo_code) {
      this.promo = this.jobs.promo_code;
    } else {
      this.promo = null;
    }

    if (this.jobs.corporate_booking_name) {
      this.corporate_booking_name = this.jobs.corporate_booking_name;
    } else {
      this.corporate_booking_name = null;
    }

    if (this.jobs.customer_email) {
      this.customer_email = this.jobs.customer_email;
    } else {
      this.customer_email = null;
    }

    if (this.jobs.customer_image) {
      this.customer_image = this.jobs.customer_image;
    } else {
      this.customer_image = null;
    }

    if (this.jobs.booking_type) {
      this.booking_type = this.jobs.booking_type;
    } else {
      this.booking_type = null;
    }

    if (this.jobs.customer_name) {
      this.customer_name = this.jobs.customer_name;
    } else {
      this.customer_name = null;
    }
    var bidding = [];

    var key = this.jobs.uid;
    var ref = this.getDbRef('bookings/' + key);
    ref.on('value', (snapshot) => {
      if (
        snapshot.val().hasOwnProperty('bidding') &&
        snapshot.val().bidding.length > 0
      ) {
        //   bidding = snapshot.val().bidding;
        //delete the previous bid of cuurent user
        for (var j = 0; j < snapshot.val().bidding.length; j++) {
          if (snapshot.val().bidding[j].driver !== this.curUser.uid) {
            bidding.push(snapshot.val().bidding[j]);
          }
        }
        console.log(bidding, bidding.length);
        bidding.push({
          rate: this.biddingLimit.biddingPrice,
          driver: this.curUser.uid,
          driver_name: this.userProfile.fullname,
          driver_image: this.userProfile.profile_image,
          status: 'Bidding Start',
          driver_rating: this.jobs.driver_rating,
          driver_push_token: this.userProfile.pushToken,
          job_id: key,
        });
      } else {
        bidding = [
          {
            rate: this.biddingLimit.biddingPrice,
            driver: this.curUser.uid,
            driver_name: this.userProfile.fullname,
            driver_image: this.userProfile.profile_image,
            status: 'Bidding Start',
            driver_rating: this.jobs.driver_rating,
            driver_push_token: this.userProfile.pushToken,
            job_id: key,
          },
        ];
      }
    });

    if (this.jobs.drop) {
      this.bookingData = {
        customer: this.jobs.customer,
        customer_name: this.jobs.customer_name,
        customer_image: this.jobs.customer_image,
        pickup: this.jobs.pickup,
        drop: this.jobs.drop,
        tripdate: this.jobs.tripdate,
        status: 'Bidding Start',
        driver: this.curUser.uid,
        driver_name: this.userProfile.fullname,
        driver_image: this.userProfile.profile_image,
        distance: this.jobs.distance,
        triptime: this.jobs.triptime,
        transmission: this.jobs.transmission,
        estimate: this.jobs.estimate,
        serviceType: this.jobs.serviceType,
        customer_push_token: this.jobs.customer_push_token,
        promo_code: this.promo,
        bidding: bidding,
      };
      // console.log(this.bookingData);
      ref.update(this.bookingData);
      let ref2 = this.getDbRef('bookings/' + key + '/driver');
      ref2.on('value', (snapshot) => {
        if (snapshot.val() == this.curUser.uid) {
          //  console.log("Inside Update");
          //  console.log(snapshot.val());
          var customerData = {
            customer_email: this.customer_email,
            corporate_booking_name: this.corporate_booking_name,
            customer_image: this.customer_image,
            booking_type: this.booking_type,
            customer_name: this.customer_name,
            driver: this.curUser.uid,
            driver_name: this.userProfile.fullname,
            driver_image: this.userProfile.profile_image,
            status: 'Bidding Start',
            customer: this.bookingData.customer,

            pickupAddress: this.bookingData.pickup.add,
            dropAddress: this.bookingData.drop.add,
            tripdate: this.bookingData.tripdate,
            serviceType: this.bookingData.serviceType,
            bidding: bidding,
          };

          var updates = {};
          updates['/users/' + this.bookingData.customer + '/bookings/' + key] =
            customerData;

          this.firebaseService.getDatabase().ref().update(updates);
          //   console.log(this.bookingData.customer_push_token)
          //  firebase.database().ref("geofire").child(key).remove();
          this.usersService
            .sendPush(
              'Driver bid your trip',
              'Driver ' + this.userProfile.fullname + ' bid your trip.',
              this.bookingData.customer_push_token
            )
            .subscribe((response) => console.log(response));

          this.nav.setRoot('tabs');
        } else {
          console.log('Error');
          console.log(snapshot.val());
          this.getDbRef(
            '/users/' + this.curUser.uid + '/bookings/' + key
          ).remove();
          this.nav.pop();

          let msg = 'Cancelled by Customer or got assigned to other Driver';
          this.presentAlert(msg);
        }
      });
    } else {
      this.bookingData = {
        customer: this.jobs.customer,
        customer_name: this.jobs.customer_name,
        customer_image: this.jobs.customer_image,
        pickup: this.jobs.pickup,
        tripdate: this.jobs.tripdate,
        status: 'Bidding Start',
        driver: this.curUser.uid,
        driver_name: this.userProfile.fullname,
        driver_image: this.userProfile.profile_image,
        triptime: this.jobs.triptime,
        transmission: this.jobs.transmission,
        estimate: this.jobs.estimate,
        serviceType: this.jobs.serviceType,
        customer_push_token: this.jobs.customer_push_token,
        promo_code: this.promo,
        bidding: bidding,
      };

      ref.update(this.bookingData);
      console.log(key);
      let ref2 = this.getDbRef('bookings/' + key + '/driver');
      ref2.on('value', (snapshot) => {
        if (snapshot.val() == this.curUser.uid) {
          //  console.log("Inside Update");
          //  console.log(snapshot.val());
          var customerData = {
            customer_email: this.customer_email,
            corporate_booking_name: this.corporate_booking_name,
            customer_image: this.customer_image,
            booking_type: this.booking_type,
            customer_name: this.customer_name,
            driver: this.curUser.uid,
            driver_name: this.userProfile.fullname,
            driver_image: this.userProfile.profile_image,
            status: 'Bidding Start',
            customer: this.bookingData.customer,
            pickupAddress: this.bookingData.pickup.add,
            tripdate: this.bookingData.tripdate,
            serviceType: this.bookingData.serviceType,
            bidding: bidding,
          };

          var updates = {};
          updates['/users/' + this.bookingData.customer + '/bookings/' + key] =
            customerData;

          this.firebaseService.getDatabase().ref().update(updates);
          this.usersService
            .sendPush(
              'Driver bid your trip',
              'Driver ' + this.userProfile.fullname + ' bid your trip.',
              this.bookingData.customer_push_token
            )
            .subscribe((response) => console.log(response));

          this.nav.pop();
        } else {
          console.log('Error');
          console.log(snapshot.val());
          this.getDbRef(
            '/users/' + this.curUser.uid + '/bookings/' + key
          ).remove();
          this.nav.setRoot('tab');
          let msg = 'Cancelled by Customer or got assigned to other Driver';
          this.presentAlert(msg);
        }
      });
    }
  }

  getDbRef(ref) {
    return this.firebaseService.getDbRef(ref);
  }
}
