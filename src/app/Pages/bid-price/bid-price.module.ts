import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { BidPricePageRoutingModule } from './bid-price-routing.module';

import { BidPricePage } from './bid-price.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    BidPricePageRoutingModule
  ],
  declarations: [BidPricePage]
})
export class BidPricePageModule {}
