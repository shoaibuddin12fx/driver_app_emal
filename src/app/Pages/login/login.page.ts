import { Component, Injector, OnInit } from '@angular/core';
import { BasePage } from '../base-page/base-page';

@Component({
  selector: 'app-login',
  templateUrl: './login.page.html',
  styleUrls: ['./login.page.scss'],
})
export class LoginPage extends BasePage implements OnInit {
  email: any; //= "driver@test.com";
  password: any; //= "123456";

  testRadioOpen: boolean;
  testRadioResult;
  user: any;
  msg: any;

  constructor(injector: Injector) {
    super(injector);
  }

  ngOnInit() {
    this.menuCtrl.enable(false, 'drawer');
  }

  ionViewDidLoad() {
    console.log('Login Page');
  }

  async onSignInFaild() {
    this.alertService.Alert('Login', 'Email or Password is Invalid');
  }

  private detailsFailed() {
    this.alertService.Alert('Login', 'Please Enter Valid Details');
  }

  doLogin() {
    if (
      this.email == undefined ||
      this.password == undefined ||
      this.email == '' ||
      this.password == ''
    ) {
      this.detailsFailed();
    } else {
      this.usersService
        .loginUser(this.email, this.password)
        .then(() => {
          console.log('Log in Sucess');
          this.usersService.checkUser();
        })
        .catch((_error) => this.onSignInFaild());
    }
  }
  //end doLogin

  doRegistration() {
    this.nav.push('pages/register');
  }

  async resetPassword() {
    let alert = await this.alertCtrl.create({
      header: 'Forget password',
      inputs: [
        {
          type: 'text',
          placeholder: 'Please Enter Your Email',
        },
      ],
      buttons: [
        'Cancel',
        {
          text: 'OK',
          handler: (data) => {
            let passData = data[0];
            this.changepass(passData);
          },
        },
      ],
    });
    alert.present();
  }

  changepass(data) {
    let email = data;
    this.usersService
      .passReset(email)
      .then((success) =>
        alert(
          'An email to your account has been sent successfully!, If you do not find it please check your spam folder'
        )
      )
      .catch((_error) => alert(_error));
  }
  //forgot password end
}
