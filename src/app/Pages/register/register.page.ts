import { Component, Injector, OnInit } from '@angular/core';
import { Alert } from 'selenium-webdriver';
import { BasePage } from '../base-page/base-page';

@Component({
  selector: 'app-register',
  templateUrl: './register.page.html',
  styleUrls: ['./register.page.scss'],
})
export class RegisterPage extends BasePage implements OnInit {
  fullname: any;
  email: any;
  password: any;
  confirmPassword: any;
  phone: any;
  cityname: any;
  // vehicle:any;

  //provider:any;
  showInput: boolean;

  //params item
  item: any;

  curUser: any;

  constructor(injector: Injector) {
    super(injector);
  }

  async ngOnInit() {
    this.curUser = await this.firebaseService.getCurrentUser();
    if (this.curUser) {
      this.showInput = false;
      this.fullname = this.curUser.displayName;
      this.email = this.curUser.email;
    } else {
      this.showInput = true;
    }
  }

  public onSignUpSuccess() {
    console.log('Restration Succesfull');
  }

  public onSignUpFailed() {
    this.alertService.Alert('Registration', 'Failed');
  }

  public onFirebaseFailed(error) {
    this.alertService.Alert('Registration', error);
  }

  private wrongUserInfo() {
    this.alertService.Alert('Registration', 'Please Enter Vaild Details');
  }

  doRegistration() {
    if (!this.fullname) {
      this.utility.presentFailureToast('Please enter your good name');
    } else if (!this.email) {
      this.utility.presentFailureToast('Please enter email');
    } else if (!this.password) {
      this.utility.presentFailureToast('Please enter password');
    } else if (this.password.length < 6) {
      this.utility.presentFailureToast('Your Passwors is too short');
    } else if (!this.confirmPassword) {
      this.utility.presentFailureToast('Please enter confirm password');
    } else if (this.password != this.confirmPassword) {
      this.utility.presentFailureToast(
        'Password and confirm password do not match'
      );
    } else if (!this.phone || this.phone === '') {
      this.utility.presentFailureToast('Please fill the Phone Number field');
    } else if (!this.cityname) {
      this.utility.presentFailureToast('Please fill The Cityname field');
    } else if (!this.email) {
      this.utility.presentFailureToast('Please Enter A Valid email');
    } else {
      if (this.curUser) {
        if (
          this.fullname == undefined ||
          this.email == undefined ||
          this.phone == undefined ||
          this.cityname == undefined ||
          this.fullname == '' ||
          this.email == '' ||
          this.phone == '' ||
          this.cityname == ''
        ) {
          this.wrongUserInfo();
        } else {
          this.usersService
            .insertUser(
              this.fullname,
              this.email,
              this.phone,
              this.cityname,
              this.curUser
            )
            .then(() => this.nav.setRoot('pages/login'))
            .catch((_error) => this.onSignUpFailed());
        }
      } else {
        if (
          this.fullname == undefined ||
          this.email == undefined ||
          this.password == undefined ||
          this.confirmPassword == undefined ||
          this.phone == undefined ||
          this.cityname == undefined ||
          this.fullname == '' ||
          this.email == '' ||
          this.password == '' ||
          this.confirmPassword == '' ||
          this.phone == '' ||
          this.cityname == ''
        ) {
          this.wrongUserInfo();
        } else {
          if (this.password == this.confirmPassword) {
            this.usersService
              .signUpUser(
                this.fullname,
                this.email,
                this.password,
                this.phone,
                this.cityname
              )
              .then(() => this.nav.setRoot('pages/login'))
              .catch((_error) => this.onFirebaseFailed(_error.toString()));
          } else {
            this.alertService.Alert('Registration', 'Password do not match');
          }
        }
      }
    }
  }

  goToSignin() {
    this.nav.setRoot('pages/login');
  }
}
