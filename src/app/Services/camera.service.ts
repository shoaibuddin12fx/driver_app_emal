import { Injectable } from '@angular/core';
import { Camera, CameraResultType, CameraSource, ImageOptions, Photo } from '@capacitor/camera';
import { ActionSheetController } from '@ionic/angular';
@Injectable({
  providedIn: 'root',
})
export class CameraService {
  constructor(
    private actionSheetController: ActionSheetController
  ) { }

  async selectImage() {
    return new Promise<string>(async res => {

      const actionSheet = await this.actionSheetController.create({
        // header: "Select Image source",
        cssClass: 'select-image-css',
        buttons: [{
          // text: 'Choose photo from Gallery',
          text: "Choose from gallery",
          icon: 'images',
          cssClass: 'images-icon',
          handler: async () => {
            res(await this.pickImage(CameraSource.Photos));
          }
        },
        {
          // text: 'Take photo',
          text: "Take photo",
          icon: 'camera',
          cssClass: 'camera-icon',
          handler: async () => {
            res(await this.pickImage(CameraSource.Camera));
          }
        },
        {
          // text: 'Cancel',
          text: "Cancel",
          role: 'cancel',
          icon: 'close',
          cssClass: 'close-icon',
          handler: () => {
            res(null)
          }
        }
        ]
      });
      await actionSheet.present();
    })

  }

  pickImage(source: CameraSource) {
    return new Promise<string>(res => {
      const options: ImageOptions = {
        height: 150,
        width: 150,
        allowEditing: true,
        quality: 50,
        resultType: CameraResultType.DataUrl,
        source: source,
      }
      Camera.getPhoto(options).then((imageData) => {
        let base64Image = imageData.dataUrl;
        res(base64Image);
      }, (err) => {
        res(null);
        // Handle error
      });
    });
  }
}
