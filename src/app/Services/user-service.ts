import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import 'rxjs/add/operator/map';
import { Observable } from 'rxjs/Rx';

import { FirebaseService } from './firebase.service';
import { AngularFireAuth } from '@angular/fire/compat/auth';
import { LoadingController } from '@ionic/angular';
import { AlertService } from './alert.service';
import { NavService } from './nav.service';
import { EventsService } from './events.service';

declare var google: any;

@Injectable()
export class UserService {
  public fireAuth: AngularFireAuth;
  public userProfile: any;
  public reviewApp: any;

  public loader: HTMLIonLoadingElement;

  constructor(
    public http: HttpClient,
    public firebaseService: FirebaseService,
    public loadingCtrl: LoadingController,
    public alertService: AlertService,
    public nav: NavService,
    public events: EventsService
  ) {
    this.fireAuth = firebaseService.fireAuth;
    this.userProfile = firebaseService.getDbRef('users');
    this.reviewApp = firebaseService.getDbRef('review');
    console.log('userProfile', this.userProfile);
  }

  async presentLoading() {
    if (this.loader) await this.loader.dismiss();
    this.loader = await this.loadingCtrl.create({
      message: 'Please wait...',
    });
    setTimeout(() => {
      this.loader.present();
    });
  }

  dismissLoader() {
    setTimeout(async () => {
      console.log('dismissLoader', await this.loader?.dismiss());
    }, 1000);
  }

  //SignUpUsers
  signUpUser(
    fullname: any,
    email: any,
    password: any,
    phone: any,
    cityname: any
  ) {
    return this.fireAuth
      .createUserWithEmailAndPassword(email, password)
      .then((newUser) => {
        this.userProfile.child(newUser.user.uid).set({
          fullname: fullname,
          email: email,
          phone: phone,
          cityname: cityname,
          profile_image:
            'https://firebasestorage.googleapis.com/v0/b/pickmyride-9f86d.appspot.com/o/images%2Fdefault.png?alt=media&token=f9177f5d-4f9b-4d11-a668-db0674a44e32',
          active: false,
          driver: true,
          islogin: 'driver',
        });
      });
  }

  insertUser(
    fullname: any,
    email: any,
    phone: any,
    cityname: any,
    AuthUser: any
  ) {
    //new added by me
    /*   AuthUser.sendEmailVerification().then(function() {
      console.log("please check email");
    }, function(error) {
      // An error happened.
    });*/
    return this.userProfile.child(AuthUser.uid).update({
      profile_image: AuthUser.photoURL,
      fullname: fullname,
      email: email,
      phone: phone,
      cityname: cityname,
      active: false,
      driver: true,
    });
  }

  //LoginUser
  /*loginUser(email:any,password:any):any{
      return this.fireAuth.signInWithEmailAndPassword(email, password);
  }*/

  loginUser(email: any, password: any): any {
    //return this.fireAuth.signInWithEmailAndPassword(email, password);
    return this.fireAuth
      .signInWithEmailAndPassword(email, password)
      .then((authenticatedUser) => {
        this.userProfile.child(authenticatedUser.user.uid).update({
          driver: true,
          islogin: 'driver',
          driver_push_token: this.getToken(),
          pushToken: this.getToken(),
        });
      });
  }

  getToken() {
    return localStorage.getItem('FCM_TOKEN');
  }

  setToken(token) {
    return localStorage.setItem('FCM_TOKEN', token);
  }

  //facebook insert data on database
  signFacebook(success) {
    this.userProfile.child(success.uid).set({
      // displayname:success.displayName,
      //  provider:success.providerData[0].providerId,
      email: success.email,
    });
  }

  //Google insert Data on database
  signGoogle(success, provider) {
    this.userProfile.child(success.uid).set({
      //displayname:success.displayName
      //  provider:provider.provider,
      email: success.email,
    });
  }

  //logout
  logoutUser() {
    return this.fireAuth.signOut();
  }

  //update data from profile
  updateData(data, userId) {
    return this.userProfile.child(userId).update({
      fullname: data.fullname,
      //  lastname:data.lastname,
      email: data.email,
      phone: data.phone,
      cityname: data.cityname,
      registration: data.registration,
    });
  }

  //review app and mark favourite
  review(rate, favourite, userId) {
    return this.reviewApp.child(userId).set({
      rating: rate,
      favourite: favourite,
    });
  }

  //password reset + forgot password
  passReset(email) {
    return new Promise<boolean>((res, rej) => {
      this.fireAuth
        .sendPasswordResetEmail(email)
        .then(() => {
          console.log('passReset email -> ', email);
          res(true);
        })
        .catch((error) => {
          console.log(error);
          rej(error);
        });
    });
  }

  sendPush(title, msg, token): Observable<object> {
    let body = {
      to: token,
      notification: {
        body: msg,
        title: title,
      },
    };
    console.log('SendPush', body);

    let headers = new HttpHeaders({
      'Content-Type': 'application/json',
      Authorization:
        'key=AAAA3AtDWWI:APA91bG4NezdXvmdKnPBW-r97NKT59DsDQymxlJv68P4lbUaj709Pi-DhTtykKGtDDUUeG2jf5PK_0qjQN-1mUq2oMSTQI1FFUOmluNvDexzBzezAUiD8_0PTd3uUyf8fuu_TipVH0H3',
    });
    let options = { headers: headers };
    return this.http.post('https://fcm.googleapis.com/fcm/send', body, options);
    //  .map((res) => res.json())
    //  .catch((error:any) => Observable.throw(error.json().error || 'Server error'));
  }

  // chargePayment(card,amount): Observable<string> {
  //     console.log(card,amount);
  //     console.log("paypal my card and amount transfer..........................");
  //     let body = {
  //                 "cardID": card,
  //                 "amount": amount
  //               }
  //     let headers = new Headers({ 'Content-Type': 'application/json'});
  //     let options  = new RequestOptions({ headers: headers });
  //     return this.http.post("https://us-central1-pickmyride-7c5be.cloudfunctions.net/makePayment",body,options)
  //      .map((res:Response) => res.json())
  //      .catch((error:any) => Observable.throw(error.json().error || 'Server error'));
  // }

  stripePayment(card, cust_id, amount): Observable<object> {
    console.log('card..................', card);
    let body = {
      customer_id: cust_id,
      card_id: card,
      amount: amount,
      // "amount":0
    };
    let headers = new HttpHeaders({ 'Content-Type': 'application/json' });
    let options = { headers: headers };
    return this.http.post(
      'https://us-central1-bidrider-app.cloudfunctions.net/stripPayment',
      body,
      options
    );
    // .map((res:Response) => res.json())
    // .catch((error:any) => Observable.throw(error.json().error || 'Server error'));
  }

  async checkUser() {
    let _isVerifiedActive = await this.isVerifiedActive();
    if (_isVerifiedActive) {
      this.events.publish('login_event');
      this.nav.setRoot('pages/tabs');
    }
  }

  async isVerifiedActive() {
    let self = this;
    this.presentLoading();
    return new Promise(async (resolve) => {
      //TODO Remove this line
      resolve(true);
      return;
      let user = await this.firebaseService.getCurrentUser();
      let ref = this.firebaseService.getDbRef('/users/' + user.uid);
      ref.once('value', (snapshot: any) => {
        //log
        console.log(
          'emailVerified ',
          user.emailVerified,
          'authToken',
          snapshot.val().authToken
        );

        if (
          user.emailVerified === false &&
          (snapshot.val().authToken === null ||
            snapshot.val().authToken === undefined)
        ) {
          user.sendEmailVerification().then(
            function () {
              self.firebaseService.signOut();
              self.dismissLoader();
              var msg = 'Email has been sent. Please verify your email first.';
              self.alertService.Alert('ALERT', msg);
              console.log(msg);
              resolve(false);
            },
            function (error) {
              console.log(error);
            }
          );
        } else {
          if (!snapshot.val()?.active) {
            self.firebaseService.signOut();
            self.dismissLoader();
            var msg =
              'Please Contact Ridebidder Authority to activate your account';
            this.alertService.Alert('Inactive Account', msg);
            console.log(msg);
            resolve(false);
          } else resolve(true);
        }
      });

      // this.usersService.dismissLoader();
    }).catch((reason) => {
      console.log('SchedulePage -> constructor -> ref.once -> catch', reason);
      this.dismissLoader();
    });
  }
}
