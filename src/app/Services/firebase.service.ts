import { Injectable } from '@angular/core';
import { AngularFireAuth } from '@angular/fire/compat/auth';
import { AngularFireDatabase } from '@angular/fire/compat/database';
import { AngularFireStorage } from '@angular/fire/compat/storage';

@Injectable({
  providedIn: 'root',
})
export class FirebaseService {
  constructor(
    public fireAuth: AngularFireAuth,
    public firebase: AngularFireDatabase,
    public fireStorage: AngularFireStorage
  ) { }
  async getCurrentUser() {
    return await this.fireAuth.currentUser;
  }

  getDatabase() {
    return this.firebase.database;
  }

  getDbRef(ref) {
    return this.getDatabase().ref(ref);
  }

  async signOut() {
    await this.fireAuth.signOut();
  }
}
