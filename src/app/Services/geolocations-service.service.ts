import { Injectable } from '@angular/core';
import { Geolocation } from '@capacitor/geolocation';
import { FirebaseService } from './firebase.service';

declare var google;
@Injectable({
  providedIn: 'root',
})
export class GeolocationsService {
  constructor(private firebaseService: FirebaseService) {}

  getCoordsForGeoAddress(address, _default = true) {
    var self = this;
    return new Promise((resolve) => {
      var self = this;
      var geocoder = new google.maps.Geocoder();
      geocoder.geocode({ address: address }, function (results, status) {
        if (status === 'OK') {
          if (results[0]) {
            var loc = results[0].geometry.location;
            var lat = loc.lat();
            var lng = loc.lng();
            resolve({ lat: lat, lng: lng });
          } else {
            resolve(null);
          }
        } else {
          console.log({ results, status });
          resolve(null);
        }
      });
    });
  }

  getCoordsViaHTML5Navigator() {
    return new Promise((resolve) => {
      if (navigator.geolocation) {
        navigator.geolocation.getCurrentPosition(
          function (position) {
            var pos = {
              lat: position.coords.latitude,
              lng: position.coords.longitude,
            };
            resolve(pos);
          },
          function () {
            resolve({ lat: 51.5074, lng: 0.1278 });
          }
        );
      } else {
        // Browser doesn't support Geolocation
        resolve({ lat: 51.5074, lng: 0.1278 });
      }
    });
  }

  getCurrentLocationCoordinates() {
    return new Promise(async (resolve) => {
      let coords = await Geolocation.getCurrentPosition();

      var lt = coords.coords.latitude;
      var lg = coords.coords.longitude;

      resolve({ lat: lt, lng: lg });
    });
  }

  startTracking(uid) {
    // console.log('start tracking...');
    // var ref = this.firebaseService.getDbRef(
    //   '/users/' + uid + '/movementStatus'
    // );
    // ref.on('value', (snapshot) => {
    //   console.log('movementStatus -> onValue', snapshot.val());
    //   //*ToDO *//  if (snapshot.val())
    //   if (snapshot.val()) {
    //     Geolocation.watchPosition({ enableHighAccuracy: true }, (data) => {
    //       console.log('movementStatus -> watchPosition', data);
    //       this.firebaseService
    //         .getDbRef('/users/' + uid + '/location')
    //         .set({ lat: data.coords.latitude, lng: data.coords.longitude });
    //       if (snapshot.val() != 'LOCATE_ONLY') {
    //         this.firebaseService
    //           .getDbRef('/bookings/' + snapshot.val() + '/triplocations')
    //           .push({ lat: data.coords.latitude, lng: data.coords.longitude });
    //       }
    //     });
    //   } else {
    //     console.log('movementStatus -> , snapshot.val() is null or undefined');
    //   }
    // });
  }
}
