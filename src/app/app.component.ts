import { Component, Injector } from '@angular/core';
import { BasePage } from './Pages/base-page/base-page';
import { StatusBar } from '@ionic-native/status-bar/ngx';
import { SplashScreen } from '@ionic-native/splash-screen/ngx';
import { BackgroundMode } from '@ionic-native/background-mode/ngx';
import {
  ActionPerformed,
  PushNotificationSchema,
  PushNotifications,
  Token,
} from '@capacitor/push-notifications';
import { Plugins } from '@capacitor/core';
const { LocalNotifications } = Plugins;
// import { Firebase } from '@ionic-native/firebase/ngx';

@Component({
  selector: 'app-root',
  templateUrl: 'app.component.html',
  styleUrls: ['app.component.scss'],
})
export class AppComponent extends BasePage {
  user: any;
  ref: any;
  rootPage: any;
  public appPages = [
    {
      title: 'Home',
      url: '/pages/tabs',
      icon: '/assets/images/home-outline.svg',
    },
    {
      title: 'Profile',
      url: '/pages/profile',
      icon: '/assets/images/person-outline.svg',
    },
    {
      title: 'Statistics',
      url: '/pages/stats',
      icon: '/assets/images/stats-chart-outline.svg',
    },
    {
      title: 'About Us',
      url: '/pages/about',
      icon: '/assets/images/information-outline.svg',
    },
    { title: 'Logout', url: '', icon: '/assets/images/exit-outline.svg' },
  ];

  constructor(
    injector: Injector,
    private statusBar: StatusBar,
    private splashScreen: SplashScreen,
    private backgroundMode: BackgroundMode // private firebase: Firebase
  ) {
    super(injector);
    let self = this;
    this.events.subscribe('login_event', async () => {
      setTimeout(async () => {
        console.log('login_event RECEIVED');
        let user = await self.firebaseService.getCurrentUser();
        console.log('USER', user);

        if (!user) return;
        var ref = self.firebaseService.getDatabase().ref('/users/' + user.uid);
        ref.on('value', (_snapshot: any) => {
          if (_snapshot.val()) {
            var data = _snapshot.val();
            console.log('DB_USER', data);
            self.user = _snapshot.val();
          } else console.log('DB_USER NOT FOUND');
        });
      }, 100);

      // this.user = this.users.getLocalUser();
    });
    document.addEventListener(
      'backbutton',
      (event) => {
        event.preventDefault();
        event.stopPropagation();
        const url = this.router.url;
        console.log(url);
        this.createBackRoutingLogics(url);
      },
      false
    );
  }

  async createBackRoutingLogics(url) {
    if (
      url.includes('login') ||
      url.includes('signup') ||
      url.includes('tabs') ||
      url.includes('splash')
    ) {
      this.utility.hideLoader();

      const isModalOpen = await this.modals.modal.getTop();
      console.log(isModalOpen);
      if (isModalOpen) {
        this.modals.dismiss({ data: 'A' });
      } else {
        this.exitApp();
      }
    } else {
      // if(this.isModalOpen){
      // }
    }
  }

  exitApp() {
    navigator['app'].exitApp();
  }

  async ngOnInit() {
    this.firebaseInit();
    this.user = await this.firebaseService.getCurrentUser();
    console.log(this.user);
    console.log('setting menu ctrl disable');

    this.platform.ready().then(() => {
      this.statusBar.styleDefault();
      this.statusBar.overlaysWebView(false);
      this.splashScreen.hide();

      this.backgroundMode.enable();
      this.firebaseService.fireAuth.onAuthStateChanged((user) => {
        console.log('OnFireAuthChanged', user);
        if (user) {
          // this.firebase.grantPermission();
          console.log(user);

          //   console.log( firebase.database().ref('users/' + user.uid ))
          //  console.log(this.cordovaFirebase)
          // this.firebase
          //   .getToken()
          //   .then((token) => {
          //     console.log(token);
          //     this.getDbRef;
          //     this.firebaseService
          //       .getDatabase()
          //       .ref('users/' + user.uid + '/pushToken')
          //       .set(token);
          //   })
          //   .catch((error) => console.log(error));
          /*------------- Login Push Token ----------------------*/

          /*-------------- login push token ---------------------*/
          this.zone.run(() => {
            this.geoLocation.startTracking(user.uid);
          });
          this.ref = this.getDbRef('users/' + user.uid);
          this.ref.update({
            online: true,
          });
          this.ref.onDisconnect().update({
            online: false,
          });
          this.zone.run(() => {
            console.log('Page change requested');
            // this.nav.push('/pages/tabs');
            this.rootPage = 'tabs';
          });
        } else {
          this.zone.run(() => {
            this.rootPage = 'login';
            //this.rootPage = JobcompletePage;
          });
        }
      });
    });
    //  this.menuCtrl.enable(false, 'drawer')
    //this.routerOutlet.swipeGesture = false;
  }

  firebaseInit() {
    console.log('Initializing firebase');

    // Request permission to use push notifications
    // iOS will prompt user and return if they granted permission or not
    // Android will just grant without prompting
    PushNotifications.requestPermissions().then((result) => {
      if (result.receive === 'granted') {
        // Register with Apple / Google to receive push via APNS/FCM
        PushNotifications.register();
      } else {
        // Show some error
      }
    });

    // On success, we should be able to receive notifications
    PushNotifications.addListener('registration', (token: Token) => {
      console.log('FCM_TOKEN', token.value);
      this.usersService.setToken(token.value);
      //alert('Push registration success, token: ' + token.value);
    });

    // Some issue with our setup and push will not work
    PushNotifications.addListener('registrationError', (error: any) => {
      alert('Error on registration: ' + JSON.stringify(error));
    });

    // Show us the notification payload if the app is open on our device
    PushNotifications.addListener(
      'pushNotificationReceived',
      (notification: PushNotificationSchema) => {
        // alert('Push received: ' + JSON.stringify(notification));
        this.events.publish('NOTIFICATION_RECEIVED');
        if (notification.title.includes('Your bid accepted by'))
          this.events.publish('BID_ACCEPTED');

        this.showNotification(notification.title, notification.body);
        // this.utility.alerts.showAlert(notification.body, notification.title);
      }
    );

    // Method called when tapping on a notification
    PushNotifications.addListener(
      'pushNotificationActionPerformed',
      (notification: ActionPerformed) => {
        alert('Push action performed: ' + JSON.stringify(notification));
      }
    );
  }

  getDbRef(ref) {
    return this.firebaseService.getDbRef(ref);
  }

  navigate(path) {
    // this.nav.navigateTo(path)
  }

  handleClicked(title) {
    if (title === 'Logout') {
      console.log('onLogout');
      this.usersService.logoutUser();
    }
  }

  async showNotification(title, body) {
    const notifs = await LocalNotifications.schedule({
      notifications: [
        {
          title,
          body,
          id: 1,
          schedule: { at: new Date(Date.now() + 1000) },
          sound: 'default',
          attachments: null,
          actionTypeId: '',
          extra: null,
        },
      ],
    });
    console.log('scheduled notifications', notifs);
  }
}
