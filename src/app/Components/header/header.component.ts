import { Component, Injector, Input, OnInit } from '@angular/core';
import { BasePage } from 'src/app/Pages/base-page/base-page';

@Component({
  selector: 'header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.scss'],
})
export class HeaderComponent extends BasePage implements OnInit {
  @Input() title: string;

  constructor(injector: Injector) {
    super(injector);
  }

  ngOnInit() {}
}
