(self["webpackChunkDriverApp"] = self["webpackChunkDriverApp"] || []).push([["src_app_Pages_tabs_schedule_schedule_module_ts"],{

/***/ 58809:
/*!**********************************************************!*\
  !*** ./node_modules/geofire/dist/geofire/geofire.min.js ***!
  \**********************************************************/
/***/ (function(__unused_webpack_module, exports) {

!function(e,t){ true?t(exports):0}(this,function(e){"use strict";var t=(r.prototype.cancel=function(){void 0!==this._cancelCallback&&(this._cancelCallback(),this._cancelCallback=void 0)},r);function r(e){if(this._cancelCallback=e,"[object Function]"!==Object.prototype.toString.call(this._cancelCallback))throw new Error("callback must be a function")}var l=10,d="0123456789bcdefghjkmnpqrstuvwxyz",i=40007860,c=110574,h=5,a=22*h,n=6378137,o=.00669447819799,s=1e-12;function u(e){return Math.log(e)/Math.log(2)}function f(e){var t;if("string"!=typeof e?t="key must be a string":0===e.length?t="key cannot be the empty string":755<1+l+e.length?t="key is too long to be stored in Firebase":/[\[\].#$\/\u0000-\u001F\u007F]/.test(e)&&(t="key cannot contain any of the following characters: . # $ ] [ /"),void 0!==t)throw new Error("Invalid GeoFire key '"+e+"': "+t)}function _(e){var t,r,n;if(Array.isArray(e)?2!==e.length?t="expected array of length 2, got length "+e.length:(r=e[0],n=e[1],"number"!=typeof r||isNaN(r)?t="latitude must be a number":r<-90||90<r?t="latitude must be within the range [-90, 90]":"number"!=typeof n||isNaN(n)?t="longitude must be a number":(n<-180||180<n)&&(t="longitude must be within the range [-180, 180]")):t="location must be an array",void 0!==t)throw new Error("Invalid GeoFire location '"+e+"': "+t)}function y(e){var t;if("string"!=typeof e)t="geohash must be a string";else if(0===e.length)t="geohash cannot be the empty string";else for(var r=0,n=e;r<n.length;r++){var i=n[r];-1===d.indexOf(i)&&(t="geohash cannot contain '"+i+"'")}if(void 0!==t)throw new Error("Invalid GeoFire geohash '"+e+"': "+t)}function b(e){if("number"!=typeof e||isNaN(e))throw new Error("Error: degrees must be a number");return e*Math.PI/180}function p(e,t){if(void 0===t&&(t=l),_(e),void 0!==t){if("number"!=typeof t||isNaN(t))throw new Error("precision must be a number");if(t<=0)throw new Error("precision must be greater than 0");if(22<t)throw new Error("precision cannot be greater than 22");if(Math.round(t)!==t)throw new Error("precision must be an integer")}for(var r={min:-90,max:90},n={min:-180,max:180},i="",a=0,o=0,s=1;i.length<t;){var c=s?e[1]:e[0],h=s?n:r,u=(h.min+h.max)/2;u<c?(a=1+(a<<1),h.min=u):(a<<=1,h.max=u),s=!s,o<4?o++:(o=0,i+=d[a],a=0)}return i}function v(e,t){t=b(t),t=Math.cos(t)*n*Math.PI/180*(1/Math.sqrt(1-o*Math.sin(t)*Math.sin(t)));return t<s?0<e?360:0:Math.min(360,e/t)}function k(e,t){t=v(e,t);return 1e-6<Math.abs(t)?Math.max(1,u(360/t)):1}function g(e){if(e<=180&&-180<=e)return e;e+=180;return 0<e?e%360-180:180- -e%360}function m(e,t){var r=t/c,n=Math.min(90,e[0]+r),e=Math.max(-90,e[0]-r),r=2*Math.floor((r=t,Math.min(u(i/2/r),a))),n=2*Math.floor(k(t,n))-1,e=2*Math.floor(k(t,e))-1;return Math.min(r,n,e,a)}function w(e,t){_(e);var r,n,i,a=Math.max(1,m(e,t)),o=Math.ceil(a/h),s=(r=e,i=(n=t)/c,e=Math.min(90,r[0]+i),t=Math.max(-90,r[0]-i),i=v(n,e),n=v(n,t),n=Math.max(i,n),[[r[0],r[1]],[r[0],g(r[1]-n)],[r[0],g(r[1]+n)],[e,r[1]],[e,g(r[1]-n)],[e,g(r[1]+n)],[t,r[1]],[t,g(r[1]-n)],[t,g(r[1]+n)]]).map(function(e){return function(e,t){y(e);var r=Math.ceil(t/h);return e.length<r?[e,e+"~"]:(r=(e=e.substring(0,r)).substring(0,e.length-1),e=d.indexOf(e.charAt(e.length-1)),t-=r.length*h,31<(t=(e=e>>(t=h-t)<<t)+(1<<t))?[r+d[e],r+"~"]:[r+d[e],r+d[t]])}(p(e,o),a)});return s.filter(function(r,n){return!s.some(function(e,t){return t<n&&r[0]===e[0]&&r[1]===e[1]})})}function C(e,t){_(e),_(t);var r=b(t[0]-e[0]),n=b(t[1]-e[1]),n=Math.sin(r/2)*Math.sin(r/2)+Math.cos(b(e[0]))*Math.cos(b(t[0]))*Math.sin(n/2)*Math.sin(n/2);return 6371*(2*Math.atan2(Math.sqrt(n),Math.sqrt(1-n)))}function Q(e){if(e&&"l"in e&&Array.isArray(e.l)&&2===e.l.length)return e.l;throw new Error("Unexpected location object encountered: "+JSON.stringify(e))}function E(e){e="string"==typeof e.key||null===e.key?e.key:"function"==typeof e.key?e.key():e.name();return e}function G(e,t){if(void 0===t&&(t=!1),"object"!=typeof e)throw new Error("query criteria must be an object");if(void 0===e.center&&void 0===e.radius)throw new Error("radius and/or center must be specified");if(t&&(void 0===e.center||void 0===e.radius))throw new Error("query criteria for a new query must contain both a center and a radius");for(var r=0,n=Object.keys(e);r<n.length;r++){var i=n[r];if("center"!==i&&"radius"!==i)throw new Error("Unexpected attribute '"+i+"' found in query criteria")}if(void 0!==e.center&&_(e.center),void 0!==e.radius){if("number"!=typeof e.radius||isNaN(e.radius))throw new Error("radius must be a number");if(e.radius<0)throw new Error("radius must be greater than or equal to 0")}}var M=(T.prototype.cancel=function(){var r=this;this._cancelled=!0,this._callbacks={ready:[],key_entered:[],key_exited:[],key_moved:[]},Object.keys(this._currentGeohashesQueried).forEach(function(e){var t=r._stringToQuery(e);r._cancelGeohashQuery(t,r._currentGeohashesQueried[e]),delete r._currentGeohashesQueried[e]}),this._locationsTracked={},clearInterval(this._cleanUpCurrentGeohashesQueriedInterval)},T.prototype.center=function(){return this._center},T.prototype.on=function(e,r){var n=this;if(-1===["ready","key_entered","key_exited","key_moved"].indexOf(e))throw new Error("event type must be 'ready', 'key_entered', 'key_exited', or 'key_moved'");if("function"!=typeof r)throw new Error("callback must be a function");return this._callbacks[e].push(r),"key_entered"===e&&Object.keys(this._locationsTracked).forEach(function(e){var t=n._locationsTracked[e];void 0!==t&&t.isInQuery&&r(e,t.location,t.distanceFromCenter)}),"ready"===e&&this._valueEventFired&&r(),new t(function(){n._callbacks[e].splice(n._callbacks[e].indexOf(r),1)})},T.prototype.radius=function(){return this._radius},T.prototype.updateCriteria=function(e){G(e),this._center=e.center||this._center,this._radius=e.radius||this._radius;for(var t=0,r=Object.keys(this._locationsTracked);t<r.length;t++){var n=r[t];if(!0===this._cancelled)break;var i=this._locationsTracked[n],a=i.isInQuery;i.distanceFromCenter=C(i.location,this._center),i.isInQuery=i.distanceFromCenter<=this._radius,a&&!i.isInQuery?this._fireCallbacksForKey("key_exited",n,i.location,i.distanceFromCenter):!a&&i.isInQuery&&this._fireCallbacksForKey("key_entered",n,i.location,i.distanceFromCenter)}this._valueEventFired=!1,this._listenForNewGeohashes()},T.prototype._cancelGeohashQuery=function(e,t){e=this._firebaseRef.orderByChild("g").startAt(e[0]).endAt(e[1]);e.off("child_added",t.childAddedCallback),e.off("child_removed",t.childRemovedCallback),e.off("child_changed",t.childChangedCallback),e.off("value",t.valueCallback)},T.prototype._childAddedCallback=function(e){this._updateLocation(E(e),Q(e.val()))},T.prototype._childChangedCallback=function(e){this._updateLocation(E(e),Q(e.val()))},T.prototype._childRemovedCallback=function(e){var r=this,n=E(e);n in this._locationsTracked&&this._firebaseRef.child(n).once("value",function(e){var t=null===e.val()?null:Q(e.val()),e=null!==t?p(t):null;r._geohashInSomeQuery(e)||r._removeLocation(n,t)})},T.prototype._cleanUpCurrentGeohashesQueried=function(){var n=this,e=Object.keys(this._currentGeohashesQueried);e.forEach(function(e){var t,r=n._currentGeohashesQueried[e];!1===r.active&&(t=n._stringToQuery(e),n._cancelGeohashQuery(t,r),delete n._currentGeohashesQueried[e])}),(e=Object.keys(this._locationsTracked)).forEach(function(e){if(!n._geohashInSomeQuery(n._locationsTracked[e].geohash)){if(n._locationsTracked[e].isInQuery)throw new Error("Internal State error, trying to remove location that is still in query");delete n._locationsTracked[e]}}),this._geohashCleanupScheduled=!1,null!==this._cleanUpCurrentGeohashesQueriedTimeout&&(clearTimeout(this._cleanUpCurrentGeohashesQueriedTimeout),this._cleanUpCurrentGeohashesQueriedTimeout=null)},T.prototype._fireCallbacksForKey=function(e,t,r,n){this._callbacks[e].forEach(function(e){null==r?e(t,null,null):e(t,r,n)})},T.prototype._fireReadyEventCallbacks=function(){this._callbacks.ready.forEach(function(e){e()})},T.prototype._geohashInSomeQuery=function(e){for(var t=0,r=Object.keys(this._currentGeohashesQueried);t<r.length;t++){var n=r[t];if(n in this._currentGeohashesQueried){n=this._stringToQuery(n);if(e>=n[0]&&e<=n[1])return!0}}return!1},T.prototype._geohashQueryReadyCallback=function(e){e=this._outstandingGeohashReadyEvents.indexOf(e);-1<e&&this._outstandingGeohashReadyEvents.splice(e,1),this._valueEventFired=0===this._outstandingGeohashReadyEvents.length,this._valueEventFired&&this._fireReadyEventCallbacks()},T.prototype._listenForNewGeohashes=function(){var o=this,r=(r=w(this._center,1e3*this._radius).map(this._queryToString)).filter(function(e,t){return r.indexOf(e)===t});Object.keys(this._currentGeohashesQueried).forEach(function(e){var t=r.indexOf(e);-1===t?o._currentGeohashesQueried[e].active=!1:(o._currentGeohashesQueried[e].active=!0,r.splice(t,1))}),!1===this._geohashCleanupScheduled&&25<Object.keys(this._currentGeohashesQueried).length&&(this._geohashCleanupScheduled=!0,this._cleanUpCurrentGeohashesQueriedTimeout=setTimeout(function(){o._cleanUpCurrentGeohashesQueried()},10)),this._outstandingGeohashReadyEvents=r.slice(),r.forEach(function(e){var t=o._stringToQuery(e),r=o._firebaseRef.orderByChild("g").startAt(t[0]).endAt(t[1]),n=r.on("child_added",function(e){return o._childAddedCallback(e)}),i=r.on("child_removed",function(e){return o._childRemovedCallback(e)}),t=r.on("child_changed",function(e){return o._childChangedCallback(e)}),a=r.on("value",function(){r.off("value",a),o._geohashQueryReadyCallback(e)});o._currentGeohashesQueried[e]={active:!0,childAddedCallback:n,childRemovedCallback:i,childChangedCallback:t,valueCallback:a}}),0===r.length&&this._geohashQueryReadyCallback()},T.prototype._queryToString=function(e){if(2!==e.length)throw new Error("Not a valid geohash query: "+e);return e[0]+":"+e[1]},T.prototype._removeLocation=function(e,t){var r=this._locationsTracked[e];delete this._locationsTracked[e],void 0!==r&&r.isInQuery&&(r=t?C(t,this._center):null,this._fireCallbacksForKey("key_exited",e,t,r))},T.prototype._stringToQuery=function(e){var t=e.split(":");if(2!==t.length)throw new Error("Invalid internal state! Not a valid geohash query: "+e);return t},T.prototype._updateLocation=function(e,t){_(t);var r,n=e in this._locationsTracked&&this._locationsTracked[e].isInQuery,i=e in this._locationsTracked?this._locationsTracked[e].location:null,a=(r=C(t,this._center))<=this._radius;this._locationsTracked[e]={location:t,distanceFromCenter:r,isInQuery:a,geohash:p(t)},a&&!n?this._fireCallbacksForKey("key_entered",e,t,r):!a||null===i||t[0]===i[0]&&t[1]===i[1]?!a&&n&&this._fireCallbacksForKey("key_exited",e,t,r):this._fireCallbacksForKey("key_moved",e,t,r)},T);function T(e,t){var r=this;if(this._firebaseRef=e,this._callbacks={ready:[],key_entered:[],key_exited:[],key_moved:[]},this._cancelled=!1,this._currentGeohashesQueried={},this._locationsTracked={},this._valueEventFired=!1,this._geohashCleanupScheduled=!1,this._cleanUpCurrentGeohashesQueriedTimeout=null,"[object Object]"!==Object.prototype.toString.call(this._firebaseRef))throw new Error("firebaseRef must be an instance of Firebase");this._cleanUpCurrentGeohashesQueriedInterval=setInterval(function(){!1===r._geohashCleanupScheduled&&r._cleanUpCurrentGeohashesQueried()},1e4),G(t,!0),this._center=t.center,this._radius=t.radius,this._listenForNewGeohashes()}var F=(x.prototype.get=function(e){return f(e),this._firebaseRef.child(e).once("value").then(function(e){e=e.val();return null===e?null:Q(e)})},x.prototype.ref=function(){return this._firebaseRef},x.prototype.remove=function(e){return this.set(e,null)},x.prototype.set=function(e,t){var n;if("string"==typeof e&&0!==e.length)(n={})[e]=t;else{if("object"!=typeof e)throw new Error("keyOrLocations must be a string or a mapping of key - location pairs.");if(void 0!==t)throw new Error("The location argument should not be used if you pass an object to set().");n=e}var i={};return Object.keys(n).forEach(function(e){f(e);var t,r=n[e];null===r?i[e]=null:(_(r),t=p(r),i[e]=(t=t,_(r=r),y(t),{".priority":t,g:t,l:r}))}),this._firebaseRef.update(i)},x.prototype.query=function(e){return new M(this._firebaseRef,e)},x);function x(e){if(this._firebaseRef=e,"[object Object]"!==Object.prototype.toString.call(this._firebaseRef))throw new Error("firebaseRef must be an instance of Firebase")}e.GeoCallbackRegistration=t,e.GeoFire=F,e.GeoQuery=M,Object.defineProperty(e,"__esModule",{value:!0})});


/***/ }),

/***/ 9059:
/*!****************************************************************!*\
  !*** ./src/app/Pages/tabs/schedule/schedule-routing.module.ts ***!
  \****************************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "SchedulePageRoutingModule": () => (/* binding */ SchedulePageRoutingModule)
/* harmony export */ });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! tslib */ 64762);
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/core */ 37716);
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/router */ 39895);
/* harmony import */ var _schedule_page__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./schedule.page */ 67116);




const routes = [
    {
        path: '',
        component: _schedule_page__WEBPACK_IMPORTED_MODULE_0__.SchedulePage
    }
];
let SchedulePageRoutingModule = class SchedulePageRoutingModule {
};
SchedulePageRoutingModule = (0,tslib__WEBPACK_IMPORTED_MODULE_1__.__decorate)([
    (0,_angular_core__WEBPACK_IMPORTED_MODULE_2__.NgModule)({
        imports: [_angular_router__WEBPACK_IMPORTED_MODULE_3__.RouterModule.forChild(routes)],
        exports: [_angular_router__WEBPACK_IMPORTED_MODULE_3__.RouterModule],
    })
], SchedulePageRoutingModule);



/***/ }),

/***/ 11297:
/*!********************************************************!*\
  !*** ./src/app/Pages/tabs/schedule/schedule.module.ts ***!
  \********************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "SchedulePageModule": () => (/* binding */ SchedulePageModule)
/* harmony export */ });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! tslib */ 64762);
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @angular/core */ 37716);
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @angular/common */ 38583);
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! @angular/forms */ 3679);
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! @ionic/angular */ 80476);
/* harmony import */ var _schedule_routing_module__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./schedule-routing.module */ 9059);
/* harmony import */ var _schedule_page__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./schedule.page */ 67116);
/* harmony import */ var src_app_Components_header_header_module__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! src/app/Components/header/header.module */ 42335);








let SchedulePageModule = class SchedulePageModule {
};
SchedulePageModule = (0,tslib__WEBPACK_IMPORTED_MODULE_3__.__decorate)([
    (0,_angular_core__WEBPACK_IMPORTED_MODULE_4__.NgModule)({
        imports: [
            _angular_common__WEBPACK_IMPORTED_MODULE_5__.CommonModule,
            _angular_forms__WEBPACK_IMPORTED_MODULE_6__.FormsModule,
            _ionic_angular__WEBPACK_IMPORTED_MODULE_7__.IonicModule,
            _schedule_routing_module__WEBPACK_IMPORTED_MODULE_0__.SchedulePageRoutingModule,
            src_app_Components_header_header_module__WEBPACK_IMPORTED_MODULE_2__.HeaderModule,
        ],
        declarations: [_schedule_page__WEBPACK_IMPORTED_MODULE_1__.SchedulePage],
    })
], SchedulePageModule);



/***/ }),

/***/ 67116:
/*!******************************************************!*\
  !*** ./src/app/Pages/tabs/schedule/schedule.page.ts ***!
  \******************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "SchedulePage": () => (/* binding */ SchedulePage)
/* harmony export */ });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! tslib */ 64762);
/* harmony import */ var _raw_loader_schedule_page_html__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! !raw-loader!./schedule.page.html */ 99413);
/* harmony import */ var _schedule_page_scss__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./schedule.page.scss */ 24501);
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @angular/core */ 37716);
/* harmony import */ var _base_page_base_page__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../../base-page/base-page */ 96093);
/* harmony import */ var geofire__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! geofire */ 58809);
/* harmony import */ var geofire__WEBPACK_IMPORTED_MODULE_3___default = /*#__PURE__*/__webpack_require__.n(geofire__WEBPACK_IMPORTED_MODULE_3__);






let SchedulePage = class SchedulePage extends _base_page_base_page__WEBPACK_IMPORTED_MODULE_2__.BasePage {
    constructor(injector) {
        super(injector);
        this.biddingLimit = {
            lowerLimit: 0,
            biddingPrice: 0,
            bidText: '',
        };
        this.changeBidcolor = false;
        this.accptCustomar = true;
        this.driver_avg_rating = 0;
        this.changeBidCalled = false;
        this.jobs = [];
        console.log('SchedulePage -> constructor');
        // this.usersService.presentLoading();
    }
    ngOnInit() { }
    initialize() {
        return (0,tslib__WEBPACK_IMPORTED_MODULE_4__.__awaiter)(this, void 0, void 0, function* () {
            let self = this;
            let user = yield this.firebaseService.getCurrentUser();
            this.curUser = user;
            let ref = this.firebaseService.getDbRef('/users/' + user.uid);
            ref.once('value', (snapshot) => {
                console.log(snapshot.val(), 'DRIVER_DATA');
                this.userProfile = snapshot.val();
                if (this.userProfile != undefined &&
                    this.userProfile.hasOwnProperty('profile_image') &&
                    this.userProfile.profile_image != null) {
                    this.userProfile.profile_image = this.userProfile.profile_image;
                }
                else {
                    this.userProfile.profile_image = 'assets/images/notfound.png';
                }
                if (this.userProfile.hasOwnProperty('ratings')) {
                    let key;
                    let ratingLen = 0;
                    for (key in this.userProfile.ratings) {
                        this.driver_avg_rating += parseFloat(this.userProfile.ratings[key]);
                        ratingLen += 1;
                    }
                    this.driver_avg_rating = this.driver_avg_rating / ratingLen;
                    console.log(this.driver_avg_rating, ratingLen);
                }
                console.log(this.userProfile);
                //self.usersService.dismissLoader();
            });
        });
    }
    ionViewWillEnter() {
        this.initialize();
        console.log('ionViewWillEnter -> schedule');
        setTimeout(() => {
            this.getJobs();
        }, 200);
        // if (SchedulePage.needsVerificationCheck) this.initialize();
    }
    getJobs() {
        console.log('ionviewWillEnter');
        this.jobs = [];
        let self = this;
        this.usersService.presentLoading();
        let ref = this.firebaseService.getDbRef('bookings');
        ref.on('value', (snapshot) => {
            console.log('Bookings Received');
            //   this.jobs = [];
            let allBookings = snapshot.val();
            this.geoLocation
                .getCurrentLocationCoordinates()
                .then((position) => {
                console.log('Position ->', position);
                this.jobs = [];
                var geoFire = new geofire__WEBPACK_IMPORTED_MODULE_3__.GeoFire(this.firebaseService.getDbRef('geofire'));
                var geoQuery = geoFire.query({
                    center: [position['lat'], position['lng']],
                    radius: 50,
                });
                geoQuery.on('key_entered', (key, location, distance) => {
                    console.log('key_entered', key, location, distance);
                    //console.log(allBookings[key])
                    //  if (TabsPage.selectedTab === 'schedule') {
                    console.log('onSchedule');
                    try {
                        this.zone.run(() => {
                            allBookings[key].uid = key;
                            this.jobs.push(allBookings[key]);
                            let job = allBookings[key];
                            let dt = new Date(job.tripdate);
                            let jt = dt.getMilliseconds() + 300000; //dt.getTime(); //  trip date + 5 minutes
                            let now = new Date().getMilliseconds();
                            if (jt > now) {
                                console.log('NEWJOB ARRIVED');
                                console.log('DATE_TIME', dt);
                                job.isNew = true;
                                setTimeout(() => {
                                    self.getJobs();
                                }, jt - now);
                            }
                            if (allBookings[key].status === 'Finding your driver') {
                                //  if(allBookings[key].drop == undefined || allBookings[key].drop){    // new line added by me part 2 .....
                                // this.zone.run(() => {
                                job['changeBidcolor'] = false;
                                job['currentBid'] = job.estimate;
                                job['lowerLimit'] = (job['currentBid'] / 2).toFixed(2);
                                // this.jobs.push(allBookings[key]);
                                // this.jobs.sort(function (a, b) {
                                //     return b.tripdate - a.tripdate;
                                // });
                                // });
                                // }
                            }
                            else if (job.status === 'Bidding Start') {
                                // this.zone.run(() => {
                                //     job.uid = key;
                                // });
                                if (job.hasOwnProperty('bidding')) {
                                    for (let i = 0; i < job.bidding.length; i++) {
                                        if (job.bidding[i].driver == this.curUser.uid) {
                                            job['isBidding'] = true; //current user already bid for this ride
                                            job['yourBid'] = job.bidding[i].rate;
                                            job.isNew = false;
                                        }
                                        if (job.bidding.length > 1) {
                                            let bidLength = job.bidding.length;
                                            let amt = job.bidding[bidLength - 1].rate;
                                            job['changeBidcolor'] = true;
                                            job['currentBid'] = parseFloat(amt);
                                        }
                                        else {
                                            job['changeBidcolor'] = false;
                                            job['currentBid'] = parseFloat(job.estimate);
                                        }
                                        job['lowerLimit'] = (job['currentBid'] / 2).toFixed(2);
                                    }
                                }
                            }
                            this.jobs.sort(function (a, b) {
                                console.log('hehdew');
                                let aa = Number(new Date(a.tripdate));
                                let bb = Number(new Date(b.tripdate));
                                console.log(bb - aa);
                                return bb - aa;
                            });
                        });
                    }
                    catch (e) {
                        console.log('Undefined Error handled by Catch');
                    }
                    //   }
                    this.usersService.dismissLoader();
                });
                geoQuery.on('key_exited', (key, location, distance) => {
                    this.zone.run(() => {
                        console.log('key_exited', key);
                        for (let i = 0; i < this.jobs.length; i++) {
                            if (this.jobs[i].uid == key) {
                                this.jobs.slice(i, 1);
                            }
                        }
                    });
                });
            })
                .catch((error) => {
                console.log('Error getting location', error);
            });
        });
    }
    // setJobs(data){
    //   var promise = new Promise((resolve, reject) => {
    //     if(err){}
    //
    //   });
    // }
    //alert
    presentAlert(message) {
        this.alertService.Alert('ALERT', message);
    }
    okSchedule(i) {
        var _a, _b, _c, _d;
        this.changeBidCalled = false;
        console.log(this.jobs[i]);
        this.usersService.presentLoading();
        if (this.jobs[i].promo_code) {
            this.promo = this.jobs[i].promo_code;
        }
        else {
            this.promo = null;
        }
        if (this.jobs[i].corporate_booking_name) {
            this.corporate_booking_name = this.jobs[i].corporate_booking_name;
        }
        else {
            this.corporate_booking_name = null;
        }
        if (this.jobs[i].customer_email) {
            this.customer_email = this.jobs[i].customer_email;
        }
        else {
            this.customer_email = null;
        }
        if (this.jobs[i].customer_image) {
            this.customer_image = (_a = this.jobs[i].customer_image) !== null && _a !== void 0 ? _a : '';
        }
        else {
            this.customer_image = '';
        }
        if (this.jobs[i].booking_type) {
            this.booking_type = this.jobs[i].booking_type;
        }
        else {
            this.booking_type = null;
        }
        if (this.jobs[i].customer_name) {
            this.customer_name = this.jobs[i].customer_name;
        }
        else {
            this.customer_name = null;
        }
        var bidding = [];
        console.log();
        var key = this.jobs[i].uid;
        var ref = this.firebaseService.getDbRef('bookings/' + key);
        ref.on('value', (snapshot) => {
            var _a, _b, _c, _d;
            if (snapshot.val().hasOwnProperty('bidding') &&
                snapshot.val().bidding.length > 0) {
                for (var j = 0; j < snapshot.val().bidding.length; j++) {
                    if (snapshot.val().bidding[j].driver !== this.curUser.uid) {
                        bidding.push(snapshot.val().bidding[j]);
                    }
                }
                bidding.push({
                    rate: this.jobs[i].estimate,
                    driver: this.curUser.uid,
                    driver_name: this.userProfile.fullname,
                    driver_image: (_a = this.userProfile.profile_image) !== null && _a !== void 0 ? _a : '',
                    status: 'Bidding Start',
                    driver_rating: this.driver_avg_rating,
                    driver_push_token: (_b = this.userProfile.pushToken) !== null && _b !== void 0 ? _b : '',
                    job_id: key,
                });
            }
            else {
                bidding = [
                    {
                        rate: this.jobs[i].estimate,
                        driver: this.curUser.uid,
                        driver_name: this.userProfile.fullname,
                        driver_image: (_c = this.userProfile.profile_image) !== null && _c !== void 0 ? _c : '',
                        status: 'Bidding Start',
                        driver_rating: this.driver_avg_rating,
                        driver_push_token: (_d = this.userProfile.pushToken) !== null && _d !== void 0 ? _d : '',
                        job_id: key,
                    },
                ];
            }
        });
        this.bookingData = {
            customer: this.jobs[i].customer,
            customer_name: this.jobs[i].customer_name,
            customer_image: (_b = this.jobs[i].customer_image) !== null && _b !== void 0 ? _b : '',
            pickup: this.jobs[i].pickup,
            // drop: this.jobs[i].drop,
            tripdate: this.jobs[i].tripdate,
            status: 'Bidding Start',
            driver: this.curUser.uid,
            driver_name: this.userProfile.fullname,
            driver_image: (_c = this.userProfile.profile_image) !== null && _c !== void 0 ? _c : '',
            //   distance: this.jobs[i].distance,
            triptime: this.jobs[i].triptime,
            transmission: this.jobs[i].transmission,
            estimate: this.jobs[i].estimate,
            // tripType: this.jobs[i].tripType,
            serviceType: this.jobs[i].serviceType,
            customer_push_token: (_d = this.jobs[i].customer_push_token) !== null && _d !== void 0 ? _d : null,
            // promo_code:this.jobs[i].promo_code
            promo_code: this.promo,
            bidding: bidding,
        };
        if (this.jobs[i].drop) {
            this.bookingData['drop'] = this.jobs[i].drop;
            this.bookingData['distance'] = this.jobs[i].distance;
            ref.update(this.bookingData);
            let ref2 = this.firebaseService.getDbRef('bookings/' + key + '/driver');
            ref2.on('value', (snapshot) => {
                var _a, _b;
                if (snapshot.val() == this.curUser.uid) {
                    var customerData = {
                        customer_email: this.customer_email,
                        corporate_booking_name: this.corporate_booking_name,
                        customer_image: (_a = this.customer_image) !== null && _a !== void 0 ? _a : '',
                        booking_type: this.booking_type,
                        customer_name: this.customer_name,
                        customer: this.bookingData.customer,
                        driver: this.curUser.uid,
                        driver_name: this.userProfile.fullname,
                        driver_image: (_b = this.userProfile.profile_image) !== null && _b !== void 0 ? _b : '',
                        status: 'Bidding Start',
                        pickupAddress: this.bookingData.pickup.add,
                        dropAddress: this.bookingData.drop.add,
                        tripdate: this.bookingData.tripdate,
                        serviceType: this.bookingData.serviceType,
                        bidding: bidding,
                    };
                    var updates = {};
                    // updates['/users/' + this.curUser.uid + '/bookings/' + key] = driverData;
                    updates['/users/' + this.bookingData.customer + '/bookings/' + key] =
                        customerData;
                    console.log(key);
                    this.firebaseService.getDatabase().ref().update(updates);
                    //  firebase.database().ref("geofire").child(key).remove();
                    //this.usersService.sendPush('Driver Assigned to Job',"Your driver will arrive soon.",this.bookingData.customer_push_token).subscribe(response=>console.log(response));
                    this.usersService
                        .sendPush('Driver bid your trip', 'Driver ' + this.userProfile.fullname + ' bid your trip.', this.bookingData.customer_push_token)
                        .subscribe((response) => console.log(response));
                    this.usersService.dismissLoader();
                    this.nav.setRoot('tabs');
                }
                else {
                    this.usersService.dismissLoader();
                    console.log('Error');
                    console.log(snapshot.val());
                    this.firebaseService
                        .getDbRef('/users/' + this.curUser.uid + '/bookings/' + key)
                        .remove();
                    this.nav.setRoot('tabs');
                }
            });
        }
        else {
            ref.update(this.bookingData);
            let ref2 = this.firebaseService.getDbRef('bookings/' + key + '/driver');
            ref2.on('value', (snapshot) => {
                var _a, _b;
                console.log(snapshot.val());
                if (snapshot.val() == this.curUser.uid) {
                    var customerData = {
                        customer_email: this.customer_email,
                        corporate_booking_name: this.corporate_booking_name,
                        customer_image: (_a = this.customer_image) !== null && _a !== void 0 ? _a : '',
                        booking_type: this.booking_type,
                        customer_name: this.customer_name,
                        customer: this.bookingData.customer,
                        driver: this.curUser.uid,
                        driver_name: this.userProfile.fullname,
                        driver_image: (_b = this.userProfile.profile_image) !== null && _b !== void 0 ? _b : '',
                        status: 'Bidding Start',
                        pickupAddress: this.bookingData.pickup.add,
                        //dropAddress: this.bookingData.drop.add,
                        tripdate: this.bookingData.tripdate,
                        serviceType: this.bookingData.serviceType,
                        bidding: bidding,
                    };
                    var updates = {};
                    // updates['/users/' + this.curUser.uid + '/bookings/' + key] = driverData;
                    updates['/users/' + this.bookingData.customer + '/bookings/' + key] =
                        customerData;
                    this.firebaseService.getDatabase().ref().update(updates);
                    this.usersService
                        .sendPush('Driver is bid your trip', 'Driver ' + this.userProfile.fullname + ' bid your trip.', this.bookingData.customer_push_token)
                        .subscribe((response) => console.log(response));
                    this.usersService.dismissLoader();
                    this.nav.setRoot('tabs');
                }
                else {
                    this.usersService.dismissLoader();
                    console.log('Error');
                    console.log(snapshot.val());
                    this.firebaseService
                        .getDbRef('/users/' + this.curUser.uid + '/bookings/' + key)
                        .remove();
                    this.nav.setRoot('tabs');
                }
            });
        }
    }
    okCancle() {
        //this.navCtrl.setRoot(MapPage);
    }
    bidAmount(job) {
        job['driver_rating'] = this.driver_avg_rating;
        this.nav.push('bid-price', {
            job: job,
            currentuser: this.curUser,
            userprofile: this.userProfile,
        });
    }
    setMyClasses(job) {
        let className = '';
        if (job.changeBidcolor) {
            className = 'bid-color';
        }
        console.log('classes', job.changeBidcolor, className);
        return className;
    }
    changeBid(action, job) {
        //this.changeBidCalled=true;
        delete job.yourBid;
        if (action == 'inc') {
            job.estimate += 1;
        }
        else if (action == 'dec' && job.estimate > job.lowerLimit) {
            job.estimate -= 1;
        }
        console.log(this.biddingLimit.biddingPrice);
    }
    ImgError(source) {
        console.log('ImgError, '), source;
        source.src = 'assets/images/notfound.png';
        source.onerror = '';
        return true;
    }
};
SchedulePage.needsVerificationCheck = false;
SchedulePage.ctorParameters = () => [
    { type: _angular_core__WEBPACK_IMPORTED_MODULE_5__.Injector }
];
SchedulePage = (0,tslib__WEBPACK_IMPORTED_MODULE_4__.__decorate)([
    (0,_angular_core__WEBPACK_IMPORTED_MODULE_5__.Component)({
        selector: 'app-schedule',
        template: _raw_loader_schedule_page_html__WEBPACK_IMPORTED_MODULE_0__.default,
        styles: [_schedule_page_scss__WEBPACK_IMPORTED_MODULE_1__.default]
    })
], SchedulePage);



/***/ }),

/***/ 24501:
/*!********************************************************!*\
  !*** ./src/app/Pages/tabs/schedule/schedule.page.scss ***!
  \********************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "default": () => (__WEBPACK_DEFAULT_EXPORT__)
/* harmony export */ });
/* harmony default export */ const __WEBPACK_DEFAULT_EXPORT__ = (".customcardheader {\n  background: #222222;\n  padding: 0px;\n  margin-bottom: 10px;\n}\n\n.customcardheader-middle, .customcardheader-left,\n.customcardheader-right {\n  text-align: center;\n  font-family: \"PT Sans\";\n  font-size: 17px;\n  color: #fff;\n  line-height: 35px;\n}\n\n.customcardheader-left,\n.customcardheader-right {\n  background: #2e4b86;\n}\n\n.customcard {\n  background: #1e1e1e;\n  width: 93%;\n  margin: auto;\n  margin-top: 12px;\n  margin-bottom: 12px;\n}\n\n.customcardbody img {\n  width: 25%;\n  border-radius: 50%;\n  margin-bottom: 10px !important;\n  display: block;\n  margin: auto;\n  border: 1px solid #8c8c8c;\n}\n\n.customcardbody h2 {\n  text-align: center;\n  margin: 0;\n  color: #4267b2;\n  font-weight: bold;\n  font-size: 17px;\n  padding: 0 0 5px 0;\n  font-family: \"PT Sans\";\n}\n\n.customcardbody p {\n  margin-bottom: 10px;\n  text-align: center;\n  font-size: 15px;\n  font-family: \"PT Sans\";\n}\n\n.customcardbody p span {\n  font-weight: bold;\n}\n\n.serviceType {\n  color: #000;\n  font-style: italic;\n}\n\n.marker {\n  margin-right: 10%;\n}\n\n.loaction {\n  font-size: 15px;\n}\n\n.marker img {\n  padding-top: 0px !important;\n}\n\n.last-row {\n  border-bottom: 1px dotted #000;\n  padding-bottom: 10px;\n  margin-bottom: 7px;\n}\n\n.detail {\n  margin-left: 6%;\n  color: #ccc7c7;\n  font-size: 15px;\n}\n\n.next-row {\n  color: #ccc7c7 !important;\n}\n\n.detail-card {\n  margin-top: 0px;\n  background-color: #fff;\n}\n\n.addOn {\n  background-color: #4266b2;\n  text-align: center;\n  font-size: 20px;\n  color: #fff;\n  height: 40px;\n  width: 40px;\n  border-radius: 20px;\n  display: flex;\n  justify-content: center;\n  align-items: center;\n  border: 1px solid #e1e1e1;\n}\n\n.addOn ion-icon {\n  margin-top: -4px;\n}\n\n.bid-rate {\n  font-size: 15px;\n  text-align: center;\n  margin: 7px;\n}\n\n.submit-bid {\n  width: 94%;\n  font-size: 15px;\n  padding: 10px;\n  background-color: #4266b2;\n  color: #fff;\n  margin-left: 3%;\n  height: 50px;\n  border-radius: 25px;\n}\n\n.cust-name {\n  text-align: center;\n  color: #000;\n}\n\n.bid-color {\n  color: green !important;\n}\n\n.new {\n  border: 1px solid yellow;\n}\n\n.blink {\n  animation: blink-animation 1s steps(5, start) infinite;\n  -webkit-animation: blink-animation 1s steps(5, start) infinite;\n}\n\n@keyframes blink-animation {\n  to {\n    visibility: hidden;\n  }\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNjaGVkdWxlLnBhZ2Uuc2NzcyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiQUFBQTtFQUNFLG1CQUFBO0VBQ0EsWUFBQTtFQUNBLG1CQUFBO0FBQ0Y7O0FBQ0E7O0VBQ0Usa0JBQUE7RUFDQSxzQkFBQTtFQUNBLGVBQUE7RUFDQSxXQUFBO0VBQ0EsaUJBQUE7QUFHRjs7QUFEQTs7RUFFRSxtQkFBQTtBQUlGOztBQURBO0VBQ0UsbUJBQUE7RUFDQSxVQUFBO0VBQ0EsWUFBQTtFQUNBLGdCQUFBO0VBQ0EsbUJBQUE7QUFJRjs7QUFGQTtFQUNFLFVBQUE7RUFDQSxrQkFBQTtFQUNBLDhCQUFBO0VBQ0EsY0FBQTtFQUNBLFlBQUE7RUFDQSx5QkFBQTtBQUtGOztBQUhBO0VBQ0Usa0JBQUE7RUFDQSxTQUFBO0VBQ0EsY0FBQTtFQUNBLGlCQUFBO0VBQ0EsZUFBQTtFQUNBLGtCQUFBO0VBQ0Esc0JBQUE7QUFNRjs7QUFKQTtFQUNFLG1CQUFBO0VBQ0Esa0JBQUE7RUFDQSxlQUFBO0VBQ0Esc0JBQUE7QUFPRjs7QUFMQTtFQUNFLGlCQUFBO0FBUUY7O0FBTkE7RUFDRSxXQUFBO0VBQ0Esa0JBQUE7QUFTRjs7QUFOQTtFQUNFLGlCQUFBO0FBU0Y7O0FBUEE7RUFDRSxlQUFBO0FBVUY7O0FBUkE7RUFDRSwyQkFBQTtBQVdGOztBQVRBO0VBQ0UsOEJBQUE7RUFDQSxvQkFBQTtFQUNBLGtCQUFBO0FBWUY7O0FBVkE7RUFDRSxlQUFBO0VBQ0EsY0FBQTtFQUNBLGVBQUE7QUFhRjs7QUFYQTtFQUNFLHlCQUFBO0FBY0Y7O0FBWkE7RUFFRSxlQUFBO0VBQ0Esc0JBQUE7QUFjRjs7QUFUQTtFQUNFLHlCQUFBO0VBQ0Esa0JBQUE7RUFDQSxlQUFBO0VBQ0EsV0FBQTtFQUNBLFlBQUE7RUFDQSxXQUFBO0VBQ0EsbUJBQUE7RUFDQSxhQUFBO0VBQ0EsdUJBQUE7RUFDQSxtQkFBQTtFQUNBLHlCQUFBO0FBWUY7O0FBVkE7RUFDRSxnQkFBQTtBQWFGOztBQVhBO0VBQ0UsZUFBQTtFQUNBLGtCQUFBO0VBQ0EsV0FBQTtBQWNGOztBQVpBO0VBQ0UsVUFBQTtFQUNBLGVBQUE7RUFDQSxhQUFBO0VBQ0EseUJBQUE7RUFDQSxXQUFBO0VBQ0EsZUFBQTtFQUNBLFlBQUE7RUFDQSxtQkFBQTtBQWVGOztBQWJBO0VBQ0Usa0JBQUE7RUFDQSxXQUFBO0FBZ0JGOztBQWRBO0VBQ0UsdUJBQUE7QUFpQkY7O0FBZEE7RUFDRSx3QkFBQTtBQWlCRjs7QUFkQTtFQUNFLHNEQUFBO0VBQ0EsOERBQUE7QUFpQkY7O0FBZkE7RUFDRTtJQUNFLGtCQUFBO0VBa0JGO0FBQ0YiLCJmaWxlIjoic2NoZWR1bGUucGFnZS5zY3NzIiwic291cmNlc0NvbnRlbnQiOlsiLmN1c3RvbWNhcmRoZWFkZXIge1xyXG4gIGJhY2tncm91bmQ6ICMyMjIyMjI7XHJcbiAgcGFkZGluZzogMHB4O1xyXG4gIG1hcmdpbi1ib3R0b206IDEwcHg7XHJcbn1cclxuLmN1c3RvbWNhcmRoZWFkZXItbWlkZGxlIHtcclxuICB0ZXh0LWFsaWduOiBjZW50ZXI7XHJcbiAgZm9udC1mYW1pbHk6IFwiUFQgU2Fuc1wiO1xyXG4gIGZvbnQtc2l6ZTogMTdweDtcclxuICBjb2xvcjogI2ZmZjtcclxuICBsaW5lLWhlaWdodDogMzVweDtcclxufVxyXG4uY3VzdG9tY2FyZGhlYWRlci1sZWZ0LFxyXG4uY3VzdG9tY2FyZGhlYWRlci1yaWdodCB7XHJcbiAgYmFja2dyb3VuZDogIzJlNGI4NjtcclxuICBAZXh0ZW5kIC5jdXN0b21jYXJkaGVhZGVyLW1pZGRsZTtcclxufVxyXG4uY3VzdG9tY2FyZCB7XHJcbiAgYmFja2dyb3VuZDogIzFlMWUxZTtcclxuICB3aWR0aDogOTMlO1xyXG4gIG1hcmdpbjogYXV0bztcclxuICBtYXJnaW4tdG9wOiAxMnB4O1xyXG4gIG1hcmdpbi1ib3R0b206IDEycHg7XHJcbn1cclxuLmN1c3RvbWNhcmRib2R5IGltZyB7XHJcbiAgd2lkdGg6IDI1JTtcclxuICBib3JkZXItcmFkaXVzOiA1MCU7XHJcbiAgbWFyZ2luLWJvdHRvbTogMTBweCAhaW1wb3J0YW50O1xyXG4gIGRpc3BsYXk6IGJsb2NrO1xyXG4gIG1hcmdpbjogYXV0bztcclxuICBib3JkZXI6IDFweCBzb2xpZCAjOGM4YzhjO1xyXG59XHJcbi5jdXN0b21jYXJkYm9keSBoMiB7XHJcbiAgdGV4dC1hbGlnbjogY2VudGVyO1xyXG4gIG1hcmdpbjogMDtcclxuICBjb2xvcjogIzQyNjdiMjtcclxuICBmb250LXdlaWdodDogYm9sZDtcclxuICBmb250LXNpemU6IDE3cHg7XHJcbiAgcGFkZGluZzogMCAwIDVweCAwO1xyXG4gIGZvbnQtZmFtaWx5OiBcIlBUIFNhbnNcIjtcclxufVxyXG4uY3VzdG9tY2FyZGJvZHkgcCB7XHJcbiAgbWFyZ2luLWJvdHRvbTogMTBweDtcclxuICB0ZXh0LWFsaWduOiBjZW50ZXI7XHJcbiAgZm9udC1zaXplOiAxNXB4O1xyXG4gIGZvbnQtZmFtaWx5OiBcIlBUIFNhbnNcIjtcclxufVxyXG4uY3VzdG9tY2FyZGJvZHkgcCBzcGFuIHtcclxuICBmb250LXdlaWdodDogYm9sZDtcclxufVxyXG4uc2VydmljZVR5cGUge1xyXG4gIGNvbG9yOiAjMDAwO1xyXG4gIGZvbnQtc3R5bGU6IGl0YWxpYztcclxufVxyXG5cclxuLm1hcmtlciB7XHJcbiAgbWFyZ2luLXJpZ2h0OiAxMCU7XHJcbn1cclxuLmxvYWN0aW9uIHtcclxuICBmb250LXNpemU6IDE1cHg7XHJcbn1cclxuLm1hcmtlciBpbWcge1xyXG4gIHBhZGRpbmctdG9wOiAwcHggIWltcG9ydGFudDtcclxufVxyXG4ubGFzdC1yb3cge1xyXG4gIGJvcmRlci1ib3R0b206IDFweCBkb3R0ZWQgIzAwMDtcclxuICBwYWRkaW5nLWJvdHRvbTogMTBweDtcclxuICBtYXJnaW4tYm90dG9tOiA3cHg7XHJcbn1cclxuLmRldGFpbCB7XHJcbiAgbWFyZ2luLWxlZnQ6IDYlO1xyXG4gIGNvbG9yOiAjY2NjN2M3O1xyXG4gIGZvbnQtc2l6ZTogMTVweDtcclxufVxyXG4ubmV4dC1yb3cge1xyXG4gIGNvbG9yOiAjY2NjN2M3ICFpbXBvcnRhbnQ7XHJcbn1cclxuLmRldGFpbC1jYXJkIHtcclxuICAvLyBib3JkZXI6MHB4O1xyXG4gIG1hcmdpbi10b3A6IDBweDtcclxuICBiYWNrZ3JvdW5kLWNvbG9yOiAjZmZmO1xyXG59XHJcbi5kZXRhaWwgc3BhbiB7XHJcbiAgLy8gY29sb3I6IzAwMDtcclxufVxyXG4uYWRkT24ge1xyXG4gIGJhY2tncm91bmQtY29sb3I6ICM0MjY2YjI7XHJcbiAgdGV4dC1hbGlnbjogY2VudGVyO1xyXG4gIGZvbnQtc2l6ZTogMjBweDtcclxuICBjb2xvcjogI2ZmZjtcclxuICBoZWlnaHQ6IDQwcHg7XHJcbiAgd2lkdGg6IDQwcHg7XHJcbiAgYm9yZGVyLXJhZGl1czogMjBweDtcclxuICBkaXNwbGF5OiBmbGV4O1xyXG4gIGp1c3RpZnktY29udGVudDogY2VudGVyO1xyXG4gIGFsaWduLWl0ZW1zOiBjZW50ZXI7XHJcbiAgYm9yZGVyOiAxcHggc29saWQgI2UxZTFlMTtcclxufVxyXG4uYWRkT24gaW9uLWljb24ge1xyXG4gIG1hcmdpbi10b3A6IC00cHg7XHJcbn1cclxuLmJpZC1yYXRlIHtcclxuICBmb250LXNpemU6IDE1cHg7XHJcbiAgdGV4dC1hbGlnbjogY2VudGVyO1xyXG4gIG1hcmdpbjogN3B4O1xyXG59XHJcbi5zdWJtaXQtYmlkIHtcclxuICB3aWR0aDogOTQlO1xyXG4gIGZvbnQtc2l6ZTogMTVweDtcclxuICBwYWRkaW5nOiAxMHB4O1xyXG4gIGJhY2tncm91bmQtY29sb3I6ICM0MjY2YjI7XHJcbiAgY29sb3I6ICNmZmY7XHJcbiAgbWFyZ2luLWxlZnQ6IDMlO1xyXG4gIGhlaWdodDogNTBweDtcclxuICBib3JkZXItcmFkaXVzOiAyNXB4O1xyXG59XHJcbi5jdXN0LW5hbWUge1xyXG4gIHRleHQtYWxpZ246IGNlbnRlcjtcclxuICBjb2xvcjogIzAwMDtcclxufVxyXG4uYmlkLWNvbG9yIHtcclxuICBjb2xvcjogZ3JlZW4gIWltcG9ydGFudDtcclxufVxyXG5cclxuLm5ldyB7XHJcbiAgYm9yZGVyOiAxcHggc29saWQgeWVsbG93O1xyXG59XHJcblxyXG4uYmxpbmsge1xyXG4gIGFuaW1hdGlvbjogYmxpbmstYW5pbWF0aW9uIDFzIHN0ZXBzKDUsIHN0YXJ0KSBpbmZpbml0ZTtcclxuICAtd2Via2l0LWFuaW1hdGlvbjogYmxpbmstYW5pbWF0aW9uIDFzIHN0ZXBzKDUsIHN0YXJ0KSBpbmZpbml0ZTtcclxufVxyXG5Aa2V5ZnJhbWVzIGJsaW5rLWFuaW1hdGlvbiB7XHJcbiAgdG8ge1xyXG4gICAgdmlzaWJpbGl0eTogaGlkZGVuO1xyXG4gIH1cclxufVxyXG5ALXdlYmtpdC1rZXlmcmFtZXMgYmxpbmstYW5pbWF0aW9uIHtcclxuICB0byB7XHJcbiAgICB2aXNpYmlsaXR5OiBoaWRkZW47XHJcbiAgfVxyXG59XHJcbiJdfQ== */");

/***/ }),

/***/ 99413:
/*!**********************************************************************************************!*\
  !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/Pages/tabs/schedule/schedule.page.html ***!
  \**********************************************************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "default": () => (__WEBPACK_DEFAULT_EXPORT__)
/* harmony export */ });
/* harmony default export */ const __WEBPACK_DEFAULT_EXPORT__ = ("<header [title]=\"'JOB LIST'\"></header>\r\n\r\n<ion-content>\r\n  <h2 *ngIf=\"jobs.length==0\" class=\"review-title\" style=\"text-align: center\">\r\n    No jobs yet.\r\n  </h2>\r\n  <ion-card class=\"customcard\" [class.new]=\"job.isNew\" *ngFor=\"let job of jobs; let i = index;\">\r\n    <ion-card-header class=\"customcardheader\">\r\n      <ion-grid class=\"no-padding\">\r\n        <ion-row style=\"height: 37px\">\r\n          <!-- <ion-col width-20 class=\"customcardheader-left\">\r\n           <ion-icon name=\"md-checkmark\" *ngIf=\"!job.hasOwnProperty('isBidding')\" (click)=\"okSchedule(i)\"></ion-icon>\r\n          </ion-col> -->\r\n\r\n          <!--<ion-col width-60 class=\"customcardheader-middle\">{{job.tripdate | date:'dd-MM-yyyy HH:mm'}}</ion-col>-->\r\n          <ion-col [size]=\"job.isNew ? '8' : '12'\" class=\"customcardheader-middle\">\r\n            <ion-icon src=\"/assets/images/calendar-outline.svg\"></ion-icon>\r\n            {{job.tripdate.substring(8,10)}}{{job.tripdate.substring(4,8)}}{{job.tripdate.substring(0,4)}}\r\n            {{job.tripdate.substring(11,16)}}\r\n          </ion-col>\r\n\r\n          <ion-col *ngIf=\"job.isNew\" size=\"4\">\r\n            <ion-chip background=\"tertiary\" class=\"blink\"> New Job</ion-chip>\r\n          </ion-col>\r\n\r\n          <!-- <ion-col width-20 class=\"customcardheader-right\">\r\n            <ion-icon name=\"md-close\" (click)=\"okCancle(i)\"></ion-icon>\r\n            <ion-icon ios=\"ios-add\" md=\"md-add\" (click)=\"bidAmount(job)\"></ion-icon>\r\n          </ion-col> -->\r\n        </ion-row>\r\n      </ion-grid>\r\n    </ion-card-header>\r\n\r\n    <ion-card-content class=\"customcardbody\">\r\n      <ion-row style=\"padding-top: 5px\">\r\n        <ion-col>\r\n          <img onerror=\"this.onerror=null;this.src='assets/images/notfound.png';\"\r\n            src=\"{{job.customer_image ?? 'data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAOEAAADhCAMAAAAJbSJIAAAAb1BMVEX///9UWV1PVVmBhIZKUFSztbdCSE1FS09OU1dGTFBARkv8/Pzh4uJKT1RESU5NUlfKy8z39/fx8fFaX2NobG+JjI7q6+umqKqQk5VgZGjExcbV1tducnWanJ6Dhoh0eHu6vL2ho6Xc3d17foGur7GvHrXYAAAGTklEQVR4nO2d65KqOhBGJRPDHREEL4yCyvs/45HxOO4ZRQmk6WbvXlVW+TNfpdOXkHRmM4ZhGIZhGIb5ZnmK5+tNdvg4ZJv1PD4tsQdkEr+oP1LbDuXCcRxx+S1kaEfWuS587KGZIKnOF3HCekRINzrPc+wBDsOvPqOn6r5VhtFnNd2ZzPehfCXvJtLdT3Mi84NavJV3ZaEOAfZwtUky5XTU1+CoLMEesh5rLX3XeVxjD1qDUyo19TXI9IQ98K7svR76Grw99tA7kWz7TOCVcDWB1Vi47wNEO8ItsAW8Y97XQm94c2wJr9mrgQItKyK9GDfuYIGW5W6wZbSTmRB4kZhhC2nDyAx+SSRqqHVkSOBlLdbYYp6xG+5k7ng7bDmPBCYFWpYiV2z4RvU1UKuLD7q1xDucA7aknxhdhFcUqaW47J9styMpbTgat9EGSnZ6GppuP8ejUxGvhhRM7YgVtrAbOxtEoGXZVJxNCiTQslJsaVdiUwn3I3aMLe6LT5hV2CA+scU1nMwH+zuKgjvdQMTCGw6Bet+HcqRXXPwEfBeCKgzxAwZIwnaHQOoGFyquuNgCC3CF2JvgR1gjvZjpEVkhYLi/gh30fWgjRY8Xgbk90jYi3F034GjYgBwR112PW/Rngft9P4N2pRdnivudBtyVojtTuPL+Dm6hDx8ssPM2mG3En3iYApeQ9f0Nhbn3zQpZIX2Ff7+nmUF8VfvNAlXhGBF/i6qwHCFrK1EVbuBrC+RN4Rp+IUrc00PxCBUw7venfIRdDOTLGPAVMG6wmM3O4LuJZ2SFNfRCDLGPKQawH9fQNxMvgFsptkDYT8Do8b6hgDVTG/vT0wzaTPGNdDZbQyZuksKFthz0tAmJ26WAX/IJfMVvADwyROLA0AywDEYufu+ATSKVKbysRKATtDRWYUMCdAqa0IXSNUSFEVKIhd9szdupwN1F/E1g3k499LLpJ7Xpb6UuduX7QGk2s3GohMI7vmV2KeKfnH0gN1ko2iQy7t8U5ryNR6DufcbOlESKd2SvVGYkehW2kHaMzCLdGWyIh5cZisZNoFaCDr2vXiFCYqnMI8lqSBY+iQY1Q/qbKPz9307ETr8MznGIL8E7fubpr0bhZQQztVaKrW6t4W6J5jGtVJbOXrgUFfaAe1CldjdbFW5aYQ+2J3Gp3k+kVJ+TcTBPyNdb9aK9pwjVdk2yUNIhqEvbls5vmWIhbbusyWcw3fBP881KKtuVMgxDKV1bhavN/DSl6NCFJCh2VTWvql0RTCE3YxiGYRiG+Rfxl0meB1fyPFn+HTlpEsRVvc/KVSpcpaILdkPzRylXpKsy26+reIpZalJUx4+tGzXVxMIRbQWiEM6iqTQiNz0fq2IiQvPd8WwpN3woCF8jnNBVVnncka6H/aI+29FjsaulM7Kpvs5yKeVDe+BHi/9lStstazIHvr7w443z/C2Z3irDaLGJqUxlnIUuSHdPV2YEduFOGznoqYB3IsMNqrn61TYC76IUbedY1prv3TGuAVuWxHlMKDhoPyXTH4THhILziPoaHHUeU2N+8MbV1yDUYTRb3UfwN7ifaoz2o/icnRzHvzxDjtAWKynhb/6+IiqBK5AKyUDvCAX51M6yhL4x2gW7BGt2Ugyoi0wiHKBDDfUYzWi6oUAOgWcULPSGbb5NnT/ouJp55MpwaExSGkvwjpMaDRs5ER/zJ8IxmMTlrfuBmIiFMYkJSYHNLBoy1CW5NXhDpGZi/2r8SqkrjpFXTA54pcR7pIFrpjWlQP+IPTi7Keikas8Zehvap+pk7ohhyU0G349tKMM6KRu4AgPPoEs29Gewwekv8EirnmhD9n4+IcHddOpO1Dd7G6Hhuhn6OhvQ1jpm8fpVGcBdvEzSryPYkna69pOoT5ExQmtSc/RqcjpGi2Bz9Hhe4DQlI73UGPoJOPgzR2bp8WjStIy0h5lOKBhe0e5dV03JkzbISlPhCI26zaId9LfYI9ZGs62UP4266U+k3m7GCH26TaPZ9xv8WUPzaD6UuJuaK71Yqd52TTW9dRhWWgrnE5xDvVMorJAgrJAV0ocVskL6sEJWSB9WyArpwwpZIX1YISukDytkhfRhhayQPqyQFdKHFbJC+ugqtMXUsPUU7s4fU+NM+vEWhmEYhmEY5jX/ASVYkKOp66h3AAAAAElFTkSuQmCC'}}\"\r\n            style=\"height: 100px; width: 100px\" />\r\n          <img alt=\"line\" class=\"blueline\" src=\"assets/images/blueline.png\" />\r\n        </ion-col>\r\n        <ion-col style=\"display: flex; justify-content: center; flex-direction: column\">\r\n          <h2 style=\"text-align: left; font-size: larger\">\r\n            {{job.customer_name}}\r\n          </h2>\r\n          <span style=\"color: white\" class=\"serviceType\">{{job.serviceType}}\r\n          </span>\r\n        </ion-col>\r\n      </ion-row>\r\n\r\n      <p style=\"color: white\">\r\n        <span style=\"color: rgb(120 120 241)\">Start</span> : {{job.pickup.add}}\r\n      </p>\r\n      <p style=\"color: white\" *ngIf=\"job.drop\">\r\n        <span style=\"color: rgb(120 120 241)\">End</span> : {{job.drop.add}}\r\n      </p>\r\n      <p style=\"color: white\" *ngIf=\"job.serviceType == 'Personal driver' \">\r\n        <span style=\"color: rgb(120 120 241)\">Trip Time</span> :\r\n        {{ job.triptime > 60 ? ((job.triptime / 60).toString().substring(0,1) + ' hour(s) ' + job.triptime %\r\n        60 +'\r\n        minutes') : job.triptime + ' minutes.' }}\r\n\r\n      </p>\r\n      <!--<p><span>Car Type</span> : {{job.transmission}}</p>-->\r\n      <p class=\"detail\" style=\"margin-left: -1%; color: white; font-size: large\">\r\n        <span style=\"color: rgb(120 120 241)\">Current Bid :</span>\r\n        <span><strong *ngIf=\"job.hasOwnProperty('currentBid')\">${{job.currentBid.toFixed(2)}}\r\n          </strong></span>\r\n      </p>\r\n\r\n      <p style=\"color: white; margin-bottom: 5px\">Your Bid</p>\r\n      <ion-card class=\"customcard detail-card\">\r\n        <section>\r\n          <ion-grid class=\"no-padding\">\r\n            <ion-row style=\"height: 40px\">\r\n              <ion-col style=\"background: gray\" (click)=\"changeBid('dec',job)\" class=\"addOn\" size=\"1.6\">\r\n                <ion-icon src=\"/assets/images/remove-outline.svg\"></ion-icon>\r\n              </ion-col>\r\n\r\n              <ion-col class=\"no-padding\">\r\n                <!--[ngClass]=\"setMyClasses(job)\"-->\r\n                <!--<p class=\"bid-rate\"  style='line-height:34px;'><strong>${{job.hasOwnProperty('yourBid')?job.yourBid:job.estimate}}  </strong></p>-->\r\n                <p class=\"bid-rate\" style=\"color: black; font-size: medium\">\r\n                  <strong>${{job.estimate.toFixed(2)}} </strong>\r\n                </p>\r\n                <!-- <input class=\"field\" type=\"text\" name=\"Bid Price\" placeholder=\"Name\" [(ngModel)]=\"biddingLimit.biddingPrice\"> -->\r\n              </ion-col>\r\n\r\n              <ion-col style=\"background: gray\" class=\"addOn\" (click)=\"changeBid('inc',job)\" size=\"1.6\">\r\n                <ion-icon src=\"/assets/images/add-outline.svg\"></ion-icon>\r\n              </ion-col>\r\n            </ion-row>\r\n          </ion-grid>\r\n        </section>\r\n      </ion-card>\r\n      <button style=\"background: var(--ion-color-secondary)\" class=\"submit-bid\" (click)=\"okSchedule(i)\"\r\n        *ngIf=\"!job.hasOwnProperty('yourBid') \">\r\n        Submit Your Bid\r\n      </button>\r\n      <button style=\"background: gray\" class=\"submit-bid\" (click)=\"okSchedule(i)\"\r\n        *ngIf=\" job.hasOwnProperty('yourBid') \" style=\"background-color: gray\">\r\n        Bid Submitted\r\n      </button>\r\n    </ion-card-content>\r\n  </ion-card>\r\n</ion-content>");

/***/ })

}]);
//# sourceMappingURL=src_app_Pages_tabs_schedule_schedule_module_ts.js.map