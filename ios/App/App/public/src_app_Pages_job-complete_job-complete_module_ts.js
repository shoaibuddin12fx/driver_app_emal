(self["webpackChunkDriverApp"] = self["webpackChunkDriverApp"] || []).push([["src_app_Pages_job-complete_job-complete_module_ts"],{

/***/ 80001:
/*!*************************************************************************!*\
  !*** ./node_modules/ionic-rating/__ivy_ngcc__/fesm2015/ionic-rating.js ***!
  \*************************************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "IonRatingComponent": () => (/* binding */ IonRatingComponent),
/* harmony export */   "IonicRatingModule": () => (/* binding */ IonicRatingModule)
/* harmony export */ });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ 37716);
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/forms */ 3679);
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/common */ 38583);
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @ionic/angular */ 80476);





/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,constantProperty,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */




function IonRatingComponent_ion_button_0_Template(rf, ctx) { if (rf & 1) {
    const _r3 = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵgetCurrentView"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](0, "ion-button", 1);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵlistener"]("mouseenter", function IonRatingComponent_ion_button_0_Template_ion_button_mouseenter_0_listener() { const restoredCtx = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵrestoreView"](_r3); const i_r1 = restoredCtx.$implicit; const ctx_r2 = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵnextContext"](); return ctx_r2.onMouseEnter(i_r1); })("click", function IonRatingComponent_ion_button_0_Template_ion_button_click_0_listener() { const restoredCtx = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵrestoreView"](_r3); const i_r1 = restoredCtx.$implicit; const ctx_r4 = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵnextContext"](); return ctx_r4.onClick(i_r1); });
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](1, "ion-icon", 2);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
} if (rf & 2) {
    const i_r1 = ctx.$implicit;
    const ctx_r0 = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵnextContext"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵclassProp"]("filled", i_r1 <= ctx_r0.hoverRate || !ctx_r0.hoverRate && i_r1 <= ctx_r0.rate)("readonly", ctx_r0.readonly);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("disabled", ctx_r0.disabled || ctx_r0.readonly);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](1);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("size", ctx_r0.size);
} }
const _c0 = function () { return [1, 2, 3, 4, 5]; };
class IonRatingComponent {
    /**
     * @param {?} cd
     */
    constructor(cd) {
        this.cd = cd;
        this.hover = new _angular_core__WEBPACK_IMPORTED_MODULE_0__.EventEmitter();
        this.leave = new _angular_core__WEBPACK_IMPORTED_MODULE_0__.EventEmitter();
        this.rateChange = new _angular_core__WEBPACK_IMPORTED_MODULE_0__.EventEmitter();
    }
    /**
     * @param {?} changes
     * @return {?}
     */
    ngOnChanges(changes) {
        if (changes.rate) {
            this.update(this.rate);
        }
    }
    /**
     * @private
     * @param {?} value
     * @param {?=} internalChange
     * @return {?}
     */
    update(value, internalChange = true) {
        if (!(this.readonly || this.disabled || this.rate === value)) {
            this.rate = value;
            this.rateChange.emit(this.rate);
        }
        if (internalChange) {
            if (this.onChange) {
                this.onChange(this.rate);
            }
            if (this.onTouched) {
                this.onTouched();
            }
        }
    }
    /**
     * @param {?} rate
     * @return {?}
     */
    onClick(rate) {
        this.update(this.resettable && this.rate === rate ? 0 : rate);
    }
    /**
     * @param {?} value
     * @return {?}
     */
    onMouseEnter(value) {
        if (!(this.disabled || this.readonly)) {
            this.hoverRate = value;
        }
        this.hover.emit(value);
    }
    /**
     * @return {?}
     */
    onMouseLeave() {
        this.leave.emit(this.hoverRate);
        this.hoverRate = 0;
    }
    /**
     * @return {?}
     */
    onBlur() {
        if (this.onTouched) {
            this.onTouched();
        }
    }
    /**
     * @param {?} value
     * @return {?}
     */
    writeValue(value) {
        this.update(value, false);
        this.cd.markForCheck();
    }
    /**
     * @param {?} fn
     * @return {?}
     */
    registerOnChange(fn) {
        this.onChange = fn;
    }
    /**
     * @param {?} fn
     * @return {?}
     */
    registerOnTouched(fn) {
        this.onTouched = fn;
    }
    /**
     * @param {?} isDisabled
     * @return {?}
     */
    setDisabledState(isDisabled) {
        this.disabled = isDisabled;
    }
}
IonRatingComponent.ɵfac = function IonRatingComponent_Factory(t) { return new (t || IonRatingComponent)(_angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵdirectiveInject"](_angular_core__WEBPACK_IMPORTED_MODULE_0__.ChangeDetectorRef)); };
IonRatingComponent.ɵcmp = /*@__PURE__*/ _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵdefineComponent"]({ type: IonRatingComponent, selectors: [["ion-rating"]], hostBindings: function IonRatingComponent_HostBindings(rf, ctx) { if (rf & 1) {
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵlistener"]("mouseleave", function IonRatingComponent_mouseleave_HostBindingHandler() { return ctx.onMouseLeave(); })("blur", function IonRatingComponent_blur_HostBindingHandler() { return ctx.onBlur(); });
    } }, inputs: { rate: "rate", readonly: "readonly", resettable: "resettable", size: "size" }, outputs: { hover: "hover", leave: "leave", rateChange: "rateChange" }, features: [_angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵProvidersFeature"]([
            {
                provide: _angular_forms__WEBPACK_IMPORTED_MODULE_1__.NG_VALUE_ACCESSOR,
                useExisting: (0,_angular_core__WEBPACK_IMPORTED_MODULE_0__.forwardRef)(( /**
                 * @return {?}
                 */() => IonRatingComponent)),
                multi: true
            }
        ]), _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵNgOnChangesFeature"]], decls: 1, vars: 2, consts: [["type", "button", "fill", "clear", 3, "disabled", "filled", "readonly", "mouseenter", "click", 4, "ngFor", "ngForOf"], ["type", "button", "fill", "clear", 3, "disabled", "mouseenter", "click"], ["slot", "icon-only", "name", "star", 3, "size"]], template: function IonRatingComponent_Template(rf, ctx) { if (rf & 1) {
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtemplate"](0, IonRatingComponent_ion_button_0_Template, 2, 6, "ion-button", 0);
    } if (rf & 2) {
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("ngForOf", _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵpureFunction0"](1, _c0));
    } }, directives: [_angular_common__WEBPACK_IMPORTED_MODULE_2__.NgForOf, _ionic_angular__WEBPACK_IMPORTED_MODULE_3__.IonButton, _ionic_angular__WEBPACK_IMPORTED_MODULE_3__.IonIcon], styles: ["[_nghost-%COMP%]{--star-color:gray;--star-color-filled:orange;display:inline-block}ion-button[_ngcontent-%COMP%]{--padding-start:0;--padding-end:0;--color:var(--star-color);--color-focused:var(--star-color);--color-activated:var(--star-color)}ion-button.filled[_ngcontent-%COMP%]{--color:var(--star-color-filled);--color-focused:var(--star-color-filled);--color-activated:var(--star-color-filled)}ion-button.readonly[_ngcontent-%COMP%]{--opacity:1}"], changeDetection: 0 });
/** @nocollapse */
IonRatingComponent.ctorParameters = () => [
    { type: _angular_core__WEBPACK_IMPORTED_MODULE_0__.ChangeDetectorRef }
];
IonRatingComponent.propDecorators = {
    rate: [{ type: _angular_core__WEBPACK_IMPORTED_MODULE_0__.Input }],
    readonly: [{ type: _angular_core__WEBPACK_IMPORTED_MODULE_0__.Input }],
    resettable: [{ type: _angular_core__WEBPACK_IMPORTED_MODULE_0__.Input }],
    size: [{ type: _angular_core__WEBPACK_IMPORTED_MODULE_0__.Input }],
    hover: [{ type: _angular_core__WEBPACK_IMPORTED_MODULE_0__.Output }],
    leave: [{ type: _angular_core__WEBPACK_IMPORTED_MODULE_0__.Output }],
    rateChange: [{ type: _angular_core__WEBPACK_IMPORTED_MODULE_0__.Output }],
    onMouseLeave: [{ type: _angular_core__WEBPACK_IMPORTED_MODULE_0__.HostListener, args: ['mouseleave', [],] }],
    onBlur: [{ type: _angular_core__WEBPACK_IMPORTED_MODULE_0__.HostListener, args: ['blur', [],] }]
};
(function () { (typeof ngDevMode === "undefined" || ngDevMode) && _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵsetClassMetadata"](IonRatingComponent, [{
        type: _angular_core__WEBPACK_IMPORTED_MODULE_0__.Component,
        args: [{
                selector: 'ion-rating',
                template: "<ion-button *ngFor=\"let i of [1, 2, 3, 4, 5]\" type=\"button\" fill=\"clear\" [disabled]=\"disabled || readonly\"\n  (mouseenter)=\"onMouseEnter(i)\" (click)=\"onClick(i)\" [class.filled]=\"i <= hoverRate || (!hoverRate && i <= rate)\"\n  [class.readonly]=\"readonly\">\n  <ion-icon slot=\"icon-only\" name=\"star\" [size]=\"size\">\n  </ion-icon>\n</ion-button>",
                changeDetection: _angular_core__WEBPACK_IMPORTED_MODULE_0__.ChangeDetectionStrategy.OnPush,
                providers: [
                    {
                        provide: _angular_forms__WEBPACK_IMPORTED_MODULE_1__.NG_VALUE_ACCESSOR,
                        useExisting: (0,_angular_core__WEBPACK_IMPORTED_MODULE_0__.forwardRef)(( /**
                         * @return {?}
                         */() => IonRatingComponent)),
                        multi: true
                    }
                ],
                styles: [":host{--star-color:gray;--star-color-filled:orange;display:inline-block}ion-button{--padding-start:0;--padding-end:0;--color:var(--star-color);--color-focused:var(--star-color);--color-activated:var(--star-color)}ion-button.filled{--color:var(--star-color-filled);--color-focused:var(--star-color-filled);--color-activated:var(--star-color-filled)}ion-button.readonly{--opacity:1}"]
            }]
    }], function () { return [{ type: _angular_core__WEBPACK_IMPORTED_MODULE_0__.ChangeDetectorRef }]; }, { hover: [{
            type: _angular_core__WEBPACK_IMPORTED_MODULE_0__.Output
        }], leave: [{
            type: _angular_core__WEBPACK_IMPORTED_MODULE_0__.Output
        }], rateChange: [{
            type: _angular_core__WEBPACK_IMPORTED_MODULE_0__.Output
        }], rate: [{
            type: _angular_core__WEBPACK_IMPORTED_MODULE_0__.Input
        }], 
    /**
     * @return {?}
     */
    onMouseLeave: [{
            type: _angular_core__WEBPACK_IMPORTED_MODULE_0__.HostListener,
            args: ['mouseleave', []]
        }], 
    /**
     * @return {?}
     */
    onBlur: [{
            type: _angular_core__WEBPACK_IMPORTED_MODULE_0__.HostListener,
            args: ['blur', []]
        }], readonly: [{
            type: _angular_core__WEBPACK_IMPORTED_MODULE_0__.Input
        }], resettable: [{
            type: _angular_core__WEBPACK_IMPORTED_MODULE_0__.Input
        }], size: [{
            type: _angular_core__WEBPACK_IMPORTED_MODULE_0__.Input
        }] }); })();

/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,constantProperty,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
class IonicRatingModule {
}
IonicRatingModule.ɵfac = function IonicRatingModule_Factory(t) { return new (t || IonicRatingModule)(); };
IonicRatingModule.ɵmod = /*@__PURE__*/ _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵdefineNgModule"]({ type: IonicRatingModule });
IonicRatingModule.ɵinj = /*@__PURE__*/ _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵdefineInjector"]({ imports: [[_angular_common__WEBPACK_IMPORTED_MODULE_2__.CommonModule, _angular_forms__WEBPACK_IMPORTED_MODULE_1__.FormsModule, _ionic_angular__WEBPACK_IMPORTED_MODULE_3__.IonicModule]] });
(function () { (typeof ngDevMode === "undefined" || ngDevMode) && _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵsetClassMetadata"](IonicRatingModule, [{
        type: _angular_core__WEBPACK_IMPORTED_MODULE_0__.NgModule,
        args: [{
                imports: [_angular_common__WEBPACK_IMPORTED_MODULE_2__.CommonModule, _angular_forms__WEBPACK_IMPORTED_MODULE_1__.FormsModule, _ionic_angular__WEBPACK_IMPORTED_MODULE_3__.IonicModule],
                exports: [IonRatingComponent],
                declarations: [IonRatingComponent]
            }]
    }], null, null); })();
(function () { (typeof ngJitMode === "undefined" || ngJitMode) && _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵsetNgModuleScope"](IonicRatingModule, { declarations: function () { return [IonRatingComponent]; }, imports: function () { return [_angular_common__WEBPACK_IMPORTED_MODULE_2__.CommonModule, _angular_forms__WEBPACK_IMPORTED_MODULE_1__.FormsModule, _ionic_angular__WEBPACK_IMPORTED_MODULE_3__.IonicModule]; }, exports: function () { return [IonRatingComponent]; } }); })();



//# sourceMappingURL=ionic-rating.js.map

/***/ }),

/***/ 59138:
/*!*******************************************************************!*\
  !*** ./src/app/Pages/job-complete/job-complete-routing.module.ts ***!
  \*******************************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "JobCompletePageRoutingModule": () => (/* binding */ JobCompletePageRoutingModule)
/* harmony export */ });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! tslib */ 64762);
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/core */ 37716);
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/router */ 39895);
/* harmony import */ var _job_complete_page__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./job-complete.page */ 60637);




const routes = [
    {
        path: '',
        component: _job_complete_page__WEBPACK_IMPORTED_MODULE_0__.JobCompletePage
    }
];
let JobCompletePageRoutingModule = class JobCompletePageRoutingModule {
};
JobCompletePageRoutingModule = (0,tslib__WEBPACK_IMPORTED_MODULE_1__.__decorate)([
    (0,_angular_core__WEBPACK_IMPORTED_MODULE_2__.NgModule)({
        imports: [_angular_router__WEBPACK_IMPORTED_MODULE_3__.RouterModule.forChild(routes)],
        exports: [_angular_router__WEBPACK_IMPORTED_MODULE_3__.RouterModule],
    })
], JobCompletePageRoutingModule);



/***/ }),

/***/ 16744:
/*!***********************************************************!*\
  !*** ./src/app/Pages/job-complete/job-complete.module.ts ***!
  \***********************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "JobCompletePageModule": () => (/* binding */ JobCompletePageModule)
/* harmony export */ });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! tslib */ 64762);
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/core */ 37716);
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @angular/common */ 38583);
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @angular/forms */ 3679);
/* harmony import */ var ionic_rating__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! ionic-rating */ 80001);
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! @ionic/angular */ 80476);
/* harmony import */ var _job_complete_routing_module__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./job-complete-routing.module */ 59138);
/* harmony import */ var _job_complete_page__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./job-complete.page */ 60637);








let JobCompletePageModule = class JobCompletePageModule {
};
JobCompletePageModule = (0,tslib__WEBPACK_IMPORTED_MODULE_2__.__decorate)([
    (0,_angular_core__WEBPACK_IMPORTED_MODULE_3__.NgModule)({
        imports: [
            _angular_common__WEBPACK_IMPORTED_MODULE_4__.CommonModule,
            _angular_forms__WEBPACK_IMPORTED_MODULE_5__.FormsModule,
            _ionic_angular__WEBPACK_IMPORTED_MODULE_6__.IonicModule,
            _job_complete_routing_module__WEBPACK_IMPORTED_MODULE_0__.JobCompletePageRoutingModule,
            ionic_rating__WEBPACK_IMPORTED_MODULE_7__.IonicRatingModule,
        ],
        declarations: [_job_complete_page__WEBPACK_IMPORTED_MODULE_1__.JobCompletePage],
    })
], JobCompletePageModule);



/***/ }),

/***/ 60637:
/*!*********************************************************!*\
  !*** ./src/app/Pages/job-complete/job-complete.page.ts ***!
  \*********************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "JobCompletePage": () => (/* binding */ JobCompletePage)
/* harmony export */ });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! tslib */ 64762);
/* harmony import */ var _raw_loader_job_complete_page_html__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! !raw-loader!./job-complete.page.html */ 1194);
/* harmony import */ var _job_complete_page_scss__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./job-complete.page.scss */ 18610);
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/core */ 37716);
/* harmony import */ var _base_page_base_page__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../base-page/base-page */ 96093);





let JobCompletePage = class JobCompletePage extends _base_page_base_page__WEBPACK_IMPORTED_MODULE_2__.BasePage {
    constructor(injector) {
        super(injector);
        this.customerRating = 0;
        let self = this;
        let params = this.getQueryParams();
        this.bookingKey = params === null || params === void 0 ? void 0 : params.bookingKey;
        let ref = this.firebaseService.getDbRef('bookings/' + this.bookingKey);
        ref.on('value', (snapshot) => {
            var _a;
            if (snapshot.val()) {
                self.data = snapshot.val();
                /*if(self.data.distance != undefined){
                    self.tripDistance = (self.data.distance/1000).toFixed(1);
                }*/
                if (self.data.trip_cost != undefined) {
                    self.tripCost = self.data.trip_cost;
                }
                // if (self.data.ratings != undefined) {
                // }
                console.log('DATA', self.data);
                self.customerRating = (_a = self.data.ratings) === null || _a === void 0 ? void 0 : _a.rating;
            }
        });
    }
    ngOnInit() { }
    ionViewDidLoad() {
        console.log('ionViewDidLoad JobcompletePage');
    }
};
JobCompletePage.ctorParameters = () => [
    { type: _angular_core__WEBPACK_IMPORTED_MODULE_3__.Injector }
];
JobCompletePage = (0,tslib__WEBPACK_IMPORTED_MODULE_4__.__decorate)([
    (0,_angular_core__WEBPACK_IMPORTED_MODULE_3__.Component)({
        selector: 'app-job-complete',
        template: _raw_loader_job_complete_page_html__WEBPACK_IMPORTED_MODULE_0__.default,
        styles: [_job_complete_page_scss__WEBPACK_IMPORTED_MODULE_1__.default]
    })
], JobCompletePage);



/***/ }),

/***/ 18610:
/*!***********************************************************!*\
  !*** ./src/app/Pages/job-complete/job-complete.page.scss ***!
  \***********************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "default": () => (__WEBPACK_DEFAULT_EXPORT__)
/* harmony export */ });
/* harmony default export */ const __WEBPACK_DEFAULT_EXPORT__ = (".review-logo {\n  width: 60%;\n  display: block;\n  margin: 5% auto;\n}\n\n.rate {\n  color: #ffae00;\n  font-size: 25px;\n  margin: 0 auto;\n  width: auto;\n  margin-bottom: 20px;\n}\n\n.rate ul {\n  margin: 0 auto !important;\n  padding: 0;\n  display: block;\n  width: 80%;\n}\n\n.spantext-color-review {\n  color: #4267b2;\n  font-weight: bold;\n}\n\n.review-card-driver {\n  background: gray;\n  font-size: 2rem;\n  text-align: center;\n}\n\n.rate h2 {\n  text-align: center;\n  font-size: 1.4rem;\n  color: #000;\n  padding: 0;\n  margin: 0;\n}\n\n.text {\n  color: white;\n}\n\n.wow {\n  background-color: #1E1E1E;\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbImpvYi1jb21wbGV0ZS5wYWdlLnNjc3MiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IkFBQUE7RUFDRSxVQUFBO0VBQ0EsY0FBQTtFQUNBLGVBQUE7QUFDRjs7QUFDQTtFQUNFLGNBQUE7RUFDQSxlQUFBO0VBQ0EsY0FBQTtFQUNBLFdBQUE7RUFDQSxtQkFBQTtBQUVGOztBQUFBO0VBQ0UseUJBQUE7RUFDQSxVQUFBO0VBQ0EsY0FBQTtFQUNBLFVBQUE7QUFHRjs7QUFEQTtFQUNFLGNBQUE7RUFDQSxpQkFBQTtBQUlGOztBQUZBO0VBQ0UsZ0JBQUE7RUFDQSxlQUFBO0VBQ0Esa0JBQUE7QUFLRjs7QUFIQTtFQUNFLGtCQUFBO0VBQ0EsaUJBQUE7RUFDQSxXQUFBO0VBQ0EsVUFBQTtFQUNBLFNBQUE7QUFNRjs7QUFKQTtFQUNFLFlBQUE7QUFPRjs7QUFMQTtFQUNFLHlCQUFBO0FBUUYiLCJmaWxlIjoiam9iLWNvbXBsZXRlLnBhZ2Uuc2NzcyIsInNvdXJjZXNDb250ZW50IjpbIi5yZXZpZXctbG9nbyB7XHJcbiAgd2lkdGg6IDYwJTtcclxuICBkaXNwbGF5OiBibG9jaztcclxuICBtYXJnaW46IDUlIGF1dG87XHJcbn1cclxuLnJhdGUge1xyXG4gIGNvbG9yOiAjZmZhZTAwO1xyXG4gIGZvbnQtc2l6ZTogMjVweDtcclxuICBtYXJnaW46IDAgYXV0bztcclxuICB3aWR0aDogYXV0bztcclxuICBtYXJnaW4tYm90dG9tOiAyMHB4O1xyXG59XHJcbi5yYXRlIHVsIHtcclxuICBtYXJnaW46IDAgYXV0byAhaW1wb3J0YW50O1xyXG4gIHBhZGRpbmc6IDA7XHJcbiAgZGlzcGxheTogYmxvY2s7XHJcbiAgd2lkdGg6IDgwJTtcclxufVxyXG4uc3BhbnRleHQtY29sb3ItcmV2aWV3IHtcclxuICBjb2xvcjogIzQyNjdiMjtcclxuICBmb250LXdlaWdodDogYm9sZDtcclxufVxyXG4ucmV2aWV3LWNhcmQtZHJpdmVyIHtcclxuICBiYWNrZ3JvdW5kOiBncmF5O1xyXG4gIGZvbnQtc2l6ZTogMnJlbTtcclxuICB0ZXh0LWFsaWduOiBjZW50ZXI7XHJcbn1cclxuLnJhdGUgaDIge1xyXG4gIHRleHQtYWxpZ246IGNlbnRlcjtcclxuICBmb250LXNpemU6IDEuNHJlbTtcclxuICBjb2xvcjogIzAwMDtcclxuICBwYWRkaW5nOiAwO1xyXG4gIG1hcmdpbjogMDtcclxufVxyXG4udGV4dHtcclxuICBjb2xvcjogd2hpdGU7XHJcbn1cclxuLndvd3tcclxuICBiYWNrZ3JvdW5kLWNvbG9yOiAjMUUxRTFFIDtcclxufSJdfQ== */");

/***/ }),

/***/ 1194:
/*!*************************************************************************************************!*\
  !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/Pages/job-complete/job-complete.page.html ***!
  \*************************************************************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "default": () => (__WEBPACK_DEFAULT_EXPORT__)
/* harmony export */ });
/* harmony default export */ const __WEBPACK_DEFAULT_EXPORT__ = ("<ion-header class=\"wow\">\r\n  <ion-toolbar style=\"color: white\" color=\"headerblue\">\r\n    <ion-buttons slot=\"start\">\r\n      <ion-back-button defaultHref=\"\"></ion-back-button>\r\n    </ion-buttons>\r\n    <ion-title class=\"toptitle\">JOB COMPLETED</ion-title>\r\n  </ion-toolbar>\r\n</ion-header>\r\n\r\n<ion-content>\r\n  <!--<img class=\"review-logo\" src=\"assets/images/supportlogo.png\" alt=\"logo\">-->\r\n  <div class=\"rate\">\r\n    <h2 style=\"margin-top: 10%; color: white\">\r\n      {{customerRating ? \"The passenger rated you with\" : \"You are not reviewed\r\n      by the passenger\"}}\r\n    </h2>\r\n    <!--<h2>You are not reviewed by the passenger</h2>-->\r\n    <div class=\"ion-text-center\">\r\n      <ion-rating [rate]=\"customerRating\" *ngIf=\"customerRating\" readonly=\"true\"></ion-rating>\r\n    </div>\r\n\r\n    <!-- <rating\r\n      name=\"rating\"\r\n      *ngIf=\"customerRating\"\r\n      [(ngModel)]=\"customerRating\"\r\n      max=\"5\"\r\n      readOnly=\"true\"\r\n    ></rating> -->\r\n  </div>\r\n  <!-- <ion-card class=\"review-card-driver\">\r\n    <ion-card-content>\r\n      Total Kilometres covered: <span class=\"spantext-color-review\">{{tripDistance}}KM</span>\r\n    </ion-card-content>\r\n </ion-card>-->\r\n  <ion-card class=\"review-card-driver\">\r\n    <ion-card-content>\r\n      <span class=\"text\">Total Fare :</span>\r\n      <span class=\"spantext-color-review\">\r\n        {{tripCost ? \"$\" + tripCost : \" \"}}</span>\r\n    </ion-card-content>\r\n  </ion-card>\r\n</ion-content>");

/***/ })

}]);
//# sourceMappingURL=src_app_Pages_job-complete_job-complete_module_ts.js.map