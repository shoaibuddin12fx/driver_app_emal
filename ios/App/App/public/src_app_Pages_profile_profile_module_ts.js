(self["webpackChunkDriverApp"] = self["webpackChunkDriverApp"] || []).push([["src_app_Pages_profile_profile_module_ts"],{

/***/ 64021:
/*!****************************************************************!*\
  !*** ./node_modules/@capacitor/camera/dist/esm/definitions.js ***!
  \****************************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "CameraSource": () => (/* binding */ CameraSource),
/* harmony export */   "CameraDirection": () => (/* binding */ CameraDirection),
/* harmony export */   "CameraResultType": () => (/* binding */ CameraResultType)
/* harmony export */ });
var CameraSource;
(function (CameraSource) {
    /**
     * Prompts the user to select either the photo album or take a photo.
     */
    CameraSource["Prompt"] = "PROMPT";
    /**
     * Take a new photo using the camera.
     */
    CameraSource["Camera"] = "CAMERA";
    /**
     * Pick an existing photo fron the gallery or photo album.
     */
    CameraSource["Photos"] = "PHOTOS";
})(CameraSource || (CameraSource = {}));
var CameraDirection;
(function (CameraDirection) {
    CameraDirection["Rear"] = "REAR";
    CameraDirection["Front"] = "FRONT";
})(CameraDirection || (CameraDirection = {}));
var CameraResultType;
(function (CameraResultType) {
    CameraResultType["Uri"] = "uri";
    CameraResultType["Base64"] = "base64";
    CameraResultType["DataUrl"] = "dataUrl";
})(CameraResultType || (CameraResultType = {}));
//# sourceMappingURL=definitions.js.map

/***/ }),

/***/ 37673:
/*!**********************************************************!*\
  !*** ./node_modules/@capacitor/camera/dist/esm/index.js ***!
  \**********************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "CameraDirection": () => (/* reexport safe */ _definitions__WEBPACK_IMPORTED_MODULE_1__.CameraDirection),
/* harmony export */   "CameraResultType": () => (/* reexport safe */ _definitions__WEBPACK_IMPORTED_MODULE_1__.CameraResultType),
/* harmony export */   "CameraSource": () => (/* reexport safe */ _definitions__WEBPACK_IMPORTED_MODULE_1__.CameraSource),
/* harmony export */   "Camera": () => (/* binding */ Camera)
/* harmony export */ });
/* harmony import */ var _capacitor_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @capacitor/core */ 68384);
/* harmony import */ var _definitions__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./definitions */ 64021);

const Camera = (0,_capacitor_core__WEBPACK_IMPORTED_MODULE_0__.registerPlugin)('Camera', {
    web: () => __webpack_require__.e(/*! import() */ "node_modules_capacitor_camera_dist_esm_web_js").then(__webpack_require__.bind(__webpack_require__, /*! ./web */ 14028)).then(m => new m.CameraWeb()),
});


//# sourceMappingURL=index.js.map

/***/ }),

/***/ 20237:
/*!*********************************************************!*\
  !*** ./src/app/Pages/profile/profile-routing.module.ts ***!
  \*********************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "ProfilePageRoutingModule": () => (/* binding */ ProfilePageRoutingModule)
/* harmony export */ });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! tslib */ 64762);
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/core */ 37716);
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/router */ 39895);
/* harmony import */ var _profile_page__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./profile.page */ 81257);




const routes = [
    {
        path: '',
        component: _profile_page__WEBPACK_IMPORTED_MODULE_0__.ProfilePage
    }
];
let ProfilePageRoutingModule = class ProfilePageRoutingModule {
};
ProfilePageRoutingModule = (0,tslib__WEBPACK_IMPORTED_MODULE_1__.__decorate)([
    (0,_angular_core__WEBPACK_IMPORTED_MODULE_2__.NgModule)({
        imports: [_angular_router__WEBPACK_IMPORTED_MODULE_3__.RouterModule.forChild(routes)],
        exports: [_angular_router__WEBPACK_IMPORTED_MODULE_3__.RouterModule],
    })
], ProfilePageRoutingModule);



/***/ }),

/***/ 60824:
/*!*************************************************!*\
  !*** ./src/app/Pages/profile/profile.module.ts ***!
  \*************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "ProfilePageModule": () => (/* binding */ ProfilePageModule)
/* harmony export */ });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! tslib */ 64762);
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/core */ 37716);
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @angular/common */ 38583);
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @angular/forms */ 3679);
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! @ionic/angular */ 80476);
/* harmony import */ var _profile_routing_module__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./profile-routing.module */ 20237);
/* harmony import */ var _profile_page__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./profile.page */ 81257);







let ProfilePageModule = class ProfilePageModule {
};
ProfilePageModule = (0,tslib__WEBPACK_IMPORTED_MODULE_2__.__decorate)([
    (0,_angular_core__WEBPACK_IMPORTED_MODULE_3__.NgModule)({
        imports: [
            _angular_common__WEBPACK_IMPORTED_MODULE_4__.CommonModule,
            _angular_forms__WEBPACK_IMPORTED_MODULE_5__.FormsModule,
            _ionic_angular__WEBPACK_IMPORTED_MODULE_6__.IonicModule,
            _profile_routing_module__WEBPACK_IMPORTED_MODULE_0__.ProfilePageRoutingModule
        ],
        declarations: [_profile_page__WEBPACK_IMPORTED_MODULE_1__.ProfilePage]
    })
], ProfilePageModule);



/***/ }),

/***/ 81257:
/*!***********************************************!*\
  !*** ./src/app/Pages/profile/profile.page.ts ***!
  \***********************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "ProfilePage": () => (/* binding */ ProfilePage)
/* harmony export */ });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! tslib */ 64762);
/* harmony import */ var _raw_loader_profile_page_html__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! !raw-loader!./profile.page.html */ 82447);
/* harmony import */ var _profile_page_scss__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./profile.page.scss */ 44700);
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @angular/core */ 37716);
/* harmony import */ var src_app_Services_camera_service__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! src/app/Services/camera.service */ 34054);
/* harmony import */ var _base_page_base_page__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../base-page/base-page */ 96093);






let ProfilePage = class ProfilePage extends _base_page_base_page__WEBPACK_IMPORTED_MODULE_3__.BasePage {
    constructor(injecor, camera) {
        super(injecor);
        this.camera = camera;
        this.driver = 'driver';
        this.customer = 'customer';
        //upload database
        this.users = [];
        this.usersService.presentLoading();
        this.data = {
            fullname: '',
            email: '',
            phone: '',
            registration: '',
            cityname: '',
        };
    }
    ngOnInit() {
        return (0,tslib__WEBPACK_IMPORTED_MODULE_4__.__awaiter)(this, void 0, void 0, function* () {
            this.curUser = yield this.firebaseService.getCurrentUser();
            console.log('curr_user', this.curUser);
            var ref = this.getDbRef('/users/' + this.curUser.uid);
            ref.on('value', (_snapshot) => {
                if (_snapshot.val()) {
                    this.data = _snapshot.val();
                    this.profileImage = _snapshot.val().profile_image;
                    this.usersService.dismissLoader();
                    //  this.usertype = _snapshot.val().type;
                    // console.log(this.usertype);
                    /*  if(this.usertype==this.customer){
                          this.showCustomer= true;
                      }else if((this.usertype==this.driver)) {
                          this.showCustomer= false;
                      }else{
                        alert("no type found");
                      }*/
                }
                else {
                    this.usersService.dismissLoader();
                    console.log('no type added');
                }
            });
        });
    }
    //constructor end
    doAdd() {
        return (0,tslib__WEBPACK_IMPORTED_MODULE_4__.__awaiter)(this, void 0, void 0, function* () {
            // console.log(this.data,userId);
            // this.usersService.updateData(this.data,this.curUser.uid)
            let profileRef = this.getDbRef('/users/' + this.curUser.uid);
            profileRef.child('fullname').set(this.data.fullname);
            profileRef.child('email').set(this.data.email);
            profileRef.child('phone').set(this.data.phone);
            profileRef.child('cityname').set(this.data.cityname);
            profileRef.child('profile_image').set(this.profileImage);
            let alert = yield this.alertCtrl.create({
                header: 'Success',
                subHeader: 'Profile updated successfully',
                buttons: [
                    {
                        text: 'Ok',
                        handler: () => {
                            this.events.publish('login_event');
                            // this.nav.push('pages/tabs/schedule');
                            this.nav.pop();
                        },
                    },
                ],
            });
            alert.present();
        });
    }
    //open camera picture
    // openCamera() {
    //   this.camera.
    //     Camera.getPicture(cameraOptions).then((imageData) => {
    //       this.captureDataUrl = 'data:image/jpeg;base64,' + imageData;
    //       console.log("CaptureDataUrl:" + this.captureDataUrl);
    //       this.upload();
    //     }, (err) => {
    //     });
    // }
    // //open galary picture
    // openGalary() {
    //   const cameraOptions: CameraOptions = {
    //     targetHeight: 150,
    //     targetWidth: 150,
    //     allowEdit: true,
    //     quality: 50,
    //     destinationType: Camera.DestinationType.DATA_URL,
    //     encodingType: Camera.EncodingType.JPEG,
    //     mediaType: Camera.MediaType.PICTURE,
    //     sourceType: Camera.PictureSourceType.PHOTOLIBRARY,
    //   };
    //   Camera.getPicture(cameraOptions).then((imageData) => {
    //     this.captureDataUrl = 'data:image/jpeg;base64,' + imageData;
    //     console.log("CaptureDataUrl:" + this.captureDataUrl);
    //     this.upload();
    //   }, (err) => {
    //   });
    // }
    //upload firebase
    upload() {
        this.profileImage = this.captureDataUrl;
        return;
        let storageRef = this.firebaseService.fireStorage.ref('/');
        const filename = Math.floor(Date.now() / 1000);
        const imageRef = storageRef.child(`images/${filename}.jpg`);
        console.log('Upload Const ImageRef:' + imageRef);
        //*TODO*// firebase.storage.StringFormat.DATA_URL
        imageRef.putString(this.captureDataUrl).then((snapshot) => {
            console.log('SnapShot:' + JSON.stringify(snapshot.ref.getDownloadURL()));
            this.saveDatabase(snapshot);
        });
    }
    saveDatabase(snapshot) {
        return (0,tslib__WEBPACK_IMPORTED_MODULE_4__.__awaiter)(this, void 0, void 0, function* () {
            let imgUrl = snapshot.ref.getDownloadURL();
            var ref = this.getDbRef('users/' + this.curUser.uid);
            ref.child('profile_image').set(snapshot.downloadURL);
            let user = yield this.firebaseService.getCurrentUser();
            var ref = this.getDbRef('/users/' + user.uid + '/bookings');
            // var bookingref = this.getDbRef('bookings');
            ref.on('value', (snap1) => {
                let userdata = snap1.val();
                for (var key in userdata) {
                    userdata[key].uid = key;
                    if (key != user.uid) {
                        this.users.push(userdata[key]);
                    }
                }
                this.bookingsUpdateData(this.users, imgUrl);
            });
        });
    }
    bookingsUpdateData(userIds, imgUrl) {
        return (0,tslib__WEBPACK_IMPORTED_MODULE_4__.__awaiter)(this, void 0, void 0, function* () {
            let currentuser = yield this.firebaseService.getCurrentUser();
            var ref = this.getDbRef('/users/' + currentuser.uid);
            ref.once('value', (snap2) => {
                let userType = snap2.val().type;
                if (userType == 'customer') {
                    for (let i = 0; i < userIds.length; i++) {
                        console.log(userIds[i].uid);
                        var bookingref = this.getDbRef('/bookings/' + userIds[i].uid);
                        let userBookings = this.getDbRef('/users/' + userIds[i].driver + '/bookings/' + userIds[i].uid);
                        bookingref.child('customer_image').set(imgUrl);
                        userBookings.child('customer_image').set(imgUrl);
                    }
                }
                else {
                    for (let i = 0; i < userIds.length; i++) {
                        // let id = userIds[i].uid;
                        var bookingref = this.getDbRef('/bookings/' + userIds[i].uid);
                        let userBookings = this.getDbRef('/users/' + userIds[i].customer + '/bookings/' + userIds[i].uid);
                        bookingref.child('driver_image').set(imgUrl);
                        userBookings.child('driver_image').set(imgUrl);
                    }
                }
            });
        });
    }
    //option Select Camera or Album
    doGetPicture() {
        return (0,tslib__WEBPACK_IMPORTED_MODULE_4__.__awaiter)(this, void 0, void 0, function* () {
            let base64Image = yield this.camera.selectImage();
            if (base64Image && base64Image !== '') {
                this.captureDataUrl = base64Image;
                console.log(this.captureDataUrl);
                this.upload();
            }
        });
    }
    //End Camera and Album related work
    getDbRef(ref) {
        return this.firebaseService.getDbRef(ref);
    }
};
ProfilePage.ctorParameters = () => [
    { type: _angular_core__WEBPACK_IMPORTED_MODULE_5__.Injector },
    { type: src_app_Services_camera_service__WEBPACK_IMPORTED_MODULE_2__.CameraService }
];
ProfilePage = (0,tslib__WEBPACK_IMPORTED_MODULE_4__.__decorate)([
    (0,_angular_core__WEBPACK_IMPORTED_MODULE_5__.Component)({
        selector: 'app-profile',
        template: _raw_loader_profile_page_html__WEBPACK_IMPORTED_MODULE_0__.default,
        styles: [_profile_page_scss__WEBPACK_IMPORTED_MODULE_1__.default]
    })
], ProfilePage);



/***/ }),

/***/ 34054:
/*!********************************************!*\
  !*** ./src/app/Services/camera.service.ts ***!
  \********************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "CameraService": () => (/* binding */ CameraService)
/* harmony export */ });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! tslib */ 64762);
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/core */ 37716);
/* harmony import */ var _capacitor_camera__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @capacitor/camera */ 37673);
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @ionic/angular */ 80476);




let CameraService = class CameraService {
    constructor(actionSheetController) {
        this.actionSheetController = actionSheetController;
    }
    selectImage() {
        return (0,tslib__WEBPACK_IMPORTED_MODULE_1__.__awaiter)(this, void 0, void 0, function* () {
            return new Promise((res) => (0,tslib__WEBPACK_IMPORTED_MODULE_1__.__awaiter)(this, void 0, void 0, function* () {
                const actionSheet = yield this.actionSheetController.create({
                    // header: "Select Image source",
                    cssClass: 'select-image-css',
                    buttons: [{
                            // text: 'Choose photo from Gallery',
                            text: "Choose from gallery",
                            icon: 'images',
                            cssClass: 'images-icon',
                            handler: () => (0,tslib__WEBPACK_IMPORTED_MODULE_1__.__awaiter)(this, void 0, void 0, function* () {
                                res(yield this.pickImage(_capacitor_camera__WEBPACK_IMPORTED_MODULE_0__.CameraSource.Photos));
                            })
                        },
                        {
                            // text: 'Take photo',
                            text: "Take photo",
                            icon: 'camera',
                            cssClass: 'camera-icon',
                            handler: () => (0,tslib__WEBPACK_IMPORTED_MODULE_1__.__awaiter)(this, void 0, void 0, function* () {
                                res(yield this.pickImage(_capacitor_camera__WEBPACK_IMPORTED_MODULE_0__.CameraSource.Camera));
                            })
                        }, {
                            // text: 'Cancel',
                            text: "Cancel",
                            role: 'cancel',
                            icon: 'close',
                            cssClass: 'close-icon',
                            handler: () => {
                                res(null);
                            }
                        }]
                });
                yield actionSheet.present();
            }));
        });
    }
    pickImage(source) {
        return new Promise(res => {
            const options = {
                height: 150,
                width: 150,
                allowEditing: true,
                quality: 50,
                resultType: _capacitor_camera__WEBPACK_IMPORTED_MODULE_0__.CameraResultType.DataUrl,
                source: source,
            };
            _capacitor_camera__WEBPACK_IMPORTED_MODULE_0__.Camera.getPhoto(options).then((imageData) => {
                let base64Image = imageData.dataUrl;
                res(base64Image);
            }, (err) => {
                res(null);
                // Handle error
            });
        });
    }
};
CameraService.ctorParameters = () => [
    { type: _ionic_angular__WEBPACK_IMPORTED_MODULE_2__.ActionSheetController }
];
CameraService = (0,tslib__WEBPACK_IMPORTED_MODULE_1__.__decorate)([
    (0,_angular_core__WEBPACK_IMPORTED_MODULE_3__.Injectable)({
        providedIn: 'root',
    })
], CameraService);



/***/ }),

/***/ 44700:
/*!*************************************************!*\
  !*** ./src/app/Pages/profile/profile.page.scss ***!
  \*************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "default": () => (__WEBPACK_DEFAULT_EXPORT__)
/* harmony export */ });
/* harmony default export */ const __WEBPACK_DEFAULT_EXPORT__ = (".darkbutton, .bluebutton, .redbutton, .lightgreenbutton, .blackbutton {\n  padding: 25px 110px;\n  margin: 10px auto;\n  display: block;\n  background: #2e2e2e;\n  font-weight: normal;\n  text-transform: none;\n  color: #fff;\n  font-family: \"PT Sans\";\n  font-size: 19px;\n}\n\n.back {\n  background: url(\"/assets/images/background.png\") repeat;\n}\n\n.loginimage {\n  width: 30% !important;\n  display: block;\n  margin: 0 auto;\n  padding-bottom: 10px;\n}\n\n.formwrapper, .profileformwrapper {\n  width: 90%;\n  margin: 0 auto;\n  overflow: hidden;\n  height: auto;\n}\n\n.field {\n  padding: 15px;\n  background: #f4f4f4;\n  border: 1px solid #d4d4d4;\n  border-radius: 25px;\n  color: #8d8d8d;\n  font-family: \"PT Sans\";\n  font-size: 16px;\n  width: 100%;\n  display: block;\n  margin-bottom: 15px;\n}\n\n.blackbutton {\n  width: 100%;\n  margin-top: 15px;\n  border-radius: 50px;\n}\n\n.clearbutton, .buttonfont {\n  display: block;\n  margin: 0 auto;\n  text-transform: none !important;\n  height: 1.5em;\n}\n\n.buttonfont {\n  font-size: 16px;\n}\n\n.lightgreenbutton {\n  background: #09738d !important;\n  margin-bottom: 25px !important;\n  font-size: 15px;\n  font-weight: bold;\n  margin-top: 10px;\n  border-radius: 50px;\n  white-space: nowrap;\n  padding: 15px 32px;\n}\n\n.redbutton {\n  background: #eb2020 !important;\n  padding: 25px 95px !important;\n  width: 100%;\n}\n\n.bluebutton {\n  width: 100%;\n  background: #3b5998 !important;\n  margin-bottom: 25px !important;\n}\n\n.alert-md .alert-head {\n  padding: 10px;\n  text-align: center;\n  color: #000;\n  background: none;\n  font-family: \"PT Sans\";\n}\n\n.alert-md .alert-title {\n  font-size: 18px;\n  text-transform: uppercase;\n}\n\n.alert-md .alert-button {\n  padding: 0;\n  margin: 0 auto;\n  display: block;\n}\n\n.clearbutton-text {\n  text-align: center;\n  font-family: \"PT Sans\";\n  font-size: 16px;\n  color: #8d8d8d;\n  width: 100%;\n}\n\n@media (min-width: 568px) {\n  .loginimage {\n    width: 15% !important;\n  }\n}\n\n.bannerprofile {\n  width: 100%;\n  background: url(\"/assets/images/profileback.png\") no-repeat;\n  background-size: cover;\n  padding-top: 20px;\n  margin-bottom: 20px;\n}\n\n.bannerprofile img {\n  display: block;\n  margin: 0 auto;\n  border-radius: 50px;\n  width: 100px;\n  height: 100px;\n  border: 1px solid #fff;\n}\n\n.bannerprofile p {\n  color: #fff;\n  margin: 0;\n  padding: 5px 0 18px 0;\n  font-family: \"PT Sans\";\n  font-size: 23px;\n  text-align: center;\n}\n\n.bannerprofile p span {\n  font-size: 16px;\n}\n\n.profileformwrapper {\n  width: 85%;\n  padding-bottom: 30px;\n}\n\n.header {\n  background-color: #1E1E1E;\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIi4uXFxsb2dpblxcbG9naW4ucGFnZS5zY3NzIiwicHJvZmlsZS5wYWdlLnNjc3MiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IkFBQUE7RUFDRSxtQkFBQTtFQUNBLGlCQUFBO0VBQ0EsY0FBQTtFQUNBLG1CQUFBO0VBQ0EsbUJBQUE7RUFDQSxvQkFBQTtFQUNBLFdBQUE7RUFDQSxzQkFBQTtFQUNBLGVBQUE7QUNDRjs7QURFQTtFQUNFLHVEQUFBO0FDQ0Y7O0FEQ0E7RUFDRSxxQkFBQTtFQUNBLGNBQUE7RUFDQSxjQUFBO0VBQ0Esb0JBQUE7QUNFRjs7QURDQTtFQUNFLFVBQUE7RUFDQSxjQUFBO0VBQ0EsZ0JBQUE7RUFDQSxZQUFBO0FDRUY7O0FEQ0E7RUFDRSxhQUFBO0VBQ0EsbUJBQUE7RUFDQSx5QkFBQTtFQUNBLG1CQUFBO0VBQ0EsY0FBQTtFQUNBLHNCQUFBO0VBQ0EsZUFBQTtFQUNBLFdBQUE7RUFDQSxjQUFBO0VBQ0EsbUJBQUE7QUNFRjs7QURDQTtFQUVFLFdBQUE7RUFDQSxnQkFBQTtFQUNBLG1CQUFBO0FDQ0Y7O0FEQ0E7RUFDRSxjQUFBO0VBQ0EsY0FBQTtFQUNBLCtCQUFBO0VBQ0EsYUFBQTtBQ0VGOztBREFBO0VBRUUsZUFBQTtBQ0VGOztBRENBO0VBRUUsOEJBQUE7RUFDQSw4QkFBQTtFQUNBLGVBQUE7RUFDQSxpQkFBQTtFQUNBLGdCQUFBO0VBQ0EsbUJBQUE7RUFDQSxtQkFBQTtFQUNBLGtCQUFBO0FDQ0Y7O0FEQ0E7RUFFRSw4QkFBQTtFQUNBLDZCQUFBO0VBQ0EsV0FBQTtBQ0NGOztBRENBO0VBRUUsV0FBQTtFQUNBLDhCQUFBO0VBQ0EsOEJBQUE7QUNDRjs7QURFQTtFQUNFLGFBQUE7RUFDQSxrQkFBQTtFQUNBLFdBQUE7RUFDQSxnQkFBQTtFQUNBLHNCQUFBO0FDQ0Y7O0FEQ0E7RUFDRSxlQUFBO0VBQ0EseUJBQUE7QUNFRjs7QURDQTtFQUNFLFVBQUE7RUFDQSxjQUFBO0VBQ0EsY0FBQTtBQ0VGOztBREFBO0VBQ0Usa0JBQUE7RUFDQSxzQkFBQTtFQUNBLGVBQUE7RUFDQSxjQUFBO0VBQ0EsV0FBQTtBQ0dGOztBREFBO0VBQ0U7SUFDRSxxQkFBQTtFQ0dGO0FBQ0Y7O0FBakhBO0VBQ0UsV0FBQTtFQUNBLDJEQUFBO0VBQ0Esc0JBQUE7RUFDQSxpQkFBQTtFQUNBLG1CQUFBO0FBbUhGOztBQWpIQTtFQUNFLGNBQUE7RUFDQSxjQUFBO0VBQ0EsbUJBQUE7RUFDQSxZQUFBO0VBQ0EsYUFBQTtFQUNBLHNCQUFBO0FBb0hGOztBQWxIQTtFQUNFLFdBQUE7RUFDQSxTQUFBO0VBQ0EscUJBQUE7RUFDQSxzQkFBQTtFQUNBLGVBQUE7RUFDQSxrQkFBQTtBQXFIRjs7QUFuSEE7RUFDRSxlQUFBO0FBc0hGOztBQXBIQTtFQUVFLFVBQUE7RUFDQSxvQkFBQTtBQXNIRjs7QUFwSEE7RUFDRSx5QkFBQTtBQXVIRiIsImZpbGUiOiJwcm9maWxlLnBhZ2Uuc2NzcyIsInNvdXJjZXNDb250ZW50IjpbIi5kYXJrYnV0dG9uIHtcclxuICBwYWRkaW5nOiAyNXB4IDExMHB4O1xyXG4gIG1hcmdpbjogMTBweCBhdXRvO1xyXG4gIGRpc3BsYXk6IGJsb2NrO1xyXG4gIGJhY2tncm91bmQ6ICMyZTJlMmU7XHJcbiAgZm9udC13ZWlnaHQ6IG5vcm1hbDtcclxuICB0ZXh0LXRyYW5zZm9ybTogbm9uZTtcclxuICBjb2xvcjogI2ZmZjtcclxuICBmb250LWZhbWlseTogXCJQVCBTYW5zXCI7XHJcbiAgZm9udC1zaXplOiAxOXB4O1xyXG59XHJcblxyXG4uYmFjayB7XHJcbiAgYmFja2dyb3VuZDogdXJsKFwiL2Fzc2V0cy9pbWFnZXMvYmFja2dyb3VuZC5wbmdcIikgcmVwZWF0O1xyXG59XHJcbi5sb2dpbmltYWdlIHtcclxuICB3aWR0aDogMzAlICFpbXBvcnRhbnQ7XHJcbiAgZGlzcGxheTogYmxvY2s7XHJcbiAgbWFyZ2luOiAwIGF1dG87XHJcbiAgcGFkZGluZy1ib3R0b206IDEwcHg7XHJcbn1cclxuXHJcbi5mb3Jtd3JhcHBlciB7XHJcbiAgd2lkdGg6IDkwJTtcclxuICBtYXJnaW46IDAgYXV0bztcclxuICBvdmVyZmxvdzogaGlkZGVuO1xyXG4gIGhlaWdodDogYXV0bztcclxufVxyXG5cclxuLmZpZWxkIHtcclxuICBwYWRkaW5nOiAxNXB4O1xyXG4gIGJhY2tncm91bmQ6ICNmNGY0ZjQ7XHJcbiAgYm9yZGVyOiAxcHggc29saWQgI2Q0ZDRkNDtcclxuICBib3JkZXItcmFkaXVzOiAyNXB4O1xyXG4gIGNvbG9yOiAjOGQ4ZDhkO1xyXG4gIGZvbnQtZmFtaWx5OiBcIlBUIFNhbnNcIjtcclxuICBmb250LXNpemU6IDE2cHg7XHJcbiAgd2lkdGg6IDEwMCU7XHJcbiAgZGlzcGxheTogYmxvY2s7XHJcbiAgbWFyZ2luLWJvdHRvbTogMTVweDtcclxufVxyXG5cclxuLmJsYWNrYnV0dG9uIHtcclxuICBAZXh0ZW5kIC5kYXJrYnV0dG9uO1xyXG4gIHdpZHRoOiAxMDAlO1xyXG4gIG1hcmdpbi10b3A6IDE1cHg7XHJcbiAgYm9yZGVyLXJhZGl1czogNTBweDtcclxufVxyXG4uY2xlYXJidXR0b24ge1xyXG4gIGRpc3BsYXk6IGJsb2NrO1xyXG4gIG1hcmdpbjogMCBhdXRvO1xyXG4gIHRleHQtdHJhbnNmb3JtOiBub25lICFpbXBvcnRhbnQ7XHJcbiAgaGVpZ2h0OiAxLjVlbTtcclxufVxyXG4uYnV0dG9uZm9udCB7XHJcbiAgQGV4dGVuZCAuY2xlYXJidXR0b247XHJcbiAgZm9udC1zaXplOiAxNnB4O1xyXG59XHJcblxyXG4ubGlnaHRncmVlbmJ1dHRvbiB7XHJcbiAgQGV4dGVuZCAuZGFya2J1dHRvbjtcclxuICBiYWNrZ3JvdW5kOiAjMDk3MzhkICFpbXBvcnRhbnQ7XHJcbiAgbWFyZ2luLWJvdHRvbTogMjVweCAhaW1wb3J0YW50O1xyXG4gIGZvbnQtc2l6ZTogMTVweDtcclxuICBmb250LXdlaWdodDogYm9sZDtcclxuICBtYXJnaW4tdG9wOiAxMHB4O1xyXG4gIGJvcmRlci1yYWRpdXM6IDUwcHg7XHJcbiAgd2hpdGUtc3BhY2U6IG5vd3JhcDtcclxuICBwYWRkaW5nOiAxNXB4IDMycHg7XHJcbn1cclxuLnJlZGJ1dHRvbiB7XHJcbiAgQGV4dGVuZCAuZGFya2J1dHRvbjtcclxuICBiYWNrZ3JvdW5kOiAjZWIyMDIwICFpbXBvcnRhbnQ7XHJcbiAgcGFkZGluZzogMjVweCA5NXB4ICFpbXBvcnRhbnQ7XHJcbiAgd2lkdGg6IDEwMCU7XHJcbn1cclxuLmJsdWVidXR0b24ge1xyXG4gIEBleHRlbmQgLmRhcmtidXR0b247XHJcbiAgd2lkdGg6IDEwMCU7XHJcbiAgYmFja2dyb3VuZDogIzNiNTk5OCAhaW1wb3J0YW50O1xyXG4gIG1hcmdpbi1ib3R0b206IDI1cHggIWltcG9ydGFudDtcclxufVxyXG5cclxuLmFsZXJ0LW1kIC5hbGVydC1oZWFkIHtcclxuICBwYWRkaW5nOiAxMHB4O1xyXG4gIHRleHQtYWxpZ246IGNlbnRlcjtcclxuICBjb2xvcjogIzAwMDtcclxuICBiYWNrZ3JvdW5kOiBub25lO1xyXG4gIGZvbnQtZmFtaWx5OiBcIlBUIFNhbnNcIjtcclxufVxyXG4uYWxlcnQtbWQgLmFsZXJ0LXRpdGxlIHtcclxuICBmb250LXNpemU6IDE4cHg7XHJcbiAgdGV4dC10cmFuc2Zvcm06IHVwcGVyY2FzZTtcclxufVxyXG5cclxuLmFsZXJ0LW1kIC5hbGVydC1idXR0b24ge1xyXG4gIHBhZGRpbmc6IDA7XHJcbiAgbWFyZ2luOiAwIGF1dG87XHJcbiAgZGlzcGxheTogYmxvY2s7XHJcbn1cclxuLmNsZWFyYnV0dG9uLXRleHQge1xyXG4gIHRleHQtYWxpZ246IGNlbnRlcjtcclxuICBmb250LWZhbWlseTogXCJQVCBTYW5zXCI7XHJcbiAgZm9udC1zaXplOiAxNnB4O1xyXG4gIGNvbG9yOiAjOGQ4ZDhkO1xyXG4gIHdpZHRoOiAxMDAlO1xyXG59XHJcblxyXG5AbWVkaWEgKG1pbi13aWR0aDogNTY4cHgpIHtcclxuICAubG9naW5pbWFnZSB7XHJcbiAgICB3aWR0aDogMTUlICFpbXBvcnRhbnQ7XHJcbiAgfVxyXG59XHJcbiIsIi5kYXJrYnV0dG9uLCAuYmx1ZWJ1dHRvbiwgLnJlZGJ1dHRvbiwgLmxpZ2h0Z3JlZW5idXR0b24sIC5ibGFja2J1dHRvbiB7XG4gIHBhZGRpbmc6IDI1cHggMTEwcHg7XG4gIG1hcmdpbjogMTBweCBhdXRvO1xuICBkaXNwbGF5OiBibG9jaztcbiAgYmFja2dyb3VuZDogIzJlMmUyZTtcbiAgZm9udC13ZWlnaHQ6IG5vcm1hbDtcbiAgdGV4dC10cmFuc2Zvcm06IG5vbmU7XG4gIGNvbG9yOiAjZmZmO1xuICBmb250LWZhbWlseTogXCJQVCBTYW5zXCI7XG4gIGZvbnQtc2l6ZTogMTlweDtcbn1cblxuLmJhY2sge1xuICBiYWNrZ3JvdW5kOiB1cmwoXCIvYXNzZXRzL2ltYWdlcy9iYWNrZ3JvdW5kLnBuZ1wiKSByZXBlYXQ7XG59XG5cbi5sb2dpbmltYWdlIHtcbiAgd2lkdGg6IDMwJSAhaW1wb3J0YW50O1xuICBkaXNwbGF5OiBibG9jaztcbiAgbWFyZ2luOiAwIGF1dG87XG4gIHBhZGRpbmctYm90dG9tOiAxMHB4O1xufVxuXG4uZm9ybXdyYXBwZXIsIC5wcm9maWxlZm9ybXdyYXBwZXIge1xuICB3aWR0aDogOTAlO1xuICBtYXJnaW46IDAgYXV0bztcbiAgb3ZlcmZsb3c6IGhpZGRlbjtcbiAgaGVpZ2h0OiBhdXRvO1xufVxuXG4uZmllbGQge1xuICBwYWRkaW5nOiAxNXB4O1xuICBiYWNrZ3JvdW5kOiAjZjRmNGY0O1xuICBib3JkZXI6IDFweCBzb2xpZCAjZDRkNGQ0O1xuICBib3JkZXItcmFkaXVzOiAyNXB4O1xuICBjb2xvcjogIzhkOGQ4ZDtcbiAgZm9udC1mYW1pbHk6IFwiUFQgU2Fuc1wiO1xuICBmb250LXNpemU6IDE2cHg7XG4gIHdpZHRoOiAxMDAlO1xuICBkaXNwbGF5OiBibG9jaztcbiAgbWFyZ2luLWJvdHRvbTogMTVweDtcbn1cblxuLmJsYWNrYnV0dG9uIHtcbiAgd2lkdGg6IDEwMCU7XG4gIG1hcmdpbi10b3A6IDE1cHg7XG4gIGJvcmRlci1yYWRpdXM6IDUwcHg7XG59XG5cbi5jbGVhcmJ1dHRvbiwgLmJ1dHRvbmZvbnQge1xuICBkaXNwbGF5OiBibG9jaztcbiAgbWFyZ2luOiAwIGF1dG87XG4gIHRleHQtdHJhbnNmb3JtOiBub25lICFpbXBvcnRhbnQ7XG4gIGhlaWdodDogMS41ZW07XG59XG5cbi5idXR0b25mb250IHtcbiAgZm9udC1zaXplOiAxNnB4O1xufVxuXG4ubGlnaHRncmVlbmJ1dHRvbiB7XG4gIGJhY2tncm91bmQ6ICMwOTczOGQgIWltcG9ydGFudDtcbiAgbWFyZ2luLWJvdHRvbTogMjVweCAhaW1wb3J0YW50O1xuICBmb250LXNpemU6IDE1cHg7XG4gIGZvbnQtd2VpZ2h0OiBib2xkO1xuICBtYXJnaW4tdG9wOiAxMHB4O1xuICBib3JkZXItcmFkaXVzOiA1MHB4O1xuICB3aGl0ZS1zcGFjZTogbm93cmFwO1xuICBwYWRkaW5nOiAxNXB4IDMycHg7XG59XG5cbi5yZWRidXR0b24ge1xuICBiYWNrZ3JvdW5kOiAjZWIyMDIwICFpbXBvcnRhbnQ7XG4gIHBhZGRpbmc6IDI1cHggOTVweCAhaW1wb3J0YW50O1xuICB3aWR0aDogMTAwJTtcbn1cblxuLmJsdWVidXR0b24ge1xuICB3aWR0aDogMTAwJTtcbiAgYmFja2dyb3VuZDogIzNiNTk5OCAhaW1wb3J0YW50O1xuICBtYXJnaW4tYm90dG9tOiAyNXB4ICFpbXBvcnRhbnQ7XG59XG5cbi5hbGVydC1tZCAuYWxlcnQtaGVhZCB7XG4gIHBhZGRpbmc6IDEwcHg7XG4gIHRleHQtYWxpZ246IGNlbnRlcjtcbiAgY29sb3I6ICMwMDA7XG4gIGJhY2tncm91bmQ6IG5vbmU7XG4gIGZvbnQtZmFtaWx5OiBcIlBUIFNhbnNcIjtcbn1cblxuLmFsZXJ0LW1kIC5hbGVydC10aXRsZSB7XG4gIGZvbnQtc2l6ZTogMThweDtcbiAgdGV4dC10cmFuc2Zvcm06IHVwcGVyY2FzZTtcbn1cblxuLmFsZXJ0LW1kIC5hbGVydC1idXR0b24ge1xuICBwYWRkaW5nOiAwO1xuICBtYXJnaW46IDAgYXV0bztcbiAgZGlzcGxheTogYmxvY2s7XG59XG5cbi5jbGVhcmJ1dHRvbi10ZXh0IHtcbiAgdGV4dC1hbGlnbjogY2VudGVyO1xuICBmb250LWZhbWlseTogXCJQVCBTYW5zXCI7XG4gIGZvbnQtc2l6ZTogMTZweDtcbiAgY29sb3I6ICM4ZDhkOGQ7XG4gIHdpZHRoOiAxMDAlO1xufVxuXG5AbWVkaWEgKG1pbi13aWR0aDogNTY4cHgpIHtcbiAgLmxvZ2luaW1hZ2Uge1xuICAgIHdpZHRoOiAxNSUgIWltcG9ydGFudDtcbiAgfVxufVxuLmJhbm5lcnByb2ZpbGUge1xuICB3aWR0aDogMTAwJTtcbiAgYmFja2dyb3VuZDogdXJsKFwiL2Fzc2V0cy9pbWFnZXMvcHJvZmlsZWJhY2sucG5nXCIpIG5vLXJlcGVhdDtcbiAgYmFja2dyb3VuZC1zaXplOiBjb3ZlcjtcbiAgcGFkZGluZy10b3A6IDIwcHg7XG4gIG1hcmdpbi1ib3R0b206IDIwcHg7XG59XG5cbi5iYW5uZXJwcm9maWxlIGltZyB7XG4gIGRpc3BsYXk6IGJsb2NrO1xuICBtYXJnaW46IDAgYXV0bztcbiAgYm9yZGVyLXJhZGl1czogNTBweDtcbiAgd2lkdGg6IDEwMHB4O1xuICBoZWlnaHQ6IDEwMHB4O1xuICBib3JkZXI6IDFweCBzb2xpZCAjZmZmO1xufVxuXG4uYmFubmVycHJvZmlsZSBwIHtcbiAgY29sb3I6ICNmZmY7XG4gIG1hcmdpbjogMDtcbiAgcGFkZGluZzogNXB4IDAgMThweCAwO1xuICBmb250LWZhbWlseTogXCJQVCBTYW5zXCI7XG4gIGZvbnQtc2l6ZTogMjNweDtcbiAgdGV4dC1hbGlnbjogY2VudGVyO1xufVxuXG4uYmFubmVycHJvZmlsZSBwIHNwYW4ge1xuICBmb250LXNpemU6IDE2cHg7XG59XG5cbi5wcm9maWxlZm9ybXdyYXBwZXIge1xuICB3aWR0aDogODUlO1xuICBwYWRkaW5nLWJvdHRvbTogMzBweDtcbn1cblxuLmhlYWRlciB7XG4gIGJhY2tncm91bmQtY29sb3I6ICMxRTFFMUU7XG59Il19 */");

/***/ }),

/***/ 82447:
/*!***************************************************************************************!*\
  !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/Pages/profile/profile.page.html ***!
  \***************************************************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "default": () => (__WEBPACK_DEFAULT_EXPORT__)
/* harmony export */ });
/* harmony default export */ const __WEBPACK_DEFAULT_EXPORT__ = ("<ion-header class=\"header\">\r\n  <ion-toolbar style=\"color: white\" color=\"headerblue\">\r\n    <ion-buttons slot=\"start\">\r\n      <ion-button (click)=\"ToggleMenuBar()\">\r\n        <ion-icon class=\"toggleButton\" name=\"menu\"></ion-icon>\r\n      </ion-button>\r\n    </ion-buttons>\r\n\r\n    <!-- <button ion-button (click)=\"ToggleMenuBar()\">\r\n      <ion-icon name=\"menu\"></ion-icon>\r\n    </button> -->\r\n    <ion-title class=\"toptitle\"><span class=\"heavy\">PROFILE</span></ion-title>\r\n  </ion-toolbar>\r\n</ion-header>\r\n\r\n<ion-content>\r\n  <section class=\"bannerprofile\">\r\n    <!--<img src=\"assets/images/profileavataar.png\" (click)=\"doGetPicture()\">-->\r\n    <img\r\n      src=\"{{profileImage ?? 'data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAOEAAADhCAMAAAAJbSJIAAAAb1BMVEX///9UWV1PVVmBhIZKUFSztbdCSE1FS09OU1dGTFBARkv8/Pzh4uJKT1RESU5NUlfKy8z39/fx8fFaX2NobG+JjI7q6+umqKqQk5VgZGjExcbV1tducnWanJ6Dhoh0eHu6vL2ho6Xc3d17foGur7GvHrXYAAAGTklEQVR4nO2d65KqOhBGJRPDHREEL4yCyvs/45HxOO4ZRQmk6WbvXlVW+TNfpdOXkHRmM4ZhGIZhGIb5ZnmK5+tNdvg4ZJv1PD4tsQdkEr+oP1LbDuXCcRxx+S1kaEfWuS587KGZIKnOF3HCekRINzrPc+wBDsOvPqOn6r5VhtFnNd2ZzPehfCXvJtLdT3Mi84NavJV3ZaEOAfZwtUky5XTU1+CoLMEesh5rLX3XeVxjD1qDUyo19TXI9IQ98K7svR76Grw99tA7kWz7TOCVcDWB1Vi47wNEO8ItsAW8Y97XQm94c2wJr9mrgQItKyK9GDfuYIGW5W6wZbSTmRB4kZhhC2nDyAx+SSRqqHVkSOBlLdbYYp6xG+5k7ng7bDmPBCYFWpYiV2z4RvU1UKuLD7q1xDucA7aknxhdhFcUqaW47J9styMpbTgat9EGSnZ6GppuP8ejUxGvhhRM7YgVtrAbOxtEoGXZVJxNCiTQslJsaVdiUwn3I3aMLe6LT5hV2CA+scU1nMwH+zuKgjvdQMTCGw6Bet+HcqRXXPwEfBeCKgzxAwZIwnaHQOoGFyquuNgCC3CF2JvgR1gjvZjpEVkhYLi/gh30fWgjRY8Xgbk90jYi3F034GjYgBwR112PW/Rngft9P4N2pRdnivudBtyVojtTuPL+Dm6hDx8ssPM2mG3En3iYApeQ9f0Nhbn3zQpZIX2Ff7+nmUF8VfvNAlXhGBF/i6qwHCFrK1EVbuBrC+RN4Rp+IUrc00PxCBUw7venfIRdDOTLGPAVMG6wmM3O4LuJZ2SFNfRCDLGPKQawH9fQNxMvgFsptkDYT8Do8b6hgDVTG/vT0wzaTPGNdDZbQyZuksKFthz0tAmJ26WAX/IJfMVvADwyROLA0AywDEYufu+ATSKVKbysRKATtDRWYUMCdAqa0IXSNUSFEVKIhd9szdupwN1F/E1g3k499LLpJ7Xpb6UuduX7QGk2s3GohMI7vmV2KeKfnH0gN1ko2iQy7t8U5ryNR6DufcbOlESKd2SvVGYkehW2kHaMzCLdGWyIh5cZisZNoFaCDr2vXiFCYqnMI8lqSBY+iQY1Q/qbKPz9307ETr8MznGIL8E7fubpr0bhZQQztVaKrW6t4W6J5jGtVJbOXrgUFfaAe1CldjdbFW5aYQ+2J3Gp3k+kVJ+TcTBPyNdb9aK9pwjVdk2yUNIhqEvbls5vmWIhbbusyWcw3fBP881KKtuVMgxDKV1bhavN/DSl6NCFJCh2VTWvql0RTCE3YxiGYRiG+Rfxl0meB1fyPFn+HTlpEsRVvc/KVSpcpaILdkPzRylXpKsy26+reIpZalJUx4+tGzXVxMIRbQWiEM6iqTQiNz0fq2IiQvPd8WwpN3woCF8jnNBVVnncka6H/aI+29FjsaulM7Kpvs5yKeVDe+BHi/9lStstazIHvr7w443z/C2Z3irDaLGJqUxlnIUuSHdPV2YEduFOGznoqYB3IsMNqrn61TYC76IUbedY1prv3TGuAVuWxHlMKDhoPyXTH4THhILziPoaHHUeU2N+8MbV1yDUYTRb3UfwN7ifaoz2o/icnRzHvzxDjtAWKynhb/6+IiqBK5AKyUDvCAX51M6yhL4x2gW7BGt2Ugyoi0wiHKBDDfUYzWi6oUAOgWcULPSGbb5NnT/ouJp55MpwaExSGkvwjpMaDRs5ER/zJ8IxmMTlrfuBmIiFMYkJSYHNLBoy1CW5NXhDpGZi/2r8SqkrjpFXTA54pcR7pIFrpjWlQP+IPTi7Keikas8Zehvap+pk7ohhyU0G349tKMM6KRu4AgPPoEs29Gewwekv8EirnmhD9n4+IcHddOpO1Dd7G6Hhuhn6OhvQ1jpm8fpVGcBdvEzSryPYkna69pOoT5ExQmtSc/RqcjpGi2Bz9Hhe4DQlI73UGPoJOPgzR2bp8WjStIy0h5lOKBhe0e5dV03JkzbISlPhCI26zaId9LfYI9ZGs62UP4266U+k3m7GCH26TaPZ9xv8WUPzaD6UuJuaK71Yqd52TTW9dRhWWgrnE5xDvVMorJAgrJAV0ocVskL6sEJWSB9WyArpwwpZIX1YISukDytkhfRhhayQPqyQFdKHFbJC+ugqtMXUsPUU7s4fU+NM+vEWhmEYhmEY5jX/ASVYkKOp66h3AAAAAElFTkSuQmCC'}}\"\r\n      (click)=\"doGetPicture()\"\r\n      onerror=\"this.onerror=null;this.src='assets/images/notfound.png';\"\r\n    />\r\n    <p>{{data.fullname}} <span>{{data.cityname}}</span></p>\r\n  </section>\r\n  <section class=\"profileformwrapper\">\r\n    <form>\r\n      <input\r\n        class=\"field\"\r\n        type=\"text\"\r\n        name=\"fullname\"\r\n        placeholder=\"Fullname\"\r\n        [(ngModel)]=\"data.fullname\"\r\n      />\r\n      <!--\t<input class=\"field\" type=\"text\" name=\"Lastname\" placeholder=\"Lastname\" [(ngModel)]=\"data.lastname\">-->\r\n      <input\r\n        class=\"field\"\r\n        type=\"text\"\r\n        name=\"Phone\"\r\n        placeholder=\"Phone Number\"\r\n        [(ngModel)]=\"data.phone\"\r\n      />\r\n      <input\r\n        [disabled]=\"true\"\r\n        class=\"field\"\r\n        type=\"text\"\r\n        name=\"Email\"\r\n        placeholder=\"Email\"\r\n        [(ngModel)]=\"data.email\"\r\n      />\r\n      <input\r\n        *ngIf=\"showCustomer\"\r\n        class=\"field\"\r\n        type=\"text\"\r\n        name=\"Car Registration\"\r\n        placeholder=\"Car Registration\"\r\n        [(ngModel)]=\"data.registration\"\r\n      />\r\n      <input\r\n        class=\"field\"\r\n        type=\"text\"\r\n        name=\"City\"\r\n        placeholder=\"City\"\r\n        [(ngModel)]=\"data.cityname\"\r\n      />\r\n      <ion-button style=\"width: 100%\" color=\"dark\" (click)=\"doAdd()\">\r\n        <span> Save </span>\r\n      </ion-button>\r\n    </form>\r\n  </section>\r\n</ion-content>\r\n");

/***/ })

}]);
//# sourceMappingURL=src_app_Pages_profile_profile_module_ts.js.map