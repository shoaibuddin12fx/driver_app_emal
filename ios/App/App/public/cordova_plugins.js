
  cordova.define('cordova/plugin_list', function(require, exports, module) {
    module.exports = [
      {
          "id": "mx.ferreyra.callnumber.CallNumber",
          "file": "plugins/mx.ferreyra.callnumber/www/CallNumber.js",
          "pluginId": "mx.ferreyra.callnumber",
        "clobbers": [
          "call"
        ]
        },
      {
          "id": "cordova-plugin-datepicker.DatePicker",
          "file": "plugins/cordova-plugin-datepicker/www/ios/DatePicker.js",
          "pluginId": "cordova-plugin-datepicker",
        "clobbers": [
          "datePicker"
        ]
        },
      {
          "id": "uk.co.workingedge.phonegap.plugin.launchnavigator.Common",
          "file": "plugins/uk.co.workingedge.phonegap.plugin.launchnavigator/www/common.js",
          "pluginId": "uk.co.workingedge.phonegap.plugin.launchnavigator",
        "clobbers": [
          "launchnavigator"
        ]
        },
      {
          "id": "uk.co.workingedge.phonegap.plugin.launchnavigator.LocalForage",
          "file": "plugins/uk.co.workingedge.phonegap.plugin.launchnavigator/www/localforage.v1.5.0.min.js",
          "pluginId": "uk.co.workingedge.phonegap.plugin.launchnavigator",
        "clobbers": [
          "localforage"
        ]
        },
      {
          "id": "cordova-plugin-background-mode.BackgroundMode",
          "file": "plugins/cordova-plugin-background-mode/www/background-mode.js",
          "pluginId": "cordova-plugin-background-mode",
        "clobbers": [
          "cordova.plugins.backgroundMode",
          "plugin.backgroundMode"
        ]
        },
      {
          "id": "cordova-plugin-x-socialsharing.SocialSharing",
          "file": "plugins/cordova-plugin-x-socialsharing/www/SocialSharing.js",
          "pluginId": "cordova-plugin-x-socialsharing",
        "clobbers": [
          "window.plugins.socialsharing"
        ]
        },
      {
          "id": "uk.co.workingedge.phonegap.plugin.launchnavigator.LaunchNavigator",
          "file": "plugins/uk.co.workingedge.phonegap.plugin.launchnavigator/www/ios/launchnavigator.js",
          "pluginId": "uk.co.workingedge.phonegap.plugin.launchnavigator",
        "merges": [
          "launchnavigator"
        ]
        }
    ];
    module.exports.metadata =
    // TOP OF METADATA
    {
      "mx.ferreyra.callnumber": "0.0.2",
      "cordova-plugin-background-mode": "0.7.3",
      "cordova-plugin-datepicker": "0.9.3",
      "cordova-plugin-x-socialsharing": "6.0.3",
      "uk.co.workingedge.phonegap.plugin.launchnavigator": "5.0.6"
    };
    // BOTTOM OF METADATA
    });
    