(self["webpackChunkDriverApp"] = self["webpackChunkDriverApp"] || []).push([["src_app_Pages_tabs_bookings_bookings_module_ts"],{

/***/ 49868:
/*!****************************************************************!*\
  !*** ./src/app/Pages/tabs/bookings/bookings-routing.module.ts ***!
  \****************************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "BookingsPageRoutingModule": () => (/* binding */ BookingsPageRoutingModule)
/* harmony export */ });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! tslib */ 64762);
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/core */ 37716);
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/router */ 39895);
/* harmony import */ var _bookings_page__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./bookings.page */ 60540);




const routes = [
    {
        path: '',
        component: _bookings_page__WEBPACK_IMPORTED_MODULE_0__.BookingsPage
    }
];
let BookingsPageRoutingModule = class BookingsPageRoutingModule {
};
BookingsPageRoutingModule = (0,tslib__WEBPACK_IMPORTED_MODULE_1__.__decorate)([
    (0,_angular_core__WEBPACK_IMPORTED_MODULE_2__.NgModule)({
        imports: [_angular_router__WEBPACK_IMPORTED_MODULE_3__.RouterModule.forChild(routes)],
        exports: [_angular_router__WEBPACK_IMPORTED_MODULE_3__.RouterModule],
    })
], BookingsPageRoutingModule);



/***/ }),

/***/ 10426:
/*!********************************************************!*\
  !*** ./src/app/Pages/tabs/bookings/bookings.module.ts ***!
  \********************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "BookingsPageModule": () => (/* binding */ BookingsPageModule)
/* harmony export */ });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! tslib */ 64762);
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @angular/core */ 37716);
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @angular/common */ 38583);
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! @angular/forms */ 3679);
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! @ionic/angular */ 80476);
/* harmony import */ var _bookings_routing_module__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./bookings-routing.module */ 49868);
/* harmony import */ var _bookings_page__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./bookings.page */ 60540);
/* harmony import */ var src_app_Components_header_header_module__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! src/app/Components/header/header.module */ 42335);








let BookingsPageModule = class BookingsPageModule {
};
BookingsPageModule = (0,tslib__WEBPACK_IMPORTED_MODULE_3__.__decorate)([
    (0,_angular_core__WEBPACK_IMPORTED_MODULE_4__.NgModule)({
        imports: [
            _angular_common__WEBPACK_IMPORTED_MODULE_5__.CommonModule,
            _angular_forms__WEBPACK_IMPORTED_MODULE_6__.FormsModule,
            _ionic_angular__WEBPACK_IMPORTED_MODULE_7__.IonicModule,
            _bookings_routing_module__WEBPACK_IMPORTED_MODULE_0__.BookingsPageRoutingModule,
            src_app_Components_header_header_module__WEBPACK_IMPORTED_MODULE_2__.HeaderModule,
        ],
        declarations: [_bookings_page__WEBPACK_IMPORTED_MODULE_1__.BookingsPage],
    })
], BookingsPageModule);



/***/ }),

/***/ 60540:
/*!******************************************************!*\
  !*** ./src/app/Pages/tabs/bookings/bookings.page.ts ***!
  \******************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "BookingsPage": () => (/* binding */ BookingsPage)
/* harmony export */ });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! tslib */ 64762);
/* harmony import */ var _raw_loader_bookings_page_html__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! !raw-loader!./bookings.page.html */ 45993);
/* harmony import */ var _bookings_page_scss__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./bookings.page.scss */ 66613);
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @angular/core */ 37716);
/* harmony import */ var _base_page_base_page__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../../base-page/base-page */ 96093);





let BookingsPage = class BookingsPage extends _base_page_base_page__WEBPACK_IMPORTED_MODULE_2__.BasePage {
    constructor(injector) {
        super(injector);
        this.bookingList = [];
    }
    ngOnInit() { }
    ionViewDidLoad() {
        console.log('ionViewDidLoad BookingsPage');
    }
    ionViewWillEnter() {
        return (0,tslib__WEBPACK_IMPORTED_MODULE_3__.__awaiter)(this, void 0, void 0, function* () {
            //ngOnInit(){
            this.curUser = yield this.firebaseService.getCurrentUser();
            let ref = this.firebaseService.getDbRef('/users/' + this.curUser.uid);
            ref.on('value', (snapshot) => {
                if (snapshot.val() != null) {
                    this.bookingList = [];
                    this.zone.run(() => {
                        this.userProfile = snapshot.val();
                        for (let key in this.userProfile.bookings) {
                            let bs = this.userProfile.bookings[key].status;
                            this.userProfile.bookings[key].uid = key;
                            if (bs == "ENDED" || bs == "CANCELLED") {
                                this.userProfile.bookings[key].image_url = this.userProfile.bookings[key].customer_image;
                                this.bookingList.push(this.userProfile.bookings[key]);
                            }
                        }
                        this.bookingList.sort(function (a, b) {
                            console.log("hehdew");
                            let aa = Number(new Date(a.tripdate));
                            let bb = Number(new Date(b.tripdate));
                            console.log(bb - aa);
                            return bb - aa;
                        });
                        console.log(this.bookingList);
                    });
                }
            });
        });
    }
    openPreviousBooking(index) {
        console.log(this.bookingList[index]);
        this.nav.push('pages/job-complete', {
            bookingKey: this.bookingList[index].uid
        });
    }
};
BookingsPage.ctorParameters = () => [
    { type: _angular_core__WEBPACK_IMPORTED_MODULE_4__.Injector }
];
BookingsPage = (0,tslib__WEBPACK_IMPORTED_MODULE_3__.__decorate)([
    (0,_angular_core__WEBPACK_IMPORTED_MODULE_4__.Component)({
        selector: 'app-bookings',
        template: _raw_loader_bookings_page_html__WEBPACK_IMPORTED_MODULE_0__.default,
        styles: [_bookings_page_scss__WEBPACK_IMPORTED_MODULE_1__.default]
    })
], BookingsPage);



/***/ }),

/***/ 66613:
/*!********************************************************!*\
  !*** ./src/app/Pages/tabs/bookings/bookings.page.scss ***!
  \********************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "default": () => (__WEBPACK_DEFAULT_EXPORT__)
/* harmony export */ });
/* harmony default export */ const __WEBPACK_DEFAULT_EXPORT__ = (".bookings {\n  width: 95%;\n  margin: 0 auto;\n}\n\n.bookings img {\n  width: 100%;\n  display: block;\n  margin: 0 auto;\n  border-radius: 50%;\n}\n\n.list-ios .item-block .item-inner {\n  padding-right: 0;\n}\n\n.item-md.item-block .item-inner {\n  padding-right: 0;\n}\n\n.bookings h5 {\n  font-family: \"PT Sans\";\n  font-size: 18px;\n  text-align: left;\n  color: #4267b2;\n  padding-bottom: 3px;\n  margin: 0;\n}\n\n.bookings p {\n  margin: 0;\n  font-family: \"PT Sans\";\n  font-size: 13px;\n  text-align: left;\n  color: #2e2e2e;\n  padding-bottom: 2px;\n}\n\n.bookings p span {\n  font-weight: bold;\n}\n\n.bookings h3 {\n  text-align: right;\n  font-family: \"PT Sans\";\n  font-size: 24px;\n  color: #2e2e2e;\n  margin: 0;\n  font-weight: bold;\n}\n\n.no-padding {\n  padding: 0 !important;\n}\n\n.heavy {\n  font-size: 13px !important;\n}\n\nspan {\n  color: #7878F1;\n}\n\n.card {\n  margin-left: 0%;\n  width: 100%;\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbImJvb2tpbmdzLnBhZ2Uuc2NzcyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiQUFBQTtFQUNFLFVBQUE7RUFDQSxjQUFBO0FBQ0Y7O0FBQ0E7RUFDRSxXQUFBO0VBQ0EsY0FBQTtFQUNBLGNBQUE7RUFDQSxrQkFBQTtBQUVGOztBQUFBO0VBQ0UsZ0JBQUE7QUFHRjs7QUFEQTtFQUNFLGdCQUFBO0FBSUY7O0FBRkE7RUFDRSxzQkFBQTtFQUNBLGVBQUE7RUFDQSxnQkFBQTtFQUNBLGNBQUE7RUFDQSxtQkFBQTtFQUNBLFNBQUE7QUFLRjs7QUFIQTtFQUNFLFNBQUE7RUFDQSxzQkFBQTtFQUNBLGVBQUE7RUFDQSxnQkFBQTtFQUNBLGNBQUE7RUFDQSxtQkFBQTtBQU1GOztBQUpBO0VBQ0UsaUJBQUE7QUFPRjs7QUFKQTtFQUNFLGlCQUFBO0VBQ0Esc0JBQUE7RUFDQSxlQUFBO0VBQ0EsY0FBQTtFQUNBLFNBQUE7RUFDQSxpQkFBQTtBQU9GOztBQUxBO0VBQ0UscUJBQUE7QUFRRjs7QUFOQTtFQUNFLDBCQUFBO0FBU0Y7O0FBUEE7RUFDRSxjQUFBO0FBVUY7O0FBUkE7RUFDRSxlQUFBO0VBQ0EsV0FBQTtBQVdGIiwiZmlsZSI6ImJvb2tpbmdzLnBhZ2Uuc2NzcyIsInNvdXJjZXNDb250ZW50IjpbIi5ib29raW5ncyB7XHJcbiAgd2lkdGg6IDk1JTtcclxuICBtYXJnaW46IDAgYXV0bztcclxufVxyXG4uYm9va2luZ3MgaW1nIHtcclxuICB3aWR0aDogMTAwJTtcclxuICBkaXNwbGF5OiBibG9jaztcclxuICBtYXJnaW46IDAgYXV0bztcclxuICBib3JkZXItcmFkaXVzOiA1MCU7XHJcbn1cclxuLmxpc3QtaW9zIC5pdGVtLWJsb2NrIC5pdGVtLWlubmVyIHtcclxuICBwYWRkaW5nLXJpZ2h0OiAwO1xyXG59XHJcbi5pdGVtLW1kLml0ZW0tYmxvY2sgLml0ZW0taW5uZXIge1xyXG4gIHBhZGRpbmctcmlnaHQ6IDA7XHJcbn1cclxuLmJvb2tpbmdzIGg1IHtcclxuICBmb250LWZhbWlseTogXCJQVCBTYW5zXCI7XHJcbiAgZm9udC1zaXplOiAxOHB4O1xyXG4gIHRleHQtYWxpZ246IGxlZnQ7XHJcbiAgY29sb3I6ICM0MjY3YjI7XHJcbiAgcGFkZGluZy1ib3R0b206IDNweDtcclxuICBtYXJnaW46IDA7XHJcbn1cclxuLmJvb2tpbmdzIHAge1xyXG4gIG1hcmdpbjogMDtcclxuICBmb250LWZhbWlseTogXCJQVCBTYW5zXCI7XHJcbiAgZm9udC1zaXplOiAxM3B4O1xyXG4gIHRleHQtYWxpZ246IGxlZnQ7XHJcbiAgY29sb3I6ICMyZTJlMmU7XHJcbiAgcGFkZGluZy1ib3R0b206IDJweDtcclxufVxyXG4uYm9va2luZ3MgcCBzcGFuIHtcclxuICBmb250LXdlaWdodDogYm9sZDtcclxufVxyXG5cclxuLmJvb2tpbmdzIGgzIHtcclxuICB0ZXh0LWFsaWduOiAgcmlnaHQ7XHJcbiAgZm9udC1mYW1pbHk6IFwiUFQgU2Fuc1wiO1xyXG4gIGZvbnQtc2l6ZTogMjRweDtcclxuICBjb2xvcjogIzJlMmUyZTtcclxuICBtYXJnaW46IDA7XHJcbiAgZm9udC13ZWlnaHQ6IGJvbGQ7XHJcbn1cclxuLm5vLXBhZGRpbmcge1xyXG4gIHBhZGRpbmc6IDAgIWltcG9ydGFudDtcclxufVxyXG4uaGVhdnkge1xyXG4gIGZvbnQtc2l6ZTogMTNweCAhaW1wb3J0YW50O1xyXG59XHJcbnNwYW57XHJcbiAgY29sb3I6ICM3ODc4RjE7XHJcbn1cclxuLmNhcmR7XHJcbiAgbWFyZ2luLWxlZnQ6IC0wJTtcclxuICB3aWR0aDogMTAwJTtcclxufVxyXG4iXX0= */");

/***/ }),

/***/ 45993:
/*!**********************************************************************************************!*\
  !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/Pages/tabs/bookings/bookings.page.html ***!
  \**********************************************************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "default": () => (__WEBPACK_DEFAULT_EXPORT__)
/* harmony export */ });
/* harmony default export */ const __WEBPACK_DEFAULT_EXPORT__ = ("<header [title]=\"'PREVIOUS BOOKINGS'\"></header>\r\n\r\n<ion-content>\r\n  <section class=\"bookings\">\r\n    <h2 *ngIf=\"bookingList.length==0\" class=\"review-title\">\r\n      No booking history.\r\n    </h2>\r\n    <ion-card\r\n      class=\"card\"\r\n      *ngFor=\"let booking of bookingList; let i = index;\"\r\n      (click)=\"openPreviousBooking(i)\"\r\n    >\r\n      <ion-card-content class=\"ion-no-margin ion-no-padding\">\r\n        <ion-item color=\"none\" class=\"ion-no-margin ion-no-padding\">\r\n          <ion-icon\r\n            class=\"ion-margin\"\r\n            src=\"/assets/images/calendar-outline.svg\"\r\n          ></ion-icon>\r\n          <ion-label>\r\n            {{booking.tripdate.substring(8,10)}}{{booking.tripdate.substring(4,8)}}{{booking.tripdate.substring(0,4)}}\r\n            {{booking.tripdate.substring(11,16)}}\r\n          </ion-label>\r\n          <ion-label slot=\"end\">\r\n            <h6>${{booking.trip_cost ? booking.trip_cost : \"0\"}}</h6>\r\n          </ion-label>\r\n        </ion-item>\r\n        <ion-grid>\r\n          <ion-row>\r\n            <ion-col size=\"12\">\r\n              <ion-item lines=\"none\" color=\"none\">\r\n                <ion-avatar style=\"width: 60px; height: 60px\">\r\n                  <img\r\n                    src=\"{{booking.image_url}}\"\r\n                    onerror=\"this.onerror=null;this.src='assets/images/notfound.png';\"\r\n                    alt=\"\"\r\n                  />\r\n                </ion-avatar>\r\n                <ion-label class=\"ion-margin\">\r\n                  <p class=\"ion-text-wrap\" style=\"font-size: 15px\">\r\n                    <b style=\"color: rgb(120 120 241)\"\r\n                      >{{booking.serviceType}}</b\r\n                    >\r\n                  </p>\r\n                  <p class=\"ion-text-wrap\" style=\"font-size: 12px\">\r\n                    <b style=\"color: rgb(120 120 241)\">Start : </b\r\n                    ><span style=\"color: white\">{{booking.pickupAddress}}</span>\r\n                  </p>\r\n                  <p class=\"ion-text-wrap\" style=\"font-size: 12px\">\r\n                    <b style=\"color: rgb(120 120 241)\">End : </b\r\n                    ><span style=\"color: white\">{{booking.dropAddress}}</span>\r\n                  </p>\r\n                </ion-label>\r\n              </ion-item>\r\n            </ion-col>\r\n          </ion-row>\r\n        </ion-grid>\r\n      </ion-card-content>\r\n    </ion-card>\r\n  </section>\r\n</ion-content>\r\n");

/***/ })

}]);
//# sourceMappingURL=src_app_Pages_tabs_bookings_bookings_module_ts.js.map