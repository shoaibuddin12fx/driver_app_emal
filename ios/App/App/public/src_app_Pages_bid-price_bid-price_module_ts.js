(self["webpackChunkDriverApp"] = self["webpackChunkDriverApp"] || []).push([["src_app_Pages_bid-price_bid-price_module_ts"],{

/***/ 75869:
/*!*************************************************************!*\
  !*** ./src/app/Pages/bid-price/bid-price-routing.module.ts ***!
  \*************************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "BidPricePageRoutingModule": () => (/* binding */ BidPricePageRoutingModule)
/* harmony export */ });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! tslib */ 64762);
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/core */ 37716);
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/router */ 39895);
/* harmony import */ var _bid_price_page__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./bid-price.page */ 73393);




const routes = [
    {
        path: '',
        component: _bid_price_page__WEBPACK_IMPORTED_MODULE_0__.BidPricePage
    }
];
let BidPricePageRoutingModule = class BidPricePageRoutingModule {
};
BidPricePageRoutingModule = (0,tslib__WEBPACK_IMPORTED_MODULE_1__.__decorate)([
    (0,_angular_core__WEBPACK_IMPORTED_MODULE_2__.NgModule)({
        imports: [_angular_router__WEBPACK_IMPORTED_MODULE_3__.RouterModule.forChild(routes)],
        exports: [_angular_router__WEBPACK_IMPORTED_MODULE_3__.RouterModule],
    })
], BidPricePageRoutingModule);



/***/ }),

/***/ 54467:
/*!*****************************************************!*\
  !*** ./src/app/Pages/bid-price/bid-price.module.ts ***!
  \*****************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "BidPricePageModule": () => (/* binding */ BidPricePageModule)
/* harmony export */ });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! tslib */ 64762);
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/core */ 37716);
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @angular/common */ 38583);
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @angular/forms */ 3679);
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! @ionic/angular */ 80476);
/* harmony import */ var _bid_price_routing_module__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./bid-price-routing.module */ 75869);
/* harmony import */ var _bid_price_page__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./bid-price.page */ 73393);







let BidPricePageModule = class BidPricePageModule {
};
BidPricePageModule = (0,tslib__WEBPACK_IMPORTED_MODULE_2__.__decorate)([
    (0,_angular_core__WEBPACK_IMPORTED_MODULE_3__.NgModule)({
        imports: [
            _angular_common__WEBPACK_IMPORTED_MODULE_4__.CommonModule,
            _angular_forms__WEBPACK_IMPORTED_MODULE_5__.FormsModule,
            _ionic_angular__WEBPACK_IMPORTED_MODULE_6__.IonicModule,
            _bid_price_routing_module__WEBPACK_IMPORTED_MODULE_0__.BidPricePageRoutingModule
        ],
        declarations: [_bid_price_page__WEBPACK_IMPORTED_MODULE_1__.BidPricePage]
    })
], BidPricePageModule);



/***/ }),

/***/ 73393:
/*!***************************************************!*\
  !*** ./src/app/Pages/bid-price/bid-price.page.ts ***!
  \***************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "BidPricePage": () => (/* binding */ BidPricePage)
/* harmony export */ });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! tslib */ 64762);
/* harmony import */ var _raw_loader_bid_price_page_html__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! !raw-loader!./bid-price.page.html */ 40617);
/* harmony import */ var _bid_price_page_scss__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./bid-price.page.scss */ 92828);
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @angular/core */ 37716);
/* harmony import */ var _base_page_base_page__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../base-page/base-page */ 96093);





let BidPricePage = class BidPricePage extends _base_page_base_page__WEBPACK_IMPORTED_MODULE_2__.BasePage {
    constructor(injector) {
        super(injector);
        this.selected = {
            pickup: '',
            drop: '',
            pickupservice: false,
        };
        this.biddingLimit = {
            lowerLimit: 0,
            biddingPrice: 0,
            bidText: '',
        };
        this.changeBidcolor = false;
        let params = this.getQueryParams();
        //console.log(navParams.get('job'));
        this.jobs = params === null || params === void 0 ? void 0 : params.job;
        // this.curUser = navParams.get('currentuser');
        this.userProfile = params === null || params === void 0 ? void 0 : params.userprofile;
        if (this.userProfile != undefined &&
            this.userProfile.hasOwnProperty('profile_image') &&
            this.userProfile.profile_image != undefined) {
            this.userProfile.profile_image = this.userProfile.profile_image;
        }
        else {
            this.userProfile.profile_image = 'assets/images/notfound.png';
        }
    }
    ngOnInit() {
        return (0,tslib__WEBPACK_IMPORTED_MODULE_3__.__awaiter)(this, void 0, void 0, function* () {
            this.curUser = yield this.firebaseService.getCurrentUser();
            let ref = this.getDbRef('/users/' + this.curUser.uid);
            let self = this;
            ref
                .once('value', (snapshot) => {
                if (snapshot.val().phone) {
                    if (this.curUser.emailVerified || snapshot.val().authToken) {
                        console.log(snapshot.val().active);
                        if (snapshot.val().active) {
                            this.userProfile = snapshot.val();
                            console.log(this.userProfile, this.curUser);
                        }
                        else {
                            this.firebaseService.signOut();
                            this.nav.pop();
                            var msg = 'Please Contact Ridebidder Authority to activate your account';
                            //this.presentAlert(this.msg);
                            // alert("Please Contact PickMyRide Authority to activate your account");
                            this.alertService.Alert('ALERT', msg);
                        }
                    }
                    else {
                        this.curUser.sendEmailVerification().then(function () {
                            self.firebaseService.signOut();
                            var msg = 'Email has been sent. Please verify your email first.';
                            // this.presentAlert(msg);
                            // alert("Email has been sent. Please verify your email first.");
                            this.alertService.Alert('ALERT', msg);
                        }, (error) => {
                            console.log(error);
                        });
                    }
                }
                else {
                    // this.navCtrl.setRoot(RegistercustomerPage);
                }
                this.usersService.dismissLoader();
            })
                .catch(() => {
                this.usersService.dismissLoader();
            });
        });
    }
    setMyClasses() {
        let className = '';
        if (this.changeBidcolor) {
            className = 'bid-color';
        }
        return className;
    }
    ionViewDidLoad() {
        console.log(this.jobs);
        this.selected.pickup = this.jobs.pickup.add;
        if (this.jobs.hasOwnProperty('bidding') && this.jobs.bidding.length > 1) {
            let bidLength = this.jobs.bidding.length;
            let amt = this.jobs.bidding[bidLength - 1].rate;
            this.changeBidcolor = true;
            this.biddingLimit.biddingPrice = parseFloat(amt);
        }
        else {
            this.changeBidcolor = false;
            this.biddingLimit.biddingPrice = parseFloat(this.jobs.estimate);
        }
        this.biddingLimit.lowerLimit = (this.biddingLimit.biddingPrice / 2).toFixed(2);
        if (this.jobs.hasOwnProperty('yourBid')) {
            this.biddingLimit.bidText =
                'You had bidden $' + this.jobs.yourBid.toFixed(2) + ' for this ride.';
        }
        else {
            this.biddingLimit.bidText = 'Put your bid.';
        }
        if (this.jobs.hasOwnProperty('drop') && this.jobs.drop !== undefined) {
            this.selected.drop = this.jobs.drop.add;
        }
        this.selected.pickupservice = true;
    }
    changeBid(action) {
        if (action == 'inc') {
            this.biddingLimit.biddingPrice += 1;
        }
        else if (action == 'dec' &&
            this.biddingLimit.biddingPrice > this.biddingLimit.lowerLimit) {
            this.biddingLimit.biddingPrice -= 1;
        }
        console.log(this.biddingLimit.biddingPrice);
    }
    presentAlert(message) {
        this.alertService.Alert('ALERT', message);
    }
    okSchedule() {
        if (this.jobs.promo_code) {
            this.promo = this.jobs.promo_code;
        }
        else {
            this.promo = null;
        }
        if (this.jobs.corporate_booking_name) {
            this.corporate_booking_name = this.jobs.corporate_booking_name;
        }
        else {
            this.corporate_booking_name = null;
        }
        if (this.jobs.customer_email) {
            this.customer_email = this.jobs.customer_email;
        }
        else {
            this.customer_email = null;
        }
        if (this.jobs.customer_image) {
            this.customer_image = this.jobs.customer_image;
        }
        else {
            this.customer_image = null;
        }
        if (this.jobs.booking_type) {
            this.booking_type = this.jobs.booking_type;
        }
        else {
            this.booking_type = null;
        }
        if (this.jobs.customer_name) {
            this.customer_name = this.jobs.customer_name;
        }
        else {
            this.customer_name = null;
        }
        var bidding = [];
        var key = this.jobs.uid;
        var ref = this.getDbRef('bookings/' + key);
        ref.on('value', (snapshot) => {
            if (snapshot.val().hasOwnProperty('bidding') &&
                snapshot.val().bidding.length > 0) {
                //   bidding = snapshot.val().bidding;
                //delete the previous bid of cuurent user
                for (var j = 0; j < snapshot.val().bidding.length; j++) {
                    if (snapshot.val().bidding[j].driver !== this.curUser.uid) {
                        bidding.push(snapshot.val().bidding[j]);
                    }
                }
                console.log(bidding, bidding.length);
                bidding.push({
                    rate: this.biddingLimit.biddingPrice,
                    driver: this.curUser.uid,
                    driver_name: this.userProfile.fullname,
                    driver_image: this.userProfile.profile_image,
                    status: 'Bidding Start',
                    driver_rating: this.jobs.driver_rating,
                    driver_push_token: this.userProfile.pushToken,
                    job_id: key,
                });
            }
            else {
                bidding = [
                    {
                        rate: this.biddingLimit.biddingPrice,
                        driver: this.curUser.uid,
                        driver_name: this.userProfile.fullname,
                        driver_image: this.userProfile.profile_image,
                        status: 'Bidding Start',
                        driver_rating: this.jobs.driver_rating,
                        driver_push_token: this.userProfile.pushToken,
                        job_id: key,
                    },
                ];
            }
        });
        if (this.jobs.drop) {
            this.bookingData = {
                customer: this.jobs.customer,
                customer_name: this.jobs.customer_name,
                customer_image: this.jobs.customer_image,
                pickup: this.jobs.pickup,
                drop: this.jobs.drop,
                tripdate: this.jobs.tripdate,
                status: 'Bidding Start',
                driver: this.curUser.uid,
                driver_name: this.userProfile.fullname,
                driver_image: this.userProfile.profile_image,
                distance: this.jobs.distance,
                triptime: this.jobs.triptime,
                transmission: this.jobs.transmission,
                estimate: this.jobs.estimate,
                serviceType: this.jobs.serviceType,
                customer_push_token: this.jobs.customer_push_token,
                promo_code: this.promo,
                bidding: bidding,
            };
            // console.log(this.bookingData);
            ref.update(this.bookingData);
            let ref2 = this.getDbRef('bookings/' + key + '/driver');
            ref2.on('value', (snapshot) => {
                if (snapshot.val() == this.curUser.uid) {
                    //  console.log("Inside Update");
                    //  console.log(snapshot.val());
                    var customerData = {
                        customer_email: this.customer_email,
                        corporate_booking_name: this.corporate_booking_name,
                        customer_image: this.customer_image,
                        booking_type: this.booking_type,
                        customer_name: this.customer_name,
                        driver: this.curUser.uid,
                        driver_name: this.userProfile.fullname,
                        driver_image: this.userProfile.profile_image,
                        status: 'Bidding Start',
                        customer: this.bookingData.customer,
                        pickupAddress: this.bookingData.pickup.add,
                        dropAddress: this.bookingData.drop.add,
                        tripdate: this.bookingData.tripdate,
                        serviceType: this.bookingData.serviceType,
                        bidding: bidding,
                    };
                    var updates = {};
                    updates['/users/' + this.bookingData.customer + '/bookings/' + key] =
                        customerData;
                    this.firebaseService.getDatabase().ref().update(updates);
                    //   console.log(this.bookingData.customer_push_token)
                    //  firebase.database().ref("geofire").child(key).remove();
                    this.usersService
                        .sendPush('Driver bid your trip', 'Driver ' + this.userProfile.fullname + ' bid your trip.', this.bookingData.customer_push_token)
                        .subscribe((response) => console.log(response));
                    this.nav.setRoot('tabs');
                }
                else {
                    console.log('Error');
                    console.log(snapshot.val());
                    this.getDbRef('/users/' + this.curUser.uid + '/bookings/' + key).remove();
                    this.nav.pop();
                    let msg = 'Cancelled by Customer or got assigned to other Driver';
                    this.presentAlert(msg);
                }
            });
        }
        else {
            this.bookingData = {
                customer: this.jobs.customer,
                customer_name: this.jobs.customer_name,
                customer_image: this.jobs.customer_image,
                pickup: this.jobs.pickup,
                tripdate: this.jobs.tripdate,
                status: 'Bidding Start',
                driver: this.curUser.uid,
                driver_name: this.userProfile.fullname,
                driver_image: this.userProfile.profile_image,
                triptime: this.jobs.triptime,
                transmission: this.jobs.transmission,
                estimate: this.jobs.estimate,
                serviceType: this.jobs.serviceType,
                customer_push_token: this.jobs.customer_push_token,
                promo_code: this.promo,
                bidding: bidding,
            };
            ref.update(this.bookingData);
            console.log(key);
            let ref2 = this.getDbRef('bookings/' + key + '/driver');
            ref2.on('value', (snapshot) => {
                if (snapshot.val() == this.curUser.uid) {
                    //  console.log("Inside Update");
                    //  console.log(snapshot.val());
                    var customerData = {
                        customer_email: this.customer_email,
                        corporate_booking_name: this.corporate_booking_name,
                        customer_image: this.customer_image,
                        booking_type: this.booking_type,
                        customer_name: this.customer_name,
                        driver: this.curUser.uid,
                        driver_name: this.userProfile.fullname,
                        driver_image: this.userProfile.profile_image,
                        status: 'Bidding Start',
                        customer: this.bookingData.customer,
                        pickupAddress: this.bookingData.pickup.add,
                        tripdate: this.bookingData.tripdate,
                        serviceType: this.bookingData.serviceType,
                        bidding: bidding,
                    };
                    var updates = {};
                    updates['/users/' + this.bookingData.customer + '/bookings/' + key] =
                        customerData;
                    this.firebaseService.getDatabase().ref().update(updates);
                    this.usersService
                        .sendPush('Driver bid your trip', 'Driver ' + this.userProfile.fullname + ' bid your trip.', this.bookingData.customer_push_token)
                        .subscribe((response) => console.log(response));
                    this.nav.pop();
                }
                else {
                    console.log('Error');
                    console.log(snapshot.val());
                    this.getDbRef('/users/' + this.curUser.uid + '/bookings/' + key).remove();
                    this.nav.setRoot('tab');
                    let msg = 'Cancelled by Customer or got assigned to other Driver';
                    this.presentAlert(msg);
                }
            });
        }
    }
    getDbRef(ref) {
        return this.firebaseService.getDbRef(ref);
    }
};
BidPricePage.ctorParameters = () => [
    { type: _angular_core__WEBPACK_IMPORTED_MODULE_4__.Injector }
];
BidPricePage = (0,tslib__WEBPACK_IMPORTED_MODULE_3__.__decorate)([
    (0,_angular_core__WEBPACK_IMPORTED_MODULE_4__.Component)({
        selector: 'app-bid-price',
        template: _raw_loader_bid_price_page_html__WEBPACK_IMPORTED_MODULE_0__.default,
        styles: [_bid_price_page_scss__WEBPACK_IMPORTED_MODULE_1__.default]
    })
], BidPricePage);



/***/ }),

/***/ 92828:
/*!*****************************************************!*\
  !*** ./src/app/Pages/bid-price/bid-price.page.scss ***!
  \*****************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "default": () => (__WEBPACK_DEFAULT_EXPORT__)
/* harmony export */ });
/* harmony default export */ const __WEBPACK_DEFAULT_EXPORT__ = ("page-bid-price .marker {\n  margin-right: 10%;\n}\npage-bid-price .loaction {\n  font-size: 15px;\n}\npage-bid-price .marker img {\n  padding-top: 0px !important;\n}\npage-bid-price .last-row {\n  border-bottom: 1px dotted #000;\n  padding-bottom: 10px;\n  margin-bottom: 7px;\n}\npage-bid-price .detail {\n  margin-left: 6%;\n  color: #ccc7c7;\n  font-size: 15px;\n}\npage-bid-price .next-row {\n  color: #ccc7c7 !important;\n}\npage-bid-price .addOn {\n  background-color: #4266b2;\n  text-align: center;\n  font-size: 20px;\n  color: #fff;\n}\npage-bid-price .addOn ion-icon {\n  margin-top: 4px;\n}\npage-bid-price .bid-rate {\n  font-size: 15px;\n  text-align: center;\n  margin: 7px;\n}\npage-bid-price .submit-bid {\n  width: 100%;\n  font-size: 15px;\n  padding: 10px;\n  background-color: #4266b2;\n  color: #fff;\n}\npage-bid-price .cust-name {\n  text-align: center;\n  color: #000;\n}\npage-bid-price .bid-color {\n  color: green;\n}\npage-bid-price .customcard {\n  --color: black;\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbImJpZC1wcmljZS5wYWdlLnNjc3MiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IkFBQ0U7RUFDRSxpQkFBQTtBQUFKO0FBRUU7RUFDRSxlQUFBO0FBQUo7QUFFRTtFQUNFLDJCQUFBO0FBQUo7QUFFRTtFQUNFLDhCQUFBO0VBQ0Esb0JBQUE7RUFDQSxrQkFBQTtBQUFKO0FBRUU7RUFDRSxlQUFBO0VBQ0EsY0FBQTtFQUNBLGVBQUE7QUFBSjtBQUVFO0VBQ0UseUJBQUE7QUFBSjtBQVNFO0VBQ0UseUJBQUE7RUFDQSxrQkFBQTtFQUNBLGVBQUE7RUFDQSxXQUFBO0FBUEo7QUFTRTtFQUNFLGVBQUE7QUFQSjtBQVNFO0VBQ0UsZUFBQTtFQUNBLGtCQUFBO0VBQ0EsV0FBQTtBQVBKO0FBU0U7RUFDRSxXQUFBO0VBQ0EsZUFBQTtFQUNBLGFBQUE7RUFDQSx5QkFBQTtFQUNBLFdBQUE7QUFQSjtBQVNFO0VBQ0Usa0JBQUE7RUFDQSxXQUFBO0FBUEo7QUFTRTtFQUNFLFlBQUE7QUFQSjtBQVNFO0VBQ0ksY0FBQTtBQVBOIiwiZmlsZSI6ImJpZC1wcmljZS5wYWdlLnNjc3MiLCJzb3VyY2VzQ29udGVudCI6WyJwYWdlLWJpZC1wcmljZSB7XHJcbiAgLm1hcmtlciB7XHJcbiAgICBtYXJnaW4tcmlnaHQ6IDEwJTtcclxuICB9XHJcbiAgLmxvYWN0aW9uIHtcclxuICAgIGZvbnQtc2l6ZTogMTVweDtcclxuICB9XHJcbiAgLm1hcmtlciBpbWcge1xyXG4gICAgcGFkZGluZy10b3A6IDBweCAhaW1wb3J0YW50O1xyXG4gIH1cclxuICAubGFzdC1yb3cge1xyXG4gICAgYm9yZGVyLWJvdHRvbTogMXB4IGRvdHRlZCAjMDAwO1xyXG4gICAgcGFkZGluZy1ib3R0b206IDEwcHg7XHJcbiAgICBtYXJnaW4tYm90dG9tOiA3cHg7XHJcbiAgfVxyXG4gIC5kZXRhaWwge1xyXG4gICAgbWFyZ2luLWxlZnQ6IDYlO1xyXG4gICAgY29sb3I6ICNjY2M3Yzc7XHJcbiAgICBmb250LXNpemU6IDE1cHg7XHJcbiAgfVxyXG4gIC5uZXh0LXJvdyB7XHJcbiAgICBjb2xvcjogI2NjYzdjNyAhaW1wb3J0YW50O1xyXG4gIH1cclxuICAvLyAuZGV0YWlsLWNhcmQge1xyXG4gIC8vICAgLy8gYm9yZGVyOjBweDtcclxuICAvLyAgIGJhY2tncm91bmQtY29sb3I6ICNmZmY7XHJcbiAgLy8gfVxyXG4gIC5kZXRhaWwgc3BhbiB7XHJcbiAgICAvLyBjb2xvcjojMDAwO1xyXG4gIH1cclxuICAuYWRkT24ge1xyXG4gICAgYmFja2dyb3VuZC1jb2xvcjogIzQyNjZiMjtcclxuICAgIHRleHQtYWxpZ246IGNlbnRlcjtcclxuICAgIGZvbnQtc2l6ZTogMjBweDtcclxuICAgIGNvbG9yOiAjZmZmO1xyXG4gIH1cclxuICAuYWRkT24gaW9uLWljb24ge1xyXG4gICAgbWFyZ2luLXRvcDogNHB4O1xyXG4gIH1cclxuICAuYmlkLXJhdGUge1xyXG4gICAgZm9udC1zaXplOiAxNXB4O1xyXG4gICAgdGV4dC1hbGlnbjogY2VudGVyO1xyXG4gICAgbWFyZ2luOiA3cHg7XHJcbiAgfVxyXG4gIC5zdWJtaXQtYmlkIHtcclxuICAgIHdpZHRoOiAxMDAlO1xyXG4gICAgZm9udC1zaXplOiAxNXB4O1xyXG4gICAgcGFkZGluZzogMTBweDtcclxuICAgIGJhY2tncm91bmQtY29sb3I6ICM0MjY2YjI7XHJcbiAgICBjb2xvcjogI2ZmZjtcclxuICB9XHJcbiAgLmN1c3QtbmFtZSB7XHJcbiAgICB0ZXh0LWFsaWduOiBjZW50ZXI7XHJcbiAgICBjb2xvcjogIzAwMDtcclxuICB9XHJcbiAgLmJpZC1jb2xvciB7XHJcbiAgICBjb2xvcjogZ3JlZW47XHJcbiAgfVxyXG4gIC5jdXN0b21jYXJkIHtcclxuICAgICAgLS1jb2xvcjogYmxhY2s7XHJcbiAgICB9XHJcbn1cclxuIl19 */");

/***/ }),

/***/ 40617:
/*!*******************************************************************************************!*\
  !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/Pages/bid-price/bid-price.page.html ***!
  \*******************************************************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "default": () => (__WEBPACK_DEFAULT_EXPORT__)
/* harmony export */ });
/* harmony default export */ const __WEBPACK_DEFAULT_EXPORT__ = ("<!--\r\n  Generated template for the BidPricePage page.\r\n\r\n  See http://ionicframework.com/docs/v2/components/#navigation for more info on\r\n  Ionic pages and navigation.\r\n-->\r\n\r\n<ion-header>\r\n  <ion-toolbar color=\"headerblue\">\r\n    <ion-icon\r\n      class=\"toggleButton\"\r\n      name=\"menu\"\r\n      (click)=\"ToggleMenuBar()\"\r\n    ></ion-icon>\r\n    <!-- <button ion-button (click)=\"ToggleMenuBar()\">\r\n      <ion-icon name=\"menu\"></ion-icon>\r\n    </button> -->\r\n    <ion-title class=\"toptitle\"><span class=\"heavy\">BID PRICE</span></ion-title>\r\n  </ion-toolbar>\r\n</ion-header>\r\n\r\n<ion-content>\r\n  <p class=\"detail\">Job Detail</p>\r\n  <ion-card class=\"customcard\">\r\n    <ion-card-content class=\"customcard\">\r\n      <ion-grid class=\"no-padding\">\r\n        <ion-row>\r\n          <ion-col class=\"customcardbody\">\r\n            <img\r\n              src=\"{{jobs.customer_image}}\"\r\n              style=\"height: 70px; width: 100px\"\r\n              onerror=\"this.onerror=null;this.src='assets/images/notfound.png';\"\r\n            />\r\n            <h2 class=\"cust-name\">{{jobs.customer_name}}</h2>\r\n          </ion-col>\r\n        </ion-row>\r\n        <ion-row style=\"margin-top: 24px\">\r\n          <ion-col width-10 class=\"marker\">\r\n            <img src=\"assets/images/car.png\" />\r\n          </ion-col>\r\n\r\n          <ion-col width-90 class=\"no-padding\">\r\n            <p class=\"loaction\">\r\n              <strong\r\n                >{{jobs.tripdate.substring(8,10)}}{{jobs.tripdate.substring(4,8)}}{{jobs.tripdate.substring(0,4)}}\r\n                {{jobs.tripdate.substring(11,16)}}</strong\r\n              >\r\n            </p>\r\n            <!-- <p class=\"loaction\" ><strong>Sat, 6 Jan 12:56 PM</strong></p>  -->\r\n          </ion-col>\r\n        </ion-row>\r\n        <ion-row class=\"last-row\">\r\n          <ion-col width-10 class=\"marker\"> </ion-col>\r\n\r\n          <ion-col width-90 class=\"no-padding\">\r\n            <p class=\"loaction\">{{jobs.transmission}} : {{jobs.serviceType}}</p>\r\n          </ion-col>\r\n        </ion-row>\r\n        <ion-row>\r\n          <ion-col width-20 class=\"next-row\">\r\n            <p class=\"loaction next-row\"><strong> From</strong>:</p>\r\n          </ion-col>\r\n\r\n          <ion-col width-80 class=\"no-padding\">\r\n            <p class=\"loaction next-row\">{{selected.pickup}}</p>\r\n          </ion-col>\r\n        </ion-row>\r\n        <ion-row *ngIf=\"jobs.serviceType == 'Pickup' \">\r\n          <ion-col width-20 class=\"next-row\">\r\n            <p class=\"loaction next-row\"><strong>To</strong>:</p>\r\n          </ion-col>\r\n\r\n          <ion-col width-80 class=\"no-padding\">\r\n            <p class=\"loaction next-row\">{{selected.drop}}</p>\r\n          </ion-col>\r\n          <!-- <p *ngIf=\"job.serviceType == 'Personal driver' \"><span>Trip Time</span> : {{job.triptime}} hours</p> -->\r\n        </ion-row>\r\n        <ion-row *ngIf=\"jobs.serviceType == 'Personal driver' \">\r\n          <ion-col width-20 class=\"next-row\">\r\n            <p class=\"loaction next-row\"><strong>Trip Time</strong>:</p>\r\n          </ion-col>\r\n\r\n          <ion-col width-80 class=\"no-padding\">\r\n            <p class=\"loaction next-row\">{{jobs.triptime}} minutes</p>\r\n          </ion-col>\r\n        </ion-row>\r\n      </ion-grid>\r\n    </ion-card-content>\r\n  </ion-card>\r\n\r\n  <ion-card class=\"customcard detail-card\">\r\n    <section>\r\n      <ion-grid class=\"no-padding\">\r\n        <ion-row>\r\n          <ion-col (tap)=\"changeBid('dec')\" width-25 class=\"addOn\">\r\n            <ion-icon ios=\"ios-remove\" md=\"md-remove\"></ion-icon>\r\n          </ion-col>\r\n\r\n          <ion-col width-50 class=\"no-padding\">\r\n            <p class=\"bid-rate\" [ngClass]=\"setMyClasses()\">\r\n              <strong>${{biddingLimit.biddingPrice.toFixed(2)}} </strong>\r\n            </p>\r\n            <!-- <input class=\"field\" type=\"text\" name=\"Bid Price\" placeholder=\"Name\" [(ngModel)]=\"biddingLimit.biddingPrice\"> -->\r\n          </ion-col>\r\n          <ion-col width-25 class=\"addOn\" (tap)=\"changeBid('inc')\">\r\n            <ion-icon ios=\"ios-add\" md=\"md-add\"></ion-icon>\r\n          </ion-col>\r\n        </ion-row>\r\n      </ion-grid>\r\n    </section>\r\n  </ion-card>\r\n  <p class=\"detail\" style=\"color: #000\">\r\n    Estimated fare is\r\n    <span><strong>${{jobs.estimate.toFixed(2)}} </strong></span>.\r\n    {{biddingLimit.bidText}}\r\n  </p>\r\n\r\n  <ion-card class=\"customcard detail-card\" style=\"margin-top: 3%\">\r\n    <button class=\"submit-bid\" (click)=\"okSchedule()\">Submit Your Bid</button>\r\n  </ion-card>\r\n</ion-content>\r\n");

/***/ })

}]);
//# sourceMappingURL=src_app_Pages_bid-price_bid-price_module_ts.js.map