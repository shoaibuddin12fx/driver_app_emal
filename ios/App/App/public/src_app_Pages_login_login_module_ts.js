(self["webpackChunkDriverApp"] = self["webpackChunkDriverApp"] || []).push([["src_app_Pages_login_login_module_ts"],{

/***/ 26875:
/*!*****************************************************!*\
  !*** ./src/app/Pages/login/login-routing.module.ts ***!
  \*****************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "LoginPageRoutingModule": () => (/* binding */ LoginPageRoutingModule)
/* harmony export */ });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! tslib */ 64762);
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/core */ 37716);
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/router */ 39895);
/* harmony import */ var _login_page__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./login.page */ 66318);




const routes = [
    {
        path: '',
        component: _login_page__WEBPACK_IMPORTED_MODULE_0__.LoginPage
    }
];
let LoginPageRoutingModule = class LoginPageRoutingModule {
};
LoginPageRoutingModule = (0,tslib__WEBPACK_IMPORTED_MODULE_1__.__decorate)([
    (0,_angular_core__WEBPACK_IMPORTED_MODULE_2__.NgModule)({
        imports: [_angular_router__WEBPACK_IMPORTED_MODULE_3__.RouterModule.forChild(routes)],
        exports: [_angular_router__WEBPACK_IMPORTED_MODULE_3__.RouterModule],
    })
], LoginPageRoutingModule);



/***/ }),

/***/ 45342:
/*!*********************************************!*\
  !*** ./src/app/Pages/login/login.module.ts ***!
  \*********************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "LoginPageModule": () => (/* binding */ LoginPageModule)
/* harmony export */ });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! tslib */ 64762);
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/core */ 37716);
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @angular/common */ 38583);
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @angular/forms */ 3679);
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! @ionic/angular */ 80476);
/* harmony import */ var _login_routing_module__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./login-routing.module */ 26875);
/* harmony import */ var _login_page__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./login.page */ 66318);







let LoginPageModule = class LoginPageModule {
};
LoginPageModule = (0,tslib__WEBPACK_IMPORTED_MODULE_2__.__decorate)([
    (0,_angular_core__WEBPACK_IMPORTED_MODULE_3__.NgModule)({
        imports: [
            _angular_common__WEBPACK_IMPORTED_MODULE_4__.CommonModule,
            _angular_forms__WEBPACK_IMPORTED_MODULE_5__.FormsModule,
            _ionic_angular__WEBPACK_IMPORTED_MODULE_6__.IonicModule,
            _login_routing_module__WEBPACK_IMPORTED_MODULE_0__.LoginPageRoutingModule
        ],
        declarations: [_login_page__WEBPACK_IMPORTED_MODULE_1__.LoginPage]
    })
], LoginPageModule);



/***/ }),

/***/ 66318:
/*!*******************************************!*\
  !*** ./src/app/Pages/login/login.page.ts ***!
  \*******************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "LoginPage": () => (/* binding */ LoginPage)
/* harmony export */ });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! tslib */ 64762);
/* harmony import */ var _raw_loader_login_page_html__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! !raw-loader!./login.page.html */ 3540);
/* harmony import */ var _login_page_scss__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./login.page.scss */ 4158);
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @angular/core */ 37716);
/* harmony import */ var _base_page_base_page__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../base-page/base-page */ 96093);





let LoginPage = class LoginPage extends _base_page_base_page__WEBPACK_IMPORTED_MODULE_2__.BasePage {
    constructor(injector) {
        super(injector);
    }
    ngOnInit() {
        this.menuCtrl.enable(false, 'drawer');
    }
    ionViewDidLoad() {
        console.log('Login Page');
    }
    onSignInFaild() {
        return (0,tslib__WEBPACK_IMPORTED_MODULE_3__.__awaiter)(this, void 0, void 0, function* () {
            this.alertService.Alert('Login', 'Email or Password is Invalid');
        });
    }
    detailsFailed() {
        this.alertService.Alert('Login', 'Please Enter Valid Details');
    }
    doLogin() {
        if (this.email == undefined ||
            this.password == undefined ||
            this.email == '' ||
            this.password == '') {
            this.detailsFailed();
        }
        else {
            this.usersService
                .loginUser(this.email, this.password)
                .then(() => {
                console.log('Log in Sucess');
                this.usersService.checkUser();
            })
                .catch((_error) => this.onSignInFaild());
        }
    }
    //end doLogin
    doRegistration() {
        this.nav.push('pages/register');
    }
    resetPassword() {
        return (0,tslib__WEBPACK_IMPORTED_MODULE_3__.__awaiter)(this, void 0, void 0, function* () {
            let alert = yield this.alertCtrl.create({
                header: 'Forget password',
                inputs: [
                    {
                        type: 'text',
                        placeholder: 'Please Enter Your Email',
                    },
                ],
                buttons: [
                    'Cancel',
                    {
                        text: 'OK',
                        handler: (data) => {
                            let passData = data[0];
                            this.changepass(passData);
                        },
                    },
                ],
            });
            alert.present();
        });
    }
    changepass(data) {
        let email = data;
        this.usersService
            .passReset(email)
            .then((success) => alert('An email to your account has been sent successfully!, If you do not find it please check your spam folder'))
            .catch((_error) => alert(_error));
    }
};
LoginPage.ctorParameters = () => [
    { type: _angular_core__WEBPACK_IMPORTED_MODULE_4__.Injector }
];
LoginPage = (0,tslib__WEBPACK_IMPORTED_MODULE_3__.__decorate)([
    (0,_angular_core__WEBPACK_IMPORTED_MODULE_4__.Component)({
        selector: 'app-login',
        template: _raw_loader_login_page_html__WEBPACK_IMPORTED_MODULE_0__.default,
        styles: [_login_page_scss__WEBPACK_IMPORTED_MODULE_1__.default]
    })
], LoginPage);



/***/ }),

/***/ 4158:
/*!*********************************************!*\
  !*** ./src/app/Pages/login/login.page.scss ***!
  \*********************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "default": () => (__WEBPACK_DEFAULT_EXPORT__)
/* harmony export */ });
/* harmony default export */ const __WEBPACK_DEFAULT_EXPORT__ = (".darkbutton, .bluebutton, .redbutton, .lightgreenbutton, .blackbutton {\n  padding: 25px 110px;\n  margin: 10px auto;\n  display: block;\n  background: #2e2e2e;\n  font-weight: normal;\n  text-transform: none;\n  color: #fff;\n  font-family: \"PT Sans\";\n  font-size: 19px;\n}\n\n.back {\n  background: url(\"/assets/images/background.png\") repeat;\n}\n\n.loginimage {\n  width: 30% !important;\n  display: block;\n  margin: 0 auto;\n  padding-bottom: 10px;\n}\n\n.formwrapper {\n  width: 90%;\n  margin: 0 auto;\n  overflow: hidden;\n  height: auto;\n}\n\n.field {\n  padding: 15px;\n  background: #f4f4f4;\n  border: 1px solid #d4d4d4;\n  border-radius: 25px;\n  color: #8d8d8d;\n  font-family: \"PT Sans\";\n  font-size: 16px;\n  width: 100%;\n  display: block;\n  margin-bottom: 15px;\n}\n\n.blackbutton {\n  width: 100%;\n  margin-top: 15px;\n  border-radius: 50px;\n}\n\n.clearbutton, .buttonfont {\n  display: block;\n  margin: 0 auto;\n  text-transform: none !important;\n  height: 1.5em;\n}\n\n.buttonfont {\n  font-size: 16px;\n}\n\n.lightgreenbutton {\n  background: #09738d !important;\n  margin-bottom: 25px !important;\n  font-size: 15px;\n  font-weight: bold;\n  margin-top: 10px;\n  border-radius: 50px;\n  white-space: nowrap;\n  padding: 15px 32px;\n}\n\n.redbutton {\n  background: #eb2020 !important;\n  padding: 25px 95px !important;\n  width: 100%;\n}\n\n.bluebutton {\n  width: 100%;\n  background: #3b5998 !important;\n  margin-bottom: 25px !important;\n}\n\n.alert-md .alert-head {\n  padding: 10px;\n  text-align: center;\n  color: #000;\n  background: none;\n  font-family: \"PT Sans\";\n}\n\n.alert-md .alert-title {\n  font-size: 18px;\n  text-transform: uppercase;\n}\n\n.alert-md .alert-button {\n  padding: 0;\n  margin: 0 auto;\n  display: block;\n}\n\n.clearbutton-text {\n  text-align: center;\n  font-family: \"PT Sans\";\n  font-size: 16px;\n  color: #8d8d8d;\n  width: 100%;\n}\n\n@media (min-width: 568px) {\n  .loginimage {\n    width: 15% !important;\n  }\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbImxvZ2luLnBhZ2Uuc2NzcyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiQUFBQTtFQUNFLG1CQUFBO0VBQ0EsaUJBQUE7RUFDQSxjQUFBO0VBQ0EsbUJBQUE7RUFDQSxtQkFBQTtFQUNBLG9CQUFBO0VBQ0EsV0FBQTtFQUNBLHNCQUFBO0VBQ0EsZUFBQTtBQUNGOztBQUVBO0VBQ0UsdURBQUE7QUFDRjs7QUFDQTtFQUNFLHFCQUFBO0VBQ0EsY0FBQTtFQUNBLGNBQUE7RUFDQSxvQkFBQTtBQUVGOztBQUNBO0VBQ0UsVUFBQTtFQUNBLGNBQUE7RUFDQSxnQkFBQTtFQUNBLFlBQUE7QUFFRjs7QUFDQTtFQUNFLGFBQUE7RUFDQSxtQkFBQTtFQUNBLHlCQUFBO0VBQ0EsbUJBQUE7RUFDQSxjQUFBO0VBQ0Esc0JBQUE7RUFDQSxlQUFBO0VBQ0EsV0FBQTtFQUNBLGNBQUE7RUFDQSxtQkFBQTtBQUVGOztBQUNBO0VBRUUsV0FBQTtFQUNBLGdCQUFBO0VBQ0EsbUJBQUE7QUFDRjs7QUFDQTtFQUNFLGNBQUE7RUFDQSxjQUFBO0VBQ0EsK0JBQUE7RUFDQSxhQUFBO0FBRUY7O0FBQUE7RUFFRSxlQUFBO0FBRUY7O0FBQ0E7RUFFRSw4QkFBQTtFQUNBLDhCQUFBO0VBQ0EsZUFBQTtFQUNBLGlCQUFBO0VBQ0EsZ0JBQUE7RUFDQSxtQkFBQTtFQUNBLG1CQUFBO0VBQ0Esa0JBQUE7QUFDRjs7QUFDQTtFQUVFLDhCQUFBO0VBQ0EsNkJBQUE7RUFDQSxXQUFBO0FBQ0Y7O0FBQ0E7RUFFRSxXQUFBO0VBQ0EsOEJBQUE7RUFDQSw4QkFBQTtBQUNGOztBQUVBO0VBQ0UsYUFBQTtFQUNBLGtCQUFBO0VBQ0EsV0FBQTtFQUNBLGdCQUFBO0VBQ0Esc0JBQUE7QUFDRjs7QUFDQTtFQUNFLGVBQUE7RUFDQSx5QkFBQTtBQUVGOztBQUNBO0VBQ0UsVUFBQTtFQUNBLGNBQUE7RUFDQSxjQUFBO0FBRUY7O0FBQUE7RUFDRSxrQkFBQTtFQUNBLHNCQUFBO0VBQ0EsZUFBQTtFQUNBLGNBQUE7RUFDQSxXQUFBO0FBR0Y7O0FBQUE7RUFDRTtJQUNFLHFCQUFBO0VBR0Y7QUFDRiIsImZpbGUiOiJsb2dpbi5wYWdlLnNjc3MiLCJzb3VyY2VzQ29udGVudCI6WyIuZGFya2J1dHRvbiB7XHJcbiAgcGFkZGluZzogMjVweCAxMTBweDtcclxuICBtYXJnaW46IDEwcHggYXV0bztcclxuICBkaXNwbGF5OiBibG9jaztcclxuICBiYWNrZ3JvdW5kOiAjMmUyZTJlO1xyXG4gIGZvbnQtd2VpZ2h0OiBub3JtYWw7XHJcbiAgdGV4dC10cmFuc2Zvcm06IG5vbmU7XHJcbiAgY29sb3I6ICNmZmY7XHJcbiAgZm9udC1mYW1pbHk6IFwiUFQgU2Fuc1wiO1xyXG4gIGZvbnQtc2l6ZTogMTlweDtcclxufVxyXG5cclxuLmJhY2sge1xyXG4gIGJhY2tncm91bmQ6IHVybChcIi9hc3NldHMvaW1hZ2VzL2JhY2tncm91bmQucG5nXCIpIHJlcGVhdDtcclxufVxyXG4ubG9naW5pbWFnZSB7XHJcbiAgd2lkdGg6IDMwJSAhaW1wb3J0YW50O1xyXG4gIGRpc3BsYXk6IGJsb2NrO1xyXG4gIG1hcmdpbjogMCBhdXRvO1xyXG4gIHBhZGRpbmctYm90dG9tOiAxMHB4O1xyXG59XHJcblxyXG4uZm9ybXdyYXBwZXIge1xyXG4gIHdpZHRoOiA5MCU7XHJcbiAgbWFyZ2luOiAwIGF1dG87XHJcbiAgb3ZlcmZsb3c6IGhpZGRlbjtcclxuICBoZWlnaHQ6IGF1dG87XHJcbn1cclxuXHJcbi5maWVsZCB7XHJcbiAgcGFkZGluZzogMTVweDtcclxuICBiYWNrZ3JvdW5kOiAjZjRmNGY0O1xyXG4gIGJvcmRlcjogMXB4IHNvbGlkICNkNGQ0ZDQ7XHJcbiAgYm9yZGVyLXJhZGl1czogMjVweDtcclxuICBjb2xvcjogIzhkOGQ4ZDtcclxuICBmb250LWZhbWlseTogXCJQVCBTYW5zXCI7XHJcbiAgZm9udC1zaXplOiAxNnB4O1xyXG4gIHdpZHRoOiAxMDAlO1xyXG4gIGRpc3BsYXk6IGJsb2NrO1xyXG4gIG1hcmdpbi1ib3R0b206IDE1cHg7XHJcbn1cclxuXHJcbi5ibGFja2J1dHRvbiB7XHJcbiAgQGV4dGVuZCAuZGFya2J1dHRvbjtcclxuICB3aWR0aDogMTAwJTtcclxuICBtYXJnaW4tdG9wOiAxNXB4O1xyXG4gIGJvcmRlci1yYWRpdXM6IDUwcHg7XHJcbn1cclxuLmNsZWFyYnV0dG9uIHtcclxuICBkaXNwbGF5OiBibG9jaztcclxuICBtYXJnaW46IDAgYXV0bztcclxuICB0ZXh0LXRyYW5zZm9ybTogbm9uZSAhaW1wb3J0YW50O1xyXG4gIGhlaWdodDogMS41ZW07XHJcbn1cclxuLmJ1dHRvbmZvbnQge1xyXG4gIEBleHRlbmQgLmNsZWFyYnV0dG9uO1xyXG4gIGZvbnQtc2l6ZTogMTZweDtcclxufVxyXG5cclxuLmxpZ2h0Z3JlZW5idXR0b24ge1xyXG4gIEBleHRlbmQgLmRhcmtidXR0b247XHJcbiAgYmFja2dyb3VuZDogIzA5NzM4ZCAhaW1wb3J0YW50O1xyXG4gIG1hcmdpbi1ib3R0b206IDI1cHggIWltcG9ydGFudDtcclxuICBmb250LXNpemU6IDE1cHg7XHJcbiAgZm9udC13ZWlnaHQ6IGJvbGQ7XHJcbiAgbWFyZ2luLXRvcDogMTBweDtcclxuICBib3JkZXItcmFkaXVzOiA1MHB4O1xyXG4gIHdoaXRlLXNwYWNlOiBub3dyYXA7XHJcbiAgcGFkZGluZzogMTVweCAzMnB4O1xyXG59XHJcbi5yZWRidXR0b24ge1xyXG4gIEBleHRlbmQgLmRhcmtidXR0b247XHJcbiAgYmFja2dyb3VuZDogI2ViMjAyMCAhaW1wb3J0YW50O1xyXG4gIHBhZGRpbmc6IDI1cHggOTVweCAhaW1wb3J0YW50O1xyXG4gIHdpZHRoOiAxMDAlO1xyXG59XHJcbi5ibHVlYnV0dG9uIHtcclxuICBAZXh0ZW5kIC5kYXJrYnV0dG9uO1xyXG4gIHdpZHRoOiAxMDAlO1xyXG4gIGJhY2tncm91bmQ6ICMzYjU5OTggIWltcG9ydGFudDtcclxuICBtYXJnaW4tYm90dG9tOiAyNXB4ICFpbXBvcnRhbnQ7XHJcbn1cclxuXHJcbi5hbGVydC1tZCAuYWxlcnQtaGVhZCB7XHJcbiAgcGFkZGluZzogMTBweDtcclxuICB0ZXh0LWFsaWduOiBjZW50ZXI7XHJcbiAgY29sb3I6ICMwMDA7XHJcbiAgYmFja2dyb3VuZDogbm9uZTtcclxuICBmb250LWZhbWlseTogXCJQVCBTYW5zXCI7XHJcbn1cclxuLmFsZXJ0LW1kIC5hbGVydC10aXRsZSB7XHJcbiAgZm9udC1zaXplOiAxOHB4O1xyXG4gIHRleHQtdHJhbnNmb3JtOiB1cHBlcmNhc2U7XHJcbn1cclxuXHJcbi5hbGVydC1tZCAuYWxlcnQtYnV0dG9uIHtcclxuICBwYWRkaW5nOiAwO1xyXG4gIG1hcmdpbjogMCBhdXRvO1xyXG4gIGRpc3BsYXk6IGJsb2NrO1xyXG59XHJcbi5jbGVhcmJ1dHRvbi10ZXh0IHtcclxuICB0ZXh0LWFsaWduOiBjZW50ZXI7XHJcbiAgZm9udC1mYW1pbHk6IFwiUFQgU2Fuc1wiO1xyXG4gIGZvbnQtc2l6ZTogMTZweDtcclxuICBjb2xvcjogIzhkOGQ4ZDtcclxuICB3aWR0aDogMTAwJTtcclxufVxyXG5cclxuQG1lZGlhIChtaW4td2lkdGg6IDU2OHB4KSB7XHJcbiAgLmxvZ2luaW1hZ2Uge1xyXG4gICAgd2lkdGg6IDE1JSAhaW1wb3J0YW50O1xyXG4gIH1cclxufVxyXG4iXX0= */");

/***/ }),

/***/ 3540:
/*!***********************************************************************************!*\
  !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/Pages/login/login.page.html ***!
  \***********************************************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "default": () => (__WEBPACK_DEFAULT_EXPORT__)
/* harmony export */ });
/* harmony default export */ const __WEBPACK_DEFAULT_EXPORT__ = ("<!--\r\n  Generated template for the Login page.\r\n\r\n  See http://ionicframework.com/docs/v2/components/#navigation for more info on\r\n  Ionic pages and navigation.\r\n-->\r\n<!-- <ion-header>\r\n  <ion-toolbar color=\"headerblue\">\r\n    <ion-title style=\"text-align: center\"\r\n      ><span class=\"heavy\"></span\r\n    ></ion-title>\r\n  </ion-toolbar>\r\n</ion-header> -->\r\n\r\n<ion-content>\r\n  <section class=\"support-section\">\r\n    <h4 style=\"text-align: center; font-size: 35px\">RideBidder</h4>\r\n    <!-- <img src=\"assets/images/supportlogo.png\"> -->\r\n    <img class=\"loginimage\" src=\"/assets/images/login.png\" />\r\n  </section>\r\n  <section class=\"formwrapper\">\r\n    <form style=\"margin-top: 20px\">\r\n      <input class=\"field\" type=\"text\" name=\"Email\" placeholder=\"Email \" [(ngModel)]=\"email\" />\r\n      <input class=\"field\" type=\"password\" name=\"Password\" placeholder=\"Password\" [(ngModel)]=\"password\" />\r\n      <!-- <ion-button\r\n        ion-button-round\r\n        style=\"margin-bottom: 5%; width: 100%\"\r\n        color=\"dark\"\r\n        (click)=\"doLogin()\"\r\n      >\r\n        Login\r\n      </ion-button> -->\r\n\r\n      <button class=\"lightgreenbutton login-btn\" ion-button color=\"primary\" (click)='doLogin()'>SIGN IN</button>\r\n\r\n      <div class=\"clearbutton-text\">\r\n        <span (click)=\"resetPassword()\" color=\"dark\">Forgot Password?</span> |\r\n        <span (click)=\"doRegistration()\" color=\"primary\">Register Now</span>\r\n      </div>\r\n\r\n      <!--<ion-grid>\r\n\t\t\t\t<ion-row>\r\n\t\t\t\t\t<ion-col>\r\n\t\t\t\t\t\t<button class=\"clearbutton buttonfont\" ion-button color=\"dark\" clear (click)='resetPassword()'>Forgot Password ?</button>\r\n\t\t\t\t\t</ion-col>\r\n\t\t\t\t\t<ion-col>\r\n\t\t\t\t\t\t<button class=\"clearbutton buttonfont\"  ion-button color=\"primary\" clear (click)='doRegistration()'>Register Now</button>\r\n\t\t\t\t\t</ion-col>\r\n\t\t\t\t</ion-row>\r\n\t\t\t</ion-grid>-->\r\n\r\n      <!--<button class=\"redbutton\" ion-button icon-left round (click)=\"loginSocial('Google')\">\r\n\t\t\t\t<ion-icon name=\"logo-googleplus\"></ion-icon>\r\n\t\t\t\t<span class=\"heavy\">Login</span>&nbsp;With&nbsp;<span class=\"heavy\">Google</span>\r\n\t\t\t</button>\r\n\t\t\t<button class=\"bluebutton\" ion-button icon-left round (click)=\"loginSocial('Facebook')\">\r\n\t\t\t\t<ion-icon name=\"logo-facebook\"></ion-icon>\r\n\t\t\t\t<span class=\"heavy\">Login</span>&nbsp;With&nbsp;<span class=\"heavy\">Facebook</span>\r\n\t\t\t</button>-->\r\n    </form>\r\n  </section>\r\n</ion-content>");

/***/ })

}]);
//# sourceMappingURL=src_app_Pages_login_login_module_ts.js.map