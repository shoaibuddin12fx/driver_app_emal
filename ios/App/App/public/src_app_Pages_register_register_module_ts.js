(self["webpackChunkDriverApp"] = self["webpackChunkDriverApp"] || []).push([["src_app_Pages_register_register_module_ts"],{

/***/ 94392:
/*!***********************************************************!*\
  !*** ./src/app/Pages/register/register-routing.module.ts ***!
  \***********************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "RegisterPageRoutingModule": () => (/* binding */ RegisterPageRoutingModule)
/* harmony export */ });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! tslib */ 64762);
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/core */ 37716);
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/router */ 39895);
/* harmony import */ var _register_page__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./register.page */ 1699);




const routes = [
    {
        path: '',
        component: _register_page__WEBPACK_IMPORTED_MODULE_0__.RegisterPage
    }
];
let RegisterPageRoutingModule = class RegisterPageRoutingModule {
};
RegisterPageRoutingModule = (0,tslib__WEBPACK_IMPORTED_MODULE_1__.__decorate)([
    (0,_angular_core__WEBPACK_IMPORTED_MODULE_2__.NgModule)({
        imports: [_angular_router__WEBPACK_IMPORTED_MODULE_3__.RouterModule.forChild(routes)],
        exports: [_angular_router__WEBPACK_IMPORTED_MODULE_3__.RouterModule],
    })
], RegisterPageRoutingModule);



/***/ }),

/***/ 25010:
/*!***************************************************!*\
  !*** ./src/app/Pages/register/register.module.ts ***!
  \***************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "RegisterPageModule": () => (/* binding */ RegisterPageModule)
/* harmony export */ });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! tslib */ 64762);
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/core */ 37716);
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @angular/common */ 38583);
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @angular/forms */ 3679);
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! @ionic/angular */ 80476);
/* harmony import */ var _register_routing_module__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./register-routing.module */ 94392);
/* harmony import */ var _register_page__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./register.page */ 1699);







let RegisterPageModule = class RegisterPageModule {
};
RegisterPageModule = (0,tslib__WEBPACK_IMPORTED_MODULE_2__.__decorate)([
    (0,_angular_core__WEBPACK_IMPORTED_MODULE_3__.NgModule)({
        imports: [
            _angular_common__WEBPACK_IMPORTED_MODULE_4__.CommonModule,
            _angular_forms__WEBPACK_IMPORTED_MODULE_5__.FormsModule,
            _ionic_angular__WEBPACK_IMPORTED_MODULE_6__.IonicModule,
            _register_routing_module__WEBPACK_IMPORTED_MODULE_0__.RegisterPageRoutingModule
        ],
        declarations: [_register_page__WEBPACK_IMPORTED_MODULE_1__.RegisterPage]
    })
], RegisterPageModule);



/***/ }),

/***/ 1699:
/*!*************************************************!*\
  !*** ./src/app/Pages/register/register.page.ts ***!
  \*************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "RegisterPage": () => (/* binding */ RegisterPage)
/* harmony export */ });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! tslib */ 64762);
/* harmony import */ var _raw_loader_register_page_html__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! !raw-loader!./register.page.html */ 78100);
/* harmony import */ var _register_page_scss__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./register.page.scss */ 82165);
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @angular/core */ 37716);
/* harmony import */ var _base_page_base_page__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../base-page/base-page */ 96093);





let RegisterPage = class RegisterPage extends _base_page_base_page__WEBPACK_IMPORTED_MODULE_2__.BasePage {
    constructor(injector) {
        super(injector);
    }
    ngOnInit() {
        return (0,tslib__WEBPACK_IMPORTED_MODULE_3__.__awaiter)(this, void 0, void 0, function* () {
            this.curUser = yield this.firebaseService.getCurrentUser();
            if (this.curUser) {
                this.showInput = false;
                this.fullname = this.curUser.displayName;
                this.email = this.curUser.email;
            }
            else {
                this.showInput = true;
            }
        });
    }
    onSignUpSuccess() {
        console.log('Restration Succesfull');
    }
    onSignUpFailed() {
        this.alertService.Alert('Registration', 'Failed');
    }
    onFirebaseFailed(error) {
        this.alertService.Alert('Registration', error);
    }
    wrongUserInfo() {
        this.alertService.Alert('Registration', 'Please Enter Vaild Details');
    }
    doRegistration() {
        if (!this.fullname) {
            this.utility.presentFailureToast('Please enter your good name');
        }
        else if (!this.email) {
            this.utility.presentFailureToast('Please enter email');
        }
        else if (!this.password) {
            this.utility.presentFailureToast('Please enter password');
        }
        else if (this.password.length < 6) {
            this.utility.presentFailureToast('Your Passwors is too short');
        }
        else if (!this.confirmPassword) {
            this.utility.presentFailureToast('Please enter confirm password');
        }
        else if (this.password != this.confirmPassword) {
            this.utility.presentFailureToast('Password and confirm password do not match');
        }
        else if (!this.phone || this.phone === '') {
            this.utility.presentFailureToast('Please fill the Phone Number field');
        }
        else if (!this.cityname) {
            this.utility.presentFailureToast('Please fill The Cityname field');
        }
        else if (!this.email) {
            this.utility.presentFailureToast('Please Enter A Valid email');
        }
        else {
            if (this.curUser) {
                if (this.fullname == undefined ||
                    this.email == undefined ||
                    this.phone == undefined ||
                    this.cityname == undefined ||
                    this.fullname == '' ||
                    this.email == '' ||
                    this.phone == '' ||
                    this.cityname == '') {
                    this.wrongUserInfo();
                }
                else {
                    this.usersService
                        .insertUser(this.fullname, this.email, this.phone, this.cityname, this.curUser)
                        .then(() => this.nav.setRoot('pages/login'))
                        .catch((_error) => this.onSignUpFailed());
                }
            }
            else {
                if (this.fullname == undefined ||
                    this.email == undefined ||
                    this.password == undefined ||
                    this.confirmPassword == undefined ||
                    this.phone == undefined ||
                    this.cityname == undefined ||
                    this.fullname == '' ||
                    this.email == '' ||
                    this.password == '' ||
                    this.confirmPassword == '' ||
                    this.phone == '' ||
                    this.cityname == '') {
                    this.wrongUserInfo();
                }
                else {
                    if (this.password == this.confirmPassword) {
                        this.usersService
                            .signUpUser(this.fullname, this.email, this.password, this.phone, this.cityname)
                            .then(() => this.nav.setRoot('pages/login'))
                            .catch((_error) => this.onFirebaseFailed(_error.toString()));
                    }
                    else {
                        this.alertService.Alert('Registration', 'Password do not match');
                    }
                }
            }
        }
    }
    goToSignin() {
        this.nav.setRoot('pages/login');
    }
};
RegisterPage.ctorParameters = () => [
    { type: _angular_core__WEBPACK_IMPORTED_MODULE_4__.Injector }
];
RegisterPage = (0,tslib__WEBPACK_IMPORTED_MODULE_3__.__decorate)([
    (0,_angular_core__WEBPACK_IMPORTED_MODULE_4__.Component)({
        selector: 'app-register',
        template: _raw_loader_register_page_html__WEBPACK_IMPORTED_MODULE_0__.default,
        styles: [_register_page_scss__WEBPACK_IMPORTED_MODULE_1__.default]
    })
], RegisterPage);



/***/ }),

/***/ 82165:
/*!***************************************************!*\
  !*** ./src/app/Pages/register/register.page.scss ***!
  \***************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "default": () => (__WEBPACK_DEFAULT_EXPORT__)
/* harmony export */ });
/* harmony default export */ const __WEBPACK_DEFAULT_EXPORT__ = (".darkbutton, .bluebutton, .redbutton, .blackbutton {\n  padding: 25px 110px;\n  margin: 10px auto;\n  display: block;\n  background: #2e2e2e;\n  font-weight: normal;\n  text-transform: none;\n  color: #fff;\n  font-family: \"PT Sans\";\n  font-size: 19px;\n}\n\n.back {\n  background: url(\"/assets/images/background.png\") repeat;\n}\n\n.loginimage {\n  width: 30% !important;\n  display: block;\n  margin: 0 auto;\n  padding-bottom: 10px;\n}\n\n.formwrapper {\n  width: 90%;\n  margin: 0 auto;\n  overflow: hidden;\n  height: auto;\n}\n\n.field {\n  padding: 15px;\n  background: #f4f4f4;\n  border: 1px solid #d4d4d4;\n  border-radius: 10px;\n  color: #8d8d8d;\n  font-family: \"PT Sans\";\n  font-size: 16px;\n  width: 100%;\n  display: block;\n  margin-bottom: 15px;\n}\n\n.blackbutton {\n  width: 100%;\n  margin-top: 15px;\n  border-radius: 15px;\n  margin-top: 20px;\n}\n\n.clearbutton, .buttonfont {\n  display: block;\n  margin: 0 auto;\n  text-transform: none !important;\n  height: 1.5em;\n}\n\n.buttonfont {\n  font-size: 16px;\n}\n\n.redbutton {\n  background: #eb2020 !important;\n  padding: 25px 95px !important;\n  width: 100%;\n}\n\n.bluebutton {\n  width: 100%;\n  background: #3b5998 !important;\n  margin-bottom: 25px !important;\n}\n\n.alert-md .alert-head {\n  padding: 10px;\n  text-align: center;\n  color: #000;\n  background: none;\n  font-family: \"PT Sans\";\n}\n\n.alert-md .alert-title {\n  font-size: 18px;\n  text-transform: uppercase;\n}\n\n.alert-md .alert-button {\n  padding: 0;\n  margin: 0 auto;\n  display: block;\n}\n\n.clearbutton-text {\n  text-align: center;\n  font-family: \"PT Sans\";\n  font-size: 16px;\n  color: #8d8d8d;\n  width: 100%;\n}\n\n@media (min-width: 568px) {\n  .loginimage {\n    width: 15% !important;\n  }\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInJlZ2lzdGVyLnBhZ2Uuc2NzcyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiQUFBQTtFQUNFLG1CQUFBO0VBQ0EsaUJBQUE7RUFDQSxjQUFBO0VBQ0EsbUJBQUE7RUFDQSxtQkFBQTtFQUNBLG9CQUFBO0VBQ0EsV0FBQTtFQUNBLHNCQUFBO0VBQ0EsZUFBQTtBQUNGOztBQUVBO0VBQ0UsdURBQUE7QUFDRjs7QUFDQTtFQUNFLHFCQUFBO0VBQ0EsY0FBQTtFQUNBLGNBQUE7RUFDQSxvQkFBQTtBQUVGOztBQUNBO0VBQ0UsVUFBQTtFQUNBLGNBQUE7RUFDQSxnQkFBQTtFQUNBLFlBQUE7QUFFRjs7QUFDQTtFQUNFLGFBQUE7RUFDQSxtQkFBQTtFQUNBLHlCQUFBO0VBQ0EsbUJBQUE7RUFDQSxjQUFBO0VBQ0Esc0JBQUE7RUFDQSxlQUFBO0VBQ0EsV0FBQTtFQUNBLGNBQUE7RUFDQSxtQkFBQTtBQUVGOztBQUNBO0VBRUUsV0FBQTtFQUNBLGdCQUFBO0VBQ0EsbUJBQUE7RUFDQSxnQkFBQTtBQUNGOztBQUNBO0VBQ0UsY0FBQTtFQUNBLGNBQUE7RUFDQSwrQkFBQTtFQUNBLGFBQUE7QUFFRjs7QUFBQTtFQUVFLGVBQUE7QUFFRjs7QUFBQTtFQUVFLDhCQUFBO0VBQ0EsNkJBQUE7RUFDQSxXQUFBO0FBRUY7O0FBQUE7RUFFRSxXQUFBO0VBQ0EsOEJBQUE7RUFDQSw4QkFBQTtBQUVGOztBQUNBO0VBQ0UsYUFBQTtFQUNBLGtCQUFBO0VBQ0EsV0FBQTtFQUNBLGdCQUFBO0VBQ0Esc0JBQUE7QUFFRjs7QUFBQTtFQUNFLGVBQUE7RUFDQSx5QkFBQTtBQUdGOztBQUFBO0VBQ0UsVUFBQTtFQUNBLGNBQUE7RUFDQSxjQUFBO0FBR0Y7O0FBREE7RUFDRSxrQkFBQTtFQUNBLHNCQUFBO0VBQ0EsZUFBQTtFQUNBLGNBQUE7RUFDQSxXQUFBO0FBSUY7O0FBREE7RUFDRTtJQUNFLHFCQUFBO0VBSUY7QUFDRiIsImZpbGUiOiJyZWdpc3Rlci5wYWdlLnNjc3MiLCJzb3VyY2VzQ29udGVudCI6WyIuZGFya2J1dHRvbiB7XHJcbiAgcGFkZGluZzogMjVweCAxMTBweDtcclxuICBtYXJnaW46IDEwcHggYXV0bztcclxuICBkaXNwbGF5OiBibG9jaztcclxuICBiYWNrZ3JvdW5kOiAjMmUyZTJlO1xyXG4gIGZvbnQtd2VpZ2h0OiBub3JtYWw7XHJcbiAgdGV4dC10cmFuc2Zvcm06IG5vbmU7XHJcbiAgY29sb3I6ICNmZmY7XHJcbiAgZm9udC1mYW1pbHk6IFwiUFQgU2Fuc1wiO1xyXG4gIGZvbnQtc2l6ZTogMTlweDtcclxufVxyXG5cclxuLmJhY2sge1xyXG4gIGJhY2tncm91bmQ6IHVybChcIi9hc3NldHMvaW1hZ2VzL2JhY2tncm91bmQucG5nXCIpIHJlcGVhdDtcclxufVxyXG4ubG9naW5pbWFnZSB7XHJcbiAgd2lkdGg6IDMwJSAhaW1wb3J0YW50O1xyXG4gIGRpc3BsYXk6IGJsb2NrO1xyXG4gIG1hcmdpbjogMCBhdXRvO1xyXG4gIHBhZGRpbmctYm90dG9tOiAxMHB4O1xyXG59XHJcblxyXG4uZm9ybXdyYXBwZXIge1xyXG4gIHdpZHRoOiA5MCU7XHJcbiAgbWFyZ2luOiAwIGF1dG87XHJcbiAgb3ZlcmZsb3c6IGhpZGRlbjtcclxuICBoZWlnaHQ6IGF1dG87XHJcbn1cclxuXHJcbi5maWVsZCB7XHJcbiAgcGFkZGluZzogMTVweDtcclxuICBiYWNrZ3JvdW5kOiAjZjRmNGY0O1xyXG4gIGJvcmRlcjogMXB4IHNvbGlkICNkNGQ0ZDQ7XHJcbiAgYm9yZGVyLXJhZGl1czogMTBweDtcclxuICBjb2xvcjogIzhkOGQ4ZDtcclxuICBmb250LWZhbWlseTogXCJQVCBTYW5zXCI7XHJcbiAgZm9udC1zaXplOiAxNnB4O1xyXG4gIHdpZHRoOiAxMDAlO1xyXG4gIGRpc3BsYXk6IGJsb2NrO1xyXG4gIG1hcmdpbi1ib3R0b206IDE1cHg7XHJcbn1cclxuXHJcbi5ibGFja2J1dHRvbiB7XHJcbiAgQGV4dGVuZCAuZGFya2J1dHRvbjtcclxuICB3aWR0aDogMTAwJTtcclxuICBtYXJnaW4tdG9wOiAxNXB4O1xyXG4gIGJvcmRlci1yYWRpdXM6IDE1cHg7XHJcbiAgbWFyZ2luLXRvcDogMjBweDtcclxufVxyXG4uY2xlYXJidXR0b24ge1xyXG4gIGRpc3BsYXk6IGJsb2NrO1xyXG4gIG1hcmdpbjogMCBhdXRvO1xyXG4gIHRleHQtdHJhbnNmb3JtOiBub25lICFpbXBvcnRhbnQ7XHJcbiAgaGVpZ2h0OiAxLjVlbTtcclxufVxyXG4uYnV0dG9uZm9udCB7XHJcbiAgQGV4dGVuZCAuY2xlYXJidXR0b247XHJcbiAgZm9udC1zaXplOiAxNnB4O1xyXG59XHJcbi5yZWRidXR0b24ge1xyXG4gIEBleHRlbmQgLmRhcmtidXR0b247XHJcbiAgYmFja2dyb3VuZDogI2ViMjAyMCAhaW1wb3J0YW50O1xyXG4gIHBhZGRpbmc6IDI1cHggOTVweCAhaW1wb3J0YW50O1xyXG4gIHdpZHRoOiAxMDAlO1xyXG59XHJcbi5ibHVlYnV0dG9uIHtcclxuICBAZXh0ZW5kIC5kYXJrYnV0dG9uO1xyXG4gIHdpZHRoOiAxMDAlO1xyXG4gIGJhY2tncm91bmQ6ICMzYjU5OTggIWltcG9ydGFudDtcclxuICBtYXJnaW4tYm90dG9tOiAyNXB4ICFpbXBvcnRhbnQ7XHJcbn1cclxuXHJcbi5hbGVydC1tZCAuYWxlcnQtaGVhZCB7XHJcbiAgcGFkZGluZzogMTBweDtcclxuICB0ZXh0LWFsaWduOiBjZW50ZXI7XHJcbiAgY29sb3I6ICMwMDA7XHJcbiAgYmFja2dyb3VuZDogbm9uZTtcclxuICBmb250LWZhbWlseTogXCJQVCBTYW5zXCI7XHJcbn1cclxuLmFsZXJ0LW1kIC5hbGVydC10aXRsZSB7XHJcbiAgZm9udC1zaXplOiAxOHB4O1xyXG4gIHRleHQtdHJhbnNmb3JtOiB1cHBlcmNhc2U7XHJcbn1cclxuXHJcbi5hbGVydC1tZCAuYWxlcnQtYnV0dG9uIHtcclxuICBwYWRkaW5nOiAwO1xyXG4gIG1hcmdpbjogMCBhdXRvO1xyXG4gIGRpc3BsYXk6IGJsb2NrO1xyXG59XHJcbi5jbGVhcmJ1dHRvbi10ZXh0IHtcclxuICB0ZXh0LWFsaWduOiBjZW50ZXI7XHJcbiAgZm9udC1mYW1pbHk6IFwiUFQgU2Fuc1wiO1xyXG4gIGZvbnQtc2l6ZTogMTZweDtcclxuICBjb2xvcjogIzhkOGQ4ZDtcclxuICB3aWR0aDogMTAwJTtcclxufVxyXG5cclxuQG1lZGlhIChtaW4td2lkdGg6IDU2OHB4KSB7XHJcbiAgLmxvZ2luaW1hZ2Uge1xyXG4gICAgd2lkdGg6IDE1JSAhaW1wb3J0YW50O1xyXG4gIH1cclxufVxyXG4iXX0= */");

/***/ }),

/***/ 78100:
/*!*****************************************************************************************!*\
  !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/Pages/register/register.page.html ***!
  \*****************************************************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "default": () => (__WEBPACK_DEFAULT_EXPORT__)
/* harmony export */ });
/* harmony default export */ const __WEBPACK_DEFAULT_EXPORT__ = ("<!--\r\n  Generated template for the Registercustomer page.\r\n\r\n  See http://ionicframework.com/docs/v2/components/#navigation for more info on\r\n  Ionic pages and navigation.\r\n-->\r\n<ion-header>\r\n  <ion-toolbar color=\"headerblue\">\r\n    <!-- <button ion-button (click)=\"ToggleMenuBar()\">\r\n      <ion-icon name=\"menu\"></ion-icon>\r\n    </button> -->\r\n    <ion-title class=\"toptitle ion-text-center\"\r\n      ><span class=\"heavy\">REGISTER DRIVER</span></ion-title\r\n    >\r\n  </ion-toolbar>\r\n</ion-header>\r\n\r\n<ion-content>\r\n  <section class=\"support-section\">\r\n    <h4 style=\"text-align: center; font-size: 35px\">RideBidder</h4>\r\n    <!-- <img src=\"assets/images/supportlogo.png\"> -->\r\n    <section class=\"profileformwrapper\">\r\n      <form style=\"margin: 15px 20px\">\r\n        <input\r\n          class=\"field\"\r\n          type=\"text\"\r\n          name=\"fullname\"\r\n          placeholder=\"Name\"\r\n          [(ngModel)]=\"fullname\"\r\n        />\r\n\r\n        <input\r\n          class=\"field\"\r\n          type=\"text\"\r\n          name=\"Email\"\r\n          placeholder=\"Email\"\r\n          [(ngModel)]=\"email\"\r\n        />\r\n        <input\r\n          class=\"field\"\r\n          type=\"password\"\r\n          name=\"Password\"\r\n          placeholder=\"Password\"\r\n          [(ngModel)]=\"password\"\r\n        />\r\n\r\n        <input\r\n          class=\"field\"\r\n          type=\"password\"\r\n          name=\"Confirm Password\"\r\n          placeholder=\"Confirm Password\"\r\n          [(ngModel)]=\"confirmPassword\"\r\n        />\r\n        <input\r\n          class=\"field\"\r\n          type=\"text\"\r\n          name=\"Phone\"\r\n          placeholder=\"Contact Number\"\r\n          [(ngModel)]=\"phone\"\r\n        />\r\n        <input\r\n          class=\"field\"\r\n          type=\"text\"\r\n          name=\"City\"\r\n          placeholder=\"City\"\r\n          [(ngModel)]=\"cityname\"\r\n        />\r\n        <!--\t<input class=\"field\" type=\"text\" name=\"Vehicle Number\" placeholder=\"Vehicle Number\" [(ngModel)]=\"vehicle\">-->\r\n        <ion-button\r\n          style=\"margin-bottom: 5%; width: 100%\"\r\n          color=\"dark\"\r\n          (click)=\"doRegistration()\"\r\n        >\r\n          Register\r\n        </ion-button>\r\n\r\n        <div class=\"clearbutton-text\">\r\n          <span color=\"dark\">Already have an account?</span> |\r\n          <span (click)=\"goToSignin()\" color=\"primary\">Sign in here</span>\r\n        </div>\r\n      </form>\r\n    </section>\r\n  </section>\r\n</ion-content>\r\n");

/***/ })

}]);
//# sourceMappingURL=src_app_Pages_register_register_module_ts.js.map