(self["webpackChunkDriverApp"] = self["webpackChunkDriverApp"] || []).push([["src_app_Pages_support_support_module_ts"],{

/***/ 83654:
/*!*********************************************************!*\
  !*** ./src/app/Pages/support/support-routing.module.ts ***!
  \*********************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "SupportPageRoutingModule": () => (/* binding */ SupportPageRoutingModule)
/* harmony export */ });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! tslib */ 64762);
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/core */ 37716);
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/router */ 39895);
/* harmony import */ var _support_page__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./support.page */ 70739);




const routes = [
    {
        path: '',
        component: _support_page__WEBPACK_IMPORTED_MODULE_0__.SupportPage
    }
];
let SupportPageRoutingModule = class SupportPageRoutingModule {
};
SupportPageRoutingModule = (0,tslib__WEBPACK_IMPORTED_MODULE_1__.__decorate)([
    (0,_angular_core__WEBPACK_IMPORTED_MODULE_2__.NgModule)({
        imports: [_angular_router__WEBPACK_IMPORTED_MODULE_3__.RouterModule.forChild(routes)],
        exports: [_angular_router__WEBPACK_IMPORTED_MODULE_3__.RouterModule],
    })
], SupportPageRoutingModule);



/***/ }),

/***/ 22050:
/*!*************************************************!*\
  !*** ./src/app/Pages/support/support.module.ts ***!
  \*************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "SupportPageModule": () => (/* binding */ SupportPageModule)
/* harmony export */ });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! tslib */ 64762);
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/core */ 37716);
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @angular/common */ 38583);
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @angular/forms */ 3679);
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! @ionic/angular */ 80476);
/* harmony import */ var _support_routing_module__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./support-routing.module */ 83654);
/* harmony import */ var _support_page__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./support.page */ 70739);







let SupportPageModule = class SupportPageModule {
};
SupportPageModule = (0,tslib__WEBPACK_IMPORTED_MODULE_2__.__decorate)([
    (0,_angular_core__WEBPACK_IMPORTED_MODULE_3__.NgModule)({
        imports: [
            _angular_common__WEBPACK_IMPORTED_MODULE_4__.CommonModule,
            _angular_forms__WEBPACK_IMPORTED_MODULE_5__.FormsModule,
            _ionic_angular__WEBPACK_IMPORTED_MODULE_6__.IonicModule,
            _support_routing_module__WEBPACK_IMPORTED_MODULE_0__.SupportPageRoutingModule
        ],
        declarations: [_support_page__WEBPACK_IMPORTED_MODULE_1__.SupportPage]
    })
], SupportPageModule);



/***/ }),

/***/ 70739:
/*!***********************************************!*\
  !*** ./src/app/Pages/support/support.page.ts ***!
  \***********************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "SupportPage": () => (/* binding */ SupportPage)
/* harmony export */ });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! tslib */ 64762);
/* harmony import */ var _raw_loader_support_page_html__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! !raw-loader!./support.page.html */ 65510);
/* harmony import */ var _support_page_scss__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./support.page.scss */ 23529);
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/core */ 37716);
/* harmony import */ var _base_page_base_page__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../base-page/base-page */ 96093);





let SupportPage = class SupportPage extends _base_page_base_page__WEBPACK_IMPORTED_MODULE_2__.BasePage {
    constructor(injector) {
        super(injector);
        this.selectedTab = 'tab1';
    }
    ngOnInit() { }
    ionViewDidLoad() {
        console.log('ionViewDidLoad SupportPage');
    }
};
SupportPage.ctorParameters = () => [
    { type: _angular_core__WEBPACK_IMPORTED_MODULE_3__.Injector }
];
SupportPage = (0,tslib__WEBPACK_IMPORTED_MODULE_4__.__decorate)([
    (0,_angular_core__WEBPACK_IMPORTED_MODULE_3__.Component)({
        selector: 'app-support',
        template: _raw_loader_support_page_html__WEBPACK_IMPORTED_MODULE_0__.default,
        styles: [_support_page_scss__WEBPACK_IMPORTED_MODULE_1__.default]
    })
], SupportPage);



/***/ }),

/***/ 23529:
/*!*************************************************!*\
  !*** ./src/app/Pages/support/support.page.scss ***!
  \*************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "default": () => (__WEBPACK_DEFAULT_EXPORT__)
/* harmony export */ });
/* harmony default export */ const __WEBPACK_DEFAULT_EXPORT__ = (".toptitle {\n  padding-right: 16% !important;\n}\n\n.support-section {\n  padding-top: 15px;\n  width: 90%;\n  margin: 0 auto;\n}\n\n.support-section input {\n  opacity: 1 !important;\n  margin-bottom: 20 !important;\n}\n\n.support-section img {\n  width: auto;\n  display: block;\n  margin: 0 auto;\n  padding-bottom: 20px;\n}\n\n.supportpara {\n  font-family: \"PT Sans\";\n  font-size: 16px;\n  color: whitesmoke;\n  text-align: left;\n  line-height: 22px;\n  margin: 0;\n}\n\n.tab {\n  position: relative;\n  margin-bottom: 1px;\n  width: 100%;\n  color: #fff;\n  overflow: hidden;\n  margin-bottom: 10px;\n}\n\ninput {\n  position: absolute;\n  opacity: 0;\n  z-index: -1;\n}\n\nlabel {\n  position: relative;\n  display: block;\n  padding: 0 0 0 1em;\n  background: #16a085;\n  font-weight: bold;\n  line-height: 3;\n  cursor: pointer;\n}\n\n.blue label {\n  background: #f4f4f4;\n  border: 1px solid #d4d4d4;\n  border-radius: 5px;\n  color: #2e2e2e;\n  font-family: \"PT Sans\";\n  font-size: 15px;\n  line-height: 2rem;\n  font-weight: normal;\n  padding: 10px;\n}\n\n.tab-content {\n  max-height: 0;\n  overflow: hidden;\n  background: #1abc9c;\n  transition: max-height 0.35s;\n}\n\n.blue .tab-content {\n  background: #fff;\n  color: #919191;\n  font-family: \"PT Sans\";\n  font-size: 16px;\n  line-height: 22px;\n  text-align: left;\n}\n\n.tab-content p {\n  margin: 0.5em;\n}\n\n/* :checked */\n\ninput:checked ~ .tab-content {\n  max-height: 10em;\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInN1cHBvcnQucGFnZS5zY3NzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiJBQUFBO0VBQ0UsNkJBQUE7QUFDRjs7QUFDQTtFQUNFLGlCQUFBO0VBQ0EsVUFBQTtFQUNBLGNBQUE7QUFFRjs7QUFBQTtFQUNFLHFCQUFBO0VBQ0EsNEJBQUE7QUFHRjs7QUFEQTtFQUNFLFdBQUE7RUFDQSxjQUFBO0VBQ0EsY0FBQTtFQUNBLG9CQUFBO0FBSUY7O0FBRkE7RUFDRSxzQkFBQTtFQUNBLGVBQUE7RUFDQSxpQkFBQTtFQUNBLGdCQUFBO0VBQ0EsaUJBQUE7RUFDQSxTQUFBO0FBS0Y7O0FBRkE7RUFDRSxrQkFBQTtFQUNBLGtCQUFBO0VBQ0EsV0FBQTtFQUNBLFdBQUE7RUFDQSxnQkFBQTtFQUNBLG1CQUFBO0FBS0Y7O0FBRkE7RUFDRSxrQkFBQTtFQUNBLFVBQUE7RUFDQSxXQUFBO0FBS0Y7O0FBSEE7RUFDRSxrQkFBQTtFQUNBLGNBQUE7RUFDQSxrQkFBQTtFQUNBLG1CQUFBO0VBQ0EsaUJBQUE7RUFDQSxjQUFBO0VBQ0EsZUFBQTtBQU1GOztBQUpBO0VBQ0UsbUJBQUE7RUFDQSx5QkFBQTtFQUNBLGtCQUFBO0VBQ0EsY0FBQTtFQUNBLHNCQUFBO0VBQ0EsZUFBQTtFQUNBLGlCQUFBO0VBQ0EsbUJBQUE7RUFDQSxhQUFBO0FBT0Y7O0FBTEE7RUFDRSxhQUFBO0VBQ0EsZ0JBQUE7RUFDQSxtQkFBQTtFQUdBLDRCQUFBO0FBUUY7O0FBTkE7RUFDRSxnQkFBQTtFQUNBLGNBQUE7RUFDQSxzQkFBQTtFQUNBLGVBQUE7RUFDQSxpQkFBQTtFQUNBLGdCQUFBO0FBU0Y7O0FBTkE7RUFDRSxhQUFBO0FBU0Y7O0FBUEEsYUFBQTs7QUFDQTtFQUNFLGdCQUFBO0FBVUYiLCJmaWxlIjoic3VwcG9ydC5wYWdlLnNjc3MiLCJzb3VyY2VzQ29udGVudCI6WyIudG9wdGl0bGUge1xyXG4gIHBhZGRpbmctcmlnaHQ6IDE2JSAhaW1wb3J0YW50O1xyXG59XHJcbi5zdXBwb3J0LXNlY3Rpb24ge1xyXG4gIHBhZGRpbmctdG9wOiAxNXB4O1xyXG4gIHdpZHRoOiA5MCU7XHJcbiAgbWFyZ2luOiAwIGF1dG87XHJcbn1cclxuLnN1cHBvcnQtc2VjdGlvbiBpbnB1dCB7XHJcbiAgb3BhY2l0eTogMSAhaW1wb3J0YW50O1xyXG4gIG1hcmdpbi1ib3R0b206IDIwICFpbXBvcnRhbnQ7XHJcbn1cclxuLnN1cHBvcnQtc2VjdGlvbiBpbWcge1xyXG4gIHdpZHRoOiBhdXRvO1xyXG4gIGRpc3BsYXk6IGJsb2NrO1xyXG4gIG1hcmdpbjogMCBhdXRvO1xyXG4gIHBhZGRpbmctYm90dG9tOiAyMHB4O1xyXG59XHJcbi5zdXBwb3J0cGFyYSB7XHJcbiAgZm9udC1mYW1pbHk6IFwiUFQgU2Fuc1wiO1xyXG4gIGZvbnQtc2l6ZTogMTZweDtcclxuICBjb2xvcjogd2hpdGVzbW9rZTtcclxuICB0ZXh0LWFsaWduOiBsZWZ0O1xyXG4gIGxpbmUtaGVpZ2h0OiAyMnB4O1xyXG4gIG1hcmdpbjogMDtcclxufVxyXG5cclxuLnRhYiB7XHJcbiAgcG9zaXRpb246IHJlbGF0aXZlO1xyXG4gIG1hcmdpbi1ib3R0b206IDFweDtcclxuICB3aWR0aDogMTAwJTtcclxuICBjb2xvcjogI2ZmZjtcclxuICBvdmVyZmxvdzogaGlkZGVuO1xyXG4gIG1hcmdpbi1ib3R0b206IDEwcHg7XHJcbn1cclxuXHJcbmlucHV0IHtcclxuICBwb3NpdGlvbjogYWJzb2x1dGU7XHJcbiAgb3BhY2l0eTogMDtcclxuICB6LWluZGV4OiAtMTtcclxufVxyXG5sYWJlbCB7XHJcbiAgcG9zaXRpb246IHJlbGF0aXZlO1xyXG4gIGRpc3BsYXk6IGJsb2NrO1xyXG4gIHBhZGRpbmc6IDAgMCAwIDFlbTtcclxuICBiYWNrZ3JvdW5kOiAjMTZhMDg1O1xyXG4gIGZvbnQtd2VpZ2h0OiBib2xkO1xyXG4gIGxpbmUtaGVpZ2h0OiAzO1xyXG4gIGN1cnNvcjogcG9pbnRlcjtcclxufVxyXG4uYmx1ZSBsYWJlbCB7XHJcbiAgYmFja2dyb3VuZDogI2Y0ZjRmNDtcclxuICBib3JkZXI6IDFweCBzb2xpZCAjZDRkNGQ0O1xyXG4gIGJvcmRlci1yYWRpdXM6IDVweDtcclxuICBjb2xvcjogIzJlMmUyZTtcclxuICBmb250LWZhbWlseTogXCJQVCBTYW5zXCI7XHJcbiAgZm9udC1zaXplOiAxNXB4O1xyXG4gIGxpbmUtaGVpZ2h0OiAycmVtO1xyXG4gIGZvbnQtd2VpZ2h0OiBub3JtYWw7XHJcbiAgcGFkZGluZzogMTBweDtcclxufVxyXG4udGFiLWNvbnRlbnQge1xyXG4gIG1heC1oZWlnaHQ6IDA7XHJcbiAgb3ZlcmZsb3c6IGhpZGRlbjtcclxuICBiYWNrZ3JvdW5kOiAjMWFiYzljO1xyXG4gIC13ZWJraXQtdHJhbnNpdGlvbjogbWF4LWhlaWdodCAwLjM1cztcclxuICAtby10cmFuc2l0aW9uOiBtYXgtaGVpZ2h0IDAuMzVzO1xyXG4gIHRyYW5zaXRpb246IG1heC1oZWlnaHQgMC4zNXM7XHJcbn1cclxuLmJsdWUgLnRhYi1jb250ZW50IHtcclxuICBiYWNrZ3JvdW5kOiAjZmZmO1xyXG4gIGNvbG9yOiAjOTE5MTkxO1xyXG4gIGZvbnQtZmFtaWx5OiBcIlBUIFNhbnNcIjtcclxuICBmb250LXNpemU6IDE2cHg7XHJcbiAgbGluZS1oZWlnaHQ6IDIycHg7XHJcbiAgdGV4dC1hbGlnbjogbGVmdDtcclxufVxyXG5cclxuLnRhYi1jb250ZW50IHAge1xyXG4gIG1hcmdpbjogMC41ZW07XHJcbn1cclxuLyogOmNoZWNrZWQgKi9cclxuaW5wdXQ6Y2hlY2tlZCB+IC50YWItY29udGVudCB7XHJcbiAgbWF4LWhlaWdodDogMTBlbTtcclxufVxyXG4iXX0= */");

/***/ }),

/***/ 65510:
/*!***************************************************************************************!*\
  !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/Pages/support/support.page.html ***!
  \***************************************************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "default": () => (__WEBPACK_DEFAULT_EXPORT__)
/* harmony export */ });
/* harmony default export */ const __WEBPACK_DEFAULT_EXPORT__ = ("<!--\r\n  Generated template for the Support page.\r\n\r\n  See http://ionicframework.com/docs/v2/components/#navigation for more info on\r\n  Ionic pages and navigation.\r\n-->\r\n<ion-header>\r\n  <ion-toolbar color=\"dark\">\r\n    <ion-buttons slot=\"start\">\r\n      <!-- <ion-button fill=\"clear\" (click)=\"ToggleMenuBar()\">\r\n      <ion-icon name=\"menu\"></ion-icon>\r\n    </ion-button> -->\r\n      <ion-button (click)=\"nav.pop()\">\r\n        <ion-icon name=\"chevron-back-outline\"></ion-icon>\r\n      </ion-button>\r\n    </ion-buttons>\r\n    <ion-title><span class=\"heavy toptitle\">SUPPORT</span></ion-title>\r\n  </ion-toolbar>\r\n</ion-header>\r\n\r\n<ion-content>\r\n  <div padding>\r\n    <ion-segment [(ngModel)]=\"selectedTab\">\r\n      <ion-segment-button value=\"tab1\"> FAQ </ion-segment-button>\r\n      <ion-segment-button value=\"tab2\"> Code of Conduct </ion-segment-button>\r\n    </ion-segment>\r\n  </div>\r\n\r\n  <div [ngSwitch]=\"selectedTab\">\r\n    <section class=\"support-section\" *ngSwitchCase=\"'tab1'\">\r\n      <div class=\"tab blue\">\r\n        <label>How can I contact the driver?</label>\r\n        <div>\r\n          <p style=\"color: white\">\r\n            Once the job has been assigned to the driver, under the booking tab\r\n            you select the contact button which will call the driver.\r\n          </p>\r\n        </div>\r\n      </div>\r\n      <div class=\"tab blue\">\r\n        <label> Is there a charge if I cancel the driver?</label>\r\n        <div>\r\n          <p style=\"color: white\">\r\n            No, you will not be charged if you cancel the job straight away.\r\n            However, if you cancel the job 5 minutes after accepting a bid - you\r\n            will be charged $7.\r\n          </p>\r\n        </div>\r\n      </div>\r\n      <div class=\"tab blue\">\r\n        <label>Can I request a specific driver?</label>\r\n        <div>\r\n          <p style=\"color: white\">\r\n            Its better to let all the drivers who are free and close by to give\r\n            you offers. It’s possible that driver is heading on the direction of\r\n            your trip, which means you can score a big bargain.\r\n          </p>\r\n        </div>\r\n      </div>\r\n      <!--<div class=\"tab blue\">-->\r\n      <!--<label>Can I request a specific driver? </label>-->\r\n      <!--<div>-->\r\n      <!--<p style=\"color:white\">We automatically connect you with the drivers in the area to get you picked up as quickly as possible. As such, it is not possible to request a specific driver at the moment.</p>-->\r\n      <!--</div>-->\r\n      <!--</div>-->\r\n      <div class=\"tab blue\">\r\n        <label>How do I pay the driver?</label>\r\n        <div>\r\n          <p style=\"color: white\">\r\n            <strong>Ridebidder</strong> does not use cash, all the payments are\r\n            made using the app platform. This a safe and reliable way to make a\r\n            transaction.\r\n          </p>\r\n        </div>\r\n      </div>\r\n      <!-- <div class=\"tab blue\">\r\n         <label>In which city is Ridebidder available?</label>\r\n         <div>\r\n         <p style=\"color:white\">Ridebidder is available in Sydney, Australia. Find updated coverage area on www.pickmyride.com.au</p>\r\n         </div>\r\n       </div>-->\r\n      <div class=\"tab blue\">\r\n        <label>Which City is <strong> Ridebidder </strong>available?</label>\r\n        <div>\r\n          <p style=\"color: white\">\r\n            Currently the App is being launched in Sydney. We will be expanding\r\n            to other countries and cities, so keep an eye out for our presence\r\n            in your location.\r\n          </p>\r\n        </div>\r\n      </div>\r\n      <div class=\"tab blue\">\r\n        <label> Who are your drivers?</label>\r\n        <div>\r\n          <p style=\"color: white\">\r\n            Our drivers are contracted with us to provide you with the best\r\n            service that an agreed price. We have ensured that are drivers meet\r\n            our strict requirements of code of conduct to ensure your safe and\r\n            satisfaction.\r\n          </p>\r\n        </div>\r\n      </div>\r\n      <div class=\"tab blue\">\r\n        <label>Can I track the location of the driver?</label>\r\n        <div>\r\n          <p style=\"color: white\">\r\n            Yes, you can track where the driver is at any time, this allows you\r\n            to get ready to start your trip when the driver arrives at your\r\n            location.\r\n          </p>\r\n        </div>\r\n      </div>\r\n\r\n      <div class=\"tab blue\">\r\n        <label>How does your support work?</label>\r\n        <div>\r\n          <p style=\"color: white\">\r\n            In case if any issues, please use contact us and someone will be on\r\n            contact to remediate your concerns as soon as possible.\r\n          </p>\r\n        </div>\r\n\r\n        <div class=\"tab blue\">\r\n          <label>How secure is my personal information?</label>\r\n          <div>\r\n            <p style=\"color: white\">\r\n              Riddbidder doesn't store any credit card specific data, not even\r\n              in encrypted fashion. Payment is via stripe, stripe hold at\r\n              details and are PCI compliant payment gateway.\r\n            </p>\r\n          </div>\r\n        </div>\r\n      </div>\r\n    </section>\r\n\r\n    <section class=\"support-section\" *ngSwitchCase=\"'tab2'\">\r\n      <p>\r\n        <strong>1. Instruction</strong> - While technology helps ensure the\r\n        speed, dependability, and safety of our drivers and customers, people\r\n        matter most to the Ridebidder experience. And it is a two-way street\r\n        between our Driver Partners and Riders. We created this Code of Conduct\r\n        so that everyone in the vehicle has a shared standard for respect,\r\n        accountability, and common courtesy.\r\n      </p>\r\n      <p>\r\n        <strong>2. Professionalism perfect</strong> - Ridebidder maintains a\r\n        zero-tolerance policy regarding all forms of discrimination, harassment\r\n        or abuse. It is unacceptable to refuse to provide or accept services\r\n        based on a person is race, religion, national origin, disability, sexual\r\n        orientation, sex, marital status, gender identity, age or any other\r\n        characteristic protected under applicable federal or state law. This\r\n        type of behavior can result in permanent loss of access to the\r\n        Ridebidder app. No aggressive behavior, it is disrespectful to make\r\n        derogatory remarks about a person or group. Furthermore, commenting on\r\n        appearance, asking overly personal questions and making unwanted\r\n        physical contact are all inappropriate. We encourage you to be mindful\r\n        of other users privacy and personal space. The violence of any kind will\r\n        not be tolerated. Human Kindness Calm and clear communication is the\r\n        most effective way to defuse any disagreement that may arise between you\r\n        and another Ridebidder user. Ridebidder expects that all riders and\r\n        drivers will treat one another with respect and courtesy.\r\n      </p>\r\n\r\n      <p>\r\n        <strong>3. Safety</strong> - The safety of riders and drivers on the\r\n        Ridebidder platform is of utmost concern. In order to best protect\r\n        everyone in the vehicle, we require the following: Compliance with the\r\n        local law at all time. Furthermore, Ridebidder does not tolerate drug or\r\n        alcohol by drivers while using the Ridebidder app. If a rider believes a\r\n        driver may be under the influence of drugs or alcohol, please request\r\n        that the driver ends the trip immediately and alert Ridebidder Support\r\n        on contact@ridebidder.com.au. As a driver, it is your responsibility to\r\n        transport riders safely in accordance with the rules of the road in your\r\n        city. As a rider, it is your responsibility to abide by the seat belt\r\n        laws in your state. However, we recommend that you always wear a\r\n        seatbelt while riding in any vehicle. Following the rules, We require\r\n        partners to keep documents up to date to remain active. Riders,\r\n        likewise, must maintain active payment information. Riders are\r\n        responsible for guests traveling with them or anyone they request a ride\r\n        for. It is your responsibility to ensure everyone adheres to Ridebidder\r\n        Code of Conduct. Violations of this Code of Conduct could result in loss\r\n        of your Ridebidder Account. Please report any violations to Ridebidder\r\n        at contact@ridebidder.com.au.\r\n      </p>\r\n\r\n      <p>\r\n        <strong>4. Emergencies</strong> - If at any time you feel that you are\r\n        faced with a situation that requires immediate emergency attention,\r\n        please call the emergency service number in your area. Once all parties\r\n        are safe and the authorities have handled the situation, please then\r\n        notify Ridebidder at contact@ridebidder.com.au. We will assist and take\r\n        appropriate action as needed, including cooperating with law\r\n        enforcement.\r\n      </p>\r\n    </section>\r\n  </div>\r\n</ion-content>\r\n");

/***/ })

}]);
//# sourceMappingURL=src_app_Pages_support_support_module_ts.js.map