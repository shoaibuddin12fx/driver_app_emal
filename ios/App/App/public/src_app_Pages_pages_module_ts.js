(self["webpackChunkDriverApp"] = self["webpackChunkDriverApp"] || []).push([["src_app_Pages_pages_module_ts"],{

/***/ 15324:
/*!***************************************!*\
  !*** ./src/app/Pages/pages.module.ts ***!
  \***************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "PagesModule": () => (/* binding */ PagesModule)
/* harmony export */ });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! tslib */ 64762);
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/common */ 38583);
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/core */ 37716);
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @ionic/angular */ 80476);
/* harmony import */ var _pages_routing_module__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./pages.routing.module */ 47517);





let PagesModule = class PagesModule {
};
PagesModule = (0,tslib__WEBPACK_IMPORTED_MODULE_1__.__decorate)([
    (0,_angular_core__WEBPACK_IMPORTED_MODULE_2__.NgModule)({
        declarations: [],
        entryComponents: [],
        imports: [_angular_common__WEBPACK_IMPORTED_MODULE_3__.CommonModule, _ionic_angular__WEBPACK_IMPORTED_MODULE_4__.IonicModule, _pages_routing_module__WEBPACK_IMPORTED_MODULE_0__.PagesRoutingModule],
        //   providers: [{ provide: RouteReuseStrategy, useClass: IonicRouteStrategy }],
        providers: [],
    })
], PagesModule);



/***/ }),

/***/ 47517:
/*!***********************************************!*\
  !*** ./src/app/Pages/pages.routing.module.ts ***!
  \***********************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "PagesRoutingModule": () => (/* binding */ PagesRoutingModule)
/* harmony export */ });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ 64762);
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ 37716);
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/router */ 39895);



const routes = [
    {
        path: '',
        redirectTo: 'login',
        pathMatch: 'full',
    },
    {
        path: 'home',
        loadChildren: () => __webpack_require__.e(/*! import() */ "src_app_Pages_home_home_module_ts").then(__webpack_require__.bind(__webpack_require__, /*! ./home/home.module */ 87643)).then((m) => m.HomePageModule),
    },
    {
        path: 'login',
        loadChildren: () => __webpack_require__.e(/*! import() */ "src_app_Pages_login_login_module_ts").then(__webpack_require__.bind(__webpack_require__, /*! ./login/login.module */ 45342)).then((m) => m.LoginPageModule),
    },
    {
        path: 'register',
        loadChildren: () => __webpack_require__.e(/*! import() */ "src_app_Pages_register_register_module_ts").then(__webpack_require__.bind(__webpack_require__, /*! ./register/register.module */ 25010)).then((m) => m.RegisterPageModule),
    },
    {
        path: 'tabs',
        loadChildren: () => __webpack_require__.e(/*! import() */ "src_app_Pages_tabs_tabs_module_ts").then(__webpack_require__.bind(__webpack_require__, /*! ./tabs/tabs.module */ 7805)).then((m) => m.TabsPageModule),
    },
    {
        path: 'bid-price',
        loadChildren: () => __webpack_require__.e(/*! import() */ "src_app_Pages_bid-price_bid-price_module_ts").then(__webpack_require__.bind(__webpack_require__, /*! ./bid-price/bid-price.module */ 54467)).then((m) => m.BidPricePageModule),
    },
    {
        path: 'about',
        loadChildren: () => __webpack_require__.e(/*! import() */ "src_app_Pages_about_about_module_ts").then(__webpack_require__.bind(__webpack_require__, /*! ./about/about.module */ 18431)).then((m) => m.AboutPageModule),
    },
    {
        path: 'active-booking',
        loadChildren: () => __webpack_require__.e(/*! import() */ "src_app_Pages_active-booking_active-booking_module_ts").then(__webpack_require__.bind(__webpack_require__, /*! ./active-booking/active-booking.module */ 57926)).then((m) => m.ActiveBookingPageModule),
    },
    {
        path: 'job-complete',
        loadChildren: () => __webpack_require__.e(/*! import() */ "src_app_Pages_job-complete_job-complete_module_ts").then(__webpack_require__.bind(__webpack_require__, /*! ./job-complete/job-complete.module */ 16744)).then((m) => m.JobCompletePageModule),
    },
    {
        path: 'profile',
        loadChildren: () => __webpack_require__.e(/*! import() */ "src_app_Pages_profile_profile_module_ts").then(__webpack_require__.bind(__webpack_require__, /*! ./profile/profile.module */ 60824)).then((m) => m.ProfilePageModule),
    },
    {
        path: 'share',
        loadChildren: () => __webpack_require__.e(/*! import() */ "src_app_Pages_share_share_module_ts").then(__webpack_require__.bind(__webpack_require__, /*! ./share/share.module */ 34222)).then((m) => m.SharePageModule),
    },
    {
        path: 'stats',
        loadChildren: () => __webpack_require__.e(/*! import() */ "src_app_Pages_stats_stats_module_ts").then(__webpack_require__.bind(__webpack_require__, /*! ./stats/stats.module */ 33299)).then((m) => m.StatsPageModule),
    },
    {
        path: 'support',
        loadChildren: () => __webpack_require__.e(/*! import() */ "src_app_Pages_support_support_module_ts").then(__webpack_require__.bind(__webpack_require__, /*! ./support/support.module */ 22050)).then((m) => m.SupportPageModule),
    },
];
let PagesRoutingModule = class PagesRoutingModule {
};
PagesRoutingModule = (0,tslib__WEBPACK_IMPORTED_MODULE_0__.__decorate)([
    (0,_angular_core__WEBPACK_IMPORTED_MODULE_1__.NgModule)({
        imports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__.RouterModule.forChild(routes)],
        exports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__.RouterModule],
    })
], PagesRoutingModule);



/***/ })

}]);
//# sourceMappingURL=src_app_Pages_pages_module_ts.js.map