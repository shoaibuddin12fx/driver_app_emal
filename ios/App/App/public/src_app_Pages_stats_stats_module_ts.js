(self["webpackChunkDriverApp"] = self["webpackChunkDriverApp"] || []).push([["src_app_Pages_stats_stats_module_ts"],{

/***/ 37661:
/*!*****************************************************!*\
  !*** ./src/app/Pages/stats/stats-routing.module.ts ***!
  \*****************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "StatsPageRoutingModule": () => (/* binding */ StatsPageRoutingModule)
/* harmony export */ });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! tslib */ 64762);
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/core */ 37716);
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/router */ 39895);
/* harmony import */ var _stats_page__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./stats.page */ 1492);




const routes = [
    {
        path: '',
        component: _stats_page__WEBPACK_IMPORTED_MODULE_0__.StatsPage
    }
];
let StatsPageRoutingModule = class StatsPageRoutingModule {
};
StatsPageRoutingModule = (0,tslib__WEBPACK_IMPORTED_MODULE_1__.__decorate)([
    (0,_angular_core__WEBPACK_IMPORTED_MODULE_2__.NgModule)({
        imports: [_angular_router__WEBPACK_IMPORTED_MODULE_3__.RouterModule.forChild(routes)],
        exports: [_angular_router__WEBPACK_IMPORTED_MODULE_3__.RouterModule],
    })
], StatsPageRoutingModule);



/***/ }),

/***/ 33299:
/*!*********************************************!*\
  !*** ./src/app/Pages/stats/stats.module.ts ***!
  \*********************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "StatsPageModule": () => (/* binding */ StatsPageModule)
/* harmony export */ });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! tslib */ 64762);
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/core */ 37716);
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @angular/common */ 38583);
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @angular/forms */ 3679);
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! @ionic/angular */ 80476);
/* harmony import */ var _stats_routing_module__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./stats-routing.module */ 37661);
/* harmony import */ var _stats_page__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./stats.page */ 1492);







let StatsPageModule = class StatsPageModule {
};
StatsPageModule = (0,tslib__WEBPACK_IMPORTED_MODULE_2__.__decorate)([
    (0,_angular_core__WEBPACK_IMPORTED_MODULE_3__.NgModule)({
        imports: [
            _angular_common__WEBPACK_IMPORTED_MODULE_4__.CommonModule,
            _angular_forms__WEBPACK_IMPORTED_MODULE_5__.FormsModule,
            _ionic_angular__WEBPACK_IMPORTED_MODULE_6__.IonicModule,
            _stats_routing_module__WEBPACK_IMPORTED_MODULE_0__.StatsPageRoutingModule
        ],
        declarations: [_stats_page__WEBPACK_IMPORTED_MODULE_1__.StatsPage]
    })
], StatsPageModule);



/***/ }),

/***/ 1492:
/*!*******************************************!*\
  !*** ./src/app/Pages/stats/stats.page.ts ***!
  \*******************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "StatsPage": () => (/* binding */ StatsPage)
/* harmony export */ });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! tslib */ 64762);
/* harmony import */ var _raw_loader_stats_page_html__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! !raw-loader!./stats.page.html */ 85497);
/* harmony import */ var _stats_page_scss__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./stats.page.scss */ 57430);
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @angular/core */ 37716);
/* harmony import */ var _base_page_base_page__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../base-page/base-page */ 96093);





let StatsPage = class StatsPage extends _base_page_base_page__WEBPACK_IMPORTED_MODULE_2__.BasePage {
    constructor(injecotr) {
        super(injecotr);
    }
    ngOnInit() {
        return (0,tslib__WEBPACK_IMPORTED_MODULE_3__.__awaiter)(this, void 0, void 0, function* () {
            this.curUser = yield this.firebaseService.getCurrentUser();
            this.userRef = this.firebaseService.getDbRef('users/' + this.curUser.uid);
            this.ref = this.firebaseService.getDbRef('users/' + this.curUser.uid + '/bookings');
            this.ref.once('value', (snapshot) => {
                this.allbookings = [];
                let count = 0;
                let countratings = 0;
                this.todays_earning = 0;
                this.months_earning = 0;
                this.weeks_earning = 0;
                if (snapshot.val()) {
                    this.data = snapshot.val();
                    for (let key in this.data) {
                        this.data[key].key = key;
                        this.allbookings.push(this.data[key]);
                        // console.log(this.allbookings);
                        // console.log(this.allbookings.length);
                    }
                }
                for (let i = 0; i < this.allbookings.length; i++) {
                    // console.log(this.allbookings[i].ratings);
                    if (this.allbookings[i].ratings) {
                        countratings += this.allbookings[i].ratings.rating;
                        count += 1;
                        this.avgRatings = countratings / count;
                        this.avgRatings = this.avgRatings.toFixed(1);
                    }
                    // console.log(this.avgRatings);
                }
                this.completedBookings = 0;
                this.LifetimeBookings = 0;
                for (let j = 0; j < this.allbookings.length; j++) {
                    if (this.allbookings[j].status == 'ENDED' ||
                        this.allbookings[j].status == 'REVIEW') {
                        this.LifetimeBookings += this.allbookings[j].trip_cost;
                        this.completedBookings += 1;
                    }
                }
                this.LifetimeBookings = this.LifetimeBookings.toFixed(2);
                console.log('Ere', this.allbookings);
                for (let x = 0; x < this.allbookings.length; x++) {
                    if (this.allbookings[x].status == 'ENDED' ||
                        this.allbookings[x].status == 'REVIEW') {
                        console.log(this.allbookings[x]);
                        let date1 = new Date(this.allbookings[x].tripdate);
                        let date2 = new Date();
                        let timeDiff = Math.abs(date2.getTime() - date1.getTime());
                        let days_past = Math.ceil(timeDiff / (1000 * 3600 * 24));
                        console.log(days_past, this.allbookings[x].trip_cost);
                        if (days_past <= 1) {
                            this.todays_earning =
                                parseInt(this.todays_earning) +
                                    parseInt(this.allbookings[x].trip_cost);
                        }
                        if (days_past <= 7) {
                            this.weeks_earning =
                                parseInt(this.weeks_earning) +
                                    parseInt(this.allbookings[x].trip_cost);
                        }
                        if (days_past <= 30) {
                            this.months_earning =
                                parseInt(this.months_earning) +
                                    parseInt(this.allbookings[x].trip_cost);
                        }
                    }
                }
                //time difference || today's earning && month earning
                /*this.todayBookings = 0;
                this.currentDate = new Date().toISOString().substring(0, 10);
                this.monthBookings = 0;
                this.currentMonth = new Date().toISOString().substring(0, 7);
                for( let t=0; t<this.allbookings.length; t++){
                    if(this.allbookings[t].tripdate){
                        let time = this.allbookings[t].tripdate;
                        let newtime = time.substring(0, 10);
                        let newMonth = time.substring(0, 7);
                            if(this.currentDate == newtime){
                                this.todayBookings += this.allbookings[t].trip_cost;
                            }
          
                            if(this.currentMonth == newMonth){
                                this.monthBookings += this.allbookings[t].trip_cost;
                            }
                            console.log(newMonth);
                            console.log("month bookings:" + this.monthBookings);
                        //console.log(newtime);
                        console.log("bookings today :" + this.todayBookings);
                    }
                }*/
            });
            //last_billing_ammount
            this.userRef.on('value', (snap1) => {
                if (snap1.val()) {
                    if (snap1.val().last_billing_id) {
                        let billingId = snap1.val().last_billing_id;
                        let billingRef = this.firebaseService.getDbRef('users/' + this.curUser.uid + '/bookings/' + billingId);
                        billingRef.once('value', (snap2) => {
                            if (snap2.val()) {
                                if (snap2.val().trip_cost) {
                                    this.last_billing_ammount = snap2.val().trip_cost;
                                }
                            }
                        });
                    }
                }
            });
            /*
                    this.userRef.on('value',(snap1:any)=>{
                        if(snap1.val()){
        
                            this.ref = this.firebaseService.getDbRef('users/'+ this.curUser.uid+ '/bookings');
                                this.ref.once('value',(snapshot:any)=>{
                                    this.allbookings=[];
                                    let count=0;
                                    let countratings = 0;
        
                                    //last billing
                                    if(snapshot.val()){
                                      this.data = snapshot.val();
                                          if(snap1.val().last_billing_id){
                                            let billingId = snap1.val().last_billing_id;
                                            let billingRef = this.firebaseService.getDbRef('users/' + this.curUser.uid + '/bookings/'+ billingId);
        
                                                billingRef.once('value',(snap2:any)=>{
                                                  if(snap2.val()){
                                                      if(snap2.val().trip_cost){
                                                        this.last_billing_ammount = snap2.val().trip_cost;
                                                      }
                                                  }
                                                })
                                          }
        
                                    //allbookings
                                      for(let key in this.data){
                                        this.data[key].key=key;
                                        this.allbookings.push(this.data[key]);
                                          console.log(this.allbookings);
                                          console.log(this.allbookings.length);
        
                                        }
                                    }
                                        //average rating
                                        for(let i=0; i<this.allbookings.length; i++ ){
                                            console.log(this.allbookings[i].ratings);
                                            if(this.allbookings[i].ratings){
                                                countratings += this.allbookings[i].ratings.rating;
                                                count +=1;
                                                this.avgRatings = countratings/count;
                                                this.avgRatings= this.avgRatings.toFixed(1);
                                            }
                                            console.log(this.avgRatings);
                                        }
        
                                        //complete bookings || lifetime booking
                                        this.completedBookings = 0;
                                        this.LifetimeBookings = 0;
                                        for( let j = 0; j<this.allbookings.length; j++){
                                            if(this.allbookings[j].status == "ENDED" || this.allbookings[j].status == "REVIEW"){
                                                this.LifetimeBookings +=this.allbookings[j].trip_cost;
                                                this.completedBookings +=1;
                                            }
                                        }
        
                                        //time difference || today's earning && month earning
                                        this.todayBookings = 0;
                                        this.currentDate = new Date().toISOString().substring(0, 10);
                                        this.monthBookings = 0;
                                        this.currentMonth = new Date().toISOString().substring(0, 7);
                                        for( let t=0; t<this.allbookings.length; t++){
                                            if(this.allbookings[t].tripdate){
                                                let time = this.allbookings[t].tripdate;
                                                let newtime = time.substring(0, 10);
                                                let newMonth = time.substring(0, 7);
                                                    if(this.currentDate == newtime){
                                                        this.todayBookings += this.allbookings[t].trip_cost;
                                                    }
        
                                                    if(this.currentMonth == newMonth){
                                                        this.monthBookings += this.allbookings[t].trip_cost;
                                                    }
                                                    console.log(newMonth);
                                                    console.log("month bookings:" + this.monthBookings);
                                                //console.log(newtime);
                                                console.log("bookings today :" + this.todayBookings);
                                            }
                                        }
        
                                });
        
        
                         }
                    });*/
            /*  let dateTime = this.formatLocalDate();
              console.log(dateTime);*/
        });
    }
    ionViewDidLoad() {
        console.log('ionViewDidLoad StatsPage');
    }
    formatLocalDate() {
        var now = new Date(), tzo = -now.getTimezoneOffset(), dif = tzo >= 0 ? '+' : '-', pad = function (num) {
            var norm = Math.abs(Math.floor(num));
            return (norm < 10 ? '0' : '') + norm;
        };
        return (now.getFullYear() +
            '-' +
            pad(now.getMonth() + 1) +
            '-' +
            pad(now.getDate()));
    }
};
StatsPage.ctorParameters = () => [
    { type: _angular_core__WEBPACK_IMPORTED_MODULE_4__.Injector }
];
StatsPage = (0,tslib__WEBPACK_IMPORTED_MODULE_3__.__decorate)([
    (0,_angular_core__WEBPACK_IMPORTED_MODULE_4__.Component)({
        selector: 'app-stats',
        template: _raw_loader_stats_page_html__WEBPACK_IMPORTED_MODULE_0__.default,
        styles: [_stats_page_scss__WEBPACK_IMPORTED_MODULE_1__.default]
    })
], StatsPage);



/***/ }),

/***/ 57430:
/*!*********************************************!*\
  !*** ./src/app/Pages/stats/stats.page.scss ***!
  \*********************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "default": () => (__WEBPACK_DEFAULT_EXPORT__)
/* harmony export */ });
/* harmony default export */ const __WEBPACK_DEFAULT_EXPORT__ = (".statsbanner {\n  width: 100%;\n  background: url(\"/assets/images/statback.png\") no-repeat;\n  background-size: cover;\n  padding: 40px 0;\n}\n\n.statsbanner img {\n  display: block;\n  margin: 0 auto;\n  border-radius: 50%;\n  width: 25%;\n  border: 1px solid #fff;\n}\n\n.stat-list p {\n  font-family: \"PT Sans\";\n  font-size: 17px;\n  color: #2b2b2b;\n  text-align: left;\n  margin: 0;\n  padding-left: 8px;\n}\n\n.stat-list img {\n  display: block;\n  margin: 0 auto;\n}\n\n.stat-list span {\n  color: #4267b2;\n  text-align: right;\n  display: block;\n  font-size: 21px;\n}\n\n.statlist-paddingleft {\n  padding-left: 0px !important;\n}\n\n.statcustom-padding {\n  padding: 3px 0 0 0;\n}\n\n.header {\n  background-color: #1e1e1e;\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInN0YXRzLnBhZ2Uuc2NzcyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiQUFBQTtFQUNFLFdBQUE7RUFDQSx3REFBQTtFQUNBLHNCQUFBO0VBQ0EsZUFBQTtBQUNGOztBQUNBO0VBQ0UsY0FBQTtFQUNBLGNBQUE7RUFDQSxrQkFBQTtFQUNBLFVBQUE7RUFDQSxzQkFBQTtBQUVGOztBQUFBO0VBQ0Usc0JBQUE7RUFDQSxlQUFBO0VBQ0EsY0FBQTtFQUNBLGdCQUFBO0VBQ0EsU0FBQTtFQUNBLGlCQUFBO0FBR0Y7O0FBREE7RUFDRSxjQUFBO0VBQ0EsY0FBQTtBQUlGOztBQUZBO0VBQ0UsY0FBQTtFQUNBLGlCQUFBO0VBQ0EsY0FBQTtFQUNBLGVBQUE7QUFLRjs7QUFIQTtFQUNFLDRCQUFBO0FBTUY7O0FBSkE7RUFDRSxrQkFBQTtBQU9GOztBQUxBO0VBQ0UseUJBQUE7QUFRRiIsImZpbGUiOiJzdGF0cy5wYWdlLnNjc3MiLCJzb3VyY2VzQ29udGVudCI6WyIuc3RhdHNiYW5uZXIge1xyXG4gIHdpZHRoOiAxMDAlO1xyXG4gIGJhY2tncm91bmQ6IHVybChcIi9hc3NldHMvaW1hZ2VzL3N0YXRiYWNrLnBuZ1wiKSBuby1yZXBlYXQ7XHJcbiAgYmFja2dyb3VuZC1zaXplOiBjb3ZlcjtcclxuICBwYWRkaW5nOiA0MHB4IDA7XHJcbn1cclxuLnN0YXRzYmFubmVyIGltZyB7XHJcbiAgZGlzcGxheTogYmxvY2s7XHJcbiAgbWFyZ2luOiAwIGF1dG87XHJcbiAgYm9yZGVyLXJhZGl1czogNTAlO1xyXG4gIHdpZHRoOiAyNSU7XHJcbiAgYm9yZGVyOiAxcHggc29saWQgI2ZmZjtcclxufVxyXG4uc3RhdC1saXN0IHAge1xyXG4gIGZvbnQtZmFtaWx5OiBcIlBUIFNhbnNcIjtcclxuICBmb250LXNpemU6IDE3cHg7XHJcbiAgY29sb3I6ICMyYjJiMmI7XHJcbiAgdGV4dC1hbGlnbjogbGVmdDtcclxuICBtYXJnaW46IDA7XHJcbiAgcGFkZGluZy1sZWZ0OiA4cHg7XHJcbn1cclxuLnN0YXQtbGlzdCBpbWcge1xyXG4gIGRpc3BsYXk6IGJsb2NrO1xyXG4gIG1hcmdpbjogMCBhdXRvO1xyXG59XHJcbi5zdGF0LWxpc3Qgc3BhbiB7XHJcbiAgY29sb3I6ICM0MjY3YjI7XHJcbiAgdGV4dC1hbGlnbjogcmlnaHQ7XHJcbiAgZGlzcGxheTogYmxvY2s7XHJcbiAgZm9udC1zaXplOiAyMXB4O1xyXG59XHJcbi5zdGF0bGlzdC1wYWRkaW5nbGVmdCB7XHJcbiAgcGFkZGluZy1sZWZ0OiAwcHggIWltcG9ydGFudDtcclxufVxyXG4uc3RhdGN1c3RvbS1wYWRkaW5nIHtcclxuICBwYWRkaW5nOiAzcHggMCAwIDA7XHJcbn1cclxuLmhlYWRlciB7XHJcbiAgYmFja2dyb3VuZC1jb2xvcjogIzFlMWUxZTtcclxufSJdfQ== */");

/***/ }),

/***/ 85497:
/*!***********************************************************************************!*\
  !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/Pages/stats/stats.page.html ***!
  \***********************************************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "default": () => (__WEBPACK_DEFAULT_EXPORT__)
/* harmony export */ });
/* harmony default export */ const __WEBPACK_DEFAULT_EXPORT__ = ("<!--\r\n  Generated template for the Stats page.\r\n\r\n  See http://ionicframework.com/docs/v2/components/#navigation for more info on\r\n  Ionic pages and navigation.\r\n-->\r\n<ion-header class=\"header\">\r\n  <ion-toolbar style=\"color: white\" color=\"headerblue\">\r\n    <ion-buttons slot=\"start\">\r\n      <ion-button (click)=\"ToggleMenuBar()\" fill=\"clear\">\r\n        <ion-icon\r\n          style=\"margin-left: 5px\"\r\n          class=\"toggleButton\"\r\n          name=\"menu\"\r\n        ></ion-icon>\r\n      </ion-button>\r\n    </ion-buttons>\r\n    <!-- <button ion-button (click)=\"ToggleMenuBar()\">\r\n      <ion-icon name=\"menu\"></ion-icon>\r\n    </button> -->\r\n    <ion-title class=\"toptitle\"> STATISTICS</ion-title>\r\n  </ion-toolbar>\r\n</ion-header>\r\n\r\n<ion-content>\r\n  <section class=\"statsbanner\">\r\n    <img src=\"assets/images/stats.png\" alt=\" stats\" />\r\n  </section>\r\n  <section class=\"stat-list\">\r\n    <ion-list>\r\n      <ion-item class=\"statlist-paddingleft\">\r\n        <ion-grid>\r\n          <ion-row>\r\n            <ion-col size=\"6\" class=\"statcustom-padding\">\r\n              <p style=\"color: white\">Last Bill</p>\r\n            </ion-col>\r\n            <ion-col size=\"2.5\" class=\"no-padding\"\r\n              ><img src=\"assets/images/pin.png\" alt=\"pin\"\r\n            /></ion-col>\r\n            <ion-col size=\"3.5\" class=\"no-padding\"\r\n              ><span\r\n                >{{last_billing_ammount ? \"$\" + last_billing_ammount :\r\n                \"0\"}}</span\r\n              >\r\n            </ion-col>\r\n          </ion-row>\r\n        </ion-grid>\r\n      </ion-item>\r\n      <ion-item class=\"statlist-paddingleft\">\r\n        <ion-grid>\r\n          <ion-row>\r\n            <ion-col size=\"6\" class=\"statcustom-padding\">\r\n              <p style=\"color: white\">Today’s Earning</p>\r\n            </ion-col>\r\n            <ion-col size=\"2.5\" class=\"no-padding\"\r\n              ><img src=\"assets/images/pin.png\" alt=\"pin\"\r\n            /></ion-col>\r\n            <ion-col size=\"3.5\" class=\"no-padding\"\r\n              ><span>{{todays_earning ? \"$\" + todays_earning : \"0\"}}</span>\r\n            </ion-col>\r\n          </ion-row>\r\n        </ion-grid>\r\n      </ion-item>\r\n      <ion-item class=\"statlist-paddingleft\">\r\n        <ion-grid>\r\n          <ion-row>\r\n            <ion-col size=\"6\" class=\"statcustom-padding\">\r\n              <p style=\"color: white\">Earnings This Week</p>\r\n            </ion-col>\r\n            <ion-col size=\"2.5\" class=\"no-padding\"\r\n              ><img src=\"assets/images/pin.png\" alt=\"pin\"\r\n            /></ion-col>\r\n            <ion-col size=\"3.5\" class=\"no-padding\"\r\n              ><span\r\n                >{{weeks_earning ? \"$\" + weeks_earning : \"0\"}}</span\r\n              ></ion-col\r\n            >\r\n          </ion-row>\r\n        </ion-grid>\r\n      </ion-item>\r\n      <ion-item class=\"statlist-paddingleft\">\r\n        <ion-grid>\r\n          <ion-row>\r\n            <ion-col size=\"6\" class=\"statcustom-padding\">\r\n              <p style=\"color: white\">Earnings This Month</p>\r\n            </ion-col>\r\n            <ion-col size=\"2.5\" class=\"no-padding\"\r\n              ><img src=\"assets/images/pin.png\" alt=\"pin\"\r\n            /></ion-col>\r\n            <ion-col size=\"3.5\" class=\"no-padding\"\r\n              ><span>{{months_earning ? \"$\" + months_earning : \"0\"}}</span>\r\n            </ion-col>\r\n          </ion-row>\r\n        </ion-grid>\r\n      </ion-item>\r\n\r\n      <ion-item class=\"statlist-paddingleft\">\r\n        <ion-grid>\r\n          <ion-row>\r\n            <ion-col size=\"6\" class=\"statcustom-padding\">\r\n              <p style=\"color: white\">Lifetime Earnings</p>\r\n            </ion-col>\r\n            <ion-col size=\"2.5\" class=\"no-padding\"\r\n              ><img src=\"assets/images/pin.png\" alt=\"pin\"\r\n            /></ion-col>\r\n            <ion-col size=\"3.5\" class=\"no-padding\"\r\n              ><span>{{LifetimeBookings ? \"$\" + LifetimeBookings : \"0\"}}</span>\r\n            </ion-col>\r\n          </ion-row>\r\n        </ion-grid>\r\n      </ion-item>\r\n      <ion-item class=\"statlist-paddingleft\">\r\n        <ion-grid>\r\n          <ion-row>\r\n            <ion-col size=\"6\" class=\"statcustom-padding\">\r\n              <p style=\"color: white\">Completed Bookings</p>\r\n            </ion-col>\r\n            <ion-col size=\"2.5\" class=\"no-padding\"\r\n              ><img src=\"assets/images/pin.png\" alt=\"pin\"\r\n            /></ion-col>\r\n            <ion-col size=\"3.5\" class=\"no-padding\"\r\n              ><span> {{completedBookings}}</span></ion-col\r\n            >\r\n          </ion-row>\r\n        </ion-grid>\r\n      </ion-item>\r\n      <ion-item class=\"statlist-paddingleft\">\r\n        <ion-grid>\r\n          <ion-row>\r\n            <ion-col size=\"6\" class=\"statcustom-padding\">\r\n              <p style=\"color: white\">Ratings</p>\r\n            </ion-col>\r\n            <ion-col size=\"2.5\" class=\"no-padding\"\r\n              ><img src=\"assets/images/pin.png\" alt=\"pin\"\r\n            /></ion-col>\r\n            <!--<ion-col size=\"3.5\" class=\"no-padding\"><span>$00.00</span></ion-col>-->\r\n            <ion-col size=\"3.5\" class=\"no-padding\"\r\n              ><span>{{avgRatings ?? 0}}</span></ion-col\r\n            >\r\n          </ion-row>\r\n        </ion-grid>\r\n      </ion-item>\r\n    </ion-list>\r\n  </section>\r\n</ion-content>\r\n");

/***/ })

}]);
//# sourceMappingURL=src_app_Pages_stats_stats_module_ts.js.map