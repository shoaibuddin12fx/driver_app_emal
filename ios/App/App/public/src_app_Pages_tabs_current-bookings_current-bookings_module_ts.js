(self["webpackChunkDriverApp"] = self["webpackChunkDriverApp"] || []).push([["src_app_Pages_tabs_current-bookings_current-bookings_module_ts"],{

/***/ 14414:
/*!********************************************************************************!*\
  !*** ./src/app/Pages/tabs/current-bookings/current-bookings-routing.module.ts ***!
  \********************************************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "CurrentBookingsPageRoutingModule": () => (/* binding */ CurrentBookingsPageRoutingModule)
/* harmony export */ });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! tslib */ 64762);
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/core */ 37716);
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/router */ 39895);
/* harmony import */ var _current_bookings_page__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./current-bookings.page */ 67771);




const routes = [
    {
        path: '',
        component: _current_bookings_page__WEBPACK_IMPORTED_MODULE_0__.CurrentBookingsPage
    }
];
let CurrentBookingsPageRoutingModule = class CurrentBookingsPageRoutingModule {
};
CurrentBookingsPageRoutingModule = (0,tslib__WEBPACK_IMPORTED_MODULE_1__.__decorate)([
    (0,_angular_core__WEBPACK_IMPORTED_MODULE_2__.NgModule)({
        imports: [_angular_router__WEBPACK_IMPORTED_MODULE_3__.RouterModule.forChild(routes)],
        exports: [_angular_router__WEBPACK_IMPORTED_MODULE_3__.RouterModule],
    })
], CurrentBookingsPageRoutingModule);



/***/ }),

/***/ 45874:
/*!************************************************************************!*\
  !*** ./src/app/Pages/tabs/current-bookings/current-bookings.module.ts ***!
  \************************************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "CurrentBookingsPageModule": () => (/* binding */ CurrentBookingsPageModule)
/* harmony export */ });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! tslib */ 64762);
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @angular/core */ 37716);
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @angular/common */ 38583);
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! @angular/forms */ 3679);
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! @ionic/angular */ 80476);
/* harmony import */ var _current_bookings_routing_module__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./current-bookings-routing.module */ 14414);
/* harmony import */ var _current_bookings_page__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./current-bookings.page */ 67771);
/* harmony import */ var src_app_Components_header_header_module__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! src/app/Components/header/header.module */ 42335);








let CurrentBookingsPageModule = class CurrentBookingsPageModule {
};
CurrentBookingsPageModule = (0,tslib__WEBPACK_IMPORTED_MODULE_3__.__decorate)([
    (0,_angular_core__WEBPACK_IMPORTED_MODULE_4__.NgModule)({
        imports: [
            _angular_common__WEBPACK_IMPORTED_MODULE_5__.CommonModule,
            _angular_forms__WEBPACK_IMPORTED_MODULE_6__.FormsModule,
            _ionic_angular__WEBPACK_IMPORTED_MODULE_7__.IonicModule,
            _current_bookings_routing_module__WEBPACK_IMPORTED_MODULE_0__.CurrentBookingsPageRoutingModule,
            src_app_Components_header_header_module__WEBPACK_IMPORTED_MODULE_2__.HeaderModule,
        ],
        declarations: [_current_bookings_page__WEBPACK_IMPORTED_MODULE_1__.CurrentBookingsPage],
    })
], CurrentBookingsPageModule);



/***/ }),

/***/ 67771:
/*!**********************************************************************!*\
  !*** ./src/app/Pages/tabs/current-bookings/current-bookings.page.ts ***!
  \**********************************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "CurrentBookingsPage": () => (/* binding */ CurrentBookingsPage)
/* harmony export */ });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! tslib */ 64762);
/* harmony import */ var _raw_loader_current_bookings_page_html__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! !raw-loader!./current-bookings.page.html */ 72592);
/* harmony import */ var _current_bookings_page_scss__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./current-bookings.page.scss */ 50111);
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @angular/core */ 37716);
/* harmony import */ var _base_page_base_page__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../../base-page/base-page */ 96093);





let CurrentBookingsPage = class CurrentBookingsPage extends _base_page_base_page__WEBPACK_IMPORTED_MODULE_2__.BasePage {
    constructor(injector) {
        super(injector);
        this.bookingList = [];
    }
    ngOnInit() {
    }
    ionViewWillEnter() {
        return (0,tslib__WEBPACK_IMPORTED_MODULE_3__.__awaiter)(this, void 0, void 0, function* () {
            this.curUser = yield this.firebaseService.getCurrentUser();
            let ref = this.firebaseService.getDbRef('/users/' + this.curUser.uid);
            ref.on('value', (snapshot) => {
                if (snapshot.val() != null) {
                    console.log(snapshot.val());
                    console.log("userId values");
                    this.bookingList = [];
                    this.zone.run(() => {
                        this.userProfile = snapshot.val();
                        for (let key in this.userProfile.bookings) {
                            this.userProfile.bookings[key].uid = key;
                            if (this.userProfile.bookings[key].status != "ENDED" && this.userProfile.bookings[key].status != "CANCELLED" && this.userProfile.bookings[key].driver == this.curUser.uid) {
                                this.userProfile.bookings[key].image_url = this.userProfile.bookings[key].customer_image;
                                this.bookingList.push(this.userProfile.bookings[key]);
                                console.log(this.bookingList);
                            }
                        }
                        this.bookingList.sort(function (a, b) {
                            console.log("hehdew");
                            let aa = Number(new Date(a.tripdate));
                            let bb = Number(new Date(b.tripdate));
                            console.log(bb - aa);
                            return bb - aa;
                        });
                    });
                }
            });
        });
    }
    openActiveBooking(position) {
        console.log(this.bookingList[position]);
        if (this.bookingList[position].status == "REVIEW") {
            this.nav.push('pages/job-complete', {
                bookingKey: this.bookingList[position].uid
            });
        }
        else {
            this.nav.push('pages/active-booking', {
                bookingKey: this.bookingList[position].uid
            });
        }
    }
};
CurrentBookingsPage.ctorParameters = () => [
    { type: _angular_core__WEBPACK_IMPORTED_MODULE_4__.Injector }
];
CurrentBookingsPage = (0,tslib__WEBPACK_IMPORTED_MODULE_3__.__decorate)([
    (0,_angular_core__WEBPACK_IMPORTED_MODULE_4__.Component)({
        selector: 'app-current-bookings',
        template: _raw_loader_current_bookings_page_html__WEBPACK_IMPORTED_MODULE_0__.default,
        styles: [_current_bookings_page_scss__WEBPACK_IMPORTED_MODULE_1__.default]
    })
], CurrentBookingsPage);



/***/ }),

/***/ 50111:
/*!************************************************************************!*\
  !*** ./src/app/Pages/tabs/current-bookings/current-bookings.page.scss ***!
  \************************************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "default": () => (__WEBPACK_DEFAULT_EXPORT__)
/* harmony export */ });
/* harmony default export */ const __WEBPACK_DEFAULT_EXPORT__ = ("\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJjdXJyZW50LWJvb2tpbmdzLnBhZ2Uuc2NzcyJ9 */");

/***/ }),

/***/ 72592:
/*!**************************************************************************************************************!*\
  !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/Pages/tabs/current-bookings/current-bookings.page.html ***!
  \**************************************************************************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "default": () => (__WEBPACK_DEFAULT_EXPORT__)
/* harmony export */ });
/* harmony default export */ const __WEBPACK_DEFAULT_EXPORT__ = ("<header [title]=\"'BOOKINGS'\"></header>\r\n\r\n<!--<ion-content>\r\n\t<section class=\"bookings\">\r\n\t\t<ion-list>\r\n\t\t  <ion-item class=\"no-padding\" *ngFor=\"let booking of bookingList; let i = index;\" (tap)=\"openActiveBooking(i)\">\r\n\t\t  <ion-grid>\r\n\t\t  \t<ion-row>\r\n\t\t  \t\t<ion-col width-20><img src=\"{{booking.image_url}}\" alt=\"Profile Image\"></ion-col>\r\n\t\t  \t\t<ion-col width-50>\r\n\t\t  \t\t\t<h4>{{booking.tripdate | date:'medium'}}</h4>\r\n\t\t  \t\t\t<p><span>Start</span> : {{booking.pickupAddress}}</p>\r\n\t\t  \t\t\t<p><span>End</span> : {{booking.dropAddress}}</p>\r\n\t\t  \t\t</ion-col>\r\n\t\t  \t\t<ion-col><h3>{{booking.status}}</h3></ion-col>\r\n\t\t  \t</ion-row>\r\n\t\t  </ion-grid>\r\n\t\t  </ion-item>\r\n\t\t</ion-list>\r\n\t</section>\r\n</ion-content>-->\r\n\r\n<ion-content>\r\n  <!-- <section class=\"bookings\">\r\n    <h2 *ngIf=\"bookingList.length==0\" class=\"review-title\">No current bookings.</h2>\r\n    <ion-list>\r\n      <ion-item class=\"no-padding\" *ngFor=\"let booking of bookingList  ; let i = index; \"\r\n        (click)=\"openActiveBooking(i)\">\r\n        <ion-grid>\r\n          <ion-row>\r\n            <ion-col size=\"2.5\">\r\n              <ion-avatar>\r\n                <img src=\"{{booking.image_url}}\" onerror=\"this.onerror=null;this.src='assets/images/notfound.png';\"\r\n                  alt=\"\">\r\n              </ion-avatar>\r\n            </ion-col>\r\n            <ion-col size=\"7\">\r\n              <h4><strong>{{booking.tripdate.substring(8,10)}}{{booking.tripdate.substring(4,8)}}{{booking.tripdate.substring(0,4)}}\r\n                  {{booking.tripdate.substring(11,16)}}</strong></h4>\r\n              <h4><strong>{{booking.serviceType}}</strong></h4>\r\n              <p><span>Start</span> : {{booking.pickupAddress}}</p>\r\n              <p *ngIf=\"booking.dropAddress\"><span>End</span> : {{booking.dropAddress}}</p>\r\n            </ion-col>\r\n            <ion-col size=\"2.5\">\r\n              <h3>{{booking.status=='REVIEW'?'UNDER REVIEW':booking.status}}</h3>\r\n            </ion-col>\r\n          </ion-row>\r\n        </ion-grid>\r\n      </ion-item>\r\n    </ion-list>\r\n  </section> -->\r\n\r\n  <ion-card\r\n    class=\"ion-no-padding\"\r\n    *ngFor=\"let booking of bookingList  ; let i = index;\"\r\n    (click)=\"openActiveBooking(i)\"\r\n  >\r\n    <!-- <ion-card-header>\r\n      <ion-item color=\"none\">\r\n        <ion-label>\r\n          {{booking.tripdate.substring(8,10)}}{{booking.tripdate.substring(4,8)}}{{booking.tripdate.substring(0,4)}}\r\n          {{booking.tripdate.substring(11,16)}}\r\n        </ion-label>\r\n        <ion-label slot=\"end\">\r\n          {{booking.status=='REVIEW'?'UNDER REVIEW':booking.status}}\r\n        </ion-label>\r\n      </ion-item>\r\n    </ion-card-header> -->\r\n    <ion-card-content class=\"ion-no-margin ion-no-padding\">\r\n      <ion-item color=\"none\" class=\"ion-no-margin ion-no-padding\">\r\n        <ion-icon\r\n          class=\"ion-margin\"\r\n          src=\"/assets/images/calendar-outline.svg\"\r\n        ></ion-icon>\r\n        <ion-label>\r\n          <!-- <ion-icon style=\"font-size: 20px;\" name=\"calendar\"></ion-icon> -->\r\n          {{booking.tripdate.substring(8,10)}}{{booking.tripdate.substring(4,8)}}{{booking.tripdate.substring(0,4)}}\r\n          {{booking.tripdate.substring(11,16)}}\r\n        </ion-label>\r\n        <ion-label slot=\"end\">\r\n          {{booking.status=='REVIEW'?'UNDER REVIEW':booking.status}}\r\n        </ion-label>\r\n      </ion-item>\r\n      <ion-grid>\r\n        <ion-row>\r\n          <ion-col size=\"12\">\r\n            <ion-item lines=\"none\" color=\"none\">\r\n              <ion-avatar style=\"width: 60px; height: 60px\">\r\n                <img\r\n                  src=\"{{booking.image_url ? booking.image_url : 'assets/images/notfound.png'}}\"\r\n                  onerror=\"this.onerror=null;this.src='assets/images/notfound.png';\"\r\n                  alt=\"\"\r\n                />\r\n              </ion-avatar>\r\n              <ion-label class=\"ion-margin\">\r\n                <p class=\"ion-text-wrap\" style=\"font-size: 15px\">\r\n                  <b style=\"color: rgb(120 120 241)\">{{booking.serviceType}}</b>\r\n                </p>\r\n                <p class=\"ion-text-wrap\" style=\"font-size: 12px\">\r\n                  <b style=\"color: rgb(120 120 241)\">Start : </b\r\n                  >{{booking.pickupAddress}}\r\n                </p>\r\n                <p class=\"ion-text-wrap\" style=\"font-size: 12px\">\r\n                  <b style=\"color: rgb(120 120 241)\">End : </b\r\n                  >{{booking.dropAddress}}\r\n                </p>\r\n              </ion-label>\r\n            </ion-item>\r\n          </ion-col>\r\n        </ion-row>\r\n      </ion-grid>\r\n    </ion-card-content>\r\n  </ion-card>\r\n</ion-content>\r\n");

/***/ })

}]);
//# sourceMappingURL=src_app_Pages_tabs_current-bookings_current-bookings_module_ts.js.map