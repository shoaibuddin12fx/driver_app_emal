(self["webpackChunkDriverApp"] = self["webpackChunkDriverApp"] || []).push([["src_app_Pages_active-booking_active-booking_module_ts"],{

/***/ 16351:
/*!*******************************************************************************************!*\
  !*** ./node_modules/@awesome-cordova-plugins/core/__ivy_ngcc__/awesome-cordova-plugin.js ***!
  \*******************************************************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "AwesomeCordovaNativePlugin": () => (/* binding */ AwesomeCordovaNativePlugin)
/* harmony export */ });
/* harmony import */ var _decorators_common__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./decorators/common */ 9343);
/* harmony import */ var _util__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./util */ 91674);


var AwesomeCordovaNativePlugin = /** @class */ (function () {
    function AwesomeCordovaNativePlugin() {
    }
    /**
     * Returns a boolean that indicates whether the plugin is installed
     *
     * @returns {boolean}
     */
    AwesomeCordovaNativePlugin.installed = function () {
        var isAvailable = (0,_decorators_common__WEBPACK_IMPORTED_MODULE_0__.checkAvailability)(this.pluginRef) === true;
        return isAvailable;
    };
    /**
     * Returns the original plugin object
     */
    AwesomeCordovaNativePlugin.getPlugin = function () {
        if (typeof window !== 'undefined') {
            return (0,_util__WEBPACK_IMPORTED_MODULE_1__.get)(window, this.pluginRef);
        }
        return null;
    };
    /**
     * Returns the plugin's name
     */
    AwesomeCordovaNativePlugin.getPluginName = function () {
        var pluginName = this.pluginName;
        return pluginName;
    };
    /**
     * Returns the plugin's reference
     */
    AwesomeCordovaNativePlugin.getPluginRef = function () {
        var pluginRef = this.pluginRef;
        return pluginRef;
    };
    /**
     * Returns the plugin's install name
     */
    AwesomeCordovaNativePlugin.getPluginInstallName = function () {
        var plugin = this.plugin;
        return plugin;
    };
    /**
     * Returns the plugin's supported platforms
     */
    AwesomeCordovaNativePlugin.getSupportedPlatforms = function () {
        var platform = this.platforms;
        return platform;
    };
    AwesomeCordovaNativePlugin.pluginName = '';
    AwesomeCordovaNativePlugin.pluginRef = '';
    AwesomeCordovaNativePlugin.plugin = '';
    AwesomeCordovaNativePlugin.repo = '';
    AwesomeCordovaNativePlugin.platforms = [];
    AwesomeCordovaNativePlugin.install = '';
    return AwesomeCordovaNativePlugin;
}());

//# sourceMappingURL=awesome-cordova-plugin.js.map

/***/ }),

/***/ 46367:
/*!******************************************************************************!*\
  !*** ./node_modules/@awesome-cordova-plugins/core/__ivy_ngcc__/bootstrap.js ***!
  \******************************************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "checkReady": () => (/* binding */ checkReady)
/* harmony export */ });
/**
 *
 */
function checkReady() {
    if (typeof process === 'undefined') {
        var win_1 = typeof window !== 'undefined' ? window : {};
        var DEVICE_READY_TIMEOUT_1 = 5000;
        // To help developers using cordova, we listen for the device ready event and
        // log an error if it didn't fire in a reasonable amount of time. Generally,
        // when this happens, developers should remove and reinstall plugins, since
        // an inconsistent plugin is often the culprit.
        var before_1 = Date.now();
        var didFireReady_1 = false;
        win_1.document.addEventListener('deviceready', function () {
            console.log("Ionic Native: deviceready event fired after " + (Date.now() - before_1) + " ms");
            didFireReady_1 = true;
        });
        setTimeout(function () {
            if (!didFireReady_1 && win_1.cordova) {
                console.warn("Ionic Native: deviceready did not fire within " + DEVICE_READY_TIMEOUT_1 + "ms. This can happen when plugins are in an inconsistent state. Try removing plugins from plugins/ and reinstalling them.");
            }
        }, DEVICE_READY_TIMEOUT_1);
    }
}
//# sourceMappingURL=bootstrap.js.map

/***/ }),

/***/ 9343:
/*!**************************************************************************************!*\
  !*** ./node_modules/@awesome-cordova-plugins/core/__ivy_ngcc__/decorators/common.js ***!
  \**************************************************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "ERR_CORDOVA_NOT_AVAILABLE": () => (/* binding */ ERR_CORDOVA_NOT_AVAILABLE),
/* harmony export */   "ERR_PLUGIN_NOT_INSTALLED": () => (/* binding */ ERR_PLUGIN_NOT_INSTALLED),
/* harmony export */   "getPromise": () => (/* binding */ getPromise),
/* harmony export */   "wrapPromise": () => (/* binding */ wrapPromise),
/* harmony export */   "checkAvailability": () => (/* binding */ checkAvailability),
/* harmony export */   "instanceAvailability": () => (/* binding */ instanceAvailability),
/* harmony export */   "setIndex": () => (/* binding */ setIndex),
/* harmony export */   "callCordovaPlugin": () => (/* binding */ callCordovaPlugin),
/* harmony export */   "callInstance": () => (/* binding */ callInstance),
/* harmony export */   "getPlugin": () => (/* binding */ getPlugin),
/* harmony export */   "get": () => (/* binding */ get),
/* harmony export */   "pluginWarn": () => (/* binding */ pluginWarn),
/* harmony export */   "cordovaWarn": () => (/* binding */ cordovaWarn),
/* harmony export */   "wrap": () => (/* binding */ wrap),
/* harmony export */   "wrapInstance": () => (/* binding */ wrapInstance)
/* harmony export */ });
/* harmony import */ var rxjs__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! rxjs */ 69165);
/* harmony import */ var rxjs__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! rxjs */ 22759);

var ERR_CORDOVA_NOT_AVAILABLE = { error: 'cordova_not_available' };
var ERR_PLUGIN_NOT_INSTALLED = { error: 'plugin_not_installed' };
/**
 * @param callback
 */
function getPromise(callback) {
    var tryNativePromise = function () {
        if (Promise) {
            return new Promise(function (resolve, reject) {
                callback(resolve, reject);
            });
        }
        else {
            console.error('No Promise support or polyfill found. To enable Ionic Native support, please add the es6-promise polyfill before this script, or run with a library like Angular or on a recent browser.');
        }
    };
    if (typeof window !== 'undefined' && window.angular) {
        var doc = window.document;
        var injector = window.angular.element(doc.querySelector('[ng-app]') || doc.body).injector();
        if (injector) {
            var $q = injector.get('$q');
            return $q(function (resolve, reject) {
                callback(resolve, reject);
            });
        }
        console.warn("Angular 1 was detected but $q couldn't be retrieved. This is usually when the app is not bootstrapped on the html or body tag. Falling back to native promises which won't trigger an automatic digest when promises resolve.");
    }
    return tryNativePromise();
}
/**
 * @param pluginObj
 * @param methodName
 * @param args
 * @param opts
 */
function wrapPromise(pluginObj, methodName, args, opts) {
    if (opts === void 0) { opts = {}; }
    var pluginResult, rej;
    var p = getPromise(function (resolve, reject) {
        if (opts.destruct) {
            pluginResult = callCordovaPlugin(pluginObj, methodName, args, opts, function () {
                var args = [];
                for (var _i = 0; _i < arguments.length; _i++) {
                    args[_i] = arguments[_i];
                }
                return resolve(args);
            }, function () {
                var args = [];
                for (var _i = 0; _i < arguments.length; _i++) {
                    args[_i] = arguments[_i];
                }
                return reject(args);
            });
        }
        else {
            pluginResult = callCordovaPlugin(pluginObj, methodName, args, opts, resolve, reject);
        }
        rej = reject;
    });
    // Angular throws an error on unhandled rejection, but in this case we have already printed
    // a warning that Cordova is undefined or the plugin is uninstalled, so there is no reason
    // to error
    if (pluginResult && pluginResult.error) {
        p.catch(function () { });
        typeof rej === 'function' && rej(pluginResult.error);
    }
    return p;
}
/**
 * @param pluginObj
 * @param methodName
 * @param args
 * @param opts
 */
function wrapOtherPromise(pluginObj, methodName, args, opts) {
    if (opts === void 0) { opts = {}; }
    return getPromise(function (resolve, reject) {
        var pluginResult = callCordovaPlugin(pluginObj, methodName, args, opts);
        if (pluginResult) {
            if (pluginResult.error) {
                reject(pluginResult.error);
            }
            else if (pluginResult.then) {
                pluginResult.then(resolve).catch(reject);
            }
        }
        else {
            reject({ error: 'unexpected_error' });
        }
    });
}
/**
 * @param pluginObj
 * @param methodName
 * @param args
 * @param opts
 */
function wrapObservable(pluginObj, methodName, args, opts) {
    if (opts === void 0) { opts = {}; }
    return new rxjs__WEBPACK_IMPORTED_MODULE_0__.Observable(function (observer) {
        var pluginResult;
        if (opts.destruct) {
            pluginResult = callCordovaPlugin(pluginObj, methodName, args, opts, function () {
                var args = [];
                for (var _i = 0; _i < arguments.length; _i++) {
                    args[_i] = arguments[_i];
                }
                return observer.next(args);
            }, function () {
                var args = [];
                for (var _i = 0; _i < arguments.length; _i++) {
                    args[_i] = arguments[_i];
                }
                return observer.error(args);
            });
        }
        else {
            pluginResult = callCordovaPlugin(pluginObj, methodName, args, opts, observer.next.bind(observer), observer.error.bind(observer));
        }
        if (pluginResult && pluginResult.error) {
            observer.error(pluginResult.error);
            observer.complete();
        }
        return function () {
            try {
                if (opts.clearFunction) {
                    if (opts.clearWithArgs) {
                        return callCordovaPlugin(pluginObj, opts.clearFunction, args, opts, observer.next.bind(observer), observer.error.bind(observer));
                    }
                    return callCordovaPlugin(pluginObj, opts.clearFunction, []);
                }
            }
            catch (e) {
                console.warn('Unable to clear the previous observable watch for', pluginObj.constructor.getPluginName(), methodName);
                console.warn(e);
            }
        };
    });
}
/**
 * Wrap the event with an observable
 *
 * @private
 * @param event event name
 * @param element The element to attach the event listener to
 * @returns {Observable}
 */
function wrapEventObservable(event, element) {
    element =
        typeof window !== 'undefined' && element
            ? get(window, element)
            : element || (typeof window !== 'undefined' ? window : {});
    return (0,rxjs__WEBPACK_IMPORTED_MODULE_1__.fromEvent)(element, event);
}
/**
 * @param plugin
 * @param methodName
 * @param pluginName
 */
function checkAvailability(plugin, methodName, pluginName) {
    var pluginRef, pluginPackage;
    if (typeof plugin === 'string') {
        pluginRef = plugin;
    }
    else {
        pluginRef = plugin.constructor.getPluginRef();
        pluginName = plugin.constructor.getPluginName();
        pluginPackage = plugin.constructor.getPluginInstallName();
    }
    var pluginInstance = getPlugin(pluginRef);
    if (!pluginInstance || (!!methodName && typeof pluginInstance[methodName] === 'undefined')) {
        if (typeof window === 'undefined' || !window.cordova) {
            cordovaWarn(pluginName, methodName);
            return ERR_CORDOVA_NOT_AVAILABLE;
        }
        pluginWarn(pluginName, pluginPackage, methodName);
        return ERR_PLUGIN_NOT_INSTALLED;
    }
    return true;
}
/**
 * Checks if _objectInstance exists and has the method/property
 *
 * @param pluginObj
 * @param methodName
 * @private
 */
function instanceAvailability(pluginObj, methodName) {
    return pluginObj._objectInstance && (!methodName || typeof pluginObj._objectInstance[methodName] !== 'undefined');
}
/**
 * @param args
 * @param opts
 * @param resolve
 * @param reject
 */
function setIndex(args, opts, resolve, reject) {
    if (opts === void 0) { opts = {}; }
    // ignore resolve and reject in case sync
    if (opts.sync) {
        return args;
    }
    // If the plugin method expects myMethod(success, err, options)
    if (opts.callbackOrder === 'reverse') {
        // Get those arguments in the order [resolve, reject, ...restOfArgs]
        args.unshift(reject);
        args.unshift(resolve);
    }
    else if (opts.callbackStyle === 'node') {
        args.push(function (err, result) {
            if (err) {
                reject(err);
            }
            else {
                resolve(result);
            }
        });
    }
    else if (opts.callbackStyle === 'object' && opts.successName && opts.errorName) {
        var obj = {};
        obj[opts.successName] = resolve;
        obj[opts.errorName] = reject;
        args.push(obj);
    }
    else if (typeof opts.successIndex !== 'undefined' || typeof opts.errorIndex !== 'undefined') {
        var setSuccessIndex = function () {
            // If we've specified a success/error index
            if (opts.successIndex > args.length) {
                args[opts.successIndex] = resolve;
            }
            else {
                args.splice(opts.successIndex, 0, resolve);
            }
        };
        var setErrorIndex = function () {
            // We don't want that the reject cb gets spliced into the position of an optional argument that has not been
            // defined and thus causing non expected behavior.
            if (opts.errorIndex > args.length) {
                args[opts.errorIndex] = reject; // insert the reject fn at the correct specific index
            }
            else {
                args.splice(opts.errorIndex, 0, reject); // otherwise just splice it into the array
            }
        };
        if (opts.successIndex > opts.errorIndex) {
            setErrorIndex();
            setSuccessIndex();
        }
        else {
            setSuccessIndex();
            setErrorIndex();
        }
    }
    else {
        // Otherwise, let's tack them on to the end of the argument list
        // which is 90% of cases
        args.push(resolve);
        args.push(reject);
    }
    return args;
}
/**
 * @param pluginObj
 * @param methodName
 * @param args
 * @param opts
 * @param resolve
 * @param reject
 */
function callCordovaPlugin(pluginObj, methodName, args, opts, resolve, reject) {
    if (opts === void 0) { opts = {}; }
    // Try to figure out where the success/error callbacks need to be bound
    // to our promise resolve/reject handlers.
    args = setIndex(args, opts, resolve, reject);
    var availabilityCheck = checkAvailability(pluginObj, methodName);
    if (availabilityCheck === true) {
        var pluginInstance = getPlugin(pluginObj.constructor.getPluginRef());
        // eslint-disable-next-line prefer-spread
        return pluginInstance[methodName].apply(pluginInstance, args);
    }
    else {
        return availabilityCheck;
    }
}
/**
 * @param pluginObj
 * @param methodName
 * @param args
 * @param opts
 * @param resolve
 * @param reject
 */
function callInstance(pluginObj, methodName, args, opts, resolve, reject) {
    if (opts === void 0) { opts = {}; }
    args = setIndex(args, opts, resolve, reject);
    if (instanceAvailability(pluginObj, methodName)) {
        // eslint-disable-next-line prefer-spread
        return pluginObj._objectInstance[methodName].apply(pluginObj._objectInstance, args);
    }
}
/**
 * @param pluginRef
 */
function getPlugin(pluginRef) {
    if (typeof window !== 'undefined') {
        return get(window, pluginRef);
    }
    return null;
}
/**
 * @param element
 * @param path
 */
function get(element, path) {
    var paths = path.split('.');
    var obj = element;
    for (var i = 0; i < paths.length; i++) {
        if (!obj) {
            return null;
        }
        obj = obj[paths[i]];
    }
    return obj;
}
/**
 * @param pluginName
 * @param plugin
 * @param method
 */
function pluginWarn(pluginName, plugin, method) {
    if (method) {
        console.warn('Native: tried calling ' + pluginName + '.' + method + ', but the ' + pluginName + ' plugin is not installed.');
    }
    else {
        console.warn("Native: tried accessing the " + pluginName + " plugin but it's not installed.");
    }
    if (plugin) {
        console.warn("Install the " + pluginName + " plugin: 'ionic cordova plugin add " + plugin + "'");
    }
}
/**
 * @private
 * @param pluginName
 * @param method
 */
function cordovaWarn(pluginName, method) {
    if (typeof process === 'undefined') {
        if (method) {
            console.warn('Native: tried calling ' +
                pluginName +
                '.' +
                method +
                ', but Cordova is not available. Make sure to include cordova.js or run in a device/simulator');
        }
        else {
            console.warn('Native: tried accessing the ' +
                pluginName +
                ' plugin but Cordova is not available. Make sure to include cordova.js or run in a device/simulator');
        }
    }
}
/**
 * @param pluginObj
 * @param methodName
 * @param opts
 * @private
 */
var wrap = function (pluginObj, methodName, opts) {
    if (opts === void 0) { opts = {}; }
    return function () {
        var args = [];
        for (var _i = 0; _i < arguments.length; _i++) {
            args[_i] = arguments[_i];
        }
        if (opts.sync) {
            // Sync doesn't wrap the plugin with a promise or observable, it returns the result as-is
            return callCordovaPlugin(pluginObj, methodName, args, opts);
        }
        else if (opts.observable) {
            return wrapObservable(pluginObj, methodName, args, opts);
        }
        else if (opts.eventObservable && opts.event) {
            return wrapEventObservable(opts.event, opts.element);
        }
        else if (opts.otherPromise) {
            return wrapOtherPromise(pluginObj, methodName, args, opts);
        }
        else {
            return wrapPromise(pluginObj, methodName, args, opts);
        }
    };
};
/**
 * @param pluginObj
 * @param methodName
 * @param opts
 * @private
 */
function wrapInstance(pluginObj, methodName, opts) {
    if (opts === void 0) { opts = {}; }
    return function () {
        var args = [];
        for (var _i = 0; _i < arguments.length; _i++) {
            args[_i] = arguments[_i];
        }
        if (opts.sync) {
            return callInstance(pluginObj, methodName, args, opts);
        }
        else if (opts.observable) {
            return new rxjs__WEBPACK_IMPORTED_MODULE_0__.Observable(function (observer) {
                var pluginResult;
                if (opts.destruct) {
                    pluginResult = callInstance(pluginObj, methodName, args, opts, function () {
                        var args = [];
                        for (var _i = 0; _i < arguments.length; _i++) {
                            args[_i] = arguments[_i];
                        }
                        return observer.next(args);
                    }, function () {
                        var args = [];
                        for (var _i = 0; _i < arguments.length; _i++) {
                            args[_i] = arguments[_i];
                        }
                        return observer.error(args);
                    });
                }
                else {
                    pluginResult = callInstance(pluginObj, methodName, args, opts, observer.next.bind(observer), observer.error.bind(observer));
                }
                if (pluginResult && pluginResult.error) {
                    observer.error(pluginResult.error);
                }
                return function () {
                    try {
                        if (opts.clearWithArgs) {
                            return callInstance(pluginObj, opts.clearFunction, args, opts, observer.next.bind(observer), observer.error.bind(observer));
                        }
                        return callInstance(pluginObj, opts.clearFunction, []);
                    }
                    catch (e) {
                        console.warn('Unable to clear the previous observable watch for', pluginObj.constructor.getPluginName(), methodName);
                        console.warn(e);
                    }
                };
            });
        }
        else if (opts.otherPromise) {
            return getPromise(function (resolve, reject) {
                var result;
                if (opts.destruct) {
                    result = callInstance(pluginObj, methodName, args, opts, function () {
                        var args = [];
                        for (var _i = 0; _i < arguments.length; _i++) {
                            args[_i] = arguments[_i];
                        }
                        return resolve(args);
                    }, function () {
                        var args = [];
                        for (var _i = 0; _i < arguments.length; _i++) {
                            args[_i] = arguments[_i];
                        }
                        return reject(args);
                    });
                }
                else {
                    result = callInstance(pluginObj, methodName, args, opts, resolve, reject);
                }
                if (result && result.then) {
                    result.then(resolve, reject);
                }
                else {
                    reject();
                }
            });
        }
        else {
            var pluginResult_1, rej_1;
            var p = getPromise(function (resolve, reject) {
                if (opts.destruct) {
                    pluginResult_1 = callInstance(pluginObj, methodName, args, opts, function () {
                        var args = [];
                        for (var _i = 0; _i < arguments.length; _i++) {
                            args[_i] = arguments[_i];
                        }
                        return resolve(args);
                    }, function () {
                        var args = [];
                        for (var _i = 0; _i < arguments.length; _i++) {
                            args[_i] = arguments[_i];
                        }
                        return reject(args);
                    });
                }
                else {
                    pluginResult_1 = callInstance(pluginObj, methodName, args, opts, resolve, reject);
                }
                rej_1 = reject;
            });
            // Angular throws an error on unhandled rejection, but in this case we have already printed
            // a warning that Cordova is undefined or the plugin is uninstalled, so there is no reason
            // to error
            if (pluginResult_1 && pluginResult_1.error) {
                p.catch(function () { });
                typeof rej_1 === 'function' && rej_1(pluginResult_1.error);
            }
            return p;
        }
    };
}
//# sourceMappingURL=common.js.map

/***/ }),

/***/ 531:
/*!*********************************************************************************************************!*\
  !*** ./node_modules/@awesome-cordova-plugins/core/__ivy_ngcc__/decorators/cordova-function-override.js ***!
  \*********************************************************************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "cordovaFunctionOverride": () => (/* binding */ cordovaFunctionOverride)
/* harmony export */ });
/* harmony import */ var rxjs__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! rxjs */ 69165);
/* harmony import */ var _common__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./common */ 9343);


/**
 * @param pluginObj
 * @param methodName
 */
function overrideFunction(pluginObj, methodName) {
    return new rxjs__WEBPACK_IMPORTED_MODULE_1__.Observable(function (observer) {
        var availabilityCheck = (0,_common__WEBPACK_IMPORTED_MODULE_0__.checkAvailability)(pluginObj, methodName);
        if (availabilityCheck === true) {
            var pluginInstance_1 = (0,_common__WEBPACK_IMPORTED_MODULE_0__.getPlugin)(pluginObj.constructor.getPluginRef());
            pluginInstance_1[methodName] = observer.next.bind(observer);
            return function () { return (pluginInstance_1[methodName] = function () { }); };
        }
        else {
            observer.error(availabilityCheck);
            observer.complete();
        }
    });
}
/**
 * @param pluginObj
 * @param methodName
 * @param args
 */
function cordovaFunctionOverride(pluginObj, methodName, args) {
    if (args === void 0) { args = []; }
    return overrideFunction(pluginObj, methodName);
}
//# sourceMappingURL=cordova-function-override.js.map

/***/ }),

/***/ 90808:
/*!************************************************************************************************!*\
  !*** ./node_modules/@awesome-cordova-plugins/core/__ivy_ngcc__/decorators/cordova-instance.js ***!
  \************************************************************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "cordovaInstance": () => (/* binding */ cordovaInstance)
/* harmony export */ });
/* harmony import */ var _common__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./common */ 9343);

/**
 * @param pluginObj
 * @param methodName
 * @param config
 * @param args
 */
function cordovaInstance(pluginObj, methodName, config, args) {
    args = Array.from(args);
    return (0,_common__WEBPACK_IMPORTED_MODULE_0__.wrapInstance)(pluginObj, methodName, config).apply(this, args);
}
//# sourceMappingURL=cordova-instance.js.map

/***/ }),

/***/ 34070:
/*!************************************************************************************************!*\
  !*** ./node_modules/@awesome-cordova-plugins/core/__ivy_ngcc__/decorators/cordova-property.js ***!
  \************************************************************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "cordovaPropertyGet": () => (/* binding */ cordovaPropertyGet),
/* harmony export */   "cordovaPropertySet": () => (/* binding */ cordovaPropertySet)
/* harmony export */ });
/* harmony import */ var _common__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./common */ 9343);

/**
 * @param pluginObj
 * @param key
 */
function cordovaPropertyGet(pluginObj, key) {
    if ((0,_common__WEBPACK_IMPORTED_MODULE_0__.checkAvailability)(pluginObj, key) === true) {
        return (0,_common__WEBPACK_IMPORTED_MODULE_0__.getPlugin)(pluginObj.constructor.getPluginRef())[key];
    }
    return null;
}
/**
 * @param pluginObj
 * @param key
 * @param value
 */
function cordovaPropertySet(pluginObj, key, value) {
    if ((0,_common__WEBPACK_IMPORTED_MODULE_0__.checkAvailability)(pluginObj, key) === true) {
        (0,_common__WEBPACK_IMPORTED_MODULE_0__.getPlugin)(pluginObj.constructor.getPluginRef())[key] = value;
    }
}
//# sourceMappingURL=cordova-property.js.map

/***/ }),

/***/ 43364:
/*!***************************************************************************************!*\
  !*** ./node_modules/@awesome-cordova-plugins/core/__ivy_ngcc__/decorators/cordova.js ***!
  \***************************************************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "cordova": () => (/* binding */ cordova)
/* harmony export */ });
/* harmony import */ var _common__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./common */ 9343);

/**
 * @param pluginObj
 * @param methodName
 * @param config
 * @param args
 */
function cordova(pluginObj, methodName, config, args) {
    return (0,_common__WEBPACK_IMPORTED_MODULE_0__.wrap)(pluginObj, methodName, config).apply(this, args);
}
//# sourceMappingURL=cordova.js.map

/***/ }),

/***/ 70422:
/*!*************************************************************************************************!*\
  !*** ./node_modules/@awesome-cordova-plugins/core/__ivy_ngcc__/decorators/instance-property.js ***!
  \*************************************************************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "instancePropertyGet": () => (/* binding */ instancePropertyGet),
/* harmony export */   "instancePropertySet": () => (/* binding */ instancePropertySet)
/* harmony export */ });
/**
 * @param pluginObj
 * @param key
 */
function instancePropertyGet(pluginObj, key) {
    if (pluginObj._objectInstance && pluginObj._objectInstance[key]) {
        return pluginObj._objectInstance[key];
    }
    return null;
}
/**
 * @param pluginObj
 * @param key
 * @param value
 */
function instancePropertySet(pluginObj, key, value) {
    if (pluginObj._objectInstance) {
        pluginObj._objectInstance[key] = value;
    }
}
//# sourceMappingURL=instance-property.js.map

/***/ }),

/***/ 60303:
/*!******************************************************************************************!*\
  !*** ./node_modules/@awesome-cordova-plugins/core/__ivy_ngcc__/decorators/interfaces.js ***!
  \******************************************************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

"use strict";
__webpack_require__.r(__webpack_exports__);

//# sourceMappingURL=interfaces.js.map

/***/ }),

/***/ 16887:
/*!**************************************************************************!*\
  !*** ./node_modules/@awesome-cordova-plugins/core/__ivy_ngcc__/index.js ***!
  \**************************************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "AwesomeCordovaNativePlugin": () => (/* reexport safe */ _awesome_cordova_plugin__WEBPACK_IMPORTED_MODULE_1__.AwesomeCordovaNativePlugin),
/* harmony export */   "checkAvailability": () => (/* reexport safe */ _decorators_common__WEBPACK_IMPORTED_MODULE_2__.checkAvailability),
/* harmony export */   "instanceAvailability": () => (/* reexport safe */ _decorators_common__WEBPACK_IMPORTED_MODULE_2__.instanceAvailability),
/* harmony export */   "wrap": () => (/* reexport safe */ _decorators_common__WEBPACK_IMPORTED_MODULE_2__.wrap),
/* harmony export */   "getPromise": () => (/* reexport safe */ _decorators_common__WEBPACK_IMPORTED_MODULE_2__.getPromise),
/* harmony export */   "cordova": () => (/* reexport safe */ _decorators_cordova__WEBPACK_IMPORTED_MODULE_3__.cordova),
/* harmony export */   "cordovaFunctionOverride": () => (/* reexport safe */ _decorators_cordova_function_override__WEBPACK_IMPORTED_MODULE_4__.cordovaFunctionOverride),
/* harmony export */   "cordovaInstance": () => (/* reexport safe */ _decorators_cordova_instance__WEBPACK_IMPORTED_MODULE_5__.cordovaInstance),
/* harmony export */   "cordovaPropertyGet": () => (/* reexport safe */ _decorators_cordova_property__WEBPACK_IMPORTED_MODULE_6__.cordovaPropertyGet),
/* harmony export */   "cordovaPropertySet": () => (/* reexport safe */ _decorators_cordova_property__WEBPACK_IMPORTED_MODULE_6__.cordovaPropertySet),
/* harmony export */   "instancePropertyGet": () => (/* reexport safe */ _decorators_instance_property__WEBPACK_IMPORTED_MODULE_7__.instancePropertyGet),
/* harmony export */   "instancePropertySet": () => (/* reexport safe */ _decorators_instance_property__WEBPACK_IMPORTED_MODULE_7__.instancePropertySet)
/* harmony export */ });
/* harmony import */ var _bootstrap__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./bootstrap */ 46367);
/* harmony import */ var _awesome_cordova_plugin__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./awesome-cordova-plugin */ 16351);
/* harmony import */ var _decorators_common__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./decorators/common */ 9343);
/* harmony import */ var _decorators_cordova__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./decorators/cordova */ 43364);
/* harmony import */ var _decorators_cordova_function_override__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ./decorators/cordova-function-override */ 531);
/* harmony import */ var _decorators_cordova_instance__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ./decorators/cordova-instance */ 90808);
/* harmony import */ var _decorators_cordova_property__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ./decorators/cordova-property */ 34070);
/* harmony import */ var _decorators_instance_property__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! ./decorators/instance-property */ 70422);
/* harmony import */ var _decorators_interfaces__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! ./decorators/interfaces */ 60303);


// Decorators







(0,_bootstrap__WEBPACK_IMPORTED_MODULE_0__.checkReady)();

//# sourceMappingURL=index.js.map

/***/ }),

/***/ 91674:
/*!*************************************************************************!*\
  !*** ./node_modules/@awesome-cordova-plugins/core/__ivy_ngcc__/util.js ***!
  \*************************************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "get": () => (/* binding */ get),
/* harmony export */   "getPromise": () => (/* binding */ getPromise)
/* harmony export */ });
/**
 * @param element
 * @param path
 * @private
 */
function get(element, path) {
    var paths = path.split('.');
    var obj = element;
    for (var i = 0; i < paths.length; i++) {
        if (!obj) {
            return null;
        }
        obj = obj[paths[i]];
    }
    return obj;
}
/**
 * @param callback
 * @private
 */
function getPromise(callback) {
    if (callback === void 0) { callback = function () { }; }
    var tryNativePromise = function () {
        if (typeof Promise === 'function' || (typeof window !== 'undefined' && window.Promise)) {
            return new Promise(function (resolve, reject) {
                callback(resolve, reject);
            });
        }
        else {
            console.error('No Promise support or polyfill found. To enable Ionic Native support, please add the es6-promise polyfill before this script, or run with a library like Angular or on a recent browser.');
        }
    };
    return tryNativePromise();
}
//# sourceMappingURL=util.js.map

/***/ }),

/***/ 76806:
/*!******************************************************************************************!*\
  !*** ./node_modules/@awesome-cordova-plugins/launch-navigator/__ivy_ngcc__/ngx/index.js ***!
  \******************************************************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "LaunchNavigator": () => (/* binding */ LaunchNavigator)
/* harmony export */ });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! tslib */ 64762);
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/core */ 37716);
/* harmony import */ var _awesome_cordova_plugins_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @awesome-cordova-plugins/core */ 16887);




var LaunchNavigator = /** @class */ (function (_super) {
    (0,tslib__WEBPACK_IMPORTED_MODULE_1__.__extends)(LaunchNavigator, _super);
    function LaunchNavigator() {
        var _this = _super !== null && _super.apply(this, arguments) || this;
        _this.APP = {
            USER_SELECT: 'user_select',
            APPLE_MAPS: 'apple_maps',
            GOOGLE_MAPS: 'google_maps',
            WAZE: 'waze',
            CITYMAPPER: 'citymapper',
            NAVIGON: 'navigon',
            TRANSIT_APP: 'transit_app',
            YANDEX: 'yandex',
            UBER: 'uber',
            TOMTOM: 'tomtom',
            BING_MAPS: 'bing_maps',
            SYGIC: 'sygic',
            HERE_MAPS: 'here_maps',
            MOOVIT: 'moovit',
        };
        _this.TRANSPORT_MODE = {
            DRIVING: 'driving',
            WALKING: 'walking',
            BICYCLING: 'bicycling',
            TRANSIT: 'transit',
        };
        return _this;
    }
    LaunchNavigator.prototype.navigate = function (destination, options) { return (0,_awesome_cordova_plugins_core__WEBPACK_IMPORTED_MODULE_0__.cordova)(this, "navigate", { "successIndex": 2, "errorIndex": 3 }, arguments); };
    LaunchNavigator.prototype.isAppAvailable = function (app) { return (0,_awesome_cordova_plugins_core__WEBPACK_IMPORTED_MODULE_0__.cordova)(this, "isAppAvailable", {}, arguments); };
    LaunchNavigator.prototype.availableApps = function () { return (0,_awesome_cordova_plugins_core__WEBPACK_IMPORTED_MODULE_0__.cordova)(this, "availableApps", {}, arguments); };
    LaunchNavigator.prototype.getAppDisplayName = function (app) { return (0,_awesome_cordova_plugins_core__WEBPACK_IMPORTED_MODULE_0__.cordova)(this, "getAppDisplayName", { "sync": true }, arguments); };
    LaunchNavigator.prototype.getAppsForPlatform = function (platform) { return (0,_awesome_cordova_plugins_core__WEBPACK_IMPORTED_MODULE_0__.cordova)(this, "getAppsForPlatform", { "sync": true }, arguments); };
    LaunchNavigator.prototype.supportsTransportMode = function (app, platform) { return (0,_awesome_cordova_plugins_core__WEBPACK_IMPORTED_MODULE_0__.cordova)(this, "supportsTransportMode", { "sync": true }, arguments); };
    LaunchNavigator.prototype.getTransportModes = function (app, platform) { return (0,_awesome_cordova_plugins_core__WEBPACK_IMPORTED_MODULE_0__.cordova)(this, "getTransportModes", { "sync": true }, arguments); };
    LaunchNavigator.prototype.supportsDestName = function (app, platform) { return (0,_awesome_cordova_plugins_core__WEBPACK_IMPORTED_MODULE_0__.cordova)(this, "supportsDestName", { "sync": true }, arguments); };
    LaunchNavigator.prototype.supportsStart = function (app, platform) { return (0,_awesome_cordova_plugins_core__WEBPACK_IMPORTED_MODULE_0__.cordova)(this, "supportsStart", { "sync": true }, arguments); };
    LaunchNavigator.prototype.supportsStartName = function (app, platform) { return (0,_awesome_cordova_plugins_core__WEBPACK_IMPORTED_MODULE_0__.cordova)(this, "supportsStartName", { "sync": true }, arguments); };
    LaunchNavigator.prototype.supportsLaunchMode = function (app, platform) { return (0,_awesome_cordova_plugins_core__WEBPACK_IMPORTED_MODULE_0__.cordova)(this, "supportsLaunchMode", { "sync": true }, arguments); };
    LaunchNavigator.prototype.userSelect = function (destination, options) { return (0,_awesome_cordova_plugins_core__WEBPACK_IMPORTED_MODULE_0__.cordova)(this, "userSelect", { "sync": true }, arguments); };
    Object.defineProperty(LaunchNavigator.prototype, "appSelection", {
        get: function () { return (0,_awesome_cordova_plugins_core__WEBPACK_IMPORTED_MODULE_0__.cordovaPropertyGet)(this, "appSelection"); },
        set: function (value) { (0,_awesome_cordova_plugins_core__WEBPACK_IMPORTED_MODULE_0__.cordovaPropertySet)(this, "appSelection", value); },
        enumerable: false,
        configurable: true
    });
    LaunchNavigator.pluginName = "LaunchNavigator";
    LaunchNavigator.plugin = "uk.co.workingedge.phonegap.plugin.launchnavigator";
    LaunchNavigator.pluginRef = "launchnavigator";
    LaunchNavigator.repo = "https://github.com/dpa99c/phonegap-launch-navigator";
    LaunchNavigator.platforms = ["Android", "iOS", "Windows", "Windows Phone 8"];
LaunchNavigator.ɵfac = /*@__PURE__*/ function () { var ɵLaunchNavigator_BaseFactory; return function LaunchNavigator_Factory(t) { return (ɵLaunchNavigator_BaseFactory || (ɵLaunchNavigator_BaseFactory = _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵgetInheritedFactory"](LaunchNavigator)))(t || LaunchNavigator); }; }();
LaunchNavigator.ɵprov = /*@__PURE__*/ _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵdefineInjectable"]({ token: LaunchNavigator, factory: function (t) { return LaunchNavigator.ɵfac(t); } });
(function () { (typeof ngDevMode === "undefined" || ngDevMode) && _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵsetClassMetadata"](LaunchNavigator, [{
        type: _angular_core__WEBPACK_IMPORTED_MODULE_2__.Injectable
    }], null, null); })();
    return LaunchNavigator;
}(_awesome_cordova_plugins_core__WEBPACK_IMPORTED_MODULE_0__.AwesomeCordovaNativePlugin));


//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiaW5kZXguanMiLCJzb3VyY2VzIjpbIi4uLy4uLy4uLy4uLy4uL3NyYy9AYXdlc29tZS1jb3Jkb3ZhLXBsdWdpbnMvcGx1Z2lucy9sYXVuY2gtbmF2aWdhdG9yL25neC9pbmRleC50cyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiO0FBQUEsT0FBTyxFQUFFLFVBQVUsRUFBRSxNQUFNLGVBQWUsQ0FBQztBQUMzQyxPQUFPLCtFQUFnRSxNQUFNLCtCQUErQixDQUFDOztBQUM3RztBQUdnQixJQTJScUIsbUNBQTBCO0FBQUM7QUFFaEQ7QUFHaEIsUUFKRSxTQUFHLEdBQVE7QUFDYixZQUFJLFdBQVcsRUFBRSxhQUFhO0FBQzlCLFlBQUksVUFBVSxFQUFFLFlBQVk7QUFDNUIsWUFBSSxXQUFXLEVBQUUsYUFBYTtBQUM5QixZQUFJLElBQUksRUFBRSxNQUFNO0FBQ2hCLFlBQUksVUFBVSxFQUFFLFlBQVk7QUFDNUIsWUFBSSxPQUFPLEVBQUUsU0FBUztBQUN0QixZQUFJLFdBQVcsRUFBRSxhQUFhO0FBQzlCLFlBQUksTUFBTSxFQUFFLFFBQVE7QUFDcEIsWUFBSSxJQUFJLEVBQUUsTUFBTTtBQUNoQixZQUFJLE1BQU0sRUFBRSxRQUFRO0FBQ3BCLFlBQUksU0FBUyxFQUFFLFdBQVc7QUFDMUIsWUFBSSxLQUFLLEVBQUUsT0FBTztBQUNsQixZQUFJLFNBQVMsRUFBRSxXQUFXO0FBQzFCLFlBQUksTUFBTSxFQUFFLFFBQVE7QUFDcEIsU0FBRyxDQUFDO0FBQ0osUUFDRSxvQkFBYyxHQUFRO0FBQ3hCLFlBQUksT0FBTyxFQUFFLFNBQVM7QUFDdEIsWUFBSSxPQUFPLEVBQUUsU0FBUztBQUN0QixZQUFJLFNBQVMsRUFBRSxXQUFXO0FBQzFCLFlBQUksT0FBTyxFQUFFLFNBQVM7QUFDdEIsU0FBRyxDQUFDO0FBQ0o7QUFFQTtBQUFNLElBYUosa0NBQVEsYUFBQyxXQUE4QixFQUFFLE9BQWdDO0FBS3hCLElBTWpELHdDQUFjLGFBQUMsR0FBVztBQUtQLElBS25CLHVDQUFhO0FBS1ksSUFNekIsMkNBQWlCLGFBQUMsR0FBVztBQUthLElBTTFDLDRDQUFrQixhQUFDLFFBQWdCO0FBS00sSUFPekMsK0NBQXFCLGFBQUMsR0FBVyxFQUFFLFFBQWdCO0FBS04sSUFPN0MsMkNBQWlCLGFBQUMsR0FBVyxFQUFFLFFBQWdCO0FBTWhDLElBSWYsMENBQWdCLGFBQUMsR0FBVyxFQUFFLFFBQWdCO0FBS04sSUFPeEMsdUNBQWEsYUFBQyxHQUFXLEVBQUUsUUFBZ0I7QUFNL0IsSUFJWiwyQ0FBaUIsYUFBQyxHQUFXLEVBQUUsUUFBZ0I7QUFLTixJQVF6Qyw0Q0FBa0IsYUFBQyxHQUFXLEVBQUUsUUFBZ0I7QUFNbEQsSUFHRSxvQ0FBVSxhQUFDLFdBQThCLEVBQUUsT0FBK0I7QUFFakIsMEJBeEl6RCx5Q0FBWTtBQUFJO0FBS1M7QUFDUjtBQUNsQjtBQUEyQjtBQUUzQjtBQUdEO0FBQzZFO0FBS2pFO0FBRU47bURBL0NMLFVBQVU7Ozs7MEJBQ0w7QUFBQywwQkFoU1A7QUFBRSxFQWdTbUMsMEJBQTBCO0FBQzlELFNBRFksZUFBZTtBQUFJIiwic291cmNlc0NvbnRlbnQiOlsiaW1wb3J0IHsgSW5qZWN0YWJsZSB9IGZyb20gJ0Bhbmd1bGFyL2NvcmUnO1xuaW1wb3J0IHsgQ29yZG92YSwgQ29yZG92YVByb3BlcnR5LCBBd2Vzb21lQ29yZG92YU5hdGl2ZVBsdWdpbiwgUGx1Z2luIH0gZnJvbSAnQGF3ZXNvbWUtY29yZG92YS1wbHVnaW5zL2NvcmUnO1xuXG5leHBvcnQgaW50ZXJmYWNlIFByb21wdHNPcHRpb25zIHtcbiAgLyoqXG4gICAqIGEgZnVuY3Rpb24gdG8gcGFzcyB0aGUgdXNlcidzIGRlY2lzaW9uIHdoZXRoZXIgdG8gcmVtZW1iZXIgdGhlaXIgY2hvaWNlIG9mIGFwcC5cbiAgICogVGhpcyB3aWxsIGJlIHBhc3NlZCBhIHNpbmdsZSBib29sZWFuIHZhbHVlIGluZGljYXRpbmcgdGhlIHVzZXIncyBkZWNpc2lvbi5cbiAgICpcbiAgICogQHBhcmFtIHJlbWVtYmVyQ2hvaWNlXG4gICAqL1xuICBjYWxsYmFjaz86IChyZW1lbWJlckNob2ljZTogYm9vbGVhbikgPT4gdm9pZDtcblxuICAvKipcbiAgICogdGV4dCB0byBkaXNwbGF5IGluIHRoZSBuYXRpdmUgcHJvbXB0IGhlYWRlciBhc2tpbmcgdXNlciB3aGV0aGVyIHRvIHJlbWVtYmVyIHRoZWlyIGNob2ljZS5cbiAgICogRGVmYXVsdHMgdG8gXCJSZW1lbWJlciB5b3VyIGNob2ljZT9cIiBpZiBub3Qgc3BlY2lmaWVkLlxuICAgKi9cbiAgaGVhZGVyVGV4dD86IHN0cmluZztcblxuICAvKipcbiAgICogdGV4dCB0byBkaXNwbGF5IGluIHRoZSBuYXRpdmUgcHJvbXB0IGJvZHkgYXNraW5nIHVzZXIgd2hldGhlciB0byByZW1lbWJlciB0aGVpciBjaG9pY2UuXG4gICAqIERlZmF1bHRzIHRvIFwiVXNlIHRoZSBzYW1lIGFwcCBmb3IgbmF2aWdhdGluZyBuZXh0IHRpbWU/XCIgaWYgbm90IHNwZWNpZmllZC5cbiAgICovXG4gIGJvZHlUZXh0Pzogc3RyaW5nO1xuXG4gIC8qKlxuICAgKiB0ZXh0IHRvIGRpc3BsYXkgZm9yIHRoZSBZZXMgYnV0dG9uLlxuICAgKiBEZWZhdWx0cyB0byBcIlllc1wiIGlmIG5vdCBzcGVjaWZpZWQuXG4gICAqL1xuICB5ZXNCdXR0b25UZXh0Pzogc3RyaW5nO1xuXG4gIC8qKlxuICAgKiB0ZXh0IHRvIGRpc3BsYXkgZm9yIHRoZSBObyBidXR0b24uXG4gICAqIERlZmF1bHRzIHRvIFwiTm9cIiBpZiBub3Qgc3BlY2lmaWVkLlxuICAgKi9cbiAgbm9CdXR0b25UZXh0Pzogc3RyaW5nO1xufVxuXG5leHBvcnQgaW50ZXJmYWNlIFJlbWVtYmVyQ2hvaWNlT3B0aW9ucyB7XG4gIC8qKlxuICAgKiB3aGV0aGVyIHRvIHJlbWVtYmVyIHVzZXIgY2hvaWNlIG9mIGFwcCBmb3IgbmV4dCB0aW1lLCBpbnN0ZWFkIG9mIGFza2luZyBhZ2FpbiBmb3IgdXNlciBjaG9pY2UuXG4gICAqIGBcInByb21wdFwiYCAtIFByb21wdCB1c2VyIHRvIGRlY2lkZSB3aGV0aGVyIHRvIHJlbWVtYmVyIGNob2ljZS5cbiAgICogIC0gRGVmYXVsdCB2YWx1ZSBpZiB1bnNwZWNpZmllZC5cbiAgICogIC0gSWYgYHByb21wdEZuYCBpcyBkZWZpbmVkLCB0aGlzIHdpbGwgYmUgdXNlZCBmb3IgdXNlciBjb25maXJtYXRpb24uXG4gICAqICAtIE90aGVyd2lzZSAoYnkgZGVmYXVsdCksIGEgbmF0aXZlIGRpYWxvZyB3aWxsIGJlIGRpc3BsYXllZCB0byBhc2sgdXNlci5cbiAgICogYGZhbHNlYCAtIERvIG5vdCByZW1lbWJlciB1c2VyIGNob2ljZS5cbiAgICogYHRydWVgIC0gUmVtZW1iZXIgdXNlciBjaG9pY2UuXG4gICAqL1xuICBlbmFibGVkPzogYm9vbGVhbiB8IHN0cmluZztcblxuICAvKipcbiAgICogYSBmdW5jdGlvbiB3aGljaCBhc2tzIHRoZSB1c2VyIHdoZXRoZXIgdG8gcmVtZW1iZXIgdGhlaXIgY2hvaWNlIG9mIGFwcC5cbiAgICogSWYgdGhpcyBpcyBkZWZpbmVkLCB0aGVuIHRoZSBkZWZhdWx0IGRpYWxvZyBwcm9tcHQgd2lsbCBub3QgYmUgc2hvd24sIGFsbG93aW5nIGZvciBhIGN1c3RvbSBVSSBmb3IgYXNraW5nIHRoZSB1c2VyLlxuICAgKiBUaGlzIHdpbGwgYmUgcGFzc2VkIGEgY2FsbGJhY2sgZnVuY3Rpb24gd2hpY2ggc2hvdWxkIGJlIGludm9rZWQgd2l0aCBhIHNpbmdsZSBib29sZWFuIGFyZ3VtZW50IHdoaWNoIGluZGljYXRlcyB0aGUgdXNlcidzIGRlY2lzaW9uIHRvIHJlbWVtYmVyIHRoZWlyIGNob2ljZS5cbiAgICpcbiAgICogQHBhcmFtIGNhbGxiYWNrXG4gICAqL1xuICBwcm9tcHRGbj86IChjYWxsYmFjazogKHJlbWVtYmVyQ2hvaWNlOiBib29sZWFuKSA9PiB2b2lkKSA9PiB2b2lkO1xuXG4gIC8qKlxuICAgKiBvcHRpb25zIHJlbGF0ZWQgdG8gdGhlIGRlZmF1bHQgZGlhbG9nIHByb21wdCB1c2VkIHRvIGFzayB0aGUgdXNlciB3aGV0aGVyIHRvIHJlbWVtYmVyIHRoZWlyIGNob2ljZSBvZiBhcHAuXG4gICAqL1xuICBwcm9tcHQ/OiBQcm9tcHRzT3B0aW9ucztcbn1cblxuZXhwb3J0IGludGVyZmFjZSBBcHBTZWxlY3Rpb25PcHRpb25zIHtcbiAgLyoqXG4gICAqIHRleHQgdG8gZGlzcGxheSBpbiB0aGUgbmF0aXZlIHBpY2tlciB3aGljaCBlbmFibGVzIHVzZXIgdG8gc2VsZWN0IHdoaWNoIG5hdmlnYXRpb24gYXBwIHRvIGxhdW5jaC5cbiAgICogRGVmYXVsdHMgdG8gXCJTZWxlY3QgYXBwIGZvciBuYXZpZ2F0aW9uXCIgaWYgbm90IHNwZWNpZmllZC5cbiAgICovXG4gIGRpYWxvZ0hlYWRlclRleHQ/OiBzdHJpbmc7XG5cbiAgLyoqXG4gICAqIHRleHQgdG8gZGlzcGxheSBmb3IgdGhlIGNhbmNlbCBidXR0b24gaW4gdGhlIG5hdGl2ZSBwaWNrZXIgd2hpY2ggZW5hYmxlcyB1c2VyIHRvIHNlbGVjdCB3aGljaCBuYXZpZ2F0aW9uIGFwcCB0byBsYXVuY2guXG4gICAqIERlZmF1bHRzIHRvIFwiQ2FuY2VsXCIgaWYgbm90IHNwZWNpZmllZC5cbiAgICovXG4gIGNhbmNlbEJ1dHRvblRleHQ/OiBzdHJpbmc7XG5cbiAgLyoqXG4gICAqIExpc3Qgb2YgYXBwcywgZGVmaW5lZCBhcyBgbGF1bmNobmF2aWdhdG9yLkFQUGAgY29uc3RhbnRzLCB3aGljaCBzaG91bGQgYmUgZGlzcGxheWVkIGluIHRoZSBwaWNrZXIgaWYgdGhlIGFwcCBpcyBhdmFpbGFibGUuXG4gICAqIFRoaXMgY2FuIGJlIHVzZWQgdG8gcmVzdHJpY3Qgd2hpY2ggYXBwcyBhcmUgZGlzcGxheWVkLCBldmVuIGlmIHRoZXkgYXJlIGluc3RhbGxlZC5cbiAgICogQnkgZGVmYXVsdCwgYWxsIGF2YWlsYWJsZSBhcHBzIHdpbGwgYmUgZGlzcGxheWVkLlxuICAgKi9cbiAgbGlzdD86IHN0cmluZ1tdO1xuXG4gIC8qKlxuICAgKiBDYWxsYmFjayB0byBpbnZva2Ugd2hlbiB0aGUgdXNlciBzZWxlY3RzIGFuIGFwcCBpbiB0aGUgbmF0aXZlIHBpY2tlci5cbiAgICogQSBzaW5nbGUgc3RyaW5nIGFyZ3VtZW50IGlzIHBhc3NlZCB3aGljaCBpcyB0aGUgYXBwIHdoYXQgd2FzIHNlbGVjdGVkIGRlZmluZWQgYXMgYSBgbGF1bmNobmF2aWdhdG9yLkFQUGAgY29uc3RhbnQuXG4gICAqL1xuICBjYWxsYmFjaz86IChhcHA6IHN0cmluZykgPT4gdm9pZDtcblxuICAvKipcbiAgICogKEFuZHJvaWQgb25seSkgbmF0aXZlIHBpY2tlciB0aGVtZS4gU3BlY2lmeSB1c2luZyBgYWN0aW9uc2hlZXQuQU5EUk9JRF9USEVNRVNgIGNvbnN0YW50cy5cbiAgICogRGVmYXVsdCBgYWN0aW9uc2hlZXQuQU5EUk9JRF9USEVNRVMuVEhFTUVfSE9MT19MSUdIVGBcbiAgICovXG4gIGFuZHJvaWRUaGVtZT86IG51bWJlcjtcblxuICAvKipcbiAgICogb3B0aW9ucyByZWxhdGVkIHRvIHdoZXRoZXIgdG8gcmVtZW1iZXIgdXNlciBjaG9pY2Ugb2YgYXBwIGZvciBuZXh0IHRpbWUsIGluc3RlYWQgb2YgYXNraW5nIGFnYWluIGZvciB1c2VyIGNob2ljZS5cbiAgICovXG4gIHJlbWVtYmVyQ2hvaWNlPzogUmVtZW1iZXJDaG9pY2VPcHRpb25zO1xufVxuXG5leHBvcnQgaW50ZXJmYWNlIExhdW5jaE5hdmlnYXRvck9wdGlvbnMge1xuICAvKipcbiAgICogQSBjYWxsYmFjayB0byBpbnZva2Ugd2hlbiB0aGUgbmF2aWdhdGlvbiBhcHAgaXMgc3VjY2Vzc2Z1bGx5IGxhdW5jaGVkLlxuICAgKi9cbiAgc3VjY2Vzc0NhbGxiYWNrPzogRnVuY3Rpb247XG5cbiAgLyoqXG4gICAqIEEgY2FsbGJhY2sgdG8gaW52b2tlIGlmIGFuIGVycm9yIGlzIGVuY291bnRlcmVkIHdoaWxlIGxhdW5jaGluZyB0aGUgYXBwLlxuICAgKiBBIHNpbmdsZSBzdHJpbmcgYXJndW1lbnQgY29udGFpbmluZyB0aGUgZXJyb3IgbWVzc2FnZSB3aWxsIGJlIHBhc3NlZCBpbi5cbiAgICovXG4gIGVycm9yQ2FsbGJhY2s/OiAoZXJyb3I6IHN0cmluZykgPT4gdm9pZDtcblxuICAvKipcbiAgICogbmFtZSBvZiB0aGUgbmF2aWdhdGlvbiBhcHAgdG8gdXNlIGZvciBkaXJlY3Rpb25zLlxuICAgKiBTcGVjaWZ5IHVzaW5nIGxhdW5jaG5hdmlnYXRvci5BUFAgY29uc3RhbnRzLlxuICAgKiBlLmcuIGBsYXVuY2huYXZpZ2F0b3IuQVBQLkdPT0dMRV9NQVBTYC5cbiAgICogSWYgbm90IHNwZWNpZmllZCwgZGVmYXVsdHMgdG8gVXNlciBTZWxlY3Rpb24uXG4gICAqL1xuICBhcHA/OiBzdHJpbmc7XG5cbiAgLyoqXG4gICAqIG5pY2tuYW1lIHRvIGRpc3BsYXkgaW4gYXBwIGZvciBkZXN0aW5hdGlvbi4gZS5nLiBcIkJvYidzIEhvdXNlXCIuXG4gICAqL1xuICBkZXN0aW5hdGlvbk5hbWU/OiBzdHJpbmc7XG5cbiAgLyoqXG4gICAqIFN0YXJ0IHBvaW50IG9mIHRoZSBuYXZpZ2F0aW9uLlxuICAgKiBJZiBub3Qgc3BlY2lmaWVkLCB0aGUgY3VycmVudCBkZXZpY2UgbG9jYXRpb24gd2lsbCBiZSB1c2VkLlxuICAgKiBFaXRoZXI6XG4gICAqICAtIGEge3N0cmluZ30gY29udGFpbmluZyB0aGUgYWRkcmVzcy4gZS5nLiBcIkJ1Y2tpbmdoYW0gUGFsYWNlLCBMb25kb25cIlxuICAgKiAgLSBhIHtzdHJpbmd9IGNvbnRhaW5pbmcgYSBsYXRpdHVkZS9sb25naXR1ZGUgY29vcmRpbmF0ZS4gZS5nLiBcIjUwLjEuIC00LjBcIlxuICAgKiAgLSBhbiB7YXJyYXl9LCB3aGVyZSB0aGUgZmlyc3QgZWxlbWVudCBpcyB0aGUgbGF0aXR1ZGUgYW5kIHRoZSBzZWNvbmQgZWxlbWVudCBpcyBhIGxvbmdpdHVkZSwgYXMgZGVjaW1hbCBudW1iZXJzLiBlLmcuIFs1MC4xLCAtNC4wXVxuICAgKi9cbiAgc3RhcnQ/OiBzdHJpbmcgfCBudW1iZXJbXTtcblxuICAvKipcbiAgICogbmlja25hbWUgdG8gZGlzcGxheSBpbiBhcHAgZm9yIHN0YXJ0IC4gZS5nLiBcIk15IEhvdXNlXCIuXG4gICAqL1xuICBzdGFydE5hbWU/OiBzdHJpbmc7XG5cbiAgLyoqXG4gICAqIFRyYW5zcG9ydGF0aW9uIG1vZGUgZm9yIG5hdmlnYXRpb246IFwiZHJpdmluZ1wiLCBcIndhbGtpbmdcIiBvciBcInRyYW5zaXRcIi4gRGVmYXVsdHMgdG8gXCJkcml2aW5nXCIgaWYgbm90IHNwZWNpZmllZC5cbiAgICovXG4gIHRyYW5zcG9ydE1vZGU/OiBzdHJpbmc7XG5cbiAgLyoqXG4gICAqIElmIHRydWUsIGRlYnVnIGxvZyBvdXRwdXQgd2lsbCBiZSBnZW5lcmF0ZWQgYnkgdGhlIHBsdWdpbi4gRGVmYXVsdHMgdG8gZmFsc2UuXG4gICAqL1xuICBlbmFibGVEZWJ1Zz86IGJvb2xlYW47XG5cbiAgLyoqXG4gICAqIGEga2V5L3ZhbHVlIG1hcCBvZiBleHRyYSBhcHAtc3BlY2lmaWMgcGFyYW1ldGVycy4gRm9yIGV4YW1wbGUsIHRvIHRlbGwgR29vZ2xlIE1hcHMgb24gQW5kcm9pZCB0byBkaXNwbGF5IFNhdGVsbGl0ZSB2aWV3IGluIFwibWFwc1wiIGxhdW5jaCBtb2RlOiBge1widFwiOiBcImtcIn1gXG4gICAqL1xuICBleHRyYXM/OiBhbnk7XG5cbiAgLyoqXG4gICAqIChBbmRyb2lkIG9ubHkpIG1vZGUgaW4gd2hpY2ggdG8gb3BlbiBHb29nbGUgTWFwcyBhcHAuXG4gICAqIGBsYXVuY2huYXZpZ2F0b3IuTEFVTkNIX01PREUuTUFQU2Agb3IgYGxhdW5jaG5hdmlnYXRvci5MQVVOQ0hfTU9ERS5UVVJOX0JZX1RVUk5gXG4gICAqIERlZmF1bHRzIHRvIGBsYXVuY2huYXZpZ2F0b3IuTEFVTkNIX01PREUuTUFQU2AgaWYgbm90IHNwZWNpZmllZC5cbiAgICovXG4gIGxhdW5jaE1vZGVHb29nbGVNYXBzPzogc3RyaW5nO1xuXG4gIC8qKlxuICAgKiAoaU9TIG9ubHkpIG1ldGhvZCB0byB1c2UgdG8gb3BlbiBBcHBsZSBNYXBzIGFwcC5cbiAgICogYGxhdW5jaG5hdmlnYXRvci5MQVVOQ0hfTU9ERS5VUklfU0NIRU1FYCBvciBgbGF1bmNobmF2aWdhdG9yLkxBVU5DSF9NT0RFLk1BUEtJVGBcbiAgICogRGVmYXVsdHMgdG8gYGxhdW5jaG5hdmlnYXRvci5MQVVOQ0hfTU9ERS5VUklfU0NIRU1FYCBpZiBub3Qgc3BlY2lmaWVkLlxuICAgKi9cbiAgbGF1bmNoTW9kZUFwcGxlTWFwcz86IHN0cmluZztcblxuICAvKipcbiAgICogKFdpbmRvd3Mgb25seSkgSWYgZmFsc2UsIHRoZSBwbHVnaW4gd2lsbCBOT1QgYXR0ZW1wdCB0byB1c2UgdGhlIGdlb2xvY2F0aW9uIHBsdWdpbiB0byBkZXRlcm1pbmUgdGhlIGN1cnJlbnQgZGV2aWNlIHBvc2l0aW9uIHdoZW4gdGhlIHN0YXJ0IGxvY2F0aW9uIHBhcmFtZXRlciBpcyBvbWl0dGVkLiBEZWZhdWx0cyB0byB0cnVlLlxuICAgKi9cbiAgZW5hYmxlR2VvbG9jYXRpb24/OiBib29sZWFuO1xuXG4gIC8qKlxuICAgKiAoQW5kcm9pZCBhbmQgaU9TIG9ubHkpIElmIHRydWUsIGFuZCBpbnB1dCBsb2NhdGlvbiB0eXBlKHMpIGRvZXNuJ3QgbWF0Y2ggdGhvc2UgcmVxdWlyZWQgYnkgdGhlIGFwcCwgdXNlIGdlb2NvZGluZyB0byBvYnRhaW4gdGhlIGFkZHJlc3MvY29vcmRzIGFzIHJlcXVpcmVkLiBEZWZhdWx0cyB0byB0cnVlLlxuICAgKi9cbiAgZW5hYmxlR2VvY29kaW5nPzogYm9vbGVhbjtcblxuICAvKipcbiAgICogb3B0aW9ucyByZWxhdGVkIHRvIHRoZSBkZWZhdWx0IG5hdGl2ZSBhY3Rpb25zaGVldCBwaWNrZXIgd2hpY2ggZW5hYmxlcyB1c2VyIHRvIHNlbGVjdCB3aGljaCBuYXZpZ2F0aW9uIGFwcCB0byBsYXVuY2ggaWYgYGFwcGAgaXMgbm90IHNwZWNpZmllZC5cbiAgICovXG4gIGFwcFNlbGVjdGlvbj86IEFwcFNlbGVjdGlvbk9wdGlvbnM7XG59XG5cbmV4cG9ydCBpbnRlcmZhY2UgVXNlckNob2ljZSB7XG4gIC8qKlxuICAgKiBJbmRpY2F0ZXMgd2hldGhlciBhIHVzZXIgY2hvaWNlIGV4aXN0cyBmb3IgYSBwcmVmZXJyZWQgbmF2aWdhdG9yIGFwcC5cbiAgICpcbiAgICogQHBhcmFtIGNhbGxiYWNrIC0gZnVuY3Rpb24gdG8gcGFzcyByZXN1bHQgdG86IHdpbGwgcmVjZWl2ZSBhIGJvb2xlYW4gYXJndW1lbnQuXG4gICAqL1xuICBleGlzdHM6IChjYWxsYmFjazogKGV4aXN0czogYm9vbGVhbikgPT4gdm9pZCkgPT4gdm9pZDtcblxuICAvKipcbiAgICogUmV0dXJucyBjdXJyZW50IHVzZXIgY2hvaWNlIG9mIHByZWZlcnJlZCBuYXZpZ2F0b3IgYXBwLlxuICAgKlxuICAgKiBAcGFyYW0gY2FsbGJhY2sgLSBmdW5jdGlvbiB0byBwYXNzIHJlc3VsdCB0bzogd2lsbCByZWNlaXZlIGEgc3RyaW5nIGFyZ3VtZW50IGluZGljYXRpbmcgdGhlIGFwcCwgd2hpY2ggaXMgYSBjb25zdGFudCBpbiBgbGF1bmNobmF2aWdhdG9yLkFQUGAuXG4gICAqL1xuICBnZXQ6IChjYWxsYmFjazogKGFwcDogc3RyaW5nKSA9PiB2b2lkKSA9PiB2b2lkO1xuXG4gIC8qKlxuICAgKiBTZXRzIHRoZSBjdXJyZW50IHVzZXIgY2hvaWNlIG9mIHByZWZlcnJlZCBuYXZpZ2F0b3IgYXBwLlxuICAgKlxuICAgKiBAcGFyYW0gYXBwIC0gYXBwIHRvIHNldCBhcyBwcmVmZXJyZWQgY2hvaWNlIGFzIGEgY29uc3RhbnQgaW4gYGxhdW5jaG5hdmlnYXRvci5BUFBgLlxuICAgKiBAcGFyYW0gY2FsbGJhY2sgLSBmdW5jdGlvbiB0byBjYWxsIG9uY2Ugb3BlcmF0aW9uIGlzIGNvbXBsZXRlLlxuICAgKi9cbiAgc2V0OiAoYXBwOiBzdHJpbmcsIGNhbGxiYWNrOiAoKSA9PiB2b2lkKSA9PiB2b2lkO1xuXG4gIC8qKlxuICAgKiBDbGVhcnMgdGhlIGN1cnJlbnQgdXNlciBjaG9pY2Ugb2YgcHJlZmVycmVkIG5hdmlnYXRvciBhcHAuXG4gICAqXG4gICAqIEBwYXJhbSBjYWxsYmFjayAtIGZ1bmN0aW9uIHRvIGNhbGwgb25jZSBvcGVyYXRpb24gaXMgY29tcGxldGUuXG4gICAqL1xuICBjbGVhcjogKGNhbGxiYWNrOiAoKSA9PiB2b2lkKSA9PiB2b2lkO1xufVxuXG5leHBvcnQgaW50ZXJmYWNlIFVzZXJQcm9tcHRlZCB7XG4gIC8qKlxuICAgKiBJbmRpY2F0ZXMgd2hldGhlciB1c2VyIGhhcyBhbHJlYWR5IGJlZW4gcHJvbXB0ZWQgd2hldGhlciB0byByZW1lbWJlciB0aGVpciBjaG9pY2UgYSBwcmVmZXJyZWQgbmF2aWdhdG9yIGFwcC5cbiAgICpcbiAgICogQHBhcmFtIGNhbGxiYWNrIC0gZnVuY3Rpb24gdG8gcGFzcyByZXN1bHQgdG86IHdpbGwgcmVjZWl2ZSBhIGJvb2xlYW4gYXJndW1lbnQuXG4gICAqL1xuICBnZXQ6IChjYWxsYmFjazogKGV4aXN0czogYm9vbGVhbikgPT4gdm9pZCkgPT4gdm9pZDtcblxuICAvKipcbiAgICogU2V0cyBmbGFnIGluZGljYXRpbmcgdXNlciBoYXMgYWxyZWFkeSBiZWVuIHByb21wdGVkIHdoZXRoZXIgdG8gcmVtZW1iZXIgdGhlaXIgY2hvaWNlIGEgcHJlZmVycmVkIG5hdmlnYXRvciBhcHAuXG4gICAqXG4gICAqIEBwYXJhbSBjYWxsYmFjayAtIGZ1bmN0aW9uIHRvIGNhbGwgb25jZSBvcGVyYXRpb24gaXMgY29tcGxldGUuXG4gICAqL1xuICBzZXQ6IChjYWxsYmFjazogKCkgPT4gdm9pZCkgPT4gdm9pZDtcblxuICAvKipcbiAgICogQ2xlYXJzIGZsYWcgd2hpY2ggaW5kaWNhdGVzIGlmIHVzZXIgaGFzIGFscmVhZHkgYmVlbiBwcm9tcHRlZCB3aGV0aGVyIHRvIHJlbWVtYmVyIHRoZWlyIGNob2ljZSBhIHByZWZlcnJlZCBuYXZpZ2F0b3IgYXBwLlxuICAgKlxuICAgKiBAcGFyYW0gY2FsbGJhY2sgLSBmdW5jdGlvbiB0byBjYWxsIG9uY2Ugb3BlcmF0aW9uIGlzIGNvbXBsZXRlLlxuICAgKi9cbiAgY2xlYXI6IChjYWxsYmFjazogKCkgPT4gdm9pZCkgPT4gdm9pZDtcbn1cblxuZXhwb3J0IGludGVyZmFjZSBBcHBTZWxlY3Rpb24ge1xuICB1c2VyQ2hvaWNlOiBVc2VyQ2hvaWNlO1xuICB1c2VyUHJvbXB0ZWQ6IFVzZXJQcm9tcHRlZDtcbn1cblxuLyoqXG4gKiBAbmFtZSBMYXVuY2ggTmF2aWdhdG9yXG4gKiBAZGVzY3JpcHRpb25cbiAqIFJlcXVpcmVzIENvcmRvdmEgcGx1Z2luOiB1ay5jby53b3JraW5nZWRnZS5waG9uZWdhcC5wbHVnaW4ubGF1bmNobmF2aWdhdG9yLiBGb3IgbW9yZSBpbmZvLCBwbGVhc2Ugc2VlIHRoZSBbTGF1bmNoTmF2aWdhdG9yIHBsdWdpbiBkb2NzXShodHRwczovL2dpdGh1Yi5jb20vZHBhOTljL3Bob25lZ2FwLWxhdW5jaC1uYXZpZ2F0b3IpLlxuICogQHVzYWdlXG4gKiBQbGVhc2UgcmVmZXIgdG8gdGhlIHBsdWdpbidzIHJlcG8gZm9yIGRldGFpbGVkIHVzYWdlLiBUaGlzIGRvY3MgcGFnZSBvbmx5IGV4cGxhaW5zIHRoZSBOYXRpdmUgd3JhcHBlci5cbiAqXG4gKiBgYGB0eXBlc2NyaXB0XG4gKiBpbXBvcnQgeyBMYXVuY2hOYXZpZ2F0b3IsIExhdW5jaE5hdmlnYXRvck9wdGlvbnMgfSBmcm9tICdAYXdlc29tZS1jb3Jkb3ZhLXBsdWdpbnMvbGF1bmNoLW5hdmlnYXRvci9uZ3gnO1xuICpcbiAqIGNvbnN0cnVjdG9yKHByaXZhdGUgbGF1bmNoTmF2aWdhdG9yOiBMYXVuY2hOYXZpZ2F0b3IpIHsgfVxuICpcbiAqIC4uLlxuICpcbiAqIGxldCBvcHRpb25zOiBMYXVuY2hOYXZpZ2F0b3JPcHRpb25zID0ge1xuICogICBzdGFydDogJ0xvbmRvbiwgT04nLFxuICogICBhcHA6IExhdW5jaE5hdmlnYXRvci5BUFBTLlVCRVJcbiAqIH1cbiAqXG4gKiB0aGlzLmxhdW5jaE5hdmlnYXRvci5uYXZpZ2F0ZSgnVG9yb250bywgT04nLCBvcHRpb25zKVxuICogICAudGhlbihcbiAqICAgICBzdWNjZXNzID0+IGNvbnNvbGUubG9nKCdMYXVuY2hlZCBuYXZpZ2F0b3InKSxcbiAqICAgICBlcnJvciA9PiBjb25zb2xlLmxvZygnRXJyb3IgbGF1bmNoaW5nIG5hdmlnYXRvcicsIGVycm9yKVxuICogICApO1xuICogYGBgXG4gKiBAaW50ZXJmYWNlc1xuICogTGF1bmNoTmF2aWdhdG9yT3B0aW9uc1xuICogUHJvbXB0c09wdGlvbnNcbiAqIFJlbWVtYmVyQ2hvaWNlT3B0aW9uc1xuICogQXBwU2VsZWN0aW9uT3B0aW9uc1xuICogVXNlckNob2ljZVxuICogVXNlclByb21wdGVkXG4gKiBBcHBTZWxlY3Rpb25cbiAqL1xuQFBsdWdpbih7XG4gIHBsdWdpbk5hbWU6ICdMYXVuY2hOYXZpZ2F0b3InLFxuICBwbHVnaW46ICd1ay5jby53b3JraW5nZWRnZS5waG9uZWdhcC5wbHVnaW4ubGF1bmNobmF2aWdhdG9yJyxcbiAgcGx1Z2luUmVmOiAnbGF1bmNobmF2aWdhdG9yJyxcbiAgcmVwbzogJ2h0dHBzOi8vZ2l0aHViLmNvbS9kcGE5OWMvcGhvbmVnYXAtbGF1bmNoLW5hdmlnYXRvcicsXG4gIHBsYXRmb3JtczogWydBbmRyb2lkJywgJ2lPUycsICdXaW5kb3dzJywgJ1dpbmRvd3MgUGhvbmUgOCddLFxufSlcbkBJbmplY3RhYmxlKClcbmV4cG9ydCBjbGFzcyBMYXVuY2hOYXZpZ2F0b3IgZXh0ZW5kcyBBd2Vzb21lQ29yZG92YU5hdGl2ZVBsdWdpbiB7XG4gIEFQUDogYW55ID0ge1xuICAgIFVTRVJfU0VMRUNUOiAndXNlcl9zZWxlY3QnLFxuICAgIEFQUExFX01BUFM6ICdhcHBsZV9tYXBzJyxcbiAgICBHT09HTEVfTUFQUzogJ2dvb2dsZV9tYXBzJyxcbiAgICBXQVpFOiAnd2F6ZScsXG4gICAgQ0lUWU1BUFBFUjogJ2NpdHltYXBwZXInLFxuICAgIE5BVklHT046ICduYXZpZ29uJyxcbiAgICBUUkFOU0lUX0FQUDogJ3RyYW5zaXRfYXBwJyxcbiAgICBZQU5ERVg6ICd5YW5kZXgnLFxuICAgIFVCRVI6ICd1YmVyJyxcbiAgICBUT01UT006ICd0b210b20nLFxuICAgIEJJTkdfTUFQUzogJ2JpbmdfbWFwcycsXG4gICAgU1lHSUM6ICdzeWdpYycsXG4gICAgSEVSRV9NQVBTOiAnaGVyZV9tYXBzJyxcbiAgICBNT09WSVQ6ICdtb292aXQnLFxuICB9O1xuXG4gIFRSQU5TUE9SVF9NT0RFOiBhbnkgPSB7XG4gICAgRFJJVklORzogJ2RyaXZpbmcnLFxuICAgIFdBTEtJTkc6ICd3YWxraW5nJyxcbiAgICBCSUNZQ0xJTkc6ICdiaWN5Y2xpbmcnLFxuICAgIFRSQU5TSVQ6ICd0cmFuc2l0JyxcbiAgfTtcblxuICBAQ29yZG92YVByb3BlcnR5KClcbiAgYXBwU2VsZWN0aW9uOiBBcHBTZWxlY3Rpb247XG5cbiAgLyoqXG4gICAqIExhdW5jaGVzIG5hdmlnYXRvciBhcHBcbiAgICpcbiAgICogQHBhcmFtIGRlc3RpbmF0aW9uIHtzdHJpbmd8bnVtYmVyW119IExvY2F0aW9uIG5hbWUgb3IgY29vcmRpbmF0ZXMgKGFzIHN0cmluZyBvciBhcnJheSlcbiAgICogQHBhcmFtIG9wdGlvbnMge0xhdW5jaE5hdmlnYXRvck9wdGlvbnN9XG4gICAqIEByZXR1cm5zIHtQcm9taXNlPGFueT59XG4gICAqL1xuICBAQ29yZG92YSh7XG4gICAgc3VjY2Vzc0luZGV4OiAyLFxuICAgIGVycm9ySW5kZXg6IDMsXG4gIH0pXG4gIG5hdmlnYXRlKGRlc3RpbmF0aW9uOiBzdHJpbmcgfCBudW1iZXJbXSwgb3B0aW9ucz86IExhdW5jaE5hdmlnYXRvck9wdGlvbnMpOiBQcm9taXNlPGFueT4ge1xuICAgIHJldHVybjtcbiAgfVxuXG4gIC8qKlxuICAgKiBEZXRlcm1pbmVzIGlmIHRoZSBnaXZlbiBhcHAgaXMgaW5zdGFsbGVkIGFuZCBhdmFpbGFibGUgb24gdGhlIGN1cnJlbnQgZGV2aWNlLlxuICAgKlxuICAgKiBAcGFyYW0gYXBwIHtzdHJpbmd9XG4gICAqIEByZXR1cm5zIHtQcm9taXNlPGFueT59XG4gICAqL1xuICBAQ29yZG92YSgpXG4gIGlzQXBwQXZhaWxhYmxlKGFwcDogc3RyaW5nKTogUHJvbWlzZTxhbnk+IHtcbiAgICByZXR1cm47XG4gIH1cblxuICAvKipcbiAgICogUmV0dXJucyBhIGxpc3QgaW5kaWNhdGluZyB3aGljaCBhcHBzIGFyZSBpbnN0YWxsZWQgYW5kIGF2YWlsYWJsZSBvbiB0aGUgY3VycmVudCBkZXZpY2UuXG4gICAqXG4gICAqIEByZXR1cm5zIHtQcm9taXNlPHN0cmluZ1tdPn1cbiAgICovXG4gIEBDb3Jkb3ZhKClcbiAgYXZhaWxhYmxlQXBwcygpOiBQcm9taXNlPHN0cmluZ1tdPiB7XG4gICAgcmV0dXJuO1xuICB9XG5cbiAgLyoqXG4gICAqIFJldHVybnMgdGhlIGRpc3BsYXkgbmFtZSBvZiB0aGUgc3BlY2lmaWVkIGFwcC5cbiAgICpcbiAgICogQHBhcmFtIGFwcCB7c3RyaW5nfVxuICAgKiBAcmV0dXJucyB7c3RyaW5nfVxuICAgKi9cbiAgQENvcmRvdmEoeyBzeW5jOiB0cnVlIH0pXG4gIGdldEFwcERpc3BsYXlOYW1lKGFwcDogc3RyaW5nKTogc3RyaW5nIHtcbiAgICByZXR1cm47XG4gIH1cblxuICAvKipcbiAgICogUmV0dXJucyBsaXN0IG9mIHN1cHBvcnRlZCBhcHBzIG9uIGEgZ2l2ZW4gcGxhdGZvcm0uXG4gICAqXG4gICAqIEBwYXJhbSBwbGF0Zm9ybSB7c3RyaW5nfVxuICAgKiBAcmV0dXJucyB7c3RyaW5nW119XG4gICAqL1xuICBAQ29yZG92YSh7IHN5bmM6IHRydWUgfSlcbiAgZ2V0QXBwc0ZvclBsYXRmb3JtKHBsYXRmb3JtOiBzdHJpbmcpOiBzdHJpbmdbXSB7XG4gICAgcmV0dXJuO1xuICB9XG5cbiAgLyoqXG4gICAqIEluZGljYXRlcyBpZiBhbiBhcHAgb24gYSBnaXZlbiBwbGF0Zm9ybSBzdXBwb3J0cyBzcGVjaWZpY2F0aW9uIG9mIHRyYW5zcG9ydCBtb2RlLlxuICAgKlxuICAgKiBAcGFyYW0gYXBwIHtzdHJpbmd9IHNwZWNpZmllZCBhcyBhIHN0cmluZywgeW91IGNhbiB1c2Ugb25lIG9mIHRoZSBjb25zdGFudHMsIGUuZyBgTGF1bmNoTmF2aWdhdG9yLkFQUC5HT09HTEVfTUFQU2BcbiAgICogQHBhcmFtIHBsYXRmb3JtIHtzdHJpbmd9XG4gICAqIEByZXR1cm5zIHtib29sZWFufVxuICAgKi9cbiAgQENvcmRvdmEoeyBzeW5jOiB0cnVlIH0pXG4gIHN1cHBvcnRzVHJhbnNwb3J0TW9kZShhcHA6IHN0cmluZywgcGxhdGZvcm06IHN0cmluZyk6IGJvb2xlYW4ge1xuICAgIHJldHVybjtcbiAgfVxuXG4gIC8qKlxuICAgKiBSZXR1cm5zIHRoZSBsaXN0IG9mIHRyYW5zcG9ydCBtb2RlcyBzdXBwb3J0ZWQgYnkgYW4gYXBwIG9uIGEgZ2l2ZW4gcGxhdGZvcm0uXG4gICAqXG4gICAqIEBwYXJhbSBhcHAge3N0cmluZ31cbiAgICogQHBhcmFtIHBsYXRmb3JtIHtzdHJpbmd9XG4gICAqIEByZXR1cm5zIHtzdHJpbmdbXX1cbiAgICovXG4gIEBDb3Jkb3ZhKHsgc3luYzogdHJ1ZSB9KVxuICBnZXRUcmFuc3BvcnRNb2RlcyhhcHA6IHN0cmluZywgcGxhdGZvcm06IHN0cmluZyk6IHN0cmluZ1tdIHtcbiAgICByZXR1cm47XG4gIH1cblxuICAvKipcbiAgICogQHBhcmFtIGFwcCB7c3RyaW5nfVxuICAgKiBAcGFyYW0gcGxhdGZvcm0ge3N0cmluZ31cbiAgICogQHJldHVybnMge2Jvb2xlYW59XG4gICAqL1xuICBAQ29yZG92YSh7IHN5bmM6IHRydWUgfSlcbiAgc3VwcG9ydHNEZXN0TmFtZShhcHA6IHN0cmluZywgcGxhdGZvcm06IHN0cmluZyk6IGJvb2xlYW4ge1xuICAgIHJldHVybjtcbiAgfVxuXG4gIC8qKlxuICAgKiBJbmRpY2F0ZXMgaWYgYW4gYXBwIG9uIGEgZ2l2ZW4gcGxhdGZvcm0gc3VwcG9ydHMgc3BlY2lmaWNhdGlvbiBvZiBzdGFydCBsb2NhdGlvbi5cbiAgICpcbiAgICogQHBhcmFtIGFwcCB7c3RyaW5nfVxuICAgKiBAcGFyYW0gcGxhdGZvcm0ge3N0cmluZ31cbiAgICogQHJldHVybnMge2Jvb2xlYW59XG4gICAqL1xuICBAQ29yZG92YSh7IHN5bmM6IHRydWUgfSlcbiAgc3VwcG9ydHNTdGFydChhcHA6IHN0cmluZywgcGxhdGZvcm06IHN0cmluZyk6IGJvb2xlYW4ge1xuICAgIHJldHVybjtcbiAgfVxuXG4gIC8qKlxuICAgKiBAcGFyYW0gYXBwIHtzdHJpbmd9XG4gICAqIEBwYXJhbSBwbGF0Zm9ybSB7c3RyaW5nfVxuICAgKiBAcmV0dXJucyB7Ym9vbGVhbn1cbiAgICovXG4gIEBDb3Jkb3ZhKHsgc3luYzogdHJ1ZSB9KVxuICBzdXBwb3J0c1N0YXJ0TmFtZShhcHA6IHN0cmluZywgcGxhdGZvcm06IHN0cmluZyk6IGJvb2xlYW4ge1xuICAgIHJldHVybjtcbiAgfVxuXG4gIC8qKlxuICAgKiBJbmRpY2F0ZXMgaWYgYW4gYXBwIG9uIGEgZ2l2ZW4gcGxhdGZvcm0gc3VwcG9ydHMgc3BlY2lmaWNhdGlvbiBvZiBsYXVuY2ggbW9kZS5cbiAgICogTm90ZSB0aGF0IGN1cnJlbnRseSBvbmx5IEdvb2dsZSBNYXBzIG9uIEFuZHJvaWQgZG9lcy5cbiAgICpcbiAgICogQHBhcmFtIGFwcCB7c3RyaW5nfVxuICAgKiBAcGFyYW0gcGxhdGZvcm0ge3N0cmluZ31cbiAgICogQHJldHVybnMge2Jvb2xlYW59XG4gICAqL1xuICBAQ29yZG92YSh7IHN5bmM6IHRydWUgfSlcbiAgc3VwcG9ydHNMYXVuY2hNb2RlKGFwcDogc3RyaW5nLCBwbGF0Zm9ybTogc3RyaW5nKTogYm9vbGVhbiB7XG4gICAgcmV0dXJuO1xuICB9XG5cbiAgLyoqXG4gICAqIEBwYXJhbSBkZXN0aW5hdGlvbiB7c3RyaW5nIHwgbnVtYmVyW119XG4gICAqIEBwYXJhbSBvcHRpb25zIHtMYXVuY2hOYXZpZ2F0b3JPcHRpb25zfVxuICAgKi9cbiAgQENvcmRvdmEoeyBzeW5jOiB0cnVlIH0pXG4gIHVzZXJTZWxlY3QoZGVzdGluYXRpb246IHN0cmluZyB8IG51bWJlcltdLCBvcHRpb25zOiBMYXVuY2hOYXZpZ2F0b3JPcHRpb25zKTogdm9pZCB7fVxufVxuIl19

/***/ }),

/***/ 52563:
/*!***********************************************************************!*\
  !*** ./src/app/Pages/active-booking/active-booking-routing.module.ts ***!
  \***********************************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "ActiveBookingPageRoutingModule": () => (/* binding */ ActiveBookingPageRoutingModule)
/* harmony export */ });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! tslib */ 64762);
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/core */ 37716);
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/router */ 39895);
/* harmony import */ var _active_booking_page__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./active-booking.page */ 5908);




const routes = [
    {
        path: '',
        component: _active_booking_page__WEBPACK_IMPORTED_MODULE_0__.ActiveBookingPage
    }
];
let ActiveBookingPageRoutingModule = class ActiveBookingPageRoutingModule {
};
ActiveBookingPageRoutingModule = (0,tslib__WEBPACK_IMPORTED_MODULE_1__.__decorate)([
    (0,_angular_core__WEBPACK_IMPORTED_MODULE_2__.NgModule)({
        imports: [_angular_router__WEBPACK_IMPORTED_MODULE_3__.RouterModule.forChild(routes)],
        exports: [_angular_router__WEBPACK_IMPORTED_MODULE_3__.RouterModule],
    })
], ActiveBookingPageRoutingModule);



/***/ }),

/***/ 57926:
/*!***************************************************************!*\
  !*** ./src/app/Pages/active-booking/active-booking.module.ts ***!
  \***************************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "ActiveBookingPageModule": () => (/* binding */ ActiveBookingPageModule)
/* harmony export */ });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! tslib */ 64762);
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @angular/core */ 37716);
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @angular/common */ 38583);
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! @angular/forms */ 3679);
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! @ionic/angular */ 80476);
/* harmony import */ var _active_booking_routing_module__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./active-booking-routing.module */ 52563);
/* harmony import */ var _active_booking_page__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./active-booking.page */ 5908);
/* harmony import */ var _awesome_cordova_plugins_launch_navigator_ngx__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @awesome-cordova-plugins/launch-navigator/ngx */ 76806);








let ActiveBookingPageModule = class ActiveBookingPageModule {
};
ActiveBookingPageModule = (0,tslib__WEBPACK_IMPORTED_MODULE_3__.__decorate)([
    (0,_angular_core__WEBPACK_IMPORTED_MODULE_4__.NgModule)({
        imports: [
            _angular_common__WEBPACK_IMPORTED_MODULE_5__.CommonModule,
            _angular_forms__WEBPACK_IMPORTED_MODULE_6__.FormsModule,
            _ionic_angular__WEBPACK_IMPORTED_MODULE_7__.IonicModule,
            _active_booking_routing_module__WEBPACK_IMPORTED_MODULE_0__.ActiveBookingPageRoutingModule,
        ],
        declarations: [_active_booking_page__WEBPACK_IMPORTED_MODULE_1__.ActiveBookingPage],
        providers: [_awesome_cordova_plugins_launch_navigator_ngx__WEBPACK_IMPORTED_MODULE_2__.LaunchNavigator],
    })
], ActiveBookingPageModule);



/***/ }),

/***/ 5908:
/*!*************************************************************!*\
  !*** ./src/app/Pages/active-booking/active-booking.page.ts ***!
  \*************************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "ActiveBookingPage": () => (/* binding */ ActiveBookingPage)
/* harmony export */ });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! tslib */ 64762);
/* harmony import */ var _raw_loader_active_booking_page_html__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! !raw-loader!./active-booking.page.html */ 26518);
/* harmony import */ var _active_booking_page_scss__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./active-booking.page.scss */ 98846);
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! @angular/core */ 37716);
/* harmony import */ var _base_page_base_page__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../base-page/base-page */ 96093);
/* harmony import */ var _ionic_native_call_number_ngx__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @ionic-native/call-number/ngx */ 64687);
/* harmony import */ var _awesome_cordova_plugins_launch_navigator_ngx__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @awesome-cordova-plugins/launch-navigator/ngx */ 76806);







let ActiveBookingPage = class ActiveBookingPage extends _base_page_base_page__WEBPACK_IMPORTED_MODULE_2__.BasePage {
    constructor(injector, callNumber, launchNavigator) {
        super(injector);
        this.callNumber = callNumber;
        this.launchNavigator = launchNavigator;
        this.clickable = true;
        this.personal_hour = 0;
        this.personal_minimum = 0;
        this.pickup_base_km = 0;
        this.pickup_hour = 0;
        this.pickup_km = 0;
        this.pickup_minimum = 0;
        this.pickup_minute = 0;
        this.paymentclick = true;
        this.start = '';
        this.totalLocation = [];
        let params = this.getQueryParams();
        this.bookingKey = params === null || params === void 0 ? void 0 : params.bookingKey;
        console.log('BookingKey ->', this.bookingKey);
        let bookingRef = this.firebaseService.getDbRef('/bookings/' + this.bookingKey);
        this.mapReady = false;
        this.mapService.showmap = true;
        bookingRef.on('value', (snapshot) => {
            this.booking = snapshot.val();
            if (this.booking != undefined && this.booking != null) {
                this.booking.image_url = snapshot.val().customer_image;
                this.customerID = snapshot.val().customer;
                this.driverID = snapshot.val().driver;
                this.pushToken = snapshot.val().customer_push_token;
                if (this.booking.status == 'CANCELLED') {
                    this.nav.setRoot('tabs');
                }
                if (this.booking.status == 'PAYMENT') {
                    this.paymentclick = false;
                    this.setClickable(false);
                    this.mapService.showmap = false;
                }
                var paystripe = this.firebaseService.getDbRef('/users/' + this.booking.customer);
                paystripe.on('value', (_snapshot) => {
                    if (_snapshot.val()) {
                        this.stripeID = _snapshot.val().stripeID;
                        console.log('this.stripeID.................', this.stripeID);
                    }
                });
            }
            else {
                this.nav.setRoot('tabs');
            }
        });
        var rateRef = this.firebaseService.getDbRef('rates');
        rateRef.on('value', (snapshot) => {
            if (snapshot.val()) {
                this.personal_hour = snapshot.val().personal_hour;
                this.personal_minimum = snapshot.val().personal_minimum;
                this.pickup_base_km = snapshot.val().pickup_base_km;
                this.pickup_hour = snapshot.val().pickup_hour;
                this.pickup_km = snapshot.val().pickup_km;
                this.pickup_minimum = snapshot.val().pickup_minimum;
                this.pickup_minute = snapshot.val().pickup_minute;
            }
        });
    }
    ngOnInit() {
        this.platform.ready().then(() => {
            this.loadMap();
        });
    }
    loadMap() {
        let self = this;
        const location = {
            lat: parseFloat(this.booking.pickup.lat),
            lng: parseFloat(this.booking.pickup.lng),
        };
        this.map = new google.maps.Map(this.mapElement.nativeElement, {
            zoom: 15,
            center: location,
        });
        // this.map = new GoogleMap('map', {
        //   'backgroundColor': 'white',
        //   'controls': {
        //     'compass': false,
        //     'myLocationButton': false,
        //     'indoorPicker': false,
        //     'zoom': true
        //   },
        //   'gestures': {
        //     'scroll': true,
        //     'tilt': true,
        //     'rotate': true,
        //     'zoom': true
        //   },
        //   'camera': {
        //     'latLng': location,
        //     'tilt': 30,
        //     'zoom': 15//,
        //     // 'bearing': 50
        //   }
        // });
        google.maps.event.addListenerOnce(this.map, 'idle', function () {
            var _a;
            // do something only the first time the map is loaded
            (_a = this.map) === null || _a === void 0 ? void 0 : _a.markers.clear();
            this.mapReady = true;
            console.log('Map is ready!');
            this.markerSetTo = 'pickup';
        });
        google.maps.event.addListenerOnce(this.map, 'idle', function () {
            this.mapReady = true;
            let bookingRef = self.firebaseService.getDbRef('/bookings/' + this.bookingKey);
            bookingRef.on('value', (snapshot) => {
                var _a;
                console.log('activeBooking -> snapshot', snapshot.val());
                // this.map.clear();
                this.booking = snapshot.val();
                console.log('mY serice type check address........');
                console.log(this.booking);
                if (this.booking)
                    this.booking.image_url = (_a = snapshot.val()) === null || _a === void 0 ? void 0 : _a.customer_image;
                let marker = new google.maps.Marker({
                    position: location,
                    title: 'Pickup Location',
                });
                marker.setMap(this.map);
                //  marker.showInfoWindow();
                // this.map.setCenter(location);
            });
            console.log('Map is ready!');
        });
    }
    doPayemnet() {
        /*  AAded By HR PATEL  */
        var paymentbill = this.firebaseService.getDbRef('/bookings/' + this.bookingKey + '/trip_cost');
        paymentbill.once('value', (snapbill) => {
            let totalBill = snapbill.val();
            console.log('MY paypal bill.........', totalBill);
            var payRef = this.firebaseService.getDbRef('/users/' + this.booking.customer + '/payment_methods');
            payRef.once('value', (snapshot) => {
                if (snapshot.val()) {
                    let cards = snapshot.val();
                    let cardID = '';
                    console.log('cards.........', cards);
                    let GotIt = false;
                    for (let i = 0; i < cards.length; i++) {
                        if (cards[i].primary) {
                            GotIt = true;
                            cardID = cards[i].stripeCardID;
                        }
                    }
                    if (GotIt) {
                        console.log(cardID, totalBill);
                        this.usersService
                            .stripePayment(cardID, this.stripeID, totalBill)
                            .subscribe((response) => {
                            console.log('stripe payment response', response);
                            let temp;
                            temp = response;
                            if (temp.status == 'succeeded') {
                                this.alertService.Alert('Payment', 'Payment succeeded');
                                // alert("succeeded payment");
                                // alert('Payment succeeded');
                                this.usersService
                                    .sendPush('Trip Ended - Payment Success !!', 'Your cost for the trip is $' + totalBill, this.booking.customer_push_token)
                                    .subscribe((response) => console.log(response));
                                this.paymentclick = true;
                                // this.firebaseService.getDbRef('users/' + customer + '/bookings/' + this.bookingKey + '/status' ).set('PAYMENT');
                                // this.firebaseService.getDbRef('users/' + driver + '/bookings/' + this.bookingKey + '/status').set('PAYMENT');
                                this.nav.push('job-complete', {
                                    bookingKey: this.bookingKey,
                                });
                            }
                            else {
                                // let alert = this.alertCtrl.create({
                                //   title: 'Payment',
                                //   subTitle: temp.message,
                                //   buttons: ['OK']
                                // });
                                // let alert = this.alertCtrl.create();
                                // alert.present();
                                alert(temp.message);
                                // this.firebaseService.getDbRef('users/' + customer + '/bookings/' + this.bookingKey + '/status' ).set('REVIEW');
                                // this.firebaseService.getDbRef('users/' + driver + '/bookings/' + this.bookingKey + '/status').set('REVIEW');
                                this.usersService
                                    .sendPush('Payment Not Approve !!', 'No Payment Method Found. Your cost for the trip is $' +
                                    totalBill, this.booking.customer_push_token)
                                    .subscribe((response) => console.log(response));
                            }
                        }, (err) => {
                            //alert('Server Error ! Try again');
                            // this.firebaseService.getDbRef('users/' + customer + '/bookings/' + this.bookingKey + '/status' ).set('PAYMENT');
                            // this.firebaseService.getDbRef('users/' + driver + '/bookings/' + this.bookingKey + '/status').set('PAYMENT');
                        });
                    }
                    else {
                        alert('Customer Not Selected Primary card');
                        // this.firebaseService.getDbRef('users/' + customer + '/bookings/' + this.bookingKey + '/status' ).set('PAYMENT');
                        // this.firebaseService.getDbRef('users/' + driver + '/bookings/' + this.bookingKey + '/status').set('PAYMENT');
                        this.usersService
                            .sendPush('Payment Failed !!', 'Not Selected Primary card. Your cost for the trip is $' +
                            totalBill, this.booking.customer_push_token)
                            .subscribe((response) => console.log(response));
                    }
                }
                else {
                    alert('Customer Card Not Available');
                    this.usersService
                        .sendPush('Payment Failed !!', ' Card Not Available. Your cost for the trip is $' + totalBill, this.booking.customer_push_token)
                        .subscribe((response) => console.log(response));
                }
            });
        });
        // });
        /* Ended By HR PATEL */
    }
    changeStatus(statusSelected) {
        return (0,tslib__WEBPACK_IMPORTED_MODULE_5__.__awaiter)(this, void 0, void 0, function* () {
            console.log('change status......................', statusSelected);
            this.setClickable(true);
            this.mapService.showmap = true;
            let currentStatus = this.booking.status;
            if (this.getStatusPriority(statusSelected) ==
                this.getStatusPriority(currentStatus) + 1) {
                let bookingRef = this.firebaseService.getDbRef('/bookings/' + this.bookingKey);
                bookingRef.once('value', (snapshot) => {
                    this.booking = snapshot.val();
                    let customer = this.booking.customer;
                    let driver = this.booking.driver;
                    bookingRef.child('status').set(statusSelected);
                    this.firebaseService
                        .getDbRef('users/' + customer + '/bookings/' + this.bookingKey + '/status')
                        .set(statusSelected);
                    this.firebaseService
                        .getDbRef('users/' + driver + '/bookings/' + this.bookingKey + '/status')
                        .set(statusSelected);
                    if (statusSelected == 'REVIEW') {
                        this.firebaseService
                            .getDbRef('/users/' + this.booking.driver + '/movementStatus')
                            .remove();
                        // this.firebaseService.getDbRef('/users/' + this.booking.driver + '/movementStatus').set('LOCATE_ONLY');
                        let geoRef = this.firebaseService.getDbRef('geofire');
                        geoRef.child(this.bookingKey).remove();
                        let distanceref = this.firebaseService.getDbRef('/bookings/' + this.bookingKey);
                        distanceref.once('value', (snap2) => {
                            if (snap2.val()) {
                                this.allbookingDetails = snap2.val();
                                if (snap2.val().triplocations) {
                                    let locationsArr = snap2.val().triplocations;
                                    for (let key in locationsArr)
                                        this.totalLocation.push(locationsArr[key]);
                                }
                                var _eQuatorialEarthRadius = 6378.137;
                                var _d2r = Math.PI / 180.0;
                                var distanceTotal = 0;
                                for (var i = 0; i < this.totalLocation.length - 1; i++) {
                                    var dlong = (this.totalLocation[i + 1].lng - this.totalLocation[i].lng) *
                                        _d2r;
                                    var dlat = (this.totalLocation[i + 1].lat - this.totalLocation[i].lat) *
                                        _d2r;
                                    var a = Math.pow(Math.sin(dlat / 2.0), 2.0) +
                                        Math.cos(this.totalLocation[i].lat * _d2r) *
                                            Math.cos(this.totalLocation[i + 1].lat * _d2r) *
                                            Math.pow(Math.sin(dlong / 2.0), 2.0);
                                    var c = 2.0 * Math.atan2(Math.sqrt(a), Math.sqrt(1.0 - a));
                                    var d = _eQuatorialEarthRadius * c;
                                    distanceTotal += d;
                                    this.totalKm = distanceTotal.toFixed();
                                    console.log('this.Total Distance - >' + this.totalKm);
                                }
                            }
                        });
                        let etime = this.formatLocalDate();
                        this.firebaseService
                            .getDbRef('/bookings/' + this.bookingKey + '/trip_end_time')
                            .set(etime);
                        let starttime = new Date(snapshot.val().trip_start_time);
                        let endtime = new Date(etime);
                        var difference = endtime.getTime() - starttime.getTime();
                        var resultInHour = Math.round(difference / 360000);
                        // var resultInMinute = (resultInHour * 60);
                        var resultInMinute = (Math.round(difference) / (1000 * 60)) % 60;
                        console.log('time calculation - >' + resultInMinute);
                        this.firebaseService
                            .getDbRef('/bookings/' + this.bookingKey + '/total_trip_time')
                            .set(resultInMinute);
                        this.firebaseService
                            .getDbRef('/users/' + this.booking.driver + '/last_billing_id')
                            .set(this.bookingKey);
                        let totalBill = 0;
                        if (this.booking.serviceType == 'Pickup') {
                            let totalDistance = this.totalKm;
                            let billableKms = 0;
                            if (totalDistance > parseInt(this.pickup_base_km)) {
                                billableKms = totalDistance - parseInt(this.pickup_base_km);
                                console.log('bilable km - >' + billableKms);
                                console.log('minimum rate -> ' + this.pickup_minimum);
                            }
                            // let totalBill = parseInt(this.pickup_minimum)  + (billableKms * parseInt(this.pickup_km)) + (resultInMinute * parseInt(this.pickup_minute));
                            let totalBill = parseInt(this.pickup_minimum) +
                                billableKms * parseInt(this.pickup_km) +
                                resultInMinute * this.pickup_minute;
                            totalBill = Math.round(totalBill * 100) / 100;
                            if (this.booking.hasOwnProperty('estimate')) {
                                totalBill = this.booking.estimate;
                            }
                            if (this.booking.hasOwnProperty('trip_cost')) {
                                totalBill = this.booking.trip_cost;
                            }
                            console.log('total bill - >' + totalBill);
                            //promo code calculation
                            if (this.allbookingDetails.promo_code) {
                                if (this.allbookingDetails.promo_code.promoType == 'by_ammount') {
                                    this.ammount =
                                        totalBill - this.allbookingDetails.promo_code.promoValue;
                                }
                                else if (this.allbookingDetails.promo_code.promoType == 'by_percentage') {
                                    this.ammount =
                                        totalBill -
                                            totalBill / this.allbookingDetails.promo_code.promoValue;
                                }
                                else if (this.allbookingDetails.promo_code.promoType == 'by_ride') {
                                    this.ammount = totalBill - totalBill;
                                }
                                this.firebaseService
                                    .getDbRef('/bookings/' + this.bookingKey + '/trip_cost')
                                    .set(this.ammount);
                                this.firebaseService
                                    .getDbRef('/users/' +
                                    this.booking.customer +
                                    '/bookings/' +
                                    this.bookingKey +
                                    '/trip_cost')
                                    .set(this.ammount);
                                this.firebaseService
                                    .getDbRef('/users/' +
                                    this.booking.driver +
                                    '/bookings/' +
                                    this.bookingKey +
                                    '/trip_cost')
                                    .set(this.ammount);
                                this.usersService
                                    .sendPush('Trip Ended', 'Your cost for the trip is $' + this.ammount, this.booking.customer_push_token)
                                    .subscribe((response) => console.log(response));
                            }
                            else {
                                this.firebaseService
                                    .getDbRef('/bookings/' + this.bookingKey + '/trip_cost')
                                    .set(totalBill);
                                this.firebaseService
                                    .getDbRef('/users/' +
                                    this.booking.customer +
                                    '/bookings/' +
                                    this.bookingKey +
                                    '/trip_cost')
                                    .set(totalBill);
                                this.firebaseService
                                    .getDbRef('/users/' +
                                    this.booking.driver +
                                    '/bookings/' +
                                    this.bookingKey +
                                    '/trip_cost')
                                    .set(totalBill);
                                this.usersService
                                    .sendPush('Trip Ended', 'Your cost for the trip is $' + totalBill, this.booking.customer_push_token)
                                    .subscribe((response) => console.log(response));
                            }
                            this.paymentclick = false;
                            //promo code calculation End
                            //  this.firebaseService.getDbRef('/bookings/' + this.bookingKey + '/trip_cost').set(totalBill);
                            // this.firebaseService.getDbRef('/users/' + this.booking.customer + '/bookings/' + this.bookingKey + '/trip_cost' ).set(totalBill);
                            // this.firebaseService.getDbRef('/users/' + this.booking.driver + '/bookings/' + this.bookingKey + '/trip_cost' ).set(totalBill);
                            //  this.usersService.sendPush('Trip Ended',"Your cost for the trip is " + totalBill + "$",this.booking.customer_push_token).subscribe(response=>console.log(response));
                        }
                        else {
                            if (this.booking.triptime > resultInHour) {
                                totalBill = this.booking.estimate;
                            }
                            else {
                                let totalBill = parseInt(this.personal_minimum) +
                                    resultInHour * parseInt(this.personal_hour);
                            }
                            totalBill = Math.round(totalBill * 100) / 100;
                            //promo code calculation
                            if (this.allbookingDetails.promo_code) {
                                if (this.allbookingDetails.promo_code.promoType == 'by_ammount') {
                                    this.ammount =
                                        totalBill - this.allbookingDetails.promo_code.promoValue;
                                }
                                else if (this.allbookingDetails.promo_code.promoType == 'by_percentage') {
                                    this.ammount =
                                        totalBill -
                                            totalBill / this.allbookingDetails.promo_code.promoValue;
                                }
                                else if (this.allbookingDetails.promo_code.promoType == 'by_ride') {
                                    this.ammount = totalBill - totalBill;
                                }
                                this.firebaseService
                                    .getDbRef('/bookings/' + this.bookingKey + '/trip_cost')
                                    .set(this.ammount);
                                this.firebaseService
                                    .getDbRef('/users/' +
                                    this.booking.customer +
                                    '/bookings/' +
                                    this.bookingKey +
                                    '/trip_cost')
                                    .set(this.ammount);
                                this.firebaseService
                                    .getDbRef('/users/' +
                                    this.booking.driver +
                                    '/bookings/' +
                                    this.bookingKey +
                                    '/trip_cost')
                                    .set(this.ammount);
                                this.usersService
                                    .sendPush('Trip Ended', 'Your cost for the trip is $' + this.ammount, this.booking.customer_push_token)
                                    .subscribe((response) => console.log(response));
                            }
                            else {
                                this.firebaseService
                                    .getDbRef('/bookings/' + this.bookingKey + '/trip_cost')
                                    .set(totalBill);
                                this.firebaseService
                                    .getDbRef('/users/' +
                                    this.booking.customer +
                                    '/bookings/' +
                                    this.bookingKey +
                                    '/trip_cost')
                                    .set(totalBill);
                                this.firebaseService
                                    .getDbRef('/users/' +
                                    this.booking.driver +
                                    '/bookings/' +
                                    this.bookingKey +
                                    '/trip_cost')
                                    .set(totalBill);
                                this.usersService
                                    .sendPush('Trip Ended', 'Your cost for the trip is $' + totalBill, this.booking.customer_push_token)
                                    .subscribe((response) => console.log(response));
                            }
                            this.paymentclick = false;
                            //promo code calculation End
                            //  this.firebaseService.getDbRef('/bookings/' + this.bookingKey + '/trip_cost').set(totalBill);
                            // this.firebaseService.getDbRef('/users/' + this.booking.customer + '/bookings/' + this.bookingKey + '/trip_cost' ).set(totalBill);
                            // this.firebaseService.getDbRef('/users/' + this.booking.driver + '/bookings/' + this.bookingKey + '/trip_cost' ).set(totalBill);
                            // this.usersService.sendPush('Trip Ended',"Your cost for the trip is " + totalBill + "$",this.booking.customer_push_token).subscribe(response=>console.log(response));
                        }
                        // HR comment to payment check..........
                        // this.navCtrl.setRoot(JobcompletePage,{
                        //     bookingKey:this.bookingKey
                        // });
                        //**************************************** */
                        //    this.firebaseService.getDbRef('/users/' + this.booking.driver + '/movementStatus').remove();
                        //    let geoRef = this.firebaseService.getDbRef('geofire');
                        //    geoRef.child(this.bookingKey).remove();
                        //    let etime = this.formatLocalDate();
                        //    this.firebaseService.getDbRef('/bookings/' + this.bookingKey + '/trip_end_time').set(etime);
                        //    let starttime = new Date(snapshot.val().trip_start_time);
                        //    let endtime = new Date(etime);
                        //    var difference = endtime.getTime() - starttime.getTime();
                        //    var resultInHour = Math.round(difference / 360000);
                        //    this.firebaseService.getDbRef('/bookings/' + this.bookingKey + '/total_trip_time').set(resultInHour);
                        //    this.firebaseService.getDbRef('/users/' +this.booking.driver + "/last_billing_id").set(this.bookingKey);
                        //    let returntrip = false;
                        //    if(snapshot.val().tripType == "Return"){
                        //        returntrip = true;
                        //    }
                        //    let totalBill = 0;
                        //    if(this.booking.serviceType=="Pickup"){
                        //         ////////  HAVE TO CHANGE THIS  ///////
                        //        let totalDistance = this.booking.distance/1000;
                        //        let billableKms = 0;
                        //        if(totalDistance > parseInt(this.pickup_base_km)){
                        //            billableKms = totalDistance - parseInt(this.pickup_base_km);
                        //        }
                        //        let totalBill = parseInt(this.pickup_minimum)  + (billableKms * parseInt(this.pickup_km)) + (resultInHour * parseInt(this.pickup_hour));
                        //        totalBill = Math.round(totalBill * 100) / 100;
                        //    }else{
                        //        if(this.booking.triptime>resultInHour){
                        //            totalBill = this.booking.estimate;
                        //        }else{
                        //            let totalBill = parseInt(this.personal_minimum) + (resultInHour * parseInt(this.personal_hour));
                        //        }
                        //        totalBill = Math.round(totalBill * 100) / 100;
                        //    }
                        //        this.firebaseService.getDbRef('/bookings/' + this.bookingKey + '/trip_cost').set(totalBill);
                        //        this.firebaseService.getDbRef('/users/' + this.booking.customer + '/bookings/' + this.bookingKey + '/trip_cost' ).set(totalBill);
                        //        this.firebaseService.getDbRef('/users/' + this.booking.driver + '/bookings/' + this.bookingKey + '/trip_cost' ).set(totalBill);
                        //        var payRef = this.firebaseService.getDbRef("/users/" + this.booking.customer + "/payment_methods");
                        //        payRef.once("value",snapshot=>{
                        //            if(snapshot.val()){
                        //                let cards = snapshot.val();
                        //                let cardID = "";
                        //                console.log(cards);
                        //                let GotIt =false;
                        //                for(let i=0;i<cards.length;i++){
                        //                    if(cards[i].primary){
                        //                        GotIt = true;
                        //                        cardID = cards[i].id;
                        //                    }
                        //                }
                        //                if(GotIt){
                        //                        this.usersService.chargePayment(cardID,totalBill).subscribe(response=>{
                        //                            let temp :any;
                        //                            temp = response;
                        //                            if(temp.state =="approved"){
                        //                                this.usersService.sendPush('Trip Ended - Payment Success !!',"Your cost for the trip is " + totalBill + "$",this.booking.customer_push_token).subscribe(response=>console.log(response));
                        //                            }else{
                        //                                this.usersService.sendPush('Payment Failed !!',"No Payment Method Found. Your cost for the trip is " + totalBill + "$",this.booking.customer_push_token).subscribe(response=>console.log(response));
                        //                            }
                        //                        });
                        //                }else{
                        //                    this.usersService.sendPush('Payment Failed !!',"No Payment Method Found. Your cost for the trip is " + totalBill + "$",this.booking.customer_push_token).subscribe(response=>console.log(response));
                        //                }
                        //            }else{
                        //                this.usersService.sendPush('Payment Failed !!',"Your cost for the trip is " + totalBill + "$",this.booking.customer_push_token).subscribe(response=>console.log(response));
                        //            }
                        //        });
                        //       // this.usersService.sendPush('Trip Ended',"Your cost for the trip is " + totalBill + "$",this.booking.customer_push_token).subscribe(response=>console.log(response));
                        // //  this.locationTracker.stopTracking();
                        //  // this.navCtrl.setRoot(CurrentbookingsPage);
                        //   this.navCtrl.setRoot(JobcompletePage,{
                        //       bookingKey:this.bookingKey
                        //   });
                        //**************************************** */
                        this.doPayemnet();
                    }
                    if (statusSelected == 'ARRIVING') {
                        this.firebaseService
                            .getDbRef('/users/' + this.booking.driver + '/movementStatus')
                            .set('LOCATE_ONLY');
                        this.usersService
                            .sendPush('Driver is on their way to', this.booking.pickup.add, this.pushToken)
                            .subscribe((response) => console.log(response));
                    }
                    if (statusSelected == 'ARRIVED') {
                        this.firebaseService
                            .getDbRef('/users/' + this.booking.driver + '/movementStatus')
                            .remove();
                        this.usersService
                            .sendPush('Driver Arrived', this.booking.pickup.add, this.pushToken)
                            .subscribe((response) => console.log(response));
                    }
                    if (statusSelected == 'STARTED') {
                        this.firebaseService
                            .getDbRef('/bookings/' + this.bookingKey + '/trip_start_time')
                            .set(this.formatLocalDate());
                        this.firebaseService
                            .getDbRef('/users/' + this.booking.driver + '/movementStatus')
                            .set(this.bookingKey);
                    }
                });
            }
            else if (statusSelected === 'PAYMENT') {
                console.log('payment.......');
                if (!this.paymentclick) {
                    // let bookingRef = this.firebaseService.getDbRef('/bookings/' + this.bookingKey);
                    // bookingRef.once('value', (snapshot:any) => {
                    // this.booking = snapshot.val();
                    // let customer = this.booking.customer;
                    // let driver = this.booking.driver;
                    /*  AAded By HR PATEL  */
                    var paymentbill = this.firebaseService.getDbRef('/bookings/' + this.bookingKey + '/trip_cost');
                    paymentbill.once('value', (snapbill) => {
                        let totalBill = snapbill.val();
                        console.log('MY paypal bill.........', totalBill);
                        var payRef = this.firebaseService.getDbRef('/users/' + this.booking.customer + '/payment_methods');
                        payRef.once('value', (snapshot) => {
                            if (snapshot.val()) {
                                let cards = snapshot.val();
                                let cardID = '';
                                console.log('cards.........', cards);
                                let GotIt = false;
                                for (let i = 0; i < cards.length; i++) {
                                    if (cards[i].primary) {
                                        GotIt = true;
                                        cardID = cards[i].stripeCardID;
                                    }
                                }
                                if (GotIt) {
                                    console.log(cardID, totalBill);
                                    this.usersService
                                        .stripePayment(cardID, this.stripeID, totalBill)
                                        .subscribe((response) => {
                                        console.log('stripe payment response', response);
                                        let temp;
                                        temp = response;
                                        if (temp.status == 'succeeded') {
                                            // alert("succeeded payment");
                                            // let alert = this.alertCtrl.create({
                                            //   title: 'Payment',
                                            //   subTitle: 'Payment succeeded',
                                            //   buttons: ['OK']
                                            // });
                                            // alert.present();
                                            alert('Payment succeeded');
                                            this.usersService
                                                .sendPush('Trip Ended - Payment Success !!', 'Your cost for the trip is $' + totalBill, this.booking.customer_push_token)
                                                .subscribe((response) => console.log(response));
                                            this.paymentclick = true;
                                            // this.firebaseService.getDbRef('users/' + customer + '/bookings/' + this.bookingKey + '/status' ).set('PAYMENT');
                                            // this.firebaseService.getDbRef('users/' + driver + '/bookings/' + this.bookingKey + '/status').set('PAYMENT');
                                            this.nav.setRoot('job-complete', {
                                                bookingKey: this.bookingKey,
                                            });
                                        }
                                        else {
                                            // let alert = this.alertCtrl.create({
                                            //   title: 'Payment',
                                            //   subTitle: temp.message,
                                            //   buttons: ['OK']
                                            // });
                                            // let alert = this.alertCtrl.create();
                                            // alert.present();
                                            alert(temp.message);
                                            // this.firebaseService.getDbRef('users/' + customer + '/bookings/' + this.bookingKey + '/status' ).set('REVIEW');
                                            // this.firebaseService.getDbRef('users/' + driver + '/bookings/' + this.bookingKey + '/status').set('REVIEW');
                                            this.usersService
                                                .sendPush('Payment Not Approve !!', 'No Payment Method Found. Your cost for the trip is $' +
                                                totalBill, this.booking.customer_push_token)
                                                .subscribe((response) => console.log(response));
                                        }
                                    }, (err) => {
                                        // [TODO ]
                                        // alert('Server Error ! Try again');
                                        // this.firebaseService.getDbRef('users/' + customer + '/bookings/' + this.bookingKey + '/status' ).set('PAYMENT');
                                        // this.firebaseService.getDbRef('users/' + driver + '/bookings/' + this.bookingKey + '/status').set('PAYMENT');
                                    });
                                }
                                else {
                                    alert('Customer Not Selected Primary card');
                                    // this.firebaseService.getDbRef('users/' + customer + '/bookings/' + this.bookingKey + '/status' ).set('PAYMENT');
                                    // this.firebaseService.getDbRef('users/' + driver + '/bookings/' + this.bookingKey + '/status').set('PAYMENT');
                                    this.usersService
                                        .sendPush('Payment Failed !!', 'Not Selected Primary card. Your cost for the trip is $' +
                                        totalBill, this.booking.customer_push_token)
                                        .subscribe((response) => console.log(response));
                                }
                            }
                            else {
                                alert('Customer Card Not Available');
                                this.usersService
                                    .sendPush('Payment Failed !!', ' Card Not Available. Your cost for the trip is $' +
                                    totalBill, this.booking.customer_push_token)
                                    .subscribe((response) => console.log(response));
                            }
                        });
                    });
                    // });
                    /* Ended By HR PATEL */
                }
                else {
                    let alert = yield this.alertCtrl.create();
                    this.setClickable(false);
                    this.mapService.showmap = false;
                    alert.title = 'Alert';
                    alert.message = 'This status is not correct.';
                    alert.buttons.push({
                        text: 'Dismiss',
                        handler: (data) => {
                            this.setClickable(true);
                            this.mapService.showmap = true;
                            alert.dismiss();
                        },
                    });
                    alert.present();
                }
            }
            else {
                let alert = yield this.alertCtrl.create();
                this.setClickable(false);
                this.mapService.showmap = false;
                alert.title = 'Alert';
                alert.message = 'This status is not correct.';
                alert.buttons.push({
                    text: 'Dismiss',
                    handler: (data) => {
                        this.setClickable(true);
                        this.mapService.showmap = true;
                        alert.dismiss();
                    },
                });
                alert.present();
            }
        });
    }
    getStatusPriority(status) {
        if (status == 'ASSIGNED')
            return 0;
        else if (status == 'ARRIVING')
            return 1;
        else if (status == 'ARRIVED')
            return 2;
        else if (status == 'STARTED')
            return 3;
        else if (status == 'REVIEW')
            return 4;
        else if (status == 'ENDED')
            return 5;
    }
    presentActionSheet() {
        var _a;
        return (0,tslib__WEBPACK_IMPORTED_MODULE_5__.__awaiter)(this, void 0, void 0, function* () {
            console.log(this.map);
            this.setClickable(false);
            this.mapService.showmap = false;
            let buttons = [
                {
                    text: 'Going for Customer pickup',
                    handler: () => {
                        this.changeStatus('ARRIVING');
                        console.log('Going for Customer pickup clicked');
                    },
                },
                {
                    text: 'Arrived at Pickup point',
                    //*TODO*/ disbaled:'true',
                    handler: () => {
                        this.changeStatus('ARRIVED');
                        console.log('Arrived at Pickup point clicked');
                    },
                },
                {
                    text: 'Start Trip',
                    //*TODO*/ disbaled:'true',
                    handler: () => {
                        this.changeStatus('STARTED');
                        console.log('Start Trip clicked');
                    },
                },
                {
                    text: 'End Trip',
                    //*TODO*/ disbaled:'true',
                    handler: () => {
                        this.changeStatus('REVIEW');
                        console.log('End Trip clicked');
                    },
                },
                // {
                //   text: 'Payment',
                //    disbaled:'true',
                //   handler: () => {
                //     this.changeStatus('PAYMENT');
                //     console.log('Payment Pay clicked');
                //   }
                // },
                {
                    text: 'Cancel',
                    role: 'cancel',
                    handler: () => {
                        this.setClickable(true);
                        this.mapService.showmap = true;
                        console.log('Cancel clicked');
                    },
                },
            ];
            let currentStatus = this.booking.status;
            let _buttons = [];
            let x = (_a = this.getStatusPriority(currentStatus)) !== null && _a !== void 0 ? _a : 5;
            for (var y = x; y < buttons.length; y++) {
                _buttons.push(buttons[y]);
            }
            let actionSheet = yield this.actionSheetController.create({
                header: 'Change Booking Status',
                buttons: _buttons,
            });
            console.log(actionSheet);
            actionSheet.present();
        });
    }
    //get help
    getHelp() {
        this.nav.push('pages/support', {
            bookingKey: this.bookingKey,
        });
    }
    formatLocalDate() {
        var now = new Date(), tzo = -now.getTimezoneOffset(), dif = tzo >= 0 ? '+' : '-', pad = function (num) {
            var norm = Math.abs(Math.floor(num));
            return (norm < 10 ? '0' : '') + norm;
        };
        return (now.getFullYear() +
            '-' +
            pad(now.getMonth() + 1) +
            '-' +
            pad(now.getDate()) +
            'T' +
            pad(now.getHours()) +
            ':' +
            pad(now.getMinutes()) +
            ':' +
            pad(now.getSeconds()) +
            '.000Z');
    }
    giveCall() {
        let customerRef = this.firebaseService.getDbRef('/users/' + this.customerID); //customerUserId need to create in constructor
        customerRef.once('value', (snapshot) => {
            this.callCustomer = snapshot.val().phone;
            this.callNumber
                .callNumber(this.callCustomer, true)
                .then(() => console.log('Launched dialer!'))
                .catch(() => console.log('Error launching dialer'));
        });
    }
    ngDoCheck() {
        if (this.mapReady) {
            if (this.mapService.showmap) {
                this.setClickable(true);
            }
            else {
                this.setClickable(false);
            }
        }
    }
    routePickup(pickup, drop, serviceType) {
        console.log('pickup', pickup, 'serviceType', serviceType);
        if (serviceType === 'Pickup') {
            let options = {
                start: pickup,
                // app: LaunchNavigator.APP.Waze
            };
            this.launchNavigator.navigate(drop, options).then((success) => alert('Launched navigator'), (error) => alert('Error launching navigator: ' + error));
        }
        else {
            let options = {
                start: this.start,
                app: this.launchNavigator.APP.Waze,
            };
            console.log('Launching...');
            this.launchNavigator.navigate(pickup, options).then((success) => alert('Launched navigator'), (error) => alert('Error launching navigator: ' + error));
            // this.launchNavigator.navigate('Toronto, ON', options).then(
            //   (success) => console.log('Launched navigator'),
            //   (error) => console.log('Error launching navigator', error)
            // );
        }
    }
    setClickable(clickable) {
        this.clickable = clickable;
    }
};
ActiveBookingPage.ctorParameters = () => [
    { type: _angular_core__WEBPACK_IMPORTED_MODULE_6__.Injector },
    { type: _ionic_native_call_number_ngx__WEBPACK_IMPORTED_MODULE_3__.CallNumber },
    { type: _awesome_cordova_plugins_launch_navigator_ngx__WEBPACK_IMPORTED_MODULE_4__.LaunchNavigator }
];
ActiveBookingPage.propDecorators = {
    mapElement: [{ type: _angular_core__WEBPACK_IMPORTED_MODULE_6__.ViewChild, args: ['map', { static: false },] }]
};
ActiveBookingPage = (0,tslib__WEBPACK_IMPORTED_MODULE_5__.__decorate)([
    (0,_angular_core__WEBPACK_IMPORTED_MODULE_6__.Component)({
        selector: 'app-active-booking',
        template: _raw_loader_active_booking_page_html__WEBPACK_IMPORTED_MODULE_0__.default,
        styles: [_active_booking_page_scss__WEBPACK_IMPORTED_MODULE_1__.default]
    })
], ActiveBookingPage);



/***/ }),

/***/ 98846:
/*!***************************************************************!*\
  !*** ./src/app/Pages/active-booking/active-booking.page.scss ***!
  \***************************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "default": () => (__WEBPACK_DEFAULT_EXPORT__)
/* harmony export */ });
/* harmony default export */ const __WEBPACK_DEFAULT_EXPORT__ = ("\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJhY3RpdmUtYm9va2luZy5wYWdlLnNjc3MifQ== */");

/***/ }),

/***/ 26518:
/*!*****************************************************************************************************!*\
  !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/Pages/active-booking/active-booking.page.html ***!
  \*****************************************************************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "default": () => (__WEBPACK_DEFAULT_EXPORT__)
/* harmony export */ });
/* harmony default export */ const __WEBPACK_DEFAULT_EXPORT__ = ("<ion-header>\r\n  <ion-toolbar color=\"dark\">\r\n    <ion-buttons slot=\"start\">\r\n      <ion-back-button defaultHref=\"\"></ion-back-button>\r\n    </ion-buttons>\r\n    <ion-title class=\"toptitle\"><span class=\"heavy\">BOOKING</span></ion-title>\r\n\r\n    <!-- <ion-icon\r\n      color=\"primary\"\r\n      name=\"navigate\"\r\n      class=\"toggleButton\"\r\n      (click)=\"routePickup(booking?.pickup.add,'',booking?.serviceType)\"\r\n    ></ion-icon> -->\r\n    <ion-icon slot=\"end\" name=\"locate\" style=\"--color: var(--ion-color-light); font-size: 3vh\" class=\"ion-margin-end\"\r\n      (click)=\"routePickup(booking?.pickup.add,'',booking?.serviceType)\">\r\n    </ion-icon>\r\n  </ion-toolbar>\r\n</ion-header>\r\n\r\n<ion-content>\r\n  <div id=\"map\" #map></div>\r\n\r\n  <section class=\"mapcontainer\">\r\n    <div class=\"container\">\r\n      <!-- <section class=\"topnotification-wrapper\">\r\n        <section class=\"topnotification\">\r\n          <ion-grid class=\"no-padding\">\r\n            <ion-item>\r\n              <img src=\"assets/images/marker.png\">\r\n              <ion-label>\r\n                {{booking?.pickup.add}}\r\n              </ion-label>\r\n            </ion-item>\r\n            <ion-row>\r\n              <ion-col size=\"1\" class=\"marker\">\r\n\r\n              </ion-col>\r\n              <ion-col size=\"10\" class=\"no-padding\">\r\n                <ion-item class=\"bluetext-map\">{{booking?.pickup.add}}</ion-item>\r\n              </ion-col>\r\n            </ion-row>\r\n          </ion-grid>\r\n        </section>\r\n      </section> -->\r\n\r\n      <!-- <ion-item>\r\n        <img src=\"assets/images/marker.png\">\r\n        <ion-label class=\"ion-text-wrap ion-margin-start\">\r\n          {{booking?.pickup?.add}}\r\n        </ion-label>\r\n      </ion-item>\r\n      <ion-fab right bottom class=\"ionFab\">\r\n        <button *ngIf=\"booking?.serviceType=='Pickup'\"\r\n          (click)=\"routePickup(booking?.pickup.add,booking?.drop.add,booking?.serviceType)\">\r\n          <ion-icon name=\"navigate\"></ion-icon>\r\n        </button>\r\n\r\n      </ion-fab>\r\n\r\n      <ion-item class=\"ion-padding-bottom\">\r\n        <ion-avatar>\r\n          <img src=\"{{booking?.image_url}}\" onerror=\"this.onerror=null;this.src='assets/images/notfound.png';\"\r\n            alt=\"avatar\">\r\n        </ion-avatar>\r\n\r\n        <ion-label class=\"ion-text-wrap ion-margin-start\">\r\n          {{booking?.customer_name}}\r\n        </ion-label>\r\n\r\n        <ion-label slot=\"end\">\r\n          <ion-icon name=\"checkmark-circle\"></ion-icon>\r\n          {{booking?.status=='REVIEW'?'UNDER REVIEW':booking?.status }}\r\n        </ion-label>\r\n      </ion-item>\r\n\r\n      <div class=\"input-wrapper\">\r\n        <ion-item>\r\n          <ion-button class=\"btns\">\r\n            <ion-icon name=\"call-outline\"></ion-icon>\r\n            <h4> Contact</h4>\r\n          </ion-button>\r\n\r\n          <ion-button class=\"ion-margin-start btns\">\r\n            <ion-icon name=\"information-outline\"></ion-icon>\r\n            <h4> Help</h4>\r\n          </ion-button>\r\n        </ion-item>\r\n      </div>\r\n\r\n      <ion-button class=\"upt-btn\" (click)=\"presentActionSheet()\">\r\n        Update Status\r\n      </ion-button> -->\r\n\r\n      <!-- <section class=\"driver-snippet\">\r\n\r\n        <div class=\"wrapper-driversnippet\">\r\n          <div class=\"help-box\">\r\n            <img src=\"assets/images/phone.png\" (click)=\"giveCall()\" alt=\"phone\">\r\n            <ion-label>Contact</ion-label>\r\n          </div>\r\n\r\n          <div class=\"help-box\">\r\n            <img src=\"assets/images/info.png\" alt=\"help\">\r\n            <ion-label>Help</ion-label>\r\n          </div>\r\n          <ion-grid class=\"no-padding-top\">\r\n            <ion-row class=\"customer-icons\">\r\n              <ion-col class=\"centertext no-padding-top\">\r\n                <img src=\"assets/images/phone.png\" (click)=\"giveCall()\" alt=\"phone\">\r\n                Contact\r\n              </ion-col>\r\n              <ion-col (click)=\"getHelp()\" class=\"centertext no-padding-top\">\r\n                <img src=\"assets/images/info.png\" alt=\"help\">\r\n                Help\r\n              </ion-col>\r\n              <ion-col (click)=\"presentActionSheet()\" class=\"centertext no-padding-top\">\r\n                <img src=\"assets/images/cancel.png\" alt=\"cancel\">\r\n\r\n              </ion-col>\r\n              <ion-col class=\"centertext\">\r\n                <img src=\"assets/images/change.png\" alt=\"change\">\r\n                Change\r\n              </ion-col>\r\n            </ion-row>\r\n          </ion-grid>\r\n        </div>\r\n\r\n\r\n      </section> -->\r\n    </div>\r\n  </section>\r\n\r\n  <ion-card class=\"ion-no-padding\">\r\n    <ion-card-content class=\"ion-no-margin ion-no-padding\">\r\n      <ion-item color=\"none\" class=\"ion-no-margin ion-no-padding\">\r\n        <img class=\"ion-margin\" src=\"assets/images/marker.png\" />\r\n        <ion-label style=\"font-size: 14px\" class=\"ion-text-wrap ion-margin-start\">\r\n          {{booking?.pickup?.add}}\r\n        </ion-label>\r\n      </ion-item>\r\n      <ion-item lines=\"none\" color=\"none\">\r\n        <ion-avatar>\r\n          <img src=\"{{booking ? booking.image_url : 'assets/images/notfound.png'}}\"\r\n            onerror=\"this.onerror=null;this.src='assets/images/notfound.png';\" alt=\"avatar\" />\r\n        </ion-avatar>\r\n\r\n        <ion-label class=\"ion-text-wrap ion-margin-start\">\r\n          {{booking?.customer_name}}\r\n\r\n          <ion-label style=\"font-size: 70%\">\r\n            <ion-icon name=\"checkmark-circle\"></ion-icon>\r\n            {{booking?.status=='REVIEW'?'UNDER REVIEW':booking?.status }}\r\n          </ion-label>\r\n        </ion-label>\r\n        <!-- <ion-icon slot=\"end\" style=\"font-size: 25px;\" name=\"information-outline\" class=\"ion-margin\"></ion-icon> -->\r\n        <ion-icon slot=\"end\" style=\"font-size: 25px\" name=\"call-outline\" class=\"ion-margin\" (click)=\"giveCall()\">\r\n        </ion-icon>\r\n      </ion-item>\r\n      <!-- <ion-item lines=\"none\" color=\"none\">\r\n\r\n      </ion-item> -->\r\n      <ion-button size=\"small\" class=\"upt-btn ion-float-right\" (click)=\"presentActionSheet()\">\r\n        Update Status\r\n      </ion-button>\r\n      <ion-button size=\"small\" class=\"upt-btn ion-float-right\" (click)=\"getHelp()\">\r\n        Help\r\n      </ion-button>\r\n    </ion-card-content>\r\n  </ion-card>\r\n</ion-content>");

/***/ })

}]);
//# sourceMappingURL=src_app_Pages_active-booking_active-booking_module_ts.js.map