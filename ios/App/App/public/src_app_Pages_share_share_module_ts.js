(self["webpackChunkDriverApp"] = self["webpackChunkDriverApp"] || []).push([["src_app_Pages_share_share_module_ts"],{

/***/ 24276:
/*!*****************************************************************************!*\
  !*** ./node_modules/@ionic-native/social-sharing/__ivy_ngcc__/ngx/index.js ***!
  \*****************************************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "SocialSharing": () => (/* binding */ SocialSharing)
/* harmony export */ });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! tslib */ 64762);
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/core */ 37716);
/* harmony import */ var _ionic_native_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @ionic-native/core */ 60399);




var SocialSharing = /** @class */ (function (_super) {
    (0,tslib__WEBPACK_IMPORTED_MODULE_1__.__extends)(SocialSharing, _super);
    function SocialSharing() {
        return _super !== null && _super.apply(this, arguments) || this;
    }
    SocialSharing.prototype.share = function (message, subject, file, url) { return (0,_ionic_native_core__WEBPACK_IMPORTED_MODULE_0__.cordova)(this, "share", { "successIndex": 4, "errorIndex": 5 }, arguments); };
    SocialSharing.prototype.shareWithOptions = function (options) { return (0,_ionic_native_core__WEBPACK_IMPORTED_MODULE_0__.cordova)(this, "shareWithOptions", { "platforms": ["iOS", "Android"] }, arguments); };
    SocialSharing.prototype.canShareVia = function (appName, message, subject, image, url) { return (0,_ionic_native_core__WEBPACK_IMPORTED_MODULE_0__.cordova)(this, "canShareVia", { "successIndex": 5, "errorIndex": 6, "platforms": ["iOS", "Android"] }, arguments); };
    SocialSharing.prototype.shareViaTwitter = function (message, image, url) { return (0,_ionic_native_core__WEBPACK_IMPORTED_MODULE_0__.cordova)(this, "shareViaTwitter", { "successIndex": 3, "errorIndex": 4, "platforms": ["iOS", "Android"] }, arguments); };
    SocialSharing.prototype.shareViaFacebook = function (message, image, url) { return (0,_ionic_native_core__WEBPACK_IMPORTED_MODULE_0__.cordova)(this, "shareViaFacebook", { "successIndex": 3, "errorIndex": 4, "platforms": ["iOS", "Android"] }, arguments); };
    SocialSharing.prototype.shareViaFacebookWithPasteMessageHint = function (message, image, url, pasteMessageHint) { return (0,_ionic_native_core__WEBPACK_IMPORTED_MODULE_0__.cordova)(this, "shareViaFacebookWithPasteMessageHint", { "successIndex": 4, "errorIndex": 5, "platforms": ["iOS", "Android"] }, arguments); };
    SocialSharing.prototype.shareViaInstagram = function (message, image) { return (0,_ionic_native_core__WEBPACK_IMPORTED_MODULE_0__.cordova)(this, "shareViaInstagram", { "platforms": ["iOS", "Android"] }, arguments); };
    SocialSharing.prototype.shareViaWhatsApp = function (message, image, url) { return (0,_ionic_native_core__WEBPACK_IMPORTED_MODULE_0__.cordova)(this, "shareViaWhatsApp", { "successIndex": 3, "errorIndex": 4, "platforms": ["iOS", "Android"] }, arguments); };
    SocialSharing.prototype.shareViaWhatsAppToReceiver = function (receiver, message, image, url) { return (0,_ionic_native_core__WEBPACK_IMPORTED_MODULE_0__.cordova)(this, "shareViaWhatsAppToReceiver", { "successIndex": 4, "errorIndex": 5, "platforms": ["iOS", "Android"] }, arguments); };
    SocialSharing.prototype.shareViaSMS = function (messge, phoneNumber) { return (0,_ionic_native_core__WEBPACK_IMPORTED_MODULE_0__.cordova)(this, "shareViaSMS", { "platforms": ["iOS", "Android"] }, arguments); };
    SocialSharing.prototype.canShareViaEmail = function () { return (0,_ionic_native_core__WEBPACK_IMPORTED_MODULE_0__.cordova)(this, "canShareViaEmail", { "platforms": ["iOS", "Android"] }, arguments); };
    SocialSharing.prototype.shareViaEmail = function (message, subject, to, cc, bcc, files) { return (0,_ionic_native_core__WEBPACK_IMPORTED_MODULE_0__.cordova)(this, "shareViaEmail", { "platforms": ["iOS", "Android"], "successIndex": 6, "errorIndex": 7 }, arguments); };
    SocialSharing.prototype.shareVia = function (appName, message, subject, image, url) { return (0,_ionic_native_core__WEBPACK_IMPORTED_MODULE_0__.cordova)(this, "shareVia", { "successIndex": 5, "errorIndex": 6, "platforms": ["iOS", "Android"] }, arguments); };
    SocialSharing.prototype.setIPadPopupCoordinates = function (targetBounds) { return (0,_ionic_native_core__WEBPACK_IMPORTED_MODULE_0__.cordova)(this, "setIPadPopupCoordinates", { "sync": true, "platforms": ["iOS"] }, arguments); };
    SocialSharing.prototype.saveToPhotoAlbum = function (fileOrFileArray) { return (0,_ionic_native_core__WEBPACK_IMPORTED_MODULE_0__.cordova)(this, "saveToPhotoAlbum", { "platforms": ["iOS"] }, arguments); };
    SocialSharing.prototype.shareViaWhatsAppToPhone = function (phone, message, fileOrFileArray, url) { return (0,_ionic_native_core__WEBPACK_IMPORTED_MODULE_0__.cordova)(this, "shareViaWhatsAppToPhone", { "successIndex": 5, "errorIndex": 6, "platforms": ["iOS", "Android"] }, arguments); };
    SocialSharing.pluginName = "SocialSharing";
    SocialSharing.plugin = "cordova-plugin-x-socialsharing";
    SocialSharing.pluginRef = "plugins.socialsharing";
    SocialSharing.repo = "https://github.com/EddyVerbruggen/SocialSharing-PhoneGap-Plugin";
    SocialSharing.platforms = ["Android", "Browser", "iOS", "Windows", "Windows Phone"];
SocialSharing.ɵfac = /*@__PURE__*/ function () { var ɵSocialSharing_BaseFactory; return function SocialSharing_Factory(t) { return (ɵSocialSharing_BaseFactory || (ɵSocialSharing_BaseFactory = _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵgetInheritedFactory"](SocialSharing)))(t || SocialSharing); }; }();
SocialSharing.ɵprov = /*@__PURE__*/ _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵdefineInjectable"]({ token: SocialSharing, factory: function (t) { return SocialSharing.ɵfac(t); } });
(function () { (typeof ngDevMode === "undefined" || ngDevMode) && _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵsetClassMetadata"](SocialSharing, [{
        type: _angular_core__WEBPACK_IMPORTED_MODULE_2__.Injectable
    }], null, null); })();
    return SocialSharing;
}(_ionic_native_core__WEBPACK_IMPORTED_MODULE_0__.IonicNativePlugin));


//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiaW5kZXguanMiLCJzb3VyY2VzIjpbIi4uLy4uLy4uLy4uLy4uL3NyYy9AaW9uaWMtbmF0aXZlL3BsdWdpbnMvc29jaWFsLXNoYXJpbmcvbmd4L2luZGV4LnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiI7QUFBQSxPQUFPLEVBQUUsVUFBVSxFQUFFLE1BQU0sZUFBZSxDQUFDO0FBQzNDLE9BQU8sOEJBQXNDLE1BQU0sb0JBQW9CLENBQUM7O0FBQ3hFO0FBRzBCLElBcUNTLGlDQUFpQjtBQUFDO0FBRTlCO0FBQ3VDO0FBQzVELElBU0EsNkJBQUssYUFBQyxPQUFnQixFQUFFLE9BQWdCLEVBQUUsSUFBd0IsRUFBRSxHQUFZO0FBS2xDLElBTzlDLHdDQUFnQixhQUFDLE9BTWhCO0FBTUksSUFZTCxtQ0FBVyxhQUFDLE9BQWUsRUFBRSxPQUFnQixFQUFFLE9BQWdCLEVBQUUsS0FBYyxFQUFFLEdBQVk7QUFPckUsSUFTeEIsdUNBQWUsYUFBQyxPQUFlLEVBQUUsS0FBYyxFQUFFLEdBQVk7QUFRN0QsSUFRQSx3Q0FBZ0IsYUFBQyxPQUFlLEVBQUUsS0FBYyxFQUFFLEdBQVk7QUFPNUQsSUFVRiw0REFBb0MsYUFDbEMsT0FBZSxFQUNmLEtBQWMsRUFDZCxHQUFZLEVBQ1osZ0JBQXlCO0FBU1YsSUFLakIseUNBQWlCLGFBQUMsT0FBZSxFQUFFLEtBQWE7QUFNMUIsSUFVdEIsd0NBQWdCLGFBQUMsT0FBZSxFQUFFLEtBQWMsRUFBRSxHQUFZO0FBTWYsSUFXL0Msa0RBQTBCLGFBQUMsUUFBZ0IsRUFBRSxPQUFlLEVBQUUsS0FBYyxFQUFFLEdBQVk7QUFPckQsSUFNckMsbUNBQVcsYUFBQyxNQUFjLEVBQUUsV0FBbUI7QUFNckMsSUFLVix3Q0FBZ0I7QUFPQSxJQVloQixxQ0FBYSxhQUNYLE9BQWUsRUFDZixPQUFlLEVBQ2YsRUFBWSxFQUNaLEVBQWEsRUFDYixHQUFjLEVBQ2QsS0FBeUI7QUFPa0MsSUFZN0QsZ0NBQVEsYUFBQyxPQUFlLEVBQUUsT0FBZSxFQUFFLE9BQWdCLEVBQUUsS0FBYyxFQUFFLEdBQVk7QUFNckUsSUFNcEIsK0NBQXVCLGFBQUMsWUFBb0I7QUFJTCxJQU12Qyx3Q0FBZ0IsYUFBQyxlQUFrQztBQUtSLElBWTNDLCtDQUF1QixhQUNyQixLQUFhLEVBQ2IsT0FBZSxFQUNmLGVBQWtDLEVBQ2xDLEdBQVk7QUFLcUY7QUFBZ0Q7QUFBNkQ7QUFBdUQ7QUFBNEY7aURBMVFwVyxVQUFVOzs7OzBCQUNMO0FBQUMsd0JBMUNQO0FBQUUsRUEwQ2lDLGlCQUFpQjtBQUNuRCxTQURZLGFBQWE7QUFBSSIsInNvdXJjZXNDb250ZW50IjpbImltcG9ydCB7IEluamVjdGFibGUgfSBmcm9tICdAYW5ndWxhci9jb3JlJztcbmltcG9ydCB7IENvcmRvdmEsIElvbmljTmF0aXZlUGx1Z2luLCBQbHVnaW4gfSBmcm9tICdAaW9uaWMtbmF0aXZlL2NvcmUnO1xuXG4vKipcbiAqIEBuYW1lIFNvY2lhbCBTaGFyaW5nXG4gKiBAcHJlbWllciBzb2NpYWwtc2hhcmluZ1xuICogQGRlc2NyaXB0aW9uXG4gKiBTaGFyZSB0ZXh0LCBmaWxlcywgaW1hZ2VzLCBhbmQgbGlua3MgdmlhIHNvY2lhbCBuZXR3b3Jrcywgc21zLCBhbmQgZW1haWwuXG4gKlxuICogRm9yIEJyb3dzZXIgdXNhZ2UgY2hlY2sgb3V0IHRoZSBXZWIgU2hhcmUgQVBJIGRvY3M6IGh0dHBzOi8vZ2l0aHViLmNvbS9FZGR5VmVyYnJ1Z2dlbi9Tb2NpYWxTaGFyaW5nLVBob25lR2FwLVBsdWdpbiM1LXdlYi1zaGFyZS1hcGlcbiAqXG4gKiBAdXNhZ2VcbiAqIGBgYHR5cGVzY3JpcHRcbiAqIGltcG9ydCB7IFNvY2lhbFNoYXJpbmcgfSBmcm9tICdAaW9uaWMtbmF0aXZlL3NvY2lhbC1zaGFyaW5nL25neCc7XG4gKlxuICogY29uc3RydWN0b3IocHJpdmF0ZSBzb2NpYWxTaGFyaW5nOiBTb2NpYWxTaGFyaW5nKSB7IH1cbiAqXG4gKiAuLi5cbiAqXG4gKiAvLyBDaGVjayBpZiBzaGFyaW5nIHZpYSBlbWFpbCBpcyBzdXBwb3J0ZWRcbiAqIHRoaXMuc29jaWFsU2hhcmluZy5jYW5TaGFyZVZpYUVtYWlsKCkudGhlbigoKSA9PiB7XG4gKiAgIC8vIFNoYXJpbmcgdmlhIGVtYWlsIGlzIHBvc3NpYmxlXG4gKiB9KS5jYXRjaCgoKSA9PiB7XG4gKiAgIC8vIFNoYXJpbmcgdmlhIGVtYWlsIGlzIG5vdCBwb3NzaWJsZVxuICogfSk7XG4gKlxuICogLy8gU2hhcmUgdmlhIGVtYWlsXG4gKiB0aGlzLnNvY2lhbFNoYXJpbmcuc2hhcmVWaWFFbWFpbCgnQm9keScsICdTdWJqZWN0JywgWydyZWNpcGllbnRAZXhhbXBsZS5vcmcnXSkudGhlbigoKSA9PiB7XG4gKiAgIC8vIFN1Y2Nlc3MhXG4gKiB9KS5jYXRjaCgoKSA9PiB7XG4gKiAgIC8vIEVycm9yIVxuICogfSk7XG4gKiBgYGBcbiAqL1xuQFBsdWdpbih7XG4gIHBsdWdpbk5hbWU6ICdTb2NpYWxTaGFyaW5nJyxcbiAgcGx1Z2luOiAnY29yZG92YS1wbHVnaW4teC1zb2NpYWxzaGFyaW5nJyxcbiAgcGx1Z2luUmVmOiAncGx1Z2lucy5zb2NpYWxzaGFyaW5nJyxcbiAgcmVwbzogJ2h0dHBzOi8vZ2l0aHViLmNvbS9FZGR5VmVyYnJ1Z2dlbi9Tb2NpYWxTaGFyaW5nLVBob25lR2FwLVBsdWdpbicsXG4gIHBsYXRmb3JtczogWydBbmRyb2lkJywgJ0Jyb3dzZXInLCAnaU9TJywgJ1dpbmRvd3MnLCAnV2luZG93cyBQaG9uZSddLFxufSlcbkBJbmplY3RhYmxlKClcbmV4cG9ydCBjbGFzcyBTb2NpYWxTaGFyaW5nIGV4dGVuZHMgSW9uaWNOYXRpdmVQbHVnaW4ge1xuICAvKipcbiAgICogU2hhcmVzIHVzaW5nIHRoZSBzaGFyZSBzaGVldFxuICAgKiBAcGFyYW0gbWVzc2FnZSB7c3RyaW5nfSBUaGUgbWVzc2FnZSB5b3Ugd291bGQgbGlrZSB0byBzaGFyZS5cbiAgICogQHBhcmFtIHN1YmplY3Qge3N0cmluZ30gVGhlIHN1YmplY3RcbiAgICogQHBhcmFtIGZpbGUge3N0cmluZ3xzdHJpbmdbXX0gVVJMKHMpIHRvIGZpbGUocykgb3IgaW1hZ2UocyksIGxvY2FsIHBhdGgocykgdG8gZmlsZShzKSBvciBpbWFnZShzKSwgb3IgYmFzZTY0IGRhdGEgb2YgYW4gaW1hZ2UuIE9ubHkgdGhlIGZpcnN0IGZpbGUvaW1hZ2Ugd2lsbCBiZSB1c2VkIG9uIFdpbmRvd3MgUGhvbmUuXG4gICAqIEBwYXJhbSB1cmwge3N0cmluZ30gQSBVUkwgdG8gc2hhcmVcbiAgICogQHJldHVybnMge1Byb21pc2U8YW55Pn1cbiAgICovXG4gIEBDb3Jkb3ZhKHtcbiAgICBzdWNjZXNzSW5kZXg6IDQsXG4gICAgZXJyb3JJbmRleDogNSxcbiAgfSlcbiAgc2hhcmUobWVzc2FnZT86IHN0cmluZywgc3ViamVjdD86IHN0cmluZywgZmlsZT86IHN0cmluZyB8IHN0cmluZ1tdLCB1cmw/OiBzdHJpbmcpOiBQcm9taXNlPGFueT4ge1xuICAgIHJldHVybjtcbiAgfVxuXG4gIC8qKlxuICAgKiBTaGFyZXMgdXNpbmcgdGhlIHNoYXJlIHNoZWV0IHdpdGggYWRkaXRpb25hbCBvcHRpb25zIGFuZCByZXR1cm5zIGEgcmVzdWx0IG9iamVjdCBvciBhbiBlcnJvciBtZXNzYWdlIChyZXF1aXJlcyBwbHVnaW4gdmVyc2lvbiA1LjEuMCspXG4gICAqIEBwYXJhbSBvcHRpb25zIHtvYmplY3R9IFRoZSBvcHRpb25zIG9iamVjdCB3aXRoIHRoZSBtZXNzYWdlLCBzdWJqZWN0LCBmaWxlcywgdXJsIGFuZCBjaG9vc2VyVGl0bGUgcHJvcGVydGllcy5cbiAgICogQHJldHVybnMge1Byb21pc2U8YW55Pn1cbiAgICovXG4gIEBDb3Jkb3ZhKHtcbiAgICBwbGF0Zm9ybXM6IFsnaU9TJywgJ0FuZHJvaWQnXSxcbiAgfSlcbiAgc2hhcmVXaXRoT3B0aW9ucyhvcHRpb25zOiB7XG4gICAgbWVzc2FnZT86IHN0cmluZztcbiAgICBzdWJqZWN0Pzogc3RyaW5nO1xuICAgIGZpbGVzPzogc3RyaW5nIHwgc3RyaW5nW107XG4gICAgdXJsPzogc3RyaW5nO1xuICAgIGNob29zZXJUaXRsZT86IHN0cmluZztcbiAgfSk6IFByb21pc2U8YW55PiB7XG4gICAgcmV0dXJuO1xuICB9XG5cbiAgLyoqXG4gICAqIENoZWNrcyBpZiB5b3UgY2FuIHNoYXJlIHZpYSBhIHNwZWNpZmljIGFwcC5cbiAgICogQHBhcmFtIGFwcE5hbWUge3N0cmluZ30gQXBwIG5hbWUgb3IgcGFja2FnZSBuYW1lLiBFeGFtcGxlczogaW5zdGFncmFtIG9yIGNvbS5hcHBsZS5zb2NpYWwuZmFjZWJvb2tcbiAgICogQHBhcmFtIG1lc3NhZ2Uge3N0cmluZ31cbiAgICogQHBhcmFtIHN1YmplY3Qge3N0cmluZ31cbiAgICogQHBhcmFtIGltYWdlIHtzdHJpbmd9XG4gICAqIEBwYXJhbSB1cmwge3N0cmluZ31cbiAgICogQHJldHVybnMge1Byb21pc2U8YW55Pn1cbiAgICovXG4gIEBDb3Jkb3ZhKHtcbiAgICBzdWNjZXNzSW5kZXg6IDUsXG4gICAgZXJyb3JJbmRleDogNixcbiAgICBwbGF0Zm9ybXM6IFsnaU9TJywgJ0FuZHJvaWQnXSxcbiAgfSlcbiAgY2FuU2hhcmVWaWEoYXBwTmFtZTogc3RyaW5nLCBtZXNzYWdlPzogc3RyaW5nLCBzdWJqZWN0Pzogc3RyaW5nLCBpbWFnZT86IHN0cmluZywgdXJsPzogc3RyaW5nKTogUHJvbWlzZTxhbnk+IHtcbiAgICByZXR1cm47XG4gIH1cblxuICAvKipcbiAgICogU2hhcmVzIGRpcmVjdGx5IHRvIFR3aXR0ZXJcbiAgICogQHBhcmFtIG1lc3NhZ2Uge3N0cmluZ31cbiAgICogQHBhcmFtIGltYWdlIHtzdHJpbmd9XG4gICAqIEBwYXJhbSB1cmwge3N0cmluZ31cbiAgICogQHJldHVybnMge1Byb21pc2U8YW55Pn1cbiAgICovXG4gIEBDb3Jkb3ZhKHtcbiAgICBzdWNjZXNzSW5kZXg6IDMsXG4gICAgZXJyb3JJbmRleDogNCxcbiAgICBwbGF0Zm9ybXM6IFsnaU9TJywgJ0FuZHJvaWQnXSxcbiAgfSlcbiAgc2hhcmVWaWFUd2l0dGVyKG1lc3NhZ2U6IHN0cmluZywgaW1hZ2U/OiBzdHJpbmcsIHVybD86IHN0cmluZyk6IFByb21pc2U8YW55PiB7XG4gICAgcmV0dXJuO1xuICB9XG5cbiAgLyoqXG4gICAqIFNoYXJlcyBkaXJlY3RseSB0byBGYWNlYm9va1xuICAgKiBAcGFyYW0gbWVzc2FnZSB7c3RyaW5nfVxuICAgKiBAcGFyYW0gaW1hZ2Uge3N0cmluZ31cbiAgICogQHBhcmFtIHVybCB7c3RyaW5nfVxuICAgKiBAcmV0dXJucyB7UHJvbWlzZTxhbnk+fVxuICAgKi9cbiAgQENvcmRvdmEoe1xuICAgIHN1Y2Nlc3NJbmRleDogMyxcbiAgICBlcnJvckluZGV4OiA0LFxuICAgIHBsYXRmb3JtczogWydpT1MnLCAnQW5kcm9pZCddLFxuICB9KVxuICBzaGFyZVZpYUZhY2Vib29rKG1lc3NhZ2U6IHN0cmluZywgaW1hZ2U/OiBzdHJpbmcsIHVybD86IHN0cmluZyk6IFByb21pc2U8YW55PiB7XG4gICAgcmV0dXJuO1xuICB9XG5cbiAgLyoqXG4gICAqIFNoYXJlcyBkaXJlY3RseSB0byBGYWNlYm9vayB3aXRoIGEgcGFzdGUgbWVzc2FnZSBoaW50XG4gICAqIEBwYXJhbSBtZXNzYWdlIHtzdHJpbmd9XG4gICAqIEBwYXJhbSBpbWFnZSB7c3RyaW5nfVxuICAgKiBAcGFyYW0gdXJsIHtzdHJpbmd9XG4gICAqIEBwYXJhbSBwYXN0ZU1lc3NhZ2VIaW50IHtzdHJpbmd9XG4gICAqIEByZXR1cm5zIHtQcm9taXNlPGFueT59XG4gICAqL1xuICBAQ29yZG92YSh7XG4gICAgc3VjY2Vzc0luZGV4OiA0LFxuICAgIGVycm9ySW5kZXg6IDUsXG4gICAgcGxhdGZvcm1zOiBbJ2lPUycsICdBbmRyb2lkJ10sXG4gIH0pXG4gIHNoYXJlVmlhRmFjZWJvb2tXaXRoUGFzdGVNZXNzYWdlSGludChcbiAgICBtZXNzYWdlOiBzdHJpbmcsXG4gICAgaW1hZ2U/OiBzdHJpbmcsXG4gICAgdXJsPzogc3RyaW5nLFxuICAgIHBhc3RlTWVzc2FnZUhpbnQ/OiBzdHJpbmdcbiAgKTogUHJvbWlzZTxhbnk+IHtcbiAgICByZXR1cm47XG4gIH1cblxuICAvKipcbiAgICogU2hhcmVzIGRpcmVjdGx5IHRvIEluc3RhZ3JhbVxuICAgKiBAcGFyYW0gbWVzc2FnZSB7c3RyaW5nfVxuICAgKiBAcGFyYW0gaW1hZ2Uge3N0cmluZ31cbiAgICogQHJldHVybnMge1Byb21pc2U8YW55Pn1cbiAgICovXG4gIEBDb3Jkb3ZhKHtcbiAgICBwbGF0Zm9ybXM6IFsnaU9TJywgJ0FuZHJvaWQnXSxcbiAgfSlcbiAgc2hhcmVWaWFJbnN0YWdyYW0obWVzc2FnZTogc3RyaW5nLCBpbWFnZTogc3RyaW5nKTogUHJvbWlzZTxhbnk+IHtcbiAgICByZXR1cm47XG4gIH1cblxuICAvKipcbiAgICogU2hhcmVzIGRpcmVjdGx5IHRvIFdoYXRzQXBwXG4gICAqIEBwYXJhbSBtZXNzYWdlIHtzdHJpbmd9XG4gICAqIEBwYXJhbSBpbWFnZSB7c3RyaW5nfVxuICAgKiBAcGFyYW0gdXJsIHtzdHJpbmd9XG4gICAqIEByZXR1cm5zIHtQcm9taXNlPGFueT59XG4gICAqL1xuICBAQ29yZG92YSh7XG4gICAgc3VjY2Vzc0luZGV4OiAzLFxuICAgIGVycm9ySW5kZXg6IDQsXG4gICAgcGxhdGZvcm1zOiBbJ2lPUycsICdBbmRyb2lkJ10sXG4gIH0pXG4gIHNoYXJlVmlhV2hhdHNBcHAobWVzc2FnZTogc3RyaW5nLCBpbWFnZT86IHN0cmluZywgdXJsPzogc3RyaW5nKTogUHJvbWlzZTxhbnk+IHtcbiAgICByZXR1cm47XG4gIH1cblxuICAvKipcbiAgICogU2hhcmVzIGRpcmVjdGx5IHRvIGEgV2hhdHNBcHAgQ29udGFjdFxuICAgKiBAcGFyYW0gcmVjZWl2ZXIge3N0cmluZ30gUGFzcyBwaG9uZSBudW1iZXIgb24gQW5kcm9pZCwgYW5kIEFkZHJlc3Nib29rIElEIChhYmlkKSBvbiBpT1NcbiAgICogQHBhcmFtIG1lc3NhZ2Uge3N0cmluZ30gTWVzc2FnZSB0byBzZW5kXG4gICAqIEBwYXJhbSBpbWFnZSB7c3RyaW5nfSBJbWFnZSB0byBzZW5kIChkb2VzIG5vdCB3b3JrIG9uIGlPU1xuICAgKiBAcGFyYW0gdXJsIHtzdHJpbmd9IExpbmsgdG8gc2VuZFxuICAgKiBAcmV0dXJucyB7UHJvbWlzZTxhbnk+fVxuICAgKi9cbiAgQENvcmRvdmEoe1xuICAgIHN1Y2Nlc3NJbmRleDogNCxcbiAgICBlcnJvckluZGV4OiA1LFxuICAgIHBsYXRmb3JtczogWydpT1MnLCAnQW5kcm9pZCddLFxuICB9KVxuICBzaGFyZVZpYVdoYXRzQXBwVG9SZWNlaXZlcihyZWNlaXZlcjogc3RyaW5nLCBtZXNzYWdlOiBzdHJpbmcsIGltYWdlPzogc3RyaW5nLCB1cmw/OiBzdHJpbmcpOiBQcm9taXNlPGFueT4ge1xuICAgIHJldHVybjtcbiAgfVxuXG4gIC8qKlxuICAgKiBTaGFyZSB2aWEgU01TXG4gICAqIEBwYXJhbSBtZXNzZ2Uge3N0cmluZ30gbWVzc2FnZSB0byBzZW5kXG4gICAqIEBwYXJhbSBwaG9uZU51bWJlciB7c3RyaW5nfSBOdW1iZXIgb3IgbXVsdGlwbGUgbnVtYmVycyBzZXBlcmF0ZWQgYnkgY29tbWFzXG4gICAqIEByZXR1cm5zIHtQcm9taXNlPGFueT59XG4gICAqL1xuICBAQ29yZG92YSh7XG4gICAgcGxhdGZvcm1zOiBbJ2lPUycsICdBbmRyb2lkJ10sXG4gIH0pXG4gIHNoYXJlVmlhU01TKG1lc3NnZTogc3RyaW5nLCBwaG9uZU51bWJlcjogc3RyaW5nKTogUHJvbWlzZTxhbnk+IHtcbiAgICByZXR1cm47XG4gIH1cblxuICAvKipcbiAgICogQ2hlY2tzIGlmIHlvdSBjYW4gc2hhcmUgdmlhIGVtYWlsXG4gICAqIEByZXR1cm5zIHtQcm9taXNlPGFueT59XG4gICAqL1xuICBAQ29yZG92YSh7XG4gICAgcGxhdGZvcm1zOiBbJ2lPUycsICdBbmRyb2lkJ10sXG4gIH0pXG4gIGNhblNoYXJlVmlhRW1haWwoKTogUHJvbWlzZTxhbnk+IHtcbiAgICByZXR1cm47XG4gIH1cblxuICAvKipcbiAgICogU2hhcmUgdmlhIEVtYWlsXG4gICAqIEBwYXJhbSBtZXNzYWdlIHtzdHJpbmd9XG4gICAqIEBwYXJhbSBzdWJqZWN0IHtzdHJpbmd9XG4gICAqIEBwYXJhbSB0byB7c3RyaW5nW119XG4gICAqIEBwYXJhbSBjYyB7c3RyaW5nW119IE9wdGlvbmFsXG4gICAqIEBwYXJhbSBiY2Mge3N0cmluZ1tdfSBPcHRpb25hbFxuICAgKiBAcGFyYW0gZmlsZXMge3N0cmluZ3xzdHJpbmdbXX0gT3B0aW9uYWwgVVJMIG9yIGxvY2FsIHBhdGggdG8gZmlsZShzKSB0byBhdHRhY2hcbiAgICogQHJldHVybnMge1Byb21pc2U8YW55Pn1cbiAgICovXG4gIEBDb3Jkb3ZhKHtcbiAgICBwbGF0Zm9ybXM6IFsnaU9TJywgJ0FuZHJvaWQnXSxcbiAgICBzdWNjZXNzSW5kZXg6IDYsXG4gICAgZXJyb3JJbmRleDogNyxcbiAgfSlcbiAgc2hhcmVWaWFFbWFpbChcbiAgICBtZXNzYWdlOiBzdHJpbmcsXG4gICAgc3ViamVjdDogc3RyaW5nLFxuICAgIHRvOiBzdHJpbmdbXSxcbiAgICBjYz86IHN0cmluZ1tdLFxuICAgIGJjYz86IHN0cmluZ1tdLFxuICAgIGZpbGVzPzogc3RyaW5nIHwgc3RyaW5nW11cbiAgKTogUHJvbWlzZTxhbnk+IHtcbiAgICByZXR1cm47XG4gIH1cblxuICAvKipcbiAgICogU2hhcmUgdmlhIEFwcE5hbWVcbiAgICogQHBhcmFtIGFwcE5hbWUge3N0cmluZ30gQXBwIG5hbWUgb3IgcGFja2FnZSBuYW1lLiBFeGFtcGxlczogaW5zdGFncmFtIG9yIGNvbS5hcHBsZS5zb2NpYWwuZmFjZWJvb2tcbiAgICogQHBhcmFtIG1lc3NhZ2Uge3N0cmluZ31cbiAgICogQHBhcmFtIHN1YmplY3Qge3N0cmluZ31cbiAgICogQHBhcmFtIGltYWdlIHtzdHJpbmd9XG4gICAqIEBwYXJhbSB1cmwge3N0cmluZ31cbiAgICogQHJldHVybnMge1Byb21pc2U8YW55Pn1cbiAgICovXG4gIEBDb3Jkb3ZhKHtcbiAgICBzdWNjZXNzSW5kZXg6IDUsXG4gICAgZXJyb3JJbmRleDogNixcbiAgICBwbGF0Zm9ybXM6IFsnaU9TJywgJ0FuZHJvaWQnXSxcbiAgfSlcbiAgc2hhcmVWaWEoYXBwTmFtZTogc3RyaW5nLCBtZXNzYWdlOiBzdHJpbmcsIHN1YmplY3Q/OiBzdHJpbmcsIGltYWdlPzogc3RyaW5nLCB1cmw/OiBzdHJpbmcpOiBQcm9taXNlPGFueT4ge1xuICAgIHJldHVybjtcbiAgfVxuXG4gIC8qKlxuICAgKiBkZWZpbmVzIHRoZSBwb3B1cCBwb3NpdGlvbiBiZWZvcmUgY2FsbCB0aGUgc2hhcmUgbWV0aG9kLlxuICAgKiBAcGFyYW0gdGFyZ2V0Qm91bmRzIHtzdHJpbmd9IGxlZnQsIHRvcCwgd2lkdGgsIGhlaWdodFxuICAgKi9cbiAgQENvcmRvdmEoe1xuICAgIHN5bmM6IHRydWUsXG4gICAgcGxhdGZvcm1zOiBbJ2lPUyddLFxuICB9KVxuICBzZXRJUGFkUG9wdXBDb29yZGluYXRlcyh0YXJnZXRCb3VuZHM6IHN0cmluZyk6IHZvaWQge31cblxuICAvKipcbiAgICogU2F2ZSBhbiBhcnJheSBvZiBpbWFnZXMgdG8gdGhlIGNhbWVyYSByb2xsXG4gICAqIEBwYXJhbSAge3N0cmluZ3xzdHJpbmdbXX0gZmlsZU9yRmlsZUFycmF5IFNpbmdsZSBvciBtdWx0aXBsZSBmaWxlc1xuICAgKiBAcmV0dXJucyB7UHJvbWlzZTxhbnk+IH1cbiAgICovXG4gIEBDb3Jkb3ZhKHtcbiAgICBwbGF0Zm9ybXM6IFsnaU9TJ10sXG4gIH0pXG4gIHNhdmVUb1Bob3RvQWxidW0oZmlsZU9yRmlsZUFycmF5OiBzdHJpbmcgfCBzdHJpbmdbXSk6IFByb21pc2U8YW55PiB7XG4gICAgcmV0dXJuO1xuICB9XG5cbiAgLyoqXG4gICAqIFNoYXJlcyBkaXJlY3RseSB0byBhIFdoYXRzQXBwIENvbnRhY3Qgd2l0aCBwaG9uZSBudW1iZXIuXG4gICAqIEBwYXJhbSBwaG9uZSB7c3RyaW5nfSBQYXNzIHBob25lIG51bWJlclxuICAgKiBAcGFyYW0gbWVzc2FnZSB7c3RyaW5nfSBNZXNzYWdlIHRvIHNlbmRcbiAgICogQHBhcmFtIGZpbGVPckZpbGVBcnJheSBmaWxlT3JGaWxlQXJyYXkgU2luZ2xlIG9yIG11bHRpcGxlIGZpbGVzXG4gICAqIEBwYXJhbSB1cmwge3N0cmluZ30gTGluayB0byBzZW5kXG4gICAqIEByZXR1cm5zIHtQcm9taXNlPGFueT59XG4gICAqL1xuICBAQ29yZG92YSh7XG4gICAgc3VjY2Vzc0luZGV4OiA1LFxuICAgIGVycm9ySW5kZXg6IDYsXG4gICAgcGxhdGZvcm1zOiBbJ2lPUycsICdBbmRyb2lkJ10sXG4gIH0pXG4gIHNoYXJlVmlhV2hhdHNBcHBUb1Bob25lKFxuICAgIHBob25lOiBzdHJpbmcsXG4gICAgbWVzc2FnZTogc3RyaW5nLFxuICAgIGZpbGVPckZpbGVBcnJheTogc3RyaW5nIHwgc3RyaW5nW10sXG4gICAgdXJsPzogc3RyaW5nXG4gICk6IFByb21pc2U8YW55PiB7XG4gICAgcmV0dXJuO1xuICB9XG59XG4iXX0=

/***/ }),

/***/ 1288:
/*!*****************************************************!*\
  !*** ./src/app/Pages/share/share-routing.module.ts ***!
  \*****************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "SharePageRoutingModule": () => (/* binding */ SharePageRoutingModule)
/* harmony export */ });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! tslib */ 64762);
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/core */ 37716);
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/router */ 39895);
/* harmony import */ var _share_page__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./share.page */ 1474);




const routes = [
    {
        path: '',
        component: _share_page__WEBPACK_IMPORTED_MODULE_0__.SharePage
    }
];
let SharePageRoutingModule = class SharePageRoutingModule {
};
SharePageRoutingModule = (0,tslib__WEBPACK_IMPORTED_MODULE_1__.__decorate)([
    (0,_angular_core__WEBPACK_IMPORTED_MODULE_2__.NgModule)({
        imports: [_angular_router__WEBPACK_IMPORTED_MODULE_3__.RouterModule.forChild(routes)],
        exports: [_angular_router__WEBPACK_IMPORTED_MODULE_3__.RouterModule],
    })
], SharePageRoutingModule);



/***/ }),

/***/ 34222:
/*!*********************************************!*\
  !*** ./src/app/Pages/share/share.module.ts ***!
  \*********************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "SharePageModule": () => (/* binding */ SharePageModule)
/* harmony export */ });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! tslib */ 64762);
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/core */ 37716);
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @angular/common */ 38583);
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @angular/forms */ 3679);
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! @ionic/angular */ 80476);
/* harmony import */ var _share_routing_module__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./share-routing.module */ 1288);
/* harmony import */ var _share_page__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./share.page */ 1474);







let SharePageModule = class SharePageModule {
};
SharePageModule = (0,tslib__WEBPACK_IMPORTED_MODULE_2__.__decorate)([
    (0,_angular_core__WEBPACK_IMPORTED_MODULE_3__.NgModule)({
        imports: [
            _angular_common__WEBPACK_IMPORTED_MODULE_4__.CommonModule,
            _angular_forms__WEBPACK_IMPORTED_MODULE_5__.FormsModule,
            _ionic_angular__WEBPACK_IMPORTED_MODULE_6__.IonicModule,
            _share_routing_module__WEBPACK_IMPORTED_MODULE_0__.SharePageRoutingModule
        ],
        declarations: [_share_page__WEBPACK_IMPORTED_MODULE_1__.SharePage]
    })
], SharePageModule);



/***/ }),

/***/ 1474:
/*!*******************************************!*\
  !*** ./src/app/Pages/share/share.page.ts ***!
  \*******************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "SharePage": () => (/* binding */ SharePage)
/* harmony export */ });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! tslib */ 64762);
/* harmony import */ var _raw_loader_share_page_html__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! !raw-loader!./share.page.html */ 78805);
/* harmony import */ var _share_page_scss__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./share.page.scss */ 52066);
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @angular/core */ 37716);
/* harmony import */ var _base_page_base_page__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../base-page/base-page */ 96093);
/* harmony import */ var _ionic_native_social_sharing_ngx__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @ionic-native/social-sharing/ngx */ 24276);






let SharePage = class SharePage extends _base_page_base_page__WEBPACK_IMPORTED_MODULE_2__.BasePage {
    constructor(injector, socialSharing) {
        super(injector);
        this.socialSharing = socialSharing;
    }
    ngOnInit() {
    }
    //facebook share
    shareWithFacebook() {
        this.socialSharing.shareViaFacebook("message", "image", "www.PickMyRide.com").then((success) => {
            console.log("ok: " + success);
        }).catch((error) => {
            console.log(error);
        });
    }
    //twitter share
    shareWithTwitter() {
        this.socialSharing.shareViaTwitter("message", "image", "www.PickMyRide.com").then((success) => {
            console.log("ok: " + success);
        }).catch((error) => {
            console.log(error);
        });
    }
    //massage share
    shareWithMassage() {
        this.socialSharing.shareViaSMS("messge", "9038505480").then((success) => {
            console.log("ok: " + success);
        }).catch((error) => {
            console.log(error);
        });
    }
    shareWithEmail() {
        this.data = 'abc@email.com';
        this.socialSharing.shareViaEmail('Body', 'Subject', this.data).then((success) => {
            console.log("ok: " + success);
        }).catch((error) => {
            console.log(error);
        });
    }
};
SharePage.ctorParameters = () => [
    { type: _angular_core__WEBPACK_IMPORTED_MODULE_4__.Injector },
    { type: _ionic_native_social_sharing_ngx__WEBPACK_IMPORTED_MODULE_3__.SocialSharing }
];
SharePage = (0,tslib__WEBPACK_IMPORTED_MODULE_5__.__decorate)([
    (0,_angular_core__WEBPACK_IMPORTED_MODULE_4__.Component)({
        selector: 'app-share',
        template: _raw_loader_share_page_html__WEBPACK_IMPORTED_MODULE_0__.default,
        styles: [_share_page_scss__WEBPACK_IMPORTED_MODULE_1__.default]
    })
], SharePage);



/***/ }),

/***/ 52066:
/*!*********************************************!*\
  !*** ./src/app/Pages/share/share.page.scss ***!
  \*********************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "default": () => (__WEBPACK_DEFAULT_EXPORT__)
/* harmony export */ });
/* harmony default export */ const __WEBPACK_DEFAULT_EXPORT__ = (".sharediv {\n  width: 100%;\n  border-top: 1px solid #4267b2;\n  padding: 20px;\n  position: relative;\n  border-bottom: 1px solid #4267b2;\n  margin-top: 15px;\n}\n\n.sharediv h2 {\n  background: #fff;\n  margin: 0 auto;\n  color: #2e2e2e;\n  font-family: \"PT Sans\";\n  font-size: 15px;\n  padding: 5px;\n  width: 55%;\n  text-align: center;\n  margin-top: -34px;\n}\n\n.sharedivimg {\n  display: block;\n  width: auto;\n  margin: 0 auto;\n  padding-bottom: 0 !important;\n}\n\n.no-padding {\n  padding: 0;\n}\n\n.customimage-padding {\n  padding-top: 5px;\n}\n\n@media (min-width: 640px) {\n  .sharediv h2 {\n    width: 30%;\n  }\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNoYXJlLnBhZ2Uuc2NzcyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiQUFBQTtFQUNFLFdBQUE7RUFDQSw2QkFBQTtFQUNBLGFBQUE7RUFDQSxrQkFBQTtFQUNBLGdDQUFBO0VBQ0EsZ0JBQUE7QUFDRjs7QUFFQTtFQUNFLGdCQUFBO0VBQ0EsY0FBQTtFQUNBLGNBQUE7RUFDQSxzQkFBQTtFQUNBLGVBQUE7RUFDQSxZQUFBO0VBQ0EsVUFBQTtFQUNBLGtCQUFBO0VBQ0EsaUJBQUE7QUFDRjs7QUFDQTtFQUNFLGNBQUE7RUFDQSxXQUFBO0VBQ0EsY0FBQTtFQUNBLDRCQUFBO0FBRUY7O0FBQUE7RUFDRSxVQUFBO0FBR0Y7O0FBREE7RUFDRSxnQkFBQTtBQUlGOztBQURBO0VBQ0U7SUFDRSxVQUFBO0VBSUY7QUFDRiIsImZpbGUiOiJzaGFyZS5wYWdlLnNjc3MiLCJzb3VyY2VzQ29udGVudCI6WyIuc2hhcmVkaXYge1xyXG4gIHdpZHRoOiAxMDAlO1xyXG4gIGJvcmRlci10b3A6IDFweCBzb2xpZCAjNDI2N2IyO1xyXG4gIHBhZGRpbmc6IDIwcHg7XHJcbiAgcG9zaXRpb246IHJlbGF0aXZlO1xyXG4gIGJvcmRlci1ib3R0b206IDFweCBzb2xpZCAjNDI2N2IyO1xyXG4gIG1hcmdpbi10b3A6IDE1cHg7XHJcbn1cclxuXHJcbi5zaGFyZWRpdiBoMiB7XHJcbiAgYmFja2dyb3VuZDogI2ZmZjtcclxuICBtYXJnaW46IDAgYXV0bztcclxuICBjb2xvcjogIzJlMmUyZTtcclxuICBmb250LWZhbWlseTogXCJQVCBTYW5zXCI7XHJcbiAgZm9udC1zaXplOiAxNXB4O1xyXG4gIHBhZGRpbmc6IDVweDtcclxuICB3aWR0aDogNTUlO1xyXG4gIHRleHQtYWxpZ246IGNlbnRlcjtcclxuICBtYXJnaW4tdG9wOiAtMzRweDtcclxufVxyXG4uc2hhcmVkaXZpbWcge1xyXG4gIGRpc3BsYXk6IGJsb2NrO1xyXG4gIHdpZHRoOiBhdXRvO1xyXG4gIG1hcmdpbjogMCBhdXRvO1xyXG4gIHBhZGRpbmctYm90dG9tOiAwICFpbXBvcnRhbnQ7XHJcbn1cclxuLm5vLXBhZGRpbmcge1xyXG4gIHBhZGRpbmc6IDA7XHJcbn1cclxuLmN1c3RvbWltYWdlLXBhZGRpbmcge1xyXG4gIHBhZGRpbmctdG9wOiA1cHg7XHJcbn1cclxuXHJcbkBtZWRpYSAobWluLXdpZHRoOiA2NDBweCkge1xyXG4gIC5zaGFyZWRpdiBoMiB7XHJcbiAgICB3aWR0aDogMzAlO1xyXG4gIH1cclxufVxyXG4iXX0= */");

/***/ }),

/***/ 78805:
/*!***********************************************************************************!*\
  !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/Pages/share/share.page.html ***!
  \***********************************************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "default": () => (__WEBPACK_DEFAULT_EXPORT__)
/* harmony export */ });
/* harmony default export */ const __WEBPACK_DEFAULT_EXPORT__ = ("<!--\r\n  Generated template for the Share page.\r\n\r\n  See http://ionicframework.com/docs/v2/components/#navigation for more info on\r\n  Ionic pages and navigation.\r\n-->\r\n<ion-header>\r\n  <ion-toolbar color=\"headerblue\">\r\n    <ion-icon class=\"toggleButton\" name=\"menu\" (click)=\"ToggleMenuBar()\"></ion-icon>\r\n    <!-- <button ion-button (click)=\"ToggleMenuBar()\">\r\n      <ion-icon name=\"menu\"></ion-icon>\r\n    </button> -->\r\n    <ion-title class=\"toptitle\"><span class=\"heavy\">SHARE</span></ion-title>\r\n  </ion-toolbar>\r\n</ion-header>\r\n\r\n\r\n<ion-content>\r\n  <section class=\"support-section\">\r\n    <img src=\"assets/images/supportlogo.png\">\r\n    <p class=\"supportpara\">Get <span color=\"headerblue\">$10</span> discount for you &amp; your mate who will download\r\n      and uses the app for their 1st trip.</p>\r\n\r\n    <section class=\"sharediv\">\r\n      <h2>SHARE YOUR CODE</h2>\r\n      <ion-grid class=\"no-padding\">\r\n        <ion-row class=\"customimage-padding\">\r\n          <ion-col width-25><img class=\"sharedivimg\" src=\"assets/images/fb.png\" alt=\"facebook\"\r\n              (click)=\"shareWithFacebook()\"></ion-col>\r\n          <ion-col width-25><img class=\"sharedivimg\" src=\"assets/images/twitter.png\" alt=\"twitter\"\r\n              (click)=\"shareWithTwitter()\"></ion-col>\r\n          <ion-col width-25><img class=\"sharedivimg\" src=\"assets/images/mail.png\" alt=\"mail\"\r\n              (click)=\"shareWithMassage()\"></ion-col>\r\n          <ion-col width-25><img class=\"sharedivimg\" src=\"assets/images/mail1.png\" alt=\"mail1\"\r\n              (click)=\"shareWithEmail()\"></ion-col>\r\n        </ion-row>\r\n      </ion-grid>\r\n    </section>\r\n  </section>\r\n</ion-content>");

/***/ })

}]);
//# sourceMappingURL=src_app_Pages_share_share_module_ts.js.map